/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.7.9 : Database - ranapatra_com
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `admin_requests` */

DROP TABLE IF EXISTS `admin_requests`;

CREATE TABLE `admin_requests` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `request_type` int(11) NOT NULL,
  `message` text NOT NULL,
  `userID` bigint(20) NOT NULL,
  `proceed` tinyint(1) NOT NULL,
  `readed` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`),
  CONSTRAINT `admin_requests_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `admin_requests` */

insert  into `admin_requests`(`id`,`request_type`,`message`,`userID`,`proceed`,`readed`,`created`,`modified`) values (1,1,'Please remove #13 from news',2,0,0,'2016-12-03 13:45:33','2016-12-03 13:45:33');

/*Table structure for table `article_categories` */

DROP TABLE IF EXISTS `article_categories`;

CREATE TABLE `article_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent` bigint(20) NOT NULL,
  `label` varchar(1000) NOT NULL,
  `identifier` varchar(100) NOT NULL,
  `img` varchar(500) NOT NULL,
  `fontaws_icon` varchar(50) NOT NULL,
  `reserved` int(11) NOT NULL DEFAULT '0',
  `show_sibling` int(11) DEFAULT NULL,
  `is_heading` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `article_categories` */

insert  into `article_categories`(`id`,`parent`,`label`,`identifier`,`img`,`fontaws_icon`,`reserved`,`show_sibling`,`is_heading`,`sort_order`,`created`,`modified`) values (5,0,'Article','article','','',0,0,0,1,'2016-02-20 02:01:42','2016-12-26 15:20:36'),(6,5,'Public Notice','public-notice','','',1,0,0,1,'2016-02-20 02:02:09','2016-12-26 15:20:50'),(7,5,'General','general','','',0,0,0,1,'2016-02-20 02:02:28','2016-12-26 15:20:52');

/*Table structure for table `articles` */

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_name` varchar(255) NOT NULL,
  `article_heading` varchar(1000) NOT NULL,
  `article_slug` varchar(255) NOT NULL,
  `article_title` varchar(300) NOT NULL,
  `article_keywords` varchar(500) NOT NULL,
  `article_description` text NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `featured` tinyint(1) DEFAULT '0',
  `layout` varchar(30) NOT NULL,
  `main_category` int(11) NOT NULL,
  `category` bigint(20) DEFAULT NULL,
  `sub_category` bigint(20) DEFAULT NULL,
  `banner` varchar(500) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `userID` bigint(20) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `articles` */

insert  into `articles`(`id`,`article_name`,`article_heading`,`article_slug`,`article_title`,`article_keywords`,`article_description`,`content`,`status`,`featured`,`layout`,`main_category`,`category`,`sub_category`,`banner`,`sort_order`,`created`,`modified`,`userID`,`isAdmin`) values (2,'Test','Test','test','test','test stset sset se','asdasd ad ad ad a ','<p>asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a asdasd ad ad ad a</p>\r\n',1,1,'standard_article_layout',5,7,NULL,'http://ranapatra.tj/public/banner/top-header-backgrund.jpg',1,'2016-02-20 02:39:10','2016-12-26 15:32:41',8,0),(3,'Test','Test','test-1455917613','test','test','test','<div>\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n</div>\r\n\r\n<div>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<div>\r\n<h2>Where does it come from?</h2>\r\n\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\r\n</div>\r\n\r\n<div>\r\n<h2>Where can I get some?</h2>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>\r\n</div>\r\n',1,1,'standard_article_layout',5,NULL,NULL,'http://ranapatra.tj/public/banner/home-bg-price.jpg',2,'2016-02-20 03:03:33','2016-12-26 15:32:57',8,0),(4,'Consectetur adipisicing elit','Consectetur adipisicing elit','consectetur-adipisicing-elit','Test this article and news','test this article','articles articles','<div>\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n</div>\r\n\r\n<div>\r\n<h2>Why do we use it?</h2>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<div>\r\n<h2>Where does it come from?</h2>\r\n\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\r\n</div>\r\n\r\n<div>\r\n<h2>Where can I get some?</h2>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>\r\n</div>\r\n',1,1,'standard_article_layout',5,6,NULL,'http://ranapatra.tj/public/banner/home-bg-price.jpg',1,'2016-12-03 21:04:30','2016-12-26 15:33:11',2,1);

/*Table structure for table `careers` */

DROP TABLE IF EXISTS `careers`;

CREATE TABLE `careers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `position` varchar(1000) NOT NULL,
  `total_experiance` varchar(1000) NOT NULL,
  `role` text NOT NULL,
  `skills` text NOT NULL,
  `desired_skills` text NOT NULL,
  `mandatory_skills` text NOT NULL,
  `qualification` text NOT NULL,
  `job_description` text NOT NULL,
  `key_task` text NOT NULL,
  `key_responsibilities` text NOT NULL,
  `job_location` varchar(1000) NOT NULL,
  `status` int(11) NOT NULL,
  `publish_date` date NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `careers` */

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent` bigint(20) NOT NULL,
  `label` varchar(1000) NOT NULL,
  `identifier` varchar(100) NOT NULL,
  `img` varchar(500) NOT NULL,
  `fontaws_icon` varchar(50) NOT NULL,
  `reserved` int(11) NOT NULL DEFAULT '0',
  `show_sibling` int(11) DEFAULT NULL,
  `is_heading` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `categories` */

insert  into `categories`(`id`,`parent`,`label`,`identifier`,`img`,`fontaws_icon`,`reserved`,`show_sibling`,`is_heading`,`sort_order`,`created`,`modified`) values (5,0,'Main','main','','',0,0,0,1,'2016-10-15 15:09:27','2017-07-22 16:53:32');

/*Table structure for table `customer_invoices` */

DROP TABLE IF EXISTS `customer_invoices`;

CREATE TABLE `customer_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` bigint(11) NOT NULL,
  `item_type` int(11) NOT NULL,
  `item_name` varchar(100) NOT NULL,
  `item_number` varchar(200) NOT NULL,
  `payment_status` varchar(100) NOT NULL,
  `mc_gross` float NOT NULL,
  `mc_currency` varchar(100) NOT NULL,
  `txn_id` varchar(1000) NOT NULL,
  `receiver_email` varchar(100) NOT NULL,
  `payer_email` varchar(1000) NOT NULL,
  `payment_gateway` varchar(100) NOT NULL,
  `process_by` varchar(100) NOT NULL,
  `system_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`),
  CONSTRAINT `customer_invoices_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `customer_invoices` */

insert  into `customer_invoices`(`id`,`userID`,`item_type`,`item_name`,`item_number`,`payment_status`,`mc_gross`,`mc_currency`,`txn_id`,`receiver_email`,`payer_email`,`payment_gateway`,`process_by`,`system_id`,`created`,`modified`) values (1,6,1,'MAT-SILVER','MAT-SIL-210220161519','SUCCESS',100,'$','210220161519123','tejas@tejasrana.com','ad@gmail.com','Paypal','0',0,'2016-02-21 00:00:00','2016-02-22 00:00:00'),(2,6,2,'MAT-SILVER','MAT-SIL-210220161519','SUCCESS',100,'$','210220161519123','tejas@tejasrana.com','ad@gmail.com','Paypal','1',2,'2016-02-20 00:00:00','2016-02-20 00:00:00'),(3,6,3,'MAT-SILVER','MAT-SIL-210220161519','FAILED',100,'$','210220161519123','tejas@tejasrana.com','ad@gmail.com','Paypal','2',0,'2016-02-19 00:00:00','2016-02-19 00:00:00'),(4,6,4,'MAT-SILVER','MAT-SIL-210220161519','SUCCESS',100,'$','210220161519123','tejas@tejasrana.com','ad@gmail.com','Paypal','0',0,'2016-02-18 00:00:00','2016-02-18 00:00:00'),(5,6,1,'MAT-SILVER','MAT-SIL-210220161519','FAILED',100,'USD','210220161519123','payment@infitechnology.com','ad@gmail.com','cash','0',0,'2016-02-17 00:00:00','2016-02-27 19:06:47'),(6,6,1,'MAT-SILVER','MAT-SIL-210220161519','SUCCESS',100,'$','210220161519123','tejas@tejasrana.com','ad@gmail.com','Paypal','1',2,'2016-02-08 00:00:00','2016-02-08 00:00:00'),(7,6,2,'MAT-SILVER','MAT-SIL-210220161519','SUCCESS',100,'INR','210220161519123','payment@infitechnology.com','ad@gmail.com','paypal','1',2,'2016-02-04 00:00:00','2016-02-27 19:05:59'),(8,6,1,'MAT-121','123211','SUCCESS',2001,'INR','23342423424234234234','payment@infitechnology.com','test@gmail.com','cash','1',2,'2016-02-27 18:48:00','2016-02-27 19:04:50');

/*Table structure for table `customer_profiles` */

DROP TABLE IF EXISTS `customer_profiles`;

CREATE TABLE `customer_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` bigint(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `address` varchar(1000) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `placefinder` varchar(1000) DEFAULT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `place_id` varchar(1000) DEFAULT NULL,
  `profilePic` varchar(100) NOT NULL,
  `newsLatter` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`),
  CONSTRAINT `customer_profiles_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `customer_profiles` */

insert  into `customer_profiles`(`id`,`userID`,`name`,`gender`,`address`,`contact`,`placefinder`,`city`,`state`,`country`,`place_id`,`profilePic`,`newsLatter`,`created`,`modified`) values (8,6,'Tejas Kumar Rana',1,'asdas','9636732568',NULL,'60166','2371','132',NULL,'userProfileImages/test-56c8db9dae57e.jpg',1,'2016-02-21 02:15:14','2016-02-21 03:05:27'),(9,8,'TEJAS RANA',1,'H-505 Panchshlok residency','9636732568','Ahmedabad, Gujarat, India','Ahmedabad','Gujarat','India','ChIJSdRbuoqEXjkRFmVPYRHdzk8','https://lh3.googleusercontent.com/-_bqxTotWqug/AAAAAAAAAAI/AAAAAAAAAvM/3ESUM5rOhug/photo.jpg',1,'2016-12-27 13:08:04','2017-09-08 18:33:44'),(11,10,'Tejas Rana',1,'','',NULL,'','','',NULL,'https://lh5.googleusercontent.com/-bovlasCn83o/AAAAAAAAAAI/AAAAAAAAABU/5vHIOT1VbQU/photo.jpg',1,'2016-12-27 14:27:36','2016-12-28 10:48:30'),(13,14,'Tejas Rana',0,'','',NULL,'','','',NULL,'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg',1,'2016-12-30 18:05:44','2016-12-30 18:05:44');

/*Table structure for table `customers` */

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `account_type` varchar(50) DEFAULT NULL,
  `access_token` text,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `customers` */

insert  into `customers`(`id`,`username`,`password`,`email`,`status`,`account_type`,`access_token`,`created`,`modified`) values (6,'test','b4105e58a8e4365eded587528df89832f774fa10','test@gmail.com',1,NULL,NULL,'2016-02-21 02:15:13','2016-02-21 03:05:27'),(8,'tejasrana95@gmail.com','fa5d5c120ebca7b87f9524616ede592b0a558e14','tejasrana95@gmail.com',1,'google','4/-v9hyAlMGraH8Q5lsPwBU2iAdPSj0QPvmr70qhha9gc','2016-12-27 13:08:04','2017-07-22 11:12:01'),(10,'tejas@tejasrana.com','b4cd3cb2ea9334fd1d73e6426c926e470a478d8f','tejas@tejasrana.com',1,'google','4/yHn_YfxV7kI1TqVhRkt0AXTPphvSUDxR1J23ELLnTyk','2016-12-27 14:27:36','2016-12-28 10:48:30'),(14,'thetejasrana@gmail.com','a16c3013981f71d1da40aa4c75c5313866859a52','thetejasrana@gmail.com',1,'google','4/eS_iiu0pMUXhx5sBuFkGZVEddbd4DtylnwqBfmqzAfE','2016-12-30 18:05:44','2016-12-30 18:05:44'),(15,'deepdhudiya@gmail.com','aa64f8bf00e141c3a427003ac863db62e00cd977','deepdhudiya@gmail.com',1,'google','4/k54ur_7JzuaCNpEp5yhocBH1jLn904p1dAH-2bNojYc','2017-01-07 20:01:17','2017-01-07 22:07:49');

/*Table structure for table `events` */

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_title` varchar(250) NOT NULL,
  `event_slug` varchar(250) NOT NULL,
  `event_description` text NOT NULL,
  `event_start_date` date NOT NULL,
  `event_time` varchar(30) DEFAULT NULL,
  `event_end_date` date DEFAULT NULL,
  `event_venue` varchar(500) NOT NULL,
  `external_link` varchar(250) NOT NULL,
  `email` varchar(1000) NOT NULL,
  `image` varchar(1000) DEFAULT NULL,
  `gallery_category` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `userID` bigint(20) DEFAULT NULL,
  `isAdmin` bigint(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `events` */

/*Table structure for table `galleries` */

DROP TABLE IF EXISTS `galleries`;

CREATE TABLE `galleries` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category` int(11) DEFAULT NULL,
  `image` varchar(10000) DEFAULT NULL,
  `heading` varchar(1000) DEFAULT NULL,
  `description` text,
  `status` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `galleries` */

/*Table structure for table `gcategories` */

DROP TABLE IF EXISTS `gcategories`;

CREATE TABLE `gcategories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `label` varchar(1000) NOT NULL,
  `identifier` varchar(100) NOT NULL,
  `img` varchar(500) NOT NULL,
  `fontaws_icon` varchar(50) NOT NULL,
  `reserved` int(11) NOT NULL DEFAULT '0',
  `show_sibling` int(11) DEFAULT NULL,
  `is_heading` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `gcategories` */

/*Table structure for table `heights` */

DROP TABLE IF EXISTS `heights`;

CREATE TABLE `heights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `height` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `heights` */

/*Table structure for table `histories` */

DROP TABLE IF EXISTS `histories`;

CREATE TABLE `histories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `decade` int(11) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `image` varchar(1000) DEFAULT NULL,
  `image_size` varchar(100) DEFAULT NULL,
  `description` text,
  `status` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `histories` */

/*Table structure for table `matrimonial_attachments` */

DROP TABLE IF EXISTS `matrimonial_attachments`;

CREATE TABLE `matrimonial_attachments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(260) NOT NULL,
  `file_path` varchar(300) NOT NULL,
  `file_type` varchar(10) NOT NULL DEFAULT 'image',
  `matrimonial_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`matrimonial_id`),
  CONSTRAINT `matrimonial_attachments_ibfk_1` FOREIGN KEY (`matrimonial_id`) REFERENCES `matrimonials` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=latin1;

/*Data for the table `matrimonial_attachments` */

insert  into `matrimonial_attachments`(`id`,`file_name`,`file_path`,`file_type`,`matrimonial_id`,`created`,`modified`) values (83,'Avril-Lavigne-96.JPG','Avril-Lavigne-96.JPG-59c3c9e5a8851.jpg','image',1,'2017-09-21 14:17:09','2017-09-21 14:17:09'),(84,'mandy-moore83.jpg','mandy-moore83.jpg-59c3c9e812f99.jpg','image',1,'2017-09-21 14:17:12','2017-09-21 14:17:12'),(86,'Avril Lavigne.jpg','Avril Lavigne.jpg-59c3d03f400ae.jpg','image',1,'2017-09-21 14:44:15','2017-09-21 14:44:15'),(87,'vlcsnap-2013-05-24-19h47m26s252.jpg','vlcsnap-2013-05-24-19h47m26s252.jpg-59c3d03f789c3.jpg','image',1,'2017-09-21 14:44:15','2017-09-21 14:44:15'),(103,'IMG_8932.JPG','IMG_8932.JPG-59c4d6406c7c5.jpg','kundali',1,'2017-09-22 09:22:08','2017-09-22 09:22:08'),(117,'IMG_9031.JPG','IMG_9031.JPG-59c4d6666bcef.jpg','image',1,'2017-09-22 09:22:46','2017-09-22 09:22:46');

/*Table structure for table `matrimonials` */

DROP TABLE IF EXISTS `matrimonials`;

CREATE TABLE `matrimonials` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) DEFAULT NULL,
  `bride_groom` varchar(10) DEFAULT NULL,
  `marital_status` varchar(15) DEFAULT NULL,
  `first_name` varchar(25) DEFAULT NULL,
  `father_name` varchar(50) DEFAULT NULL,
  `mother_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `contact` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `nationality` varchar(50) DEFAULT NULL,
  `address` text,
  `pin_code` varchar(10) DEFAULT NULL,
  `placefinder` varchar(500) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `place_id` varchar(1000) DEFAULT NULL,
  `bio` text NOT NULL,
  `dob` date DEFAULT NULL,
  `tob` time DEFAULT NULL,
  `pob` varchar(200) DEFAULT NULL COMMENT 'place of birth',
  `mother_tongue` varchar(50) DEFAULT NULL,
  `drink` varchar(50) DEFAULT NULL,
  `diet` varchar(50) DEFAULT NULL,
  `smoke` varchar(50) DEFAULT NULL,
  `complexion` varchar(25) DEFAULT NULL,
  `height` varchar(10) DEFAULT NULL,
  `weight` varchar(10) DEFAULT NULL,
  `gautra` varchar(25) DEFAULT NULL,
  `nakshtra` varchar(25) DEFAULT NULL,
  `rashi` varchar(25) DEFAULT NULL,
  `blood_group` varchar(5) DEFAULT NULL,
  `manglik` varchar(10) DEFAULT NULL,
  `education` varchar(50) DEFAULT NULL,
  `qualification` varchar(50) DEFAULT NULL,
  `employment` varchar(100) DEFAULT NULL,
  `salary` varchar(100) DEFAULT NULL,
  `native_place` varchar(250) DEFAULT NULL,
  `father_occupation` varchar(100) DEFAULT NULL,
  `mother_occupation` varchar(100) DEFAULT NULL,
  `brothers` int(5) DEFAULT NULL,
  `sisters` int(5) DEFAULT NULL,
  `ready_to_go_abroad` varchar(10) DEFAULT NULL,
  `special_remarks` text,
  `status` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `process_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`),
  CONSTRAINT `matrimonials_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `matrimonials` */

insert  into `matrimonials`(`id`,`userID`,`bride_groom`,`marital_status`,`first_name`,`father_name`,`mother_name`,`last_name`,`contact`,`email`,`nationality`,`address`,`pin_code`,`placefinder`,`city`,`state`,`country`,`place_id`,`bio`,`dob`,`tob`,`pob`,`mother_tongue`,`drink`,`diet`,`smoke`,`complexion`,`height`,`weight`,`gautra`,`nakshtra`,`rashi`,`blood_group`,`manglik`,`education`,`qualification`,`employment`,`salary`,`native_place`,`father_occupation`,`mother_occupation`,`brothers`,`sisters`,`ready_to_go_abroad`,`special_remarks`,`status`,`created`,`modified`,`process_by`) values (1,8,'Bride','Single','tejas','vipin','ranjana','rana','9636732568','test@gmail.com','Indian','H- 505 Panchshlok Residency','382424','Ahmedabad, Gujarat, India','Ahmedabad','Gujarat','India','ChIJSdRbuoqEXjkRFmVPYRHdzk8','sa dasd as','1990-10-23','02:15:00','cambay','Gujarati','Yes','Pure Vegetarian','Yes','Fair','5.9\"','60','Sisodiya','asda','Scorpius','A+','0','POST GRADUATE','MCA','0','5,51,000 - 6,00,000','Khambhat','Govt. Job','House wife',2,0,'1','sdadasda das asda s',0,'2016-05-05 12:39:25','2017-09-20 05:16:28',1),(2,6,'Bride','Single','Tejas','Vipin','Ranjana','Rana','9636732568','test@gmail.com','indian','3/62 R.H.B','313001',NULL,'78980','1992','104',NULL,'<p>This is my bio. I like to show you that I am simple boy. Please do not think about bad about me.</p>\r\n','1990-10-23','02:15:00','Cambay',NULL,NULL,NULL,NULL,NULL,'05\'09\"','055 k.g.','Sisodiya','asda','Scorpius','A+','0','POST GRADUATE','MCA','0','5,51,000 - 6,00,000','Khambhat','Govt. Job','House wife',5,2,'1','',1,'2016-05-05 14:34:38','2017-07-22 23:11:19',1),(3,8,'Bride','Single','Test','vipin','ranjana','rana','9636732568','test@gmail.com','Indian','H505','382424','Udaipur, Rajasthan, India','Udaipur','Rajasthan','India','ChIJEUShUGXlZzkRsGi4VYTC2Ns','Test this','1990-10-23','02:15:00','cambay','Gujarati','Don\'t want to disclose','Don\'t want to disclose','Don\'t want to disclose','Fair asda sda sd asd asd ','5.9\"','60','Sisodiya','asda','Scorpius','A+','0','POST GRADUATE','MCA','0','5,51,000 - 6,00,000','Khambhat','Govt. Job','House wife',1,0,'1','sdadasda das asda s',0,'2016-05-05 12:39:25','2017-09-25 05:39:50',1),(4,8,'Groom','Single','Tejas','Vipin','Ranjana','Rana','9636732568','test@gmail.com','indian','3/62 R.H.B','313001',NULL,'78980','1992','104',NULL,'<p>This is my bio. I like to show you that I am simple boy. Please do not think about bad about me.</p>\r\n','1990-10-23','02:15:00','Cambay','Gujarati','No','Pure Vegetarian','No','Fair','05\'09\"','055 k.g.','Sisodiya','asda','Scorpius','A+','0','POST GRADUATE','MCA','0','5,51,000 - 6,00,000','Khambhat','Govt. Job','House wife',5,2,'1','sdadd',1,'2016-05-05 14:34:38','2017-07-23 20:10:17',1),(5,8,'Bride','Single','tejas','vipin','ranjana','rana','9636732568','test@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','1990-10-23','02:15:00','cambay',NULL,NULL,NULL,NULL,NULL,'5.9\"','60','Sisodiya','asda','Scorpius','A+','0','POST GRADUATE','MCA','0','5,51,000 - 6,00,000','Khambhat','Govt. Job','House wife',NULL,0,'1','sdadasda das asda s',NULL,'2016-05-05 12:39:25','2016-05-05 12:39:25',1),(6,8,'Bride','Single','Tejas','Vipin','Ranjana','Rana','9636732568','test@gmail.com','indian','3/62 R.H.B','313001',NULL,'78980','1992','104',NULL,'<p>This is my bio. I like to show you that I am simple boy. Please do not think about bad about me.</p>\r\n','1990-10-23','02:15:00','Cambay',NULL,NULL,NULL,NULL,NULL,'05\'09\"','055 k.g.','Sisodiya','asda','Scorpius','A+','0','POST GRADUATE','MCA','0','5,51,000 - 6,00,000','Khambhat','Govt. Job','House wife',5,2,'1','sdadd',1,'2016-05-05 14:34:38','2017-07-22 23:11:19',1),(7,8,'Bride','Single','tejas','vipin','ranjana','rana','9636732568','test@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','1990-10-23','02:15:00','cambay',NULL,NULL,NULL,NULL,NULL,'5.9\"','60','Sisodiya','asda','Scorpius','A+','0','POST GRADUATE','MCA','0','5,51,000 - 6,00,000','Khambhat','Govt. Job','House wife',NULL,0,'1','sdadasda das asda s',NULL,'2016-05-05 12:39:25','2016-05-05 12:39:25',1),(8,8,'Bride','Single','Tejas','Vipin','Ranjana','Rana','9636732568','test@gmail.com','indian','3/62 R.H.B','313001','Udaipur, Rajasthan, India','Udaipur','Rajasthan','India','ChIJEUShUGXlZzkRsGi4VYTC2Ns','<p>This is my bio. I like to show you that I am simple boy. Please do not think about bad about me.</p>\r\n','1990-10-23','02:15:00','Cambay, Gujarat, India','','','','','','05\'09\"','055 k.g.','Sisodiya','asda','Scorpius','A+','0','POST GRADUATE','MCA','0','5,51,000 - 6,00,000','Khambhat','Govt. Job','House wife',5,2,'1','sdadd',1,'2016-05-05 14:34:38','2017-09-09 01:06:44',1),(12,8,'Bride','Single','Tejas','Vipin','Ranjana','Rana','9636732568','test@gmail.com','indian','3/62 R.H.B','313001','Udaipur, Rajasthan, India','Udaipur','Rajasthan','India','ChIJEUShUGXlZzkRsGi4VYTC2Ns','<p>This is my bio. I like to show you that I am simple boy. Please do not think about bad about me.</p>\r\n','1997-01-01','02:15:00','Cambay, Gujarat, India','Hindi','Yes','Don\'t want to disclose','Yes','Fair','05\'09\"','055 k.g.','Sisodiya','asda','Scorpius','A+','0','POST GRADUATE','MCA','0','5,51,000 - 6,00,000','Khambhat','Govt. Job','House wife',5,2,'1','sdadd',0,'2017-09-09 09:06:01','2017-09-09 09:06:01',NULL),(13,8,'Bride','Single','Tejas','Vipin','Ranjana','Rana','9636732568','test@gmail.com','indian','3/62 R.H.B','313001','Udaipur, Rajasthan, India','Udaipur','Rajasthan','India','ChIJEUShUGXlZzkRsGi4VYTC2Ns','<p>This is my bio. I like to show you that I am simple boy. Please do not think about bad about me.</p>\r\n','1990-10-23','23:19:00','Cambay, Gujarat, India','Hindi','Yes','Don\'t want to disclose','Yes','Fair','05\'09\"','055 k.g.','Sisodiya','asda','Scorpius','A+','0','POST GRADUATE','MCA','0','7,00,000 - 7,50,000','Khambhat','Govt. Job','House wife',5,2,'1','sdadd',0,'2017-09-09 09:06:44','2017-09-09 09:18:35',NULL);

/*Table structure for table `media` */

DROP TABLE IF EXISTS `media`;

CREATE TABLE `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) NOT NULL,
  `used_for` varchar(200) NOT NULL,
  `number` int(11) NOT NULL,
  `url` text,
  `content` varchar(1000) NOT NULL,
  `type` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `media` */

insert  into `media`(`id`,`media_id`,`used_for`,`number`,`url`,`content`,`type`,`created`) values (1,1,'Page',1,'','http://ranapatra.tj/public/banner/top-header-backgrund.jpg','jpg','2016-02-19 23:53:53'),(2,2,'Page',1,NULL,'http://ranapatra.tj/public/banner/home-bg-header.jpg','jpg','2016-10-15 15:30:33');

/*Table structure for table `menus` */

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` varchar(200) NOT NULL,
  `content_type` varchar(100) NOT NULL,
  `content_name` varchar(100) NOT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `url` text,
  `sort_order` int(11) NOT NULL DEFAULT '1',
  `menu_for` varchar(20) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `hierarchy` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `login_only` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

/*Data for the table `menus` */

insert  into `menus`(`id`,`parent_id`,`content_type`,`content_name`,`icon`,`url`,`sort_order`,`menu_for`,`created`,`modified`,`hierarchy`,`is_active`,`login_only`) values (54,'0','custom','Home','','/',1,'header-menu','2016-10-15 14:07:21','2016-10-15 14:07:21',1,1,0),(55,'0','custom','About','','#',1,'header-menu','2016-10-15 14:07:21','2016-10-15 14:07:21',1,1,0),(56,'About','page','1','','about-us',1,'header-menu','2016-10-15 14:07:21','2016-10-15 14:07:21',2,1,0),(57,'1','custom','Team','','#',1,'header-menu','2016-10-15 14:07:21','2016-10-15 14:07:21',3,1,0),(58,'0','custom','Services','','#',1,'header-menu','2016-10-15 14:07:21','2016-10-15 14:07:21',1,1,0),(59,'Services','custom','Matrimonial Services','','#',1,'header-menu','2016-10-15 14:07:21','2016-10-15 14:07:21',2,1,0),(60,'0','custom','Blog','','#',1,'header-menu','2016-10-15 14:07:21','2016-10-15 14:07:21',1,1,0),(61,'0','custom','Contact','','#',1,'header-menu','2016-10-15 14:07:21','2016-10-15 14:07:21',1,1,0),(62,'Contact','page','3','','contact-us',1,'header-menu','2016-10-15 14:07:21','2016-10-15 14:07:21',2,1,0),(63,'Contact','custom','Pay','','#',1,'header-menu','2016-10-15 14:07:21','2016-10-15 14:07:21',2,1,0),(64,'Contact','custom','Bug Hunter','','#',1,'header-menu','2016-10-15 14:07:21','2016-10-15 14:07:21',2,1,0),(65,'Contact','custom','FAQ','','#',1,'header-menu','2016-10-15 14:07:21','2016-10-15 14:07:21',2,1,0);

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  `news_title` varchar(250) NOT NULL,
  `news_slug` varchar(250) NOT NULL,
  `external_link` varchar(250) NOT NULL,
  `news_content` text NOT NULL,
  `news_image` varchar(200) NOT NULL,
  `publish_date` date NOT NULL,
  `news_source` varchar(200) NOT NULL,
  `image` varchar(1000) DEFAULT NULL,
  `attachment` varchar(1000) DEFAULT NULL,
  `status` char(20) NOT NULL,
  `on_home` tinyint(1) NOT NULL,
  `userID` bigint(20) DEFAULT NULL,
  `isAdmin` bigint(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `news` */

/*Table structure for table `options` */

DROP TABLE IF EXISTS `options`;

CREATE TABLE `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` text NOT NULL,
  `field_type` varchar(10) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

/*Data for the table `options` */

insert  into `options`(`id`,`meta_key`,`meta_value`,`field_type`,`created`,`modified`) values (2,'linkedin_link','http://www.linkedin.com','','2014-06-24 06:06:53','2016-12-31 14:00:42'),(3,'twitter_link','https://twitter.com/','','2014-06-24 06:06:53','2016-12-31 14:00:42'),(4,'google_plus_link','https://plus.google.com/','','2014-06-24 06:06:53','2016-12-31 14:00:42'),(5,'facebook_link','https://www.facebook.com/','','2014-06-24 06:06:53','2016-12-31 14:00:42'),(6,'youtube_link','https://www.youtube.com/','','2014-06-24 06:06:53','2016-12-31 14:00:43'),(9,'copyright_text','Copyright Â© 2017 Rana Patra. All rights reserved.','','2014-06-24 06:06:53','2016-12-31 14:00:43'),(10,'contact_info','<p>Contact us if you want know more about us. or if you are facing any issue.</p>\r\n','editor','2014-06-24 06:06:53','2016-12-31 14:00:43'),(11,'blog_url','#','','2014-07-10 09:01:46','2016-12-31 14:00:43'),(17,'publicDir','C:/wamp/www/ranapatra.com/app/webroot/public','','2014-06-24 06:06:53','2016-12-31 14:00:43'),(18,'site_url','http://ranapatra.tj/','','2014-06-24 06:06:53','2016-12-31 14:00:43'),(19,'admin_url','http://admin-ranapatra.ranapatra.tj/','','2014-06-24 06:06:53','2016-12-31 14:00:43'),(21,'admin_mail','info@jrnrvu.tk','','2014-06-24 06:06:53','2016-12-31 14:00:43'),(22,'site_name','Rana Patra','','2014-06-24 06:06:53','2016-12-31 14:00:43'),(23,'contact_mail_content','<p>Thank you for your interest in Ranapatra.</p>','editor','2014-06-24 06:06:53','2016-12-31 14:00:43'),(24,'mail_disclaimer','<p>DISCLAIMER: Nucleus Software and the Nucleus Software logo are registered trademarks of Nucleus Software Exports Ltd. All other brand or product names are trademarks or registered marks of their respective owners. Any part of this information from the document may not be copied, modified, reproduced, republished, uploaded, transmitted, posted or distributed in any form without prior written permission from NSEL. Unauthorized use of the content / information appearing here may violate copyright, trademark and other applicable laws, and could result in criminal or civil penalties.</p>','editor','2014-06-24 06:06:53','2016-12-31 14:00:43'),(25,'event_mail','<p>Thank you for your interest in Nucleus Software\'s event.&nbsp;<span>You will be contacted shortly by&nbsp;</span><span>o</span><span>ne of our representatives.</span></p>','editor','2014-06-24 06:06:53','2016-12-31 14:00:43'),(26,'enquiry_mail','<p>Thank you for requesting a meeting.&nbsp;<span>You will be contacted shortly by&nbsp;</span><span>o</span><span>ne of our representatives.</span></p>','editor','2014-06-24 06:06:53','2016-12-31 14:00:43'),(27,'request_mail_id','sales@ranapatra.com','','2014-06-24 06:06:53','2016-12-31 14:00:43'),(28,'contact_us_id','contact@ranapatra.com','','2014-06-24 06:06:53','2016-12-31 14:00:43'),(31,'payment_mail','payment@infitechnology.com','','2014-06-24 06:06:53','2016-12-31 14:00:43'),(32,'payment_address','3/62 R.H.B. Colony Goverdhan, Udaipur Rajasthan','','2014-06-24 06:06:53','2016-12-31 14:00:43'),(33,'payment_contact','+91 9636732568, +91 9530107998','','2014-06-24 06:06:53','2016-12-31 14:00:43'),(34,'payment_company','Infi Technology','','2014-06-24 06:06:53','2016-12-31 14:00:43'),(35,'payment_company_url','www.infitechnology.com','','2014-06-24 06:06:53','2016-12-31 14:00:43'),(36,'address','3/62 R.H.B. Colony Goverdhan, Udaipur Rajasthan, India','','2016-10-04 00:00:00','2016-12-31 14:00:43'),(37,'telephone','+91 9636732568, +91 9530107998','','2016-10-17 00:00:00','2016-12-31 14:00:43'),(38,'twitter_consumer_key','n5LDFleDwGk6rKQiXuIKlQ','','2016-10-04 00:00:00','2016-12-31 14:00:43'),(39,'twitter_consumer_secret','fucGKu9JnK7h5WNe1BIeAhhtzna6GE68uW1af76OIHg','','2016-10-04 00:00:00','2016-12-31 14:00:43'),(40,'twitter_access_token','261236578-K1tIcm0qaViVl2YmThlZgD471EsFi8Zmik2t5iR3','','2016-10-04 00:00:00','2016-12-31 14:00:43'),(41,'twitter_access_token_secret','asEvFtc0PE86TCsAs8mtkeSe1xbDSvHQSv7KxwZQj8FS6','','2016-10-04 00:00:00','2016-12-31 14:00:43'),(42,'twitter_user','thetejasrana','','2016-10-04 00:00:00','2016-12-31 14:00:43'),(43,'tweet_limit','10','','2016-10-04 00:00:00','2016-12-31 14:00:43'),(44,'matrimonialImages','C:\\wamp\\www\\ranapatra.com\\app\\webroot\\public\\matrimonialfiles\\images',NULL,'2017-09-21 12:40:03',NULL),(45,'matrimonialKundali','C:\\wamp\\www\\ranapatra.com\\app\\webroot\\public\\matrimonialfiles\\kundali',NULL,'2017-09-21 12:40:03',NULL),(46,'matrimonialImagesURL','/matrimonialfiles/images',NULL,'2017-09-21 19:30:03','2017-09-21 19:29:59'),(47,'matrimonialKundaliURL','/matrimonialfiles/kundali',NULL,'2017-09-21 19:30:03','2017-09-21 19:29:59');

/*Table structure for table `pages` */

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(255) NOT NULL,
  `page_heading` varchar(1000) NOT NULL,
  `page_slug` varchar(255) NOT NULL,
  `page_title` varchar(300) NOT NULL,
  `page_keywords` varchar(500) NOT NULL,
  `page_description` text NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `layout` varchar(30) NOT NULL,
  `widgets` text,
  `main_category` int(11) NOT NULL,
  `category` bigint(20) DEFAULT NULL,
  `sub_category` bigint(20) DEFAULT NULL,
  `banner_text` text,
  `sort_order` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `pages` */

insert  into `pages`(`id`,`page_name`,`page_heading`,`page_slug`,`page_title`,`page_keywords`,`page_description`,`content`,`status`,`layout`,`widgets`,`main_category`,`category`,`sub_category`,`banner_text`,`sort_order`,`created`,`modified`) values (1,'About us','About us','about-us','About us | Rana Patra | Rana Community','about us','Know about rana patra','<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6\">\r\n<div class=\"team-description wow fadeInUp animated\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s; animation-name: fadeInUp;\">\r\n<h3>Who we are</h3>\r\n\r\n<p>Phasellus justo ligula, dictum sit amet tortor eu, iaculis tristique turpis. Mauris non orci sed est suscipit tempor ut quis felis. Praesent pellentesque at metus eget luctus.</p>\r\n\r\n<p>Mauris congue sem a turpis tempor, eu bibendum magna dapibus. In malesuada. Ligula id condimentum hendrerit, metus tortor tristique quam, ut facilisis tortor purus non nisl. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas venenatis vulputate porta.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-6\">\r\n<div class=\"team-description wow fadeInUp animated\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s; animation-name: fadeInUp;\">\r\n<h3>What We Do</h3>\r\n\r\n<p>Cras interdum feugiat elit at suscipit. Fusce ac lorem lacinia, gravida velit et, scelerisque arcu. Praesent lorem nisi, porttitor ut odio vitae, eleifend bibendum quam. Aenean vitae mauris auctor, ultrices mi a, condimentum ipsum.</p>\r\n\r\n<p>Nulla vel lacus vitae lorem bibendum vehicula. Mauris in semper arcu, ac auctor risus. Etiam vulputate dui elit, eget auctor nisi dictum in. Curabitur metus urna, ullamcorper vitae ornare sit amet, viverra sed ligula.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<section class=\"team\" id=\"team\">&nbsp;</section>\r\n\r\n<div>\r\n<section class=\"wrap-timeline\">\r\n<div class=\"container\">\r\n<div class=\"row top-row\">\r\n<div class=\"col-md-12\">\r\n<div class=\"time-title\" id=\"timel\">Our history\r\n<div class=\"round-ico\">&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"row left-row\">\r\n<div class=\"round-ico little\">&nbsp;</div>\r\n\r\n<div class=\"col-md-6 col-sm-6 time-item wow fadeInUp animated\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s; animation-name: fadeInUp;\">\r\n<div class=\"date\">May 2015</div>\r\n\r\n<div class=\"title\">Vestibulum placerat mauris</div>\r\n\r\n<div class=\"text\">Our skills are specific and fine-tuned. We are specialists, not generalists. We create visual branding systems that enable</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"row right-row\">\r\n<div class=\"round-ico big\">&nbsp;</div>\r\n\r\n<div class=\"col-md-6 col-sm-6\">&nbsp;</div>\r\n\r\n<div class=\"col-md-6 col-sm-6 time-item wow fadeInUp animated\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s; animation-name: fadeInUp;\">\r\n<div class=\"date\">Jule 2015</div>\r\n\r\n<div class=\"title\">Aliquam scelerisque</div>\r\n\r\n<div class=\"time-image\"><img alt=\"\" src=\"http://admin-ranapatra.ranapatra.tj/pages/edit/assets/images/about/img1.jpg\" /></div>\r\n\r\n<div class=\"text\">Our skills are specific and fine-tuned. We are specialists, not generalists. We create visual branding systems that enable</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"row left-row\">\r\n<div class=\"round-ico little\">&nbsp;</div>\r\n\r\n<div class=\"col-md-6 col-sm-6 time-item wow fadeInUp animated\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s; animation-name: fadeInUp;\">\r\n<div class=\"date\">September 2015</div>\r\n\r\n<div class=\"title\">Donec at justo ac dui ultricies luctus</div>\r\n\r\n<div class=\"text\">Our skills are specific and fine-tuned. We are specialists, not generalists. We create visual branding systems that enable</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"plus\"><a class=\"plus-ico\" href=\"#\">+</a></div>\r\n</div>\r\n</section>\r\n</div>\r\n',1,'full_width_layout','s:0:\"\";',5,NULL,NULL,'',1,'2016-02-19 23:53:53','2017-07-22 16:53:15'),(2,'Welcome to Ranapatra','Welcome to Rana patra','home','Home | Rana Patra | World\'s first Online Rana community portal','rana patra, rana community, rana jagat, online rana, tejas rana','welcome to rana patra world\'s first rana community','<section class=\"about-product\" id=\"about\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-8 col-md-offset-2\">\r\n<header class=\"section-header\">\r\n<p class=\"subhead\">About Product</p>\r\n\r\n<h2>Interface Benefits</h2>\r\n</header>\r\n</div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-3 col-sm-offset-1 wow fadeInUp animated animated\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s;\">\r\n<article>\r\n<h5>Mobile version</h5>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>\r\n</article>\r\n</div>\r\n\r\n<div class=\"col-md-3 col-sm-offset-1 wow fadeInUp animated animated\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s;\">\r\n<article>\r\n<div class=\"tline\">&nbsp;</div>\r\n\r\n<h5>Affordable price</h5>\r\n\r\n<p>Odio, enim, ducimus provident quae non quo possimus harum autem esse maiores iste necessitatibus</p>\r\n</article>\r\n</div>\r\n\r\n<div class=\"col-md-3 col-sm-offset-1 wow fadeInUp animated animated\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s;\">\r\n<article>\r\n<h5>Targeting Setting</h5>\r\n\r\n<p>Aperiam debitis exercitationem ullam pariatur distinctio id saepe voluptate fuga alias quia dolore minus</p>\r\n</article>\r\n</div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-12\"><img alt=\"pic\" class=\"img-responsive\" src=\"http://ranapatra.tj/public/pictures/home-browsers.png\" /></div>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<section class=\"features dark parallax-section-2\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-3 col-sm-6 wow fadeInUp animated animated\" data-wow-delay=\"0.3s\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s; animation-delay: 0.3s;\">\r\n<article>\r\n<h5>Duis gravida</h5>\r\n\r\n<p>Donec congue ut est at lacinia. In volutpat consequat urna id pulvinar</p>\r\n</article>\r\n</div>\r\n\r\n<div class=\"col-md-3 col-sm-6 wow fadeInUp animated animated\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s;\">\r\n<article>\r\n<h5>Sed eget quam</h5>\r\n\r\n<p>Maecenas at commodo dui. Mauris pharetra mi erat, a gravida magna</p>\r\n</article>\r\n</div>\r\n\r\n<div class=\"col-md-3 col-sm-6 wow fadeInUp animated animated\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s;\">\r\n<article>\r\n<h5>In maximus id diam</h5>\r\n\r\n<p>Cras eu enim placerat, pellentesque ipsum vitae, porttitor lorem</p>\r\n</article>\r\n</div>\r\n\r\n<div class=\"col-md-3 col-sm-6 wow fadeInUp animated animated\" data-wow-delay=\"0.3s\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s; animation-delay: 0.3s;\">\r\n<article>\r\n<h5>Etiam congue</h5>\r\n\r\n<p>Nulla eget turpis non massa volutpat condimentum vitae vel sem</p>\r\n</article>\r\n</div>\r\n\r\n<div class=\"col-md-3 col-sm-6 wow fadeInUp animated animated\" data-wow-delay=\"0.3s\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s; animation-delay: 0.3s;\">\r\n<article>\r\n<h5>Quisque dapibus</h5>\r\n\r\n<p>Proin libero magna, viverra eu egestas volutpat, porta mattis nibh</p>\r\n</article>\r\n</div>\r\n\r\n<div class=\"col-md-3 col-sm-6 wow fadeInUp animated animated\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s;\">\r\n<article>\r\n<h5>Duis at convallis</h5>\r\n\r\n<p>Etiam accumsan velit risus, sed iaculis magna rhoncus at</p>\r\n</article>\r\n</div>\r\n\r\n<div class=\"col-md-3 col-sm-6 wow fadeInUp animated animated\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s;\">\r\n<article>\r\n<h5>Integer non commodo</h5>\r\n\r\n<p>Sed lobortis, elit vel accumsan finibus, ligula urna ultrices tortor, in vestibulum</p>\r\n</article>\r\n</div>\r\n\r\n<div class=\"col-md-3 col-sm-6 wow fadeInUp animated animated\" data-wow-delay=\"0.3s\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s; animation-delay: 0.3s;\">\r\n<article>\r\n<h5>Vestibulum luctus</h5>\r\n\r\n<p>Enim non tellus placerat pulvinar. Nulla faucibus leo eget fermentum volutpat</p>\r\n</article>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<section class=\"steps\" id=\"section-steps\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-8 col-md-offset-2\">\r\n<header class=\"section-header wow fadeInUp animated animated\" data-wow-duration=\"2\" style=\"visibility: visible;\">\r\n<p class=\"subhead\">Quick results</p>\r\n\r\n<h2>Three Simple Steps</h2>\r\n\r\n<p class=\"large\">Sed condimentum ante ac quam varius pretium. Nunc ornare laoreet augue et elementum. Phasellus enim libero, blandit sapien vitae, condimentum ultricies magna et.</p>\r\n</header>\r\n</div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"col-md-4 wow fadeInUp  animated animated\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s;\">\r\n<article>\r\n<div class=\"striped-icon-small\">1</div>\r\n<img alt=\"icon\" src=\"http://ranapatra.tj/public/icons/icon-1.svg\" />\r\n<h5>Integer Venenatis Sed</h5>\r\n\r\n<p>In suscipit, sem at vehicula commodo, elit mi aliquam turpis, et vehicula quam diam in orci</p>\r\n</article>\r\n</div>\r\n\r\n<div class=\"col-md-4 wow fadeInUp animated animated\" data-wow-delay=\"0.3s\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s; animation-delay: 0.3s;\">\r\n<article>\r\n<div class=\"striped-icon-small\">2</div>\r\n<img alt=\"icon\" src=\"http://ranapatra.tj/public/icons/icon-2.svg\" />\r\n<h5>Ut Interdum Vehicula</h5>\r\n\r\n<p>Maecenas commodo viverra urna, gravida mattis nibh dapibus sit amet. Nunc ornare laoreet</p>\r\n</article>\r\n</div>\r\n\r\n<div class=\"col-md-4 wow fadeInUp animated animated\" data-wow-delay=\"0.6s\" data-wow-duration=\"2s\" style=\"visibility: visible; animation-duration: 2s; animation-delay: 0.6s;\">\r\n<article>\r\n<div class=\"striped-icon-small\">3</div>\r\n<img alt=\"icon\" src=\"http://ranapatra.tj/public/icons/icon-3.svg\" />\r\n<h5>Pellentesque Habitant</h5>\r\n\r\n<p>Mauris nunc enim, efficitur eget sollicitudin ut, fermentum non elit justo dapibus id</p>\r\n</article>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n',1,'home','s:0:\"\";',5,NULL,NULL,'[[WIDGET][home-menu-banner]] ',1,'2016-10-02 14:03:15','2016-11-19 01:31:07'),(3,'Contact us','Contact us','contact-us','Contact us | Rana Patra | Rana Community','Contact us','Contact us','',1,'contact_layout','s:0:\"\";',5,NULL,NULL,'',1,'2016-10-15 00:42:23','2016-10-15 15:10:38');

/*Table structure for table `sliders` */

DROP TABLE IF EXISTS `sliders`;

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_name` varchar(100) DEFAULT NULL,
  `slider_data` text,
  `sort_order` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sliders` */

/*Table structure for table `testimonials` */

DROP TABLE IF EXISTS `testimonials`;

CREATE TABLE `testimonials` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `message` text,
  `image` varchar(500) DEFAULT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `testimonials` */

insert  into `testimonials`(`id`,`name`,`designation`,`message`,`image`,`modified`,`created`) values (1,'Tejas Rana','Sr. Software Engineer','It is very nice plateform to be in touch with all our Rana community memeber. Keep it up guys you are working very good work.','http://ranapatra.tj/public/tejas%202.jpg','2016-12-03 14:33:23','2016-12-03 14:28:51'),(2,'Falguni Rana','Chief Digital officer','Docoow wezat jipke wafpebo kis narinob owuca cov hej wulesuup tenfit cu no kevki lokni to mot joezufe. Zot at porukihi ewenoho efcu fil wicu mib wiunileh holnuc ceedow cujuf beehu iv lefwomtel no jura johu. Gotoniwiz bo ratpuwiv oh ze zuluti guijgow hav go judo holdap hazaz sitlife luc anjed etewekca neuv lom. Dahfermaw coga okiino zesre me ebaakhib notcik kasdi faucohu fikufi zajtusuj jef bupoufu ejeruf jussaj. Wac lued dujzusub gene moso karoc fugwubmus nep hical cijogaf tueli gul kago tunsu junhefoko tefrup gif. Oguh ibasad soh ohmo rub majsa joh guvrahej biletu miufze dat te si. Aculwad fodul wi nipagzez fokpa beb bagpolke zeb ohminmom fuc kepkihof pagnusbez mihvijki kozkazcif.','http://ranapatra.tj/public/301-redirect-directions.jpg','2016-12-03 19:39:02','2016-12-03 19:39:02'),(3,'Tejas Rana','Sr. Software Engineer','It is very nice plateform to be in touch with all our Rana community memeber. Keep it up guys you are working very good work.','http://ranapatra.tj/public/tejas%202.jpg','2016-12-03 14:33:23','2016-12-03 14:28:51'),(4,'Falguni Rana','Chief Digital officer','Docoow wezat jipke wafpebo kis narinob owuca cov hej wulesuup tenfit cu no kevki lokni to mot joezufe. Zot at porukihi ewenoho efcu fil wicu mib wiunileh holnuc ceedow cujuf beehu iv lefwomtel no jura johu. Gotoniwiz bo ratpuwiv oh ze zuluti guijgow hav go judo holdap hazaz sitlife luc anjed etewekca neuv lom. Dahfermaw coga okiino zesre me ebaakhib notcik kasdi faucohu fikufi zajtusuj jef bupoufu ejeruf jussaj. Wac lued dujzusub gene moso karoc fugwubmus nep hical cijogaf tueli gul kago tunsu junhefoko tefrup gif. Oguh ibasad soh ohmo rub majsa joh guvrahej biletu miufze dat te si. Aculwad fodul wi nipagzez fokpa beb bagpolke zeb ohminmom fuc kepkihof pagnusbez mihvijki kozkazcif.','http://ranapatra.tj/public/301-redirect-directions.jpg','2016-12-03 19:39:02','2016-12-03 19:39:02');

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) DEFAULT NULL,
  `roles` mediumtext,
  `is_admin` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `user_roles` */

insert  into `user_roles`(`id`,`name`,`roles`,`is_admin`,`status`,`created`,`modified`) values (1,'Super Admin','a:25:{s:4:\"page\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:8:\"category\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:7:\"article\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:15:\"articlecategory\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:8:\"customer\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:7:\"invoice\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:10:\"matrimonal\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:4:\"news\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:6:\"career\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:5:\"event\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:7:\"history\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:6:\"awards\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:13:\"customer_logo\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:13:\"partner_speak\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:14:\"customer_speak\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:12:\"latest_in_hr\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:9:\"media_kit\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:7:\"insight\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:12:\"insight_type\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:7:\"gallery\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:16:\"gallery_category\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:4:\"team\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:13:\"team_category\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:12:\"infographics\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:4:\"blog\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}}',1,1,'2015-03-04 07:03:58','2016-02-22 16:58:07'),(7,'Media_Updates','a:20:{s:4:\"page\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:8:\"category\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:4:\"news\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"1\";}s:6:\"career\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:5:\"event\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:7:\"history\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:6:\"awards\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"1\";}s:13:\"customer_logo\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:13:\"partner_speak\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:14:\"customer_speak\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:12:\"latest_in_hr\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:9:\"media_kit\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"1\";}s:7:\"insight\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:12:\"insight_type\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:7:\"gallery\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:16:\"gallery_category\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:4:\"team\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:13:\"team_category\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:12:\"infographics\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"0\";}s:4:\"blog\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"1\";s:6:\"review\";s:1:\"1\";}}',1,1,'2015-05-08 15:13:16','2015-08-03 09:39:28'),(8,'Career_Updates','a:20:{s:4:\"page\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:8:\"category\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:4:\"news\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:6:\"career\";a:4:{s:3:\"add\";s:1:\"1\";s:4:\"edit\";s:1:\"1\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"1\";}s:5:\"event\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:7:\"history\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:6:\"awards\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:13:\"customer_logo\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:13:\"partner_speak\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:14:\"customer_speak\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:12:\"latest_in_hr\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:9:\"media_kit\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:7:\"insight\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:12:\"insight_type\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:7:\"gallery\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:16:\"gallery_category\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:4:\"team\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:13:\"team_category\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:12:\"infographics\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}s:4:\"blog\";a:4:{s:3:\"add\";s:1:\"0\";s:4:\"edit\";s:1:\"0\";s:6:\"delete\";s:1:\"0\";s:6:\"review\";s:1:\"0\";}}',0,1,'2015-05-20 12:53:14','2015-05-20 12:53:14');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `pic` varchar(500) DEFAULT NULL,
  `last_login` datetime NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `user_roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`,`pic`,`last_login`,`role_id`,`status`,`created`,`modified`) values (2,'admin','b44671881be6bc269f48dd8223c19de170847e17','http://ranapatra.tj/public/tejas%202.jpg','2014-05-20 09:28:00',1,1,'2014-05-20 09:28:16','2016-12-03 21:17:32');

/*Table structure for table `widgets` */

DROP TABLE IF EXISTS `widgets`;

CREATE TABLE `widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `identifier` varchar(255) DEFAULT NULL,
  `value` text,
  `position` varchar(20) NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `widgets` */

insert  into `widgets`(`id`,`name`,`identifier`,`value`,`position`,`sort`,`status`,`created`,`modified`) values (1,'Test','test','','sidebar',1,1,'2016-02-19 23:12:32','2016-02-19 23:12:32'),(2,'Home Menu banner','home-menu-banner','<div class=\"col-sm-6 col-xs-10\">\r\n<h2 class=\"wow fadeInUp\" data-wow-delay=\"1s\" data-wow-duration=\"1.5s\">Welcome to Rana Patra a Rana Community</h2>\r\n<p class=\"large wow fadeInUp\" data-wow-delay=\"1.8s\" data-wow-duration=\"1.5s\">Welcome to Rana Patra. it is an open community for all Rana and its aim is to connect all Rana around the world and provide <g class=\"gr_ gr_616 gr-alert gr_gramm gr_run_anim Grammar only-ins replaceWithoutSep\" id=\"616\" data-gr-id=\"616\">latest</g> information. It also has free matrimonial service.</p>\r\n<div class=\"row wow fadeInUp\" data-wow-delay=\"2.6s\" data-wow-duration=\"1.5s\">\r\n<div class=\"col-md-5\"><a class=\"btn btn-primary btn-short\" href=\"http://admin-ranapatra.ranapatra.tj/login\">Get <g class=\"gr_ gr_328 gr-alert gr_spell gr_run_anim ContextualSpelling ins-del multiReplace\" id=\"328\" data-gr-id=\"328\">srarted</g> now</a></div>\r\n<div class=\"col-md-5\"><a class=\"btn btn-warning btn-short\" href=\"http://admin-ranapatra.ranapatra.tj/about-us\">About us</a></div>\r\n</div>\r\n</div>','sidebar',2,1,'2016-02-19 23:12:49','2016-10-15 15:26:44'),(3,'Homepage User review','homepage-user-review','<p class=\"large\">Checkout our users review. Our users are our strenght.</p>\r\n','None',NULL,1,'2016-12-03 19:47:25','2016-12-03 20:02:47'),(4,'Homepage blog','homepage-blog','<p class=\"large\">Featured and hot blog that will helpful. We hope you like this.</p>\r\n','None',NULL,1,'2016-12-03 21:07:16','2016-12-03 21:08:20'),(5,'Benefits of being registered','benefits-of-being-registered','<h4>Benefits of being registered</h4>\r\n\r\n<ul>\r\n	<li>Get latest news</li>\r\n	<li>Get the latest event around you</li>\r\n	<li>Get discount for events</li>\r\n	<li>Access all matrimonials</li>\r\n	<li>Instant interaction with matrimonial</li>\r\n	<li>Discuss with our community member about culture.</li>\r\n</ul>\r\n','None',NULL,1,'2016-12-27 12:02:04','2016-12-27 12:12:29');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
