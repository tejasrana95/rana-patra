<?php

App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 */
class CustomerProfile extends AppModel {

    public $validate = array(
        'name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Name can not be left empty',
                'required' => true,
            ),
        ),
        'gender' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Gender can not be left empty',
                'required' => true,
            ),
        ),
        'address' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Address can not be left empty',
                'required' => true,
            ),
        ),
        'contact' => array(
            'rule' => array('isValidUSPhoneFormat'),
            'required' => true,
            ),
        'placefinder' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'City, State & Country can not be left empty',
                'required' => true,
            ),
        ),
        'state' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'State can not be left empty',
                'required' => true,
            ),
        ),
        'city' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'City can not be left empty',
                'required' => true,
            ),
        ),
        'country' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Country can not be left empty',
                'required' => true,
            ),
        ),
    );

    public function beforeSave($options = array()) {
       
        return true;
    }

 function isValidUSPhoneFormat($phone){
 $phone_no=$phone['contact'];
 $errors = array();
    if(empty($phone_no)) {
        $errors [] = "Please enter Phone Number";
    }
    else if (!preg_match('/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s.]{0,1}[0-9]{3}[-\s.]{0,1}[0-9]{4}$/', $phone_no)) {
        $errors [] = "Please enter valid Phone Number";
    } 

    if (!empty($errors))
    return implode("\n", $errors);

    return true;
}
}
