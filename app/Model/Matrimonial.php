<?php

App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 */
class Matrimonial extends AppModel
{
    public $actsAs = array('Containable');
    public $hasMany = array
    (
        'MatrimonialAttachment' => array
        (
            'className'  => 'MatrimonialAttachment',
            'foreignKey' => 'matrimonial_id',
        )
    );
    public $virtualFields = array(
    'profileID' => 'CONCAT(Matrimonial.first_name, "-", Matrimonial.last_name,"-",Matrimonial.id)'
    );

    public $validate = array(
        'email' => array(
            'notEmpty' => array(
                'rule' => 'notBlank',
                'message' => 'Provide an email address'
            ),
            'validEmailRule' => array(
                'rule' => array('email'),
                'message' => 'Invalid email address'
            ),
        ),
        'bride_groom' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please select bride/groom',
                'required' => true,
            ),
        ),
        'marital_status' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please select marital status',
                'required' => true,
            ),
        ),
        'first_name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please enter your first name',
                'required' => true,
            ),
        ), 'father_name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please enter your father name',
                'required' => true,
            ),
        ),
        'mother_name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please enter your mother name',
                'required' => true,
            ),
        ),
        'last_name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please enter your last name',
                'required' => true,
            ),
        ),
        'contact' => array(
            'rule' => array('isValidUSPhoneFormat'),
            'required' => true,
        ),
        'bride_groom' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please select bride/groom',
                'required' => true,
            ),
        ),
        'nationality' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please select your nationality',
                'required' => true,
            ),
        ),
        'city' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please select your current city',
                'required' => true,
            ),
        ),
        'address' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please enter your current address',
                'required' => true,
            ),
        ),
        'pin_code' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please enter your current pin code',
                'required' => true,
            ),
        ),
        'state' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please select current state',
                'required' => true,
            ),
        ),
        'country' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please select current country',
                'required' => true,
            ),
        ),
        'bio' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please enter your information, your like-dislike, your basic details',
                'required' => true,
            ),
        ),
        'tob' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please your time of birth',
                'required' => true,
            ),
        ),
        'pob' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please enter your place of birth',
                'required' => true,
            ),
        ),
        'height' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please enter your height',
                'required' => true,
            ),
        ),
        'weight' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please enter your weight',
                'required' => true,
            ),
        ),
        'gautra' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please enter your gautra',
                'required' => true,
            ),
        ),
        'nakshtra' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please enter your nakshtra',
                'required' => true,
            ),
        ),
        'rashi' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please enter your rashi',
                'required' => true,
            ),
        ),
        'manglik' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please select are you manglik or not',
                'required' => true,
            ),
        ),
        'education' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please select your education',
                'required' => true,
            ),
        ),
        'qualification' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please select your qualification',
                'required' => true,
            ),
        ),
        
    );
    
    function getAllMatrimonials($userID, $count = false, $type = "all")
    {
        if ($userID!=0) {
            $condition=array('Matrimonial.userID' => $userID);
        } else {
            $condition=array('Matrimonial.status' => 1);
        }
        if ($type!="all") {
            $this->hasMany['MatrimonialAttachment']['conditions'] = array('MatrimonialAttachment.file_type' => $type);
        }
        $userinfo = $this->find('all', array('conditions' => $condition));
        if ($count==true) {
            $userinfo=count($userinfo);
        }
        return $userinfo;
    }
 
    function viewMatrimonial($mid, $userID = false, $type = "all")
    {
        
        $condition=array('Matrimonial.id'=>$mid);
        if ($userID!=false) {
            $condition['Matrimonial.userID']=$userID;
        }
        if ($type!="all") {
            $this->hasMany['MatrimonialAttachment']['conditions'] = array('MatrimonialAttachment.file_type' => $type);
        }
        $userinfo = $this->find('first', array('conditions' => $condition));
        return $userinfo;
    }
    
    function similarMatrimonial($currentMat, $limit = 5, $type = "all")
    {
        $condition['Matrimonial.bride_groom']=$currentMat['Matrimonial']['bride_groom'];
        $condition['Matrimonial.status']=1;
        $condition['Matrimonial.id !=']=$currentMat['Matrimonial']['id'];
        $condition['YEAR(Matrimonial.dob)']=date('Y', strtotime($currentMat['Matrimonial']['dob']));
        if ($type!="all") {
            $this->hasMany['MatrimonialAttachment']['conditions'] = array('MatrimonialAttachment.file_type' => $type);
        }
        $userinfo = $this->find('all', array('conditions' => $condition,'limit' => $limit,'order'=>array('Matrimonial.modified DESC')));
        return $userinfo;
    }
    
    function isValidUSPhoneFormat($phone)
    {
        $phone_no = $phone['contact'];
        $errors = array();
        if (empty($phone_no)) {
            $errors [] = "Please enter Phone Number";
        } elseif (!preg_match('/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s.]{0,1}[0-9]{3}[-\s.]{0,1}[0-9]{4}$/', $phone_no)) {
            $errors [] = "Please enter valid Phone Number";
        }

        if (!empty($errors)) {
            return implode("\n", $errors);
        }

        return true;
    }

    public function beforeSave($options = array())
    {
        $this->data['Matrimonial']['status'] =0;
        $this->data['Matrimonial']['dob'] = date('Y-m-d', strtotime($this->data['Matrimonial']['dob']));
        return true;
    }
    public function afterFind($results, $primary = false)
    {
        if (isset($results[0]['Matrimonial'])) {
            $results[0]['Matrimonial']['dob'] = date('d-m-Y', strtotime($results[0]['Matrimonial']['dob']));
        }
        return $results;
    }
}
