<?php

App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 */
class MatrimonialAttachment extends AppModel {
    public $actsAs = array('Containable');
    public $belongsTo  = array('Matrimonial');
    public $validate = array();
    
    function getAttachments($mid,$type="image"){
        $userinfo = $this->find('all', array('conditions' => array('matrimonial_id' => $mid,'file_type'=>$type),
        'fields' => array('MatrimonialAttachment.id', 'MatrimonialAttachment.file_name', 'MatrimonialAttachment.file_path','MatrimonialAttachment.file_type','MatrimonialAttachment.matrimonial_id')));
        return $userinfo;
    }
    
    function isValidUSPhoneFormat($phone) {
        $phone_no = $phone['contact'];
        $errors = array();
        if (empty($phone_no)) {
            $errors [] = "Please enter Phone Number";
        } else if (!preg_match('/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s.]{0,1}[0-9]{3}[-\s.]{0,1}[0-9]{4}$/', $phone_no)) {
            $errors [] = "Please enter valid Phone Number";
        }

        if (!empty($errors))
            return implode("\n", $errors);

        return true;
    }

    public function beforeSave($options = array()) {

        return true;
    }

    public function compare_password() {
        
    }

}
