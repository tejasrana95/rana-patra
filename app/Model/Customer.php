<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 *
 */
class Customer extends AppModel {
  public $validate = array(
   'email' => array(
    'notEmpty' => array(
      'rule' => 'notBlank',
      'message' => 'Provide an email address'
      ),
    'validEmailRule' => array(
      'rule' => array('email'),
      'message' => 'Invalid email address'
      ),
    'uniqueEmailRule' => array(
      'rule' => 'isUnique',
      'message' => 'Email already registered'
      )
    ),
   'password' => array(
    'required' => array(
      'rule' => array('notBlank'),
      'message' => 'A password is required'
      ),
    'alphaNumeric' => array(
     'rule'     => array('custom', '/(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/'),
        			//'required' => true,
     'message' => 'Password should be at least 7 characters, must have at least 1 upper case, lower case ,numeric, special character.',
       			// 'on' => 'create'
     )
    ),
      'confirm_password' => array(
       'notBlank' => array(
        'rule' => array('notBlank'),
        'message' => 'Re-enter your password.',
        ),
       'alphaNumeric' => array(
        'rule'     => array('custom', '/(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/'),
        //'required' => true,
        'message' => 'Password should be at least 7 characters, must have at least 1 upper case, lower case ,numeric, special character.',
        'on' => 'create'
        ),
       'checkMatch' => array(
        'rule'=> array('compare_password'),
        'message' => "Both password not matched."
        )

       )
      );
  public function beforeSave($options = array()) {
    if (isset($this->data[$this->alias]['password'])) {
        $passwordHasher = new SimplePasswordHasher();
        $this->data[$this->alias]['password'] = $passwordHasher->hash(
            $this->data[$this->alias]['password']
            );
    }
    return true;
}
public function compare_password(){
  if($this->data[$this->alias]['password']===$this->data[$this->alias]['confirm_password']){
   return true;	
}
}
}
