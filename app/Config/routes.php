<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
//Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
Router::connect('/', array('controller' => 'pages', 'action' => 'index', 'home'));
Router::redirect('/users/login', '/login');
Router::connect('/login', array('controller' => 'user', 'action' => 'login'));
Router::connect('/logout', array('controller' => 'user', 'action' => 'logout'));
Router::connect('/dashboard', array('controller' => 'profile', 'action' => 'dashboard'));
Router::connect('/login/register', array('controller' => 'user', 'action' => 'register'));
Router::connect('/login/password', array('controller' => 'user', 'action' => 'password'));
Router::connect('/login/password/:string', array('controller' => 'user', 'action' => 'password'), array('pass' => array('string')));
Router::connect('/login/confirm/:string', array('controller' => 'user', 'action' => 'confirm'), array('pass' => array('string')));
Router::connect('/login/reset', array('controller' => 'user', 'action' => 'reset'));
Router::connect('/login/fbcallback', array('controller' => 'user', 'action' => 'fbcallback'));
Router::connect('/login/googlecallback', array('controller' => 'user', 'action' => 'googlecallback'));
Router::connect('/login/facebook_connect', array('controller' => 'user', 'action' => 'facebook_connect'));
Router::connect('/login/facebook_logout', array('controller' => 'user', 'action' => 'facebook_logout'));
Router::connect('/profile', array('controller' => 'profile', 'action' => 'index'));

//matrimonials
Router::connect('/matrimonial/uploadImages', array('controller' => 'matrimonial', 'action' => 'uploadImages'));
Router::connect('/matrimonial/loadImages/:mid/:file_type', array('controller' => 'matrimonial', 'action' => 'loadImages'),array('pass' => array('mid','file_type')));
Router::connect('/matrimonial/deleteImages/:mid/:file_id', array('controller' => 'matrimonial', 'action' => 'deleteImages'),array('pass' => array('mid','file_id')));
Router::connect('/matrimonial/edit/:mid', array('controller' => 'matrimonial', 'action' => 'edit'),array('pass' => array('mid')));
Router::connect('/matrimonial/index/*', array('controller' => 'matrimonial', 'action' => 'index'));
Router::connect('/matrimonial/:username/:mid', array('controller' => 'matrimonial', 'action' => 'view'),array('pass' => array('username', 'mid')));
Router::connect('/matrimonial/myview', array('controller' => 'matrimonial', 'action' => 'myview'));
Router::connect('/matrimonial/create', array('controller' => 'matrimonial', 'action' => 'create'));
Router::connect('/matrimonial/*', array('controller' => 'matrimonial', 'action' => 'index'));




//payments
Router::connect('/payments', array('controller' => 'payments', 'action' => 'index'));


Router::connect('/blog', array('controller' => 'blog', 'action' => 'index'));
Router::connect('/blog/latestBlog', array('controller' => 'blog', 'action' => 'latestBlog'));
Router::connect('/blog/index/*', array('controller' => 'blog', 'action' => 'index'));
Router::connect('/blog/categories/*', array('controller' => 'blog', 'action' => 'category'));
Router::connect('/blog/category/*', array('controller' => 'blog', 'action' => 'categoryblog'));
Router::connect('/blog/*', array('controller' => 'blog', 'action' => 'view'));
Router::connect('/:slug', array('controller' => 'pages', 'action' => 'index'), array('pass' => array('slug')));
Router::connect('/filemanager', array('controller' => 'filemanager', 'action' => 'index'));
Router::connect('/filemanager/:userid', array('controller' => 'filemanager', 'action' => 'index'), array('pass' => array('userid')));

//Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
//pdf
Router::parseExtensions('pdf');
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
