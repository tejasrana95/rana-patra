function profileUpload() {
		var $uploadCrop;

		function readFile(input) {
 			if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            
	            reader.onload = function (e) {
					$('.profile-pic-view').addClass('ready');
	            	$uploadCrop.croppie('bind', {
	            		url: e.target.result
	            	}).then(function(){
	            		
	            	});
	            	
	            }
	            reader.readAsDataURL(input.files[0]);
	        }
	        else {
		        swal("Sorry - you're browser doesn't support the FileReader API");
		    }
		}

		$uploadCrop = $('#uploadCropPreview').croppie({
			viewport: {
				width: 200,
				height: 200,
				type: 'square'
			},
			enableExif: true
		});

		$('#otherProfilePic').on('change', function () {  readFile(this); });
		$('.upload-result').on('click', function (ev) {
			$uploadCrop.croppie('result', {
				type: 'canvas',
				size: 'viewport'
			}).then(function (resp) {
				$('#imagebase64').val(resp);
				$('#CustomerProfileEditForm').submit();
			});
			return false;
		});
	}
profileUpload();


$("#otherProfilePic").change(function(){
var p = $("#uploadCropPreview");
		// fadeOut or hide preview
		p.fadeOut();
		// prepare HTML5 FileReader
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("otherProfilePic").files[0]);

		oFReader.onload = function (oFREvent) {
	   		p.fadeIn();
	   		$("#uploadCropPreview-hint").fadeIn();
		};
	});