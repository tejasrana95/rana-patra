$(document).ready(function() {
    // Preloader
    $(window).on('load', function() {
        var $preloader = $('#page-preloader'),
            $spinner = $preloader.find('.spinner');
        $spinner.fadeOut();
        $preloader.delay(350).fadeOut(800);
    });

    // Hovers in dream team for touch screen
    if (Modernizr.touch) {
        $(document).on('touchend', '.wrap-dream-team .list-dream-team .team-item', function() {
            $('.wrap-dream-team .list-dream-team .team-item').removeClass('active');
            $(this).addClass('active');
        });
    } else {
        $('.wrap-dream-team .list-dream-team .team-item').hover(function() {
            $(this).find('.mask').css('opacity', 1);
        }, function() {
            $(this).find('.mask').css('opacity', 0);
        });
    }

    // Fancybox 
    $(".fancybox").fancybox({ loop: true, protect: true, infobar: true, preload: "auto", animationDuration: 366, zoomOpacity: 'auto', animationEffect: "zoom", transitionEffect: "circular", transitionDuration: 1000 });

    // Datepiecker
    $('#my-calendar').datepicker({
        format: 'mm/dd/yyyy',
        startDate: '-3d'
    });
    /* =============================================== */
    /* ======= Accordion panel button collapse ======= */
    /* =============================================== */
    function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .toggleClass('active')
            .find('.fa')
            .toggleClass('fa-plus-circle fa-minus-circle');
    }
    $('#accordion-two').on('hidden.bs.collapse shown.bs.collapse', toggleIcon);

    function toggleActive(e) {
        $(e.target)
            .prev('.panel-heading')
            .toggleClass('active')
    }
    $('#accordion-one').on('hidden.bs.collapse shown.bs.collapse', toggleActive);

    /* <!-- =============================================== */
    /* <!-- ======= Isotope ======= -->*/
    /* <!-- =============================================== --> */
    var $container = $('#gallery-items');

    $(window).load(function() {
        $container.isotope({
            //		    resizable: false, // disable normal resizing
            transitionDuration: '0.65s',
            masonry: {
                columnWidth: $container.find('.gallery-item:not(.wide)')[0]
            }
        });

        $(window).resize(function() {
            $container.isotope('layout');
        });
    });

    // filter items on button click
    $('#filters').on('click', 'button', function(e) {
        $(e.target).toggleClass('active').siblings().removeClass("active");
        var filterValue = $(this).attr('data-filter');
        $container.isotope({ filter: filterValue });
    });

    /* <!-- =============================================== --> */
    /* <!-- =============== Carousels ==================== --> */
    /* <!-- =============================================== -->  */

    $("#testimonials-carousel").owlCarousel({
        singleItem: true,
        autoPlay: true,
        navigation: true,
        navigationText: [, ]
    });
    $("#twitter-carousel").owlCarousel({
        singleItem: true,
        autoPlay: true,
        transitionStyle: 'goDown'
    });
    $("#team-carousel").owlCarousel({
        items: 7,
        autoPlay: true,
        navigation: false
    });

    /* <!-- =============================================== --> */
    /* <!-- ========== Menu Links Scroll ===+============== --> */
    /* <!-- =============================================== -->  */
    $('.scroll').click(function(e) {
        var off = -79;
        var target = this.hash;
        if ($(target).offset().top == 0) {
            off = 0;
        }
        $('html,body').scrollTo(target, 800, {
            offset: off,
            easing: 'easeInOutExpo'
        });
        e.preventDefault();
        //   ---- dissapearing menu on click
        if ($('.navbar-collapse').hasClass('in')) {
            $('.navbar-collapse').removeClass('in').addClass('collapse');
        }
    });

    /* <!-- =============================================== --> */
    /* <!-- ===============  Scrollspy fix ================ --> */
    /* <!-- =============================================== --> */
    $(window).on('load', function() {
        var $body = $('body'),
            $navtop = $('#nav'),
            offset = $navtop.outerHeight();
        // Enable scrollSpy with correct offset based on height of navbar
        $body.scrollspy({
            target: '#nav',
            offset: offset
        });
        // function to do the tweaking
        function fixSpy() {
            // grab a copy the scrollspy data for the element
            var data = $body.data('bs.scrollspy');
            // if there is data, lets fiddle with the offset value
            if (data) {
                // get the current height of the navbar
                offset = $navtop.outerHeight();
                // change the data's offset option to match
                data.options.offset = offset
                    // now stick it back in the element
                $body.data('bs.scrollspy', data);
                // and finally refresh scrollspy
                $body.scrollspy('refresh');
            }
        }
        // Now monitor the resize events and make the tweaks
        var resizeTimer;
        $(window).resize(function() {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(fixSpy, 200);
        });
    });

    /* <!-- =============================================== --> */
    /* <!-- ============ Init GMap on hidden tab =========== --> */
    /* <!-- =============================================== -->  */
    function waitForVisibleMapElement() {
        setTimeout(function() {
            if ($('#on-map').is(":visible")) {
                initialize();
            } else {
                waitForVisibleMapElement();
            };
        }, 10);
    };
    waitForVisibleMapElement();

    /* <!-- =============================================== --> */
    /* <!-- ============ Prarallax =========== --> */
    /* <!-- =============================================== -->  */
    if (!Modernizr.touch) {
        //        $('.parallax-section-1').parallax("50%", 0.5);
        //        $('.parallax-section-2').parallax("50%", 0.5);
        //        $('.parallax-section-3').parallax("50%", 0.5);
        //        $('.wrap-features').parallax("50%", 0.5);
        //        $('.wrap-counters').parallax("50%", 0.5);
        //        $('.wrap-rates').parallax("50%", 0.5);
        //        $('.wrap-programs').parallax("50%", 0.5);
        //        $('#programs').parallax("50%", 0.5);

    }

    /* <!-- =============================================== --> */
    /* <!-- ============ Progress Bar Animation =========== --> */
    /* <!-- =============================================== -->  */
    $('.skills').waypoint(function() {
        $('.skills-animated').each(function() {
            var persent = $(this).attr('data-percent');
            $(this).find('.progress').animate({
                width: persent + '%'
            }, 300);
        });
    }, {
        offset: '100%',
        triggerOnce: true
    });

    /* <!-- =============================================== --> */
    /* <!-- ============ Search Animation =========== --> */
    /* <!-- =============================================== -->  */
    $('#search-open, #search-close').on('click', function(e) {
        e.preventDefault();
        $('.relative-nav-container').toggleClass('open-search');
        $('.navbar-search').toggleClass('open');
    });

    /* <!-- =============================================== --> */
    /* <!-- ============ Herader Animation =========== --> */
    /* <!-- =============================================== -->  */
    $(window).scroll(function() {
        if ($(this).scrollTop() > 15) {
            $('nav').addClass("navbar-alt")
        } else {
            $('nav').removeClass("navbar-alt")
        }
    });
    if ($(window).width() < 769) {
        $('#nav').removeClass('navbar-default').addClass('navbar-inverse');
        $('.floated .with-border').removeClass('with-border');
    }
    /* <!-- =============================================== --> */
    /* <!-- ============ Tooltip  =========== --> */
    /* <!-- =============================================== -->  */
    $("[data-toggle='tooltip']").tooltip();
    $('[data-toggle="popover"]').popover({
        container: "body",
        placement: "top",
        trigger: "hover",
        delay: { show: 0, hide: 500 },
        html: true,
        content: function() {
            var content = $(this).attr("data-popover-content");
            return $(content).children(".popover-body").html();
        },
    });

    /* <!-- =============================================== --> */
    /* <!-- === Switch monthly/annual pricing tables  ===== --> */
    /* <!-- =============================================== -->  */
    //switch from monthly to annual pricing tables
    bouncy_filter($('.cd-pricing-container'));

    function bouncy_filter(container) {
        container.each(function() {
            var pricing_table = $(this);
            var filter_list_container = pricing_table.find('.pricing-switcher'),
                filter_radios = filter_list_container.find('input[type="radio"]'),
                pricing_table_wrapper = pricing_table.find('.cd-pricing-wrapper');
            //store pricing table items
            var table_elements = {};
            filter_radios.each(function() {
                var filter_type = $(this).val();
                table_elements[filter_type] = pricing_table_wrapper.find('li[data-type="' + filter_type +
                    '"]');
            });
            //detect input change event
            filter_radios.on('change', function(event) {
                event.preventDefault();
                //detect which radio input item was checked
                var selected_filter = $(event.target).val();
                //give higher z-index to the pricing table items selected by the radio input
                show_selected_items(table_elements[selected_filter]);
                //rotate each cd-pricing-wrapper
                //at the end of the animation hide the not-selected pricing tables and rotate back the .cd-pricing-wrapper
                if (!Modernizr.cssanimations) {
                    hide_not_selected_items(table_elements, selected_filter);
                    pricing_table_wrapper.removeClass('is-switched');
                } else {
                    pricing_table_wrapper.addClass('is-switched').eq(0).one(
                        'webkitAnimationEnd oanimationend msAnimationEnd animationend',
                        function() {
                            hide_not_selected_items(table_elements, selected_filter);
                            pricing_table_wrapper.removeClass('is-switched');
                            //change rotation direction if .cd-pricing-list has the .cd-bounce-invert class
                            if (pricing_table.find('.cd-pricing-list').hasClass('cd-bounce-invert'))
                                pricing_table_wrapper.toggleClass('reverse-animation');
                        });
                }
            });
        });
    }

    function show_selected_items(selected_elements) {
        selected_elements.addClass('is-selected');
    }

    function hide_not_selected_items(table_containers, filter) {
        $.each(table_containers, function(key, value) {
            if (key !== filter) {
                $(this).removeClass('is-visible is-selected').addClass('is-hidden');
            } else {
                $(this).addClass('is-visible').removeClass('is-hidden is-selected');
            }
        });
    }

    /* <!-- =============================================== --> */
    /* <!-- === Switch monthly/annual pricing tables  ===== --> */
    /* <!-- =============================================== --> */
    // Variable to hold scroll type
    var slideDrag,
        // Width of .scroll-content ul
        slideWidth = $(".scroll-slider").width(),
        // Speed of animation in ms
        slideSpeed = 400;

    // Initialize sliders
    $(".scroll-slider").slider({
        animate: slideSpeed,
        start: checkType,
        slide: doSlide,
        max: slideWidth
    });


    function checkType(e) {
        slideDrag = $(e.originalEvent.target).hasClass("ui-slider-handle");
    }

    function doSlide(e, ui) {
        //	    var target = $(e.target).prev(".scroll-content"),
        var target = $(".scroll-content"),
            maxScroll = target.prop("scrollWidth") - target.width();

        // Was it a click or drag?
        if (slideDrag === true) {
            // User dragged slider head, match position
            target.prop({ scrollLeft: ui.value * (maxScroll / slideWidth) });
        } else {
            // User clicked on slider itself, animate to position
            target.stop().animate({
                scrollLeft: ui.value * (maxScroll / slideWidth)
            }, slideSpeed);
        }
    }

    // Popovers for map 
    $('.wrap-map-item .map-marker').popover({
        container: ".wrap-map-item",
        placement: "top",
        trigger: "hover",
        delay: { "hide": 2000 },
        html: true,
        content: function() {
            var content = $(this).attr("data-popover-content");
            return $(content).children(".popover-body").html();
        }
    });


    if ($('.tweets-list-container-with-logo').length > 0) {
        $('.tweets-list-container-with-logo').tweetscroll({
            replies: true,
            position: 'append',
            animation: 'slide_up',
            time: true,
            date_format: 'ago',
            visible_tweets: 1,
            request_url: base_url + 'api/twitter',
            logo: false
        }); // TWEETSCROLL END
    }
}); // Document ready


function getcountry() {

    $.get('/common/getcountry/', function(result) {
        $('#country').html(result);
    });

}

function getstate() {
    // don't do anything if use selects "Select Cat"
    if ($("#country").val() !== "") {
        $.get('/common/getstate/' + $('#country').val(), function(result) {
            $('#state').html(result);
        });
    }
}

function getcity() {
    if ($("#state").val() !== "") {
        $.get('/common/getcity/' + $('#state').val(), function(result) {
            $('#city').html(result);
        });
    }
}

// Can also be used with $(document).ready()
if ($('.flexslider').length > 0) {
    $(window).load(function() {
        $('.flexslider').flexslider({
            animation: "slide",
            controlNav: "thumbnails"
        });
    });
}


$(document).ready(function() {
    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function(e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function() {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url'],input[type='email'],textarea,select"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');


});
$(function() {
    if ($('#dob').length > 0) {
        var defaultDate = $("#dob").val();
        var year = new Date().getFullYear();
        $('#dob').datetimepicker({
            maxDate: moment({ year: year - 20 }),
            minDate: moment({ year: year - 60 }),
            format: 'DD-MM-YYYY',
            ignoreReadonly: true
        });
        if (defaultDate != "") {
            $('#dob').data("DateTimePicker").date(defaultDate);
            $("#dob").attr('readonly', 'readonly');
        }
        $('#tob').datetimepicker({
            format: 'HH:mm',
            ignoreReadonly: true
        });
    }
});
$(function() {
    var xhr;
    $('#placefinder').autoComplete({
        minChars: 3,
        source: function(term, suggest) {
            term = term.toLowerCase();
            xhr = $.getJSON('/common/placeFinder/', { placeFinder: term }, function(data) {
                if (data.status === 'OK') {
                    var suggestions = [];
                    $.each(data, function(index, value) { if (value.hasOwnProperty("location")) { suggestions.push(value); } });
                    suggest(suggestions);
                }
            });
        },
        renderItem: function(item, search) {
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            return '<div class="autocomplete-suggestion" data-place_id="' + item.place_id + '" data-val="' + item.location + '" data-city="' + item.terms[0].value + '" data-state="' + item.terms[1].value + '" data-country="' + item.terms[2].value + '">' + item.location.replace(re, "<b>$1</b>") + '</div>';
        },
        onSelect: function(e, term, item) {
            $("#country").val(item.data('country'));
            $("#city").val(item.data('city'));
            $("#state").val(item.data('state'));
            $("#place_id").val(item.data('place_id'));
        }
    });
});

$(function() {
    var xhr;
    $('#pob').autoComplete({
        minChars: 3,
        source: function(term, suggest) {
            term = term.toLowerCase();
            xhr = $.getJSON('/common/placeFinder/', { placeFinder: term }, function(data) {
                if (data.status === 'OK') {
                    var suggestions = [];
                    $.each(data, function(index, value) { if (value.hasOwnProperty("location")) { suggestions.push(value); } });
                    suggest(suggestions);
                }
            });
        },
        renderItem: function(item, search) {
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            return '<div class="autocomplete-suggestion" data-place_id="' + item.place_id + '" data-val="' + item.location + '" data-city="' + item.terms[0].value + '" data-state="' + item.terms[1].value + '" data-country="' + item.terms[2].value + '">' + item.location.replace(re, "<b>$1</b>") + '</div>';
        },
        onSelect: function(e, term, item) {}
    });
});

function loadImage(checkFaceDetection) {
    $.get("/matrimonial/loadImages/" + mid + "/image", function(data) {
        $("#image-block").html(data);
        if (typeof checkFaceDetection != 'undefined' && checkFaceDetection == true) { face_detection_init(); }
    });
}

function loadKundali() {
    $.get("/matrimonial/loadImages/" + mid + "/kundali", function(data) {
        $("#kundali-block").html(data);
    });
}

function deleteFile(id, fileID) {
    var cnf = confirm('Are you sure you want to delete this file? Once you delete you can not get it back. You have to upload it again.');
    if (cnf != true) {
        return false;
    }
    $.get("/matrimonial/deleteImages/" + id + "/" + fileID, function(data) {
        $("#file-slug-" + fileID).fadeOut(500, function() {
            $(this).remove();
            $('.faceDetect-' + fileID).remove();
        });
    });
    return false;
}

function directFileDelete(id, fileID) {
    $.get("/matrimonial/deleteImages/" + id + "/" + fileID, function(data) {
        $("#image-" + fileID + ' .display_none.removedPicture').removeClass("display_none");
        $('.faceDetect-' + fileID).remove();
        updateImageSizeFace();
    });
    return false;
}

function face_detection_init() {
    var i = 1;
    $(".faceDetectClass .fancybox").each(function() {
        var id = 'faceId' + i;
        var width_container = document.getElementById("faceDetectWrapper").offsetWidth;
        document.getElementById("faceDetectWrapper").innerHTML = document.getElementById("faceDetectWrapper").innerHTML + "<img src='' id='" + id + "' class='" + $(this).attr('data-parant-class') + "'/>";
        document.getElementById(id).setAttribute("src", $(this).find('img').attr('src'));
        var that = this;
        document.getElementById(id).onload = function() {
            $('#' + id).faceDetection({
                complete: function(faces) {
                    var count = faces.length;
                    console.log(count);
                    if (count == 0) {
                        $('#noFace').html('<div class="alert alert-danger alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Warning!</strong> We are not able to found the face from uploaded image. So, We remove it from system. Please reupload photo with clear face.</div>')
                        $(that).attr('data-caption', 'This image has been removed because we are not able to find any valid face in this picture. Please upload picture with one clear face.');
                        directFileDelete($(that).attr('data-mat-id'), $(that).attr('data-image-id'));
                    } else if (count > 1) {
                        $('#multipleFace').html('<div class="alert alert-danger alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Warning!</strong> We found more than 1 face in uploaded image. So, we remove that photo. Please upload new photo which contain one person with clear face.</div>')
                        $(that).attr('data-caption', 'This image has been removed because we found more than 1 face (approx. ' + count + ' clear faces). Please upload picture which contain only one clear face.');
                        directFileDelete($(that).attr('data-mat-id'), $(that).attr('data-image-id'));
                    }
                }
            });

        }
        i++;
    });

}

function updateImageSizeFace() {
    $(".faceDetectClass .fancybox").each(function() {
        var img = $(this).find('img');
        $('.removedPicture').css('width', $(img)[0].clientWidth);
        $('.removedPicture').css('height', $(img)[0].clientHeight);
        $('.removedPicture').css('line-height', $(img)[0].clientHeight + '%');
    });
}

$(window).resize(function() {
    if ($('.faceDetectClass .fancybox').length > 0) {
        updateImageSizeFace();
    }
});

function toggleSearch() {
    $("#toggleSearch").slideToggle();
}
$(function() {
    if ($('#yearFrom').length > 0) {
        $("#yearFrom").removeAttr('readonly');
        $("#yearTo").removeAttr('readonly');
        var yearFrom = $("#yearFrom").val();
        var yearTo = $("#yearTo").val();
        var year = new Date().getFullYear();

        $('#yearFrom').datetimepicker({
            maxDate: moment({ year: year - 20 }),
            minDate: moment({ year: year - 60 }),
            format: 'YYYY',
            ignoreReadonly: true,
            useCurrent: false
        });
        $('#yearTo').datetimepicker({
            maxDate: moment({ year: year - 20 }),
            minDate: moment({ year: year - 60 }),
            format: 'YYYY',
            ignoreReadonly: true,
            useCurrent: false
        });
        $("#yearFrom").on("dp.change", function(e) {
            $('#yearTo').data("DateTimePicker").minDate(e.date);
            $('#yearTo').data("DateTimePicker").defaultDate(e.date);
        });
        $("#yearTo").on("dp.change", function(e) {
            $('#yearTo').data("DateTimePicker").minDate(e.date);
            $('#yearFrom').data("DateTimePicker").maxDate(e.date);
        });

        if (yearFrom != "") {
            $('#yearFrom').data("DateTimePicker").date(yearFrom);
            $("#yearFrom").attr('readonly', 'readonly');
        }
        if (yearTo != "") {
            $('#yearTo').data("DateTimePicker").date(yearTo);
            $("#yearTo").attr('readonly', 'readonly');
        }
    }
});

function fetchPrice() {
    var item = $("#item_name").val();
    var mc_currency = $("#mc_currency").val();
    var no_of_month = $("#no_of_month").val();
    if (typeof no_of_month == "undefined" || no_of_month == 0) {
        no_of_month = 1;
    }
    if (typeof mc_currency == "undefined" || mc_currency == 0) {
        mc_currency = 'INR';
    }
    $("#currencyCode").html(mc_currency);
    var totalAmount = no_of_month * amount[item];
    $("#actual_amount_inr").val(totalAmount);
    var fxCurrencyUpdate = function(data) {
        fx.rates = data.rates
        var rate = fx(totalAmount).from("INR").to(mc_currency)
        var mc_gross = rate.toFixed(2);
        $("#mc_gross").val(mc_gross);
    }
    $.getJSON("http://api.fixer.io/latest", fxCurrencyUpdate);
    $("#payment-gateway option").attr("selected", false); //Unselect each option
    if (mc_currency != "INR") {
        $("#payment-gateway option[value=paypal]").removeAttr('disabled');
        $("#payment-gateway option[value=paytm]").attr('disabled', 'disabled');
    } else {
        $("#payment-gateway option[value=paytm]").removeAttr('disabled');
        $("#payment-gateway option[value=paypal]").attr('disabled', 'disabled');
    }

}

function validateCheckout() {
    var city = $("#city").val();
    var country = $("#country").val();
    var place_id = $("#place_id").val();
    if ($.trim(city) == "" && $.trim(country) == "" && $.trim(place_id) == "") {
        alert("Please choose city from dropdown.");
        return false;
    }
    return true;
}

function validateProfile() {
    var city = $("#city").val();
    var country = $("#country").val();
    var place_id = $("#place_id").val();
    if ($.trim(city) == "" && $.trim(country) == "" && $.trim(place_id) == "") {
        alert("Please choose city from dropdown.");
        return false;
    }
    return true;
}

function validateMatrimonial() {
    var city = $("#city").val();
    var country = $("#country").val();
    var place_id = $("#place_id").val();
    if ($.trim(city) == "" && $.trim(country) == "" && $.trim(place_id) == "") {
        alert("Please choose city from dropdown.");
        return false;
    }
    return true;
}