<?php
App::import('Controller', 'Common');
$CommonController = new CommonController();
?>
<?php 
if(isset($allTestimonials) && !empty($allTestimonials)):
    ?>
<section class="testimonials" id="testimonials">
    <a class="striped-icon divider scroll" href="#testimonials">
        <i class="fa fa-chevron-down"></i>
    </a>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <header class="section-header wow fadeInUp" data-wow-duration="2s">
                    <p class="subhead">about reviews</p>
                    <h2>What Users Say?</h2>
                    <?php echo $testimonialContent; ?>
                </header>
            </div>
        </div><!--row-->
        <div id="testimonials-carousel">
            <?php  
            $i=1;
            foreach($allTestimonials as $testimonial):  
                if($i==1){
                    $startDiv= '<div class="reviews">';
                    $endDiv='';
                    $alignment="left";
                }
                if($i==2){
                    $startDiv= '';
                    $endDiv='</div>';
                    $alignment="right";  
                    $i=0; 
                }
                echo  $startDiv;
                ?>
                <div class="client <?php echo $alignment; ?>">

                    <p class="large comment"><?php echo $CommonController->truncate($testimonial['Testimonial']['message'],200,'..'); ?></p>
                    <div class="media">
                        <div class="media-left">
                            <a href="#fakelink">
                                <img class="avatar media-object " src="images/placehold/60x60.gif" alt="avatar" />
                            </a>
                        </div>
                        <div class="media-body">
                            <h5 class="media-heading"><?php echo $testimonial['Testimonial']['name']; ?></h5>
                            <p><?php echo $testimonial['Testimonial']['designation']; ?></p>
                        </div>
                    </div>
                </div>
                <?php echo $endDiv; 
                $i=$i+1;
                ?>
            <?php endforeach; ?>
        </div><!--carousel-->
    </div><!--container-->
</section>
<?php endif; ?>