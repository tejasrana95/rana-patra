<?php
if (!empty($twitterdata) && isset($twitterdata) && $twitterdata != null) :
?>
<section class="twitter dark parallax-section-3">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <article>
                    <i class="fa fa-twitter"></i>
                    <div id="twitter-carousel">
                        <?php
                        foreach ($twitterdata as $twitter):
                            $ApiController = new ApiController();
                            $tweet = $ApiController->tweet_html_text($twitter);
                        ?>
                        <div class="twitter-feed">
                            <?php print $tweet; ?>
                        </div>
                    <?php endforeach; ?>

                </div>
            </article>
        </div>
    </div><!--row-->
</div><!--container-->
</section>
<?php endif; ?>