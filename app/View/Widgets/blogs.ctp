<?php
App::import('Controller', 'Common');
$CommonController = new CommonController();
?>
<?php 
    if(isset($allArticles) && !empty($allArticles)):
        ?>
    <section class="blog" id="blog">
        <a class="striped-icon divider scroll" href="#blog">
            <i class="fa fa-chevron-down"></i>
        </a>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <header class="section-header  wow fadeInUp" data-wow-duration="2s">
                        <p class="subhead">Our blog</p>
                        <h2>Featured blogs, Worth to read</h2>
                        <?php echo $blogContent; ?>
                    </header>
                </div>
            </div><!--row-->
            <div class="row">
                <?php foreach($allArticles as $article): 
                if(isset($article['Article']['banner']) && !empty($article['Article']['banner'])){
                    $banner=$article['Article']['banner'];
                }else{
                    $banner=$site_url.'/images/placehold/360x230.gif';
                }

                ?>
                <div class="col-md-4  wow fadeInUp" data-wow-duration="2s">
                    <div class="post">
                        <div class="post-heading">
                            <a class="post-image" href="<?php echo $CommonController->urlGenrator($article['Article']['article_slug'],'Article') ?>">
                                <img src="<?php echo  $banner; ?>" alt="<?php echo $article['Article']['article_title'] ?>"  title="<?php echo $article['Article']['article_title'] ?>"/>
                            </a>
                              <a class="post-avatar" href="#"><img class="" src="<?php echo ($CommonController->getUserProfile($article['Article']['userID'],$article['Article']['isAdmin'])) ?>" alt="<?php echo ucfirst($CommonController->getUserName($article['Article']['userID'],$article['Article']['isAdmin'])) ?>"></a>
                        </div>
                        <div class="post-body">
                            <ul class="list-inline">
                            <li><i class="fa fa-user"></i> <?php echo ucfirst($CommonController->getUserName($article['Article']['userID'],$article['Article']['isAdmin'])) ?></li>
                                <li><i class="fa fa-calendar"></i> <?php echo date('d M Y',strtotime($article['Article']['created'])) ?></li>
                        </ul>
                            <a href="<?php echo $CommonController->urlGenrator($article['Article']['article_slug'],'Article') ?>" title="<?php echo $article['Article']['article_title'] ?>"><h5><?php echo $article['Article']['article_name'] ?></h5></a>
                            <?php echo $CommonController->truncate($article['Article']['content'],250,'...'); ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div><!--row-->
</div><!--container-->
</section>
 <?php endif; ?>