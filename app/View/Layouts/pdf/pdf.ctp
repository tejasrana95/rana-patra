<?php require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
spl_autoload_register('DOMPDF_autoload'); 
$dompdf = new DOMPDF(); 
$dompdf->set_paper = 'A4';
$dompdf->load_html(utf8_decode($content_for_layout), Configure::read('App.encoding'));

$dompdf->render();

//echo $dompdf->output();
$dompdf->stream('Invoice_'.$this->request->data['CustomerInvoice']['txn_id'].'.pdf',array('Attachment'=>0));
echo $this->fetch('content');
?>