<?php
App::import('Controller', 'Widget');
$WidgetsController = new WidgetsController();
?>
<!DOCTYPE html>
<html lang="en">
<?php echo $this->element('head'); ?>
<body id="home" data-offset="80" class="no-margin-top" data-target=".navbar" data-spy="scroll">
    <!--        <div id="page-preloader"><span class="spinner"></span></div>-->
    <!-- Navigation -->
    <!-- ++++++++++++++++++++++++++++++++++++++++ -->

    <?php echo $this->element('nav_bar'); ?>
    <!-- Home - Header -->
    <!-- ++++++++++++++++++++++++++++++++++++++++ -->
    <?php
    if (isset($media_file) && !empty($media_file)):
        ?>
    <style>.home-header {
        background-image: url("<?php echo Router::url('/', true) ?>images/graph-home.svg"), url("<?php echo $media_file['Media']['content']; ?>");
    }</style>
    <section class="home-header dark">
        <div class="container">
            <div class="row">
                <?php if (isset($page_detail['Page']['banner_text']) && !empty($page_detail['Page']['banner_text'])): ?>
                    <?php print $page_detail['Page']['banner_text']; ?>
                <?php endif; ?>
            </div><!--row-->
        </div><!--container-->
    </section>
<?php endif; ?>

<!-- Home - About -->
<!-- ++++++++++++++++++++++++++++++++++++++++ -->
<?php print $page_detail['Page']['content']; ?>
<!-- ++++++++++++++++++++++++++++++++++++++++ -->
<?php

echo $this->requestAction(array(
    'controller' => 'widgets',
    'action' => 'testimonials',
    ), array('return'));

echo $this->requestAction(array(
    'controller' => 'widgets',
    'action' => 'twitter',
    ), array('return'));

echo $this->requestAction(array(
    'controller' => 'widgets',
    'action' => 'blogs',
    ), array('return'));
    ?>
    <!-- Home - Blog -->
    <!-- ++++++++++++++++++++++++++++++++++++++++ -->
    

    <!-- Home - Application -->
    <!-- ++++++++++++++++++++++++++++++++++++++++ -->
    
    <!-- Footer - Tabs -->
    <!-- ++++++++++++++++++++++++++++++++++++++++ -->
    <footer>
        <?php echo $this->element('footer'); ?>
    </footer>
    <!-- ++++++++++++++++++++++++++++++++++++++++ -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <?php
    echo $this->Html->script(array('jquery-1.11.1.min', 'jquery-ui', 'bootstrap.min'
        , 'isotope.pkgd.min', 'owl.carousel.min', 'jquery.scrollTo.min', 'jquery.ui.effect.min', 'contact',
        'jquery.mb.YTPlayer', 'modernizr', 'jquery.waypoints.min', '/js/fancybox/jquery.fancybox', 'wow.min'));
        ?>
        <script>
            new WOW().init();
        </script>
        <script>
            new WOW().init();
        </script>
        <!-- Google maps -->
        <?php echo $this->Html->script(array('jquery.easypiechart.min', 'TweenMax.min', 'highcharts', 'Chart.min', 'custom'));
        ?>
    </body>
    </html>
</body>
</html>
