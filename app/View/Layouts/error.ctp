<!DOCTYPE html>
<html lang="en">
<?php echo $this->element('head'); ?>
<body id="error" data-offset="80" data-target=".navbar" data-spy="scroll">
	<!--        <div id="page-preloader"><span class="spinner"></span></div>-->
	<!-- Navigation -->
	<!-- ++++++++++++++++++++++++++++++++++++++++ -->

	<?php echo $this->element('nav_bar',array('navClass'=>'white-navbar')); ?>
	<!-- Home - Header -->
	<!-- ++++++++++++++++++++++++++++++++++++++++ -->
	<?php
	echo $this->fetch('content');
	?>

	<!-- Footer - Tabs -->
	<!-- ++++++++++++++++++++++++++++++++++++++++ -->
	<footer>
		<?php echo $this->element('main_footer'); ?>
	</footer>
	<?php
	echo $this->Html->script(array('jquery-1.11.1.min', 'jquery-ui', 'bootstrap.min'
		, 'isotope.pkgd.min', 'owl.carousel.min', 'jquery.scrollTo.min', 'jquery.ui.effect.min', 'contact',
		'jquery.mb.YTPlayer', 'modernizr', 'jquery.waypoints.min', '/js/fancybox/jquery.fancybox', 'wow.min'));
		?>
		<script>
			new WOW().init();
			(function ($) {
				$(document).ready(function () {
					if (!Modernizr.touch) {
						$(".player").mb_YTPlayer();
					} else {
						$('.video-bg ').css('display', 'block');
					}
				});
			})(jQuery);
		</script>
		<script>
			new WOW().init();
		</script>
		<!-- Google maps -->
		<?php echo $this->Html->script(array('jquery.easypiechart.min', 'TweenMax.min', 'highcharts', 'Chart.min', 'jquery.tweetscroll', 'custom'));
		?>
	</body>
	</html>
</body>
</html>
