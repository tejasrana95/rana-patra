<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts.Email.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="format-detection" content="telephone=no" /> <!-- disable auto telephone linking in iOS -->
		<title>Rana Community</title>
		<style type="text/css">
            html,body{
                margin: 0;
                padding: 0;
            }
            .wel_text{
                padding-top:15px; padding-bottom:15px; padding-right:15px; padding-left:15px;
                font-family: arial; font-size: 16px; line-height: 26px;
            }
            .wel_text h1{
                font-weight: bold; font-size: 28px; margin: 0; margin-bottom: 10px; padding: 0; line-height: normal;  text-transform: uppercase;
            }
        </style>   
    </head>
	<body bgcolor="#E1E1E1" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
	<center style="background-color:#E1E1E1;">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"  style="table-layout: fixed; max-width:600px !important;width: 100% !important;background-color:#fff; ">
				<tr>
					<td align="center" valign="top" style="border-bottom:5px solid #0d873a; padding-top:15px; padding-bottom:15px;" >
                   <img src="<?php echo $site_url; ?>public/mailer/logo.png" alt="<?php echo $site_name; ?>" />   
                    </td>
                </tr>
                <?php echo $this->fetch('content'); ?>
                <tr>
					<td align="center" valign="top" style="background-color:#f28c29; color:#fff;" >
                   <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                       <tr>
                           <td align="left" valign="top" style="border-bottom:1px solid #f5a354;  padding-top:10px; padding-bottom:10px; padding-right:15px; padding-left:15px; font-weight: bold; font-size: 24px; font-family: arial;  text-transform: uppercase; " >Benifits</td>
                       </tr>
                       <tr>
                           <td align="center" valign="top" >
                               <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                               <tr>
                                   <td align="left" valign="top" style="width:200px; padding-top:10px;" ><img src="<?php echo $site_url; ?>public/mailer/benefitimg.png" alt="" />   </td>
                                   <td align="left" valign="top" style="padding-top:25px; padding-bottom:10px; padding-left:20px; padding-right:20px;" >
                                      
                                       <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                                           <tr>
                                               <td align="left" valign="top" style="padding-top:5px;"><img src="<?php echo $site_url; ?>public/mailer/arrow.png" alt="" /></td>
                                               <td align="left" valign="top" style="margin:0; padding:0; padding-bottom:20px; padding-left:10px; font-family:arial; font-size:16px; color:#fff;">Dedicated to our community only.</td>
                                           </tr>
                                           <tr>
                                               <td align="left" valign="top" style="padding-top:5px;"><img src="<?php echo $site_url; ?>public/mailer/arrow.png" alt="" /></td>
                                               <td align="left" valign="top" style="margin:0; padding:0; padding-bottom:20px; padding-left:10px; font-family:arial; font-size:16px; color:#fff;">24x7 Support. Reliable and full privacy.</td>
                                           </tr>
                                           <tr>
                                               <td align="left" valign="top" style="padding-top:5px;"><img src="<?php echo $site_url; ?>public/mailer/arrow.png" alt="" /></td>
                                               <td align="left" valign="top" style="margin:0; padding:0; padding-bottom:20px; padding-left:10px; font-family:arial; font-size:16px; color:#fff;">Operated by our own people.</td>
                                           </tr>
                                       </table>
                                       
                                       
                                   </td>
                               </tr>
                               </table>
                           </td>
                       </tr>
                   </table>
                   
                    </td>
                </tr>
                <tr>
					<td align="center" valign="top" style="height:20px; font-size:0;" >   
                    </td>
                </tr>
            <tr>
					<td align="center" valign="top" style="background-color:#008ed0; color:#fff;" >
                   <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                       <tr>
                           <td align="left" valign="top" style="border-bottom:1px solid #33a5d9;  padding-top:10px; padding-bottom:10px; padding-right:15px; padding-left:15px; font-weight: bold; font-size: 24px; font-family: arial;  text-transform: uppercase; " >Features & Gifts</td>
                       </tr>
                       <tr>
                           <td align="center" valign="top" >
                               <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                               <tr>
                                   <td align="left" valign="top" style="width:200px; padding-top:10px;" ><img src="<?php echo $site_url; ?>public/mailer/giftimg.png" alt="" />   </td>
                                   <td align="left" valign="top" style="padding-top:25px; padding-bottom:10px; padding-left:20px; padding-right:20px;" >
                                      
                                       <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                                           <tr>
                                               <td align="left" valign="top" style="padding-top:5px;"><img src="<?php echo $site_url; ?>public/mailer/arrow.png" alt="" /></td>
                                               <td align="left" valign="top" style="margin:0; padding:0; padding-bottom:20px; padding-left:10px; font-family:arial; font-size:16px; color:#fff;">Matrimonials, Events & News.</td>
                                           </tr>
                                           <tr>
                                               <td align="left" valign="top" style="padding-top:5px;"><img src="<?php echo $site_url; ?>public/mailer/arrow.png" alt="" /></td>
                                               <td align="left" valign="top" style="margin:0; padding:0; padding-bottom:20px; padding-left:10px; font-family:arial; font-size:16px; color:#fff;">Competition for our community and win gifts.</td>
                                           </tr>
                                           <tr>
                                               <td align="left" valign="top" style="padding-top:5px;"><img src="<?php echo $site_url; ?>public/mailer/arrow.png" alt="" /></td>
                                               <td align="left" valign="top" style="margin:0; padding:0; padding-bottom:20px; padding-left:10px; font-family:arial; font-size:16px; color:#fff;">Donation for cause, achievement recognize.</td>
                                           </tr>
                                       </table>
                                       
                                       
                                   </td>
                               </tr>
                               </table>
                           </td>
                       </tr>
                   </table>
                   
                    </td>
                </tr>
                <tr>
					<td align="center" valign="top" style="height:20px; font-size:0;">   
                    </td>
                </tr>
                <tr>
					<td align="center" valign="top" >   
                   <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                       <tr>
                           <td align="left" valign="top" style=" background-color:#f28b46; width:50%;  padding-top:20px; padding-bottom:20px; padding-left:20px; padding-right:20px; color:#fcebe0; font-size:16px; font-family:arial;" >
                               <h3 style="color:#fff; font-size:22px; font-weight:bold; margin:0; padding:0;">Trouble?</h3>
                               <label style="font-weight:bold; margin-top:20px; display:block;">Customer Care:</label> 
                                <a href="mailto:<?php echo $options['contact_us_id']; ?>" style="color:#fcebe0; text-decoration:none;"><?php echo $options['contact_us_id']; ?></a>
                                <label style="font-weight:bold; margin-top:20px; display:block;">Call us:</label>
                                +91 9636732568
                           </td>
                           <td align="left" valign="top" style=" background-color:#008ed0; width:50%;  padding-top:20px; padding-bottom:20px; padding-left:20px; padding-right:20px; color:#ddebf6; font-size:16px; font-family:arial;" >
                               <h3 style="color:#fff; font-size:22px; font-weight:bold; margin:0; padding:0;">Contact us</h3>
                               <label style="font-weight:bold; margin-top:20px; display:block;">Sales Team:</label> 
                               <a href="mailto:<?php echo $options['sales_id']; ?>" style="color:#ddebf6; text-decoration:none;"><?php echo $options['sales_id']; ?></a>
                                <label style="font-weight:bold; margin-top:20px; display:block;">Call us:</label>
                                +91 9530107998
                           </td>
                       </tr>
                        </table>
                    </td>
                </tr>
                <tr>
					<td align="center" valign="top" style=" background-color:#454545;   padding-top:15px; padding-bottom:0px; padding-left:0px; padding-right:0px; color:#acacac; font-size:16px; font-family:arial;">
                   Follow Us:
                    </td>
                </tr>
                <tr>
					<td align="center" valign="top" style=" background-color:#454545;   padding-top:5px; padding-bottom:5px; padding-left:0px; padding-right:0px; ">
                       <a href="<?php echo $options['facebook_link']; ?>" style="display:inline-block; padding-right:3px; padding-left:3px;" target="_blank"><img src="<?php echo $site_url; ?>public/mailer/socico1.png" alt="Facebook" /></a>
                       <a href="<?php echo $options['twitter_link']; ?>" style="display:inline-block; padding-right:3px; padding-left:3px;" target="_blank"><img src="<?php echo $site_url; ?>public/mailer/socico2.png" alt="Twitter" /></a>
                       <a href="<?php echo $options['linkedin_link']; ?>" style="display:inline-block; padding-right:3px; padding-left:3px;" target="_blank"><img src="<?php echo $site_url; ?>public/mailer/socico3.png" alt="LinkedIn" /></a>
                       <a href="mailto:<?php echo $options['contact_us_id']; ?>" style="display:inline-block; padding-right:3px; padding-left:3px;" target="_blank"><img src="<?php echo $site_url; ?>public/mailer/socico4.png" alt="E-Mail" /></a>   
                    </td>
                </tr>
            <table>
    </center>     
	</body>
</html>
