<!DOCTYPE html>
<html lang="en">
<?php echo $this->element('head'); ?>
<body id="<?php echo $page_slug; ?>" data-offset="80" data-target=".navbar" data-spy="scroll">
    <!--        <div id="page-preloader"><span class="spinner"></span></div>-->
    <!-- Navigation -->
    <!-- ++++++++++++++++++++++++++++++++++++++++ -->

    <?php echo $this->element('nav_bar',$navbarClass); ?>
    <!-- Home - Header -->
    <!-- ++++++++++++++++++++++++++++++++++++++++ -->
    <?php
    echo $this->fetch('content');
    ?>
    <!-- Footer - Tabs -->
    <!-- ++++++++++++++++++++++++++++++++++++++++ -->
    <footer>
        <?php echo $this->element('main_footer'); ?>
    </footer>
    <!-- ++++++++++++++++++++++++++++++++++++++++ -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <?php echo $this->Html->script(array('jquery-1.11.1.min', 'jquery-ui', 'bootstrap.min'
        , 'isotope.pkgd.min', 'owl.carousel.min', 'jquery.scrollTo.min', 'jquery.ui.effect.min',
         'modernizr', 'jquery.waypoints.min','/js/fancybox/jquery.fancybox','jquery.tweetscroll', 'jquery.flexslider','imageCrop/croppie.min', 'imageCrop/script','moment.min','bootstrap-datetimepicker.min','jquery.auto-complete.min'));
        echo $this->fetch('scriptBottom');
        echo $this->fetch('script');
        echo $this->Html->script(array( 'custom'));
        echo $this->fetch('scriptBlock');
        ?>
</body>
</html>
