<?php
App::import('Controller', 'Common');
$commonController = new CommonController();
?>
<!DOCTYPE html>
<html lang="en">
<?php echo $this->element('head'); ?>
<body id="common_layout" data-offset="80" data-target=".navbar" data-spy="scroll">
    <!--        <div id="page-preloader"><span class="spinner"></span></div>-->
    <!-- Navigation -->
    <!-- ++++++++++++++++++++++++++++++++++++++++ -->

    <?php echo $this->element('nav_bar',array('navClass'=>'white-navbar')); ?>
    <!-- Home - Header -->
    <!-- ++++++++++++++++++++++++++++++++++++++++ -->
    <?php if(isset($title) && !empty($title)): ?>
    <section class="no-header-image">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-left wow fadeIn animated" data-wow-duration="2s">
                    <h1 class="title"><?php echo $title; ?></h1>
                    <div class="path"><?php echo $commonController->breadcrumb($breadcrumb); ?></div>
                </div>
            </div>
        </div>
    </section>
    <?php else: ?>
        <section class="no-header-image">&nbsp;</section>
    <?php endif; ?>
    <div class="container">
        <div class="row">
            <?php
            echo $this->fetch('content');
            ?>
        </div>
    </div>
    <!-- Footer - Tabs -->
    <!-- ++++++++++++++++++++++++++++++++++++++++ -->
    <footer>
        <?php echo $this->element('main_footer'); ?>
    </footer>
    <!-- ++++++++++++++++++++++++++++++++++++++++ -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <?php
    echo $this->Html->script(array('jquery-1.11.1.min', 'jquery-ui', 'bootstrap.min'
        , 'isotope.pkgd.min', 'owl.carousel.min', 'jquery.scrollTo.min', 'jquery.ui.effect.min', 'contact',
        'jquery.mb.YTPlayer', 'modernizr', 'jquery.waypoints.min', '/js/fancybox/jquery.fancybox', 'wow.min'));
        ?>
        <script>
            new WOW().init();
            (function ($) {
                $(document).ready(function () {
                    if (!Modernizr.touch) {
                        $(".player").mb_YTPlayer();
                    } else {
                        $('.video-bg ').css('display', 'block');
                    }
                });
            })(jQuery);
        </script>
        <script>
            new WOW().init();
        </script>
        <!-- Google maps -->
         <?php echo $this->Html->script(array('jquery.easypiechart.min', 'TweenMax.min', 'highcharts', 'Chart.min', 'jquery.tweetscroll'));
        echo $this->fetch('scriptBottom');
        echo $this->fetch('script');
        echo $this->Html->script(array( 'custom'));
        ?>
</body>
</html>
