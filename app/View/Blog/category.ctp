<?php
App::import('Controller', 'Common');
$CommonController = new CommonController();
?>
<div class="categories wow fadeInUp animated" data-wow-duration="2s" style="">
	<h5><i class="fa fa-align-left"></i>Categories</h5>
	<ul>
		<?php foreach($categories as $category): ?>
			<li class="clearfix"><a href="<?php echo $CommonController->urlGenrator($category['identifier'],'ArticleCategory') ?>"><?php echo $category['label'] ?></a>
				<?php if(isset($category['child'])): ?>
					<ul>
						<?php foreach($category['child'] as $child): ?>
							<li class="clearfix"><a href="<?php echo $CommonController->urlGenrator($child['identifier'],'ArticleCategory') ?>"><?php echo $child['label'] ?></a></li>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			</li>
		<?php endforeach; ?>
	</ul>
</div>      