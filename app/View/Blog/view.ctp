<?php
App::import('Controller', 'Common');
$CommonController = new CommonController();
?>

<?php
if (isset($blog['Article']['banner']) && !empty($blog['Article']['banner'])):
    ?>
<style>.about-header {
    position: relative;
    height: 700px;
    padding-top: 300px;
    text-align: center;
    background: url(<?php echo $blog['Article']['banner']; ?>) center no-repeat;
    background-size: cover;
}body{margin-top:0px}</style>
<section class="about-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12 wow fadeInUp" data-wow-duration="2s" >
                <h1 class="title"><?php echo $blog['Article']['article_title']; ?></h1>
                <div class="path"><?php echo $CommonController->breadcrumb($breadcrumb); ?></div>
            </div>
            <div class="bottom-line"></div>
        </div>
    </div>
</section>
<?php endif; ?>

<section class="wrap-blog-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 left-column post-column">
                <div class="col-md-12 wow fadeInUp animated" data-wow-duration="2s">
                    <div class="post">
                        <div class="post-body">
                            <ul class="list-inline">
                                <li><i class="fa fa-user"></i> <?php echo ucfirst($CommonController->getUserName($blog['Article']['userID'],$blog['Article']['isAdmin'])) ?></li>
                                <li><i class="fa fa-calendar"></i> <?php echo date('d M Y',strtotime($blog['Article']['created'])) ?></li>
                            </ul>
                            <h5><?php echo $blog['Article']['article_name'] ?></h5>
                            <?php echo ($blog['Article']['content']); ?>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="col-lg-3 col-lg-offset-1 col-md-3 col-md-offset-1 sidebar">
                <div class="form-group search has-feedback wow fadeInUp animated" data-wow-duration="2s" style="">
                    <form action="/blog"><input class="form-control" name="term" placeholder="Search"><i class="fa fa-search fa-lg form-control-feedback"></i></form>
                </div>


                <?php 
                echo $this->requestAction(array(
                    'controller' => 'blog',
                    'action' => 'categories',
                    ), array('return'));

                echo $this->requestAction(array(
                    'controller' => 'blog',
                    'action' => 'latestBlog',
                    ), array('return'));
                    ?>
                  
                </div>
            </div>
        </div>
    </section>



