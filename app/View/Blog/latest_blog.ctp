<?php
App::import('Controller', 'Common');
$CommonController = new CommonController();
?>
<?php 
if(isset($allArticles) && !empty($allArticles)):
    ?>
<div class="popular-posts wow fadeInUp animated" data-wow-duration="2s" style="">
    <h5><i class="fa fa-flash"></i>Lastest posts</h5>
    <ul class="clearfix">
     <?php foreach($allArticles as $article): ?>
    <li>
        <a href="<?php echo $CommonController->urlGenrator($article['Article']['article_slug'],'Article') ?>" class="title"><?php echo $article['Article']['article_title'] ?></a>
        <ul class="list-inline clearfix">
           <li><i class="fa fa-user"></i> <?php echo ucfirst($CommonController->getUserName($article['Article']['userID'],$article['Article']['isAdmin'])) ?></li>
           <li><i class="fa fa-calendar"></i> <?php echo date('d M Y',strtotime($article['Article']['created'])) ?></li>
       </ul>
   </li>
<?php endforeach; ?>
</ul>
</div>
<?php endif; ?>