<?php
App::import('Controller', 'Common');
$CommonController = new CommonController();
?>
<div class="row wrap-blog-content blog">
    <div class="col-lg-8 col-md-8 left-column">
        <?php
        $i = 1;
        if(!empty($blogs)):
            foreach ($blogs as $blog):
                if (isset($blog['Article']['banner']) && !empty($blog['Article']['banner'])) {
                    $banner = $blog['Article']['banner'];
                } else {
                    $banner = $site_url . '/images/placehold/360x230.gif';
                }
                ?>       
                <div class="col-md-6 wow fadeInUp animated" data-wow-duration="2s">
                    <div class="post">
                        <div class="post-heading">
                            <a class="post-image" href="<?php echo $CommonController->urlGenrator($blog['Article']['article_slug'],'Article') ?>">
                                <img src="<?php echo $banner; ?>" alt="<?php echo $blog['Article']['article_title'] ?>"  title="<?php echo $blog['Article']['article_title'] ?>" class="img-responsive"/>
                            </a>
                        </div>
                        <div class="post-body">
                            <a class="post-avatar" href="#"><img class="" src="<?php echo ($CommonController->getUserProfile($blog['Article']['userID'],$blog['Article']['isAdmin'])) ?>" alt="<?php echo ucfirst($CommonController->getUserName($blog['Article']['userID'],$blog['Article']['isAdmin'])) ?>"></a>
                            <ul class="list-inline">
                                <li><i class="fa fa-user"></i> <?php echo ucfirst($CommonController->getUserName($blog['Article']['userID'],$blog['Article']['isAdmin'])) ?></li>
                                <li><i class="fa fa-calendar"></i> <?php echo date('d M Y',strtotime($blog['Article']['created'])) ?></li>
                            </ul>
                            <a href="<?php echo $CommonController->urlGenrator($blog['Article']['article_slug'],'Article') ?>" title="<?php echo $blog['Article']['article_title'] ?>"><h5><?php echo $blog['Article']['article_name'] ?></h5></a>
                            <?php echo $CommonController->truncate($blog['Article']['content'], 250, '...'); ?>
                        </div>
                    </div>
                </div>
                <?php
                if ($i == 2) {
                    echo '<div class="clear"></div>';
                    $i = 0;
                }
                $i++;
                ?>
            <?php endforeach;

            else:
               ?>
<div class="col-md-12 wow fadeInUp animated" data-wow-duration="2s">
                   <div class="center"><h5>No Result Found.</h5></div>
                </div>

       <?php endif; ?>
       <div class="clear"></div>
       <div class="row">
        <div class="container">
            <div class="col s12 m12 l12">

                <div class="paging">
                    <ul class="pagination pagination-sm">
                        <?php
                        echo $this->Paginator->prev(
                            ' << ', array(
                                'tag' => 'li',
                                'class' => 'prev',
                                'escape' => false,
                                ), null, array(
                                'tag' => 'li',
                                'escape' => false,
                                'class' => 'prev disabled',
                                'disabledTag' => 'a',
                                ));
                                ?>
                                <?php
                                echo $this->Paginator->numbers(array(
                                    'separator' => '',
                                    'currentClass' => 'active',
                                    'currentTag' => 'a',
                                    'tag' => 'li',
                                    ));
                                    ?>
                                    <?php
                                    echo $this->Paginator->next('>>', array(
                                        'tag' => 'li',
                                        'class' => 'next',
                                        'escape' => false,
                                        ), null, array(
                                        'tag' => 'li',
                                        'escape' => false,
                                        'class' => 'next disabled',
                                        'disabledTag' => 'a',
                                        ));
                                        ?>
                                    </ul>
                                </div>
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}'),
                                    ));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-lg-offset-1 col-md-3 col-md-offset-1 sidebar">
                        <div class="form-group search has-feedback wow fadeInUp animated" data-wow-duration="2s" style="">
                            <form action="/blog"><input class="form-control" name="term" placeholder="Search"><i class="fa fa-search fa-lg form-control-feedback"></i></form>
                        </div>

                        <?php 
                        echo $this->requestAction(array(
                            'controller' => 'blog',
                            'action' => 'categories',
                            ), array('return'));

                        echo $this->requestAction(array(
                            'controller' => 'blog',
                            'action' => 'latestBlog',
                            ), array('return'));
                            ?>
                        </div>
                    </div>


