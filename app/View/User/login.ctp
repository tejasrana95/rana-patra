<?php
App::import('Controller', 'Widgets');
$WidgetsController = new WidgetsController;
?>
<div class="no-header-image login-overlay container">
  <div class='col-md-8'>
    <div class="benifits-of-register">  
      <?php echo $WidgetsController->renderWidget('[[WIDGET][benefits-of-being-registered]]'); ?>
    </div>
  </div>
  <div class="col-md-4">
    <div class="header">
      <h4 class="center" id="">Login</h4>
    </div>
    <?php echo $this->Session->flash(); ?>
    <?php echo $this->Flash->render('auth'); ?>
    <div class="loginform">
      <div class="row">
        <div class="col-xs-12">
          <div class="">

            <div class="social-login-button">
              <a onclick="window.open('<?php echo $facebookUrl; ?>', '_blank', 'location=yes,height=370,width=620,scrollbars=yes,status=yes')" class="btn btn-block btn-social btn-facebook">
                <span class="fa fa-facebook"></span> Sign in with Facebook
              </a>
               <a onclick="window.open('<?php echo $googleUrl; ?>', '_blank', 'location=yes,height=370,width=620,scrollbars=yes,status=yes')" class="btn btn-block btn-social btn-google">
                <span class="fa fa-google"></span> Sign in with Google
              </a>
            </div>
            <div class="clearfix">&nbsp;</div>
            <?php echo $this->Form->create('Customer'); ?>
            
            <?php
            echo $this->Form->input('email', array('div' => array('class' => "form-group"), 'class' => "form-control", 'empty' => '','placeholder'=>'Email id eg. abc@gmail.com','title'=>'Please enter your email id'));

             echo $this->Form->input('password', array('div' => array('class' => "form-group"), 'class' => "form-control", 'empty' => '','placeholder'=>'Upper, lower case & numeric required','title'=>'Please enter Enter your password with lower, upper case and number'));
             echo $this->Form->input('Login', array('label' => false, 'div' => array('class' => "form-group"), 'type' => "submit", 'class' => "btn btn-success btn-block"));
             ?>
             <?php echo $this->Form->end(); ?>
          </div>
          <?php echo $this->Html->link(
            'Not a registered member? Create new account. It\'s free.',
            '/login/register',
            array('class' => 'link-block', 'target' => '')
            );
          echo $this->Html->link(
            'Can\'t remember password? Recover password now.',
            '/login/reset',
            array('class' => 'link-block', 'target' => '')
            ); 
             ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>