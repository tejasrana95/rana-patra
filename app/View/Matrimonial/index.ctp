<?php
App::import('Controller', 'Common');
$CommonController = new CommonController;
?>
<div class="no-header-image login-overlay container">
<?php echo $this->element('breadcrumb'); ?> 
    <div class=''>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <h4>Matrimonials</h4>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right">
                            <a href="javascript:void(1);" class="btn btn-primary btn-sm" onclick="toggleSearch();">Search</a>
                        </div>
                    </div>
                    <div class="row display_none" id="toggleSearch">
                    <?php echo $this->Form->create('MatrimonialSearch', array('type' => 'get','url'=>'/matrimonial')); ?>
                        <div class="col-md-6 col-sm-12">
                            <?php  echo $this->Form->input('name', array('div' => array('class' => "form-group"), 'class' => "form-control")); ?>
                         </div>
                         <div class="col-md-6 col-sm-12">
                            <?php  echo $this->Form->input('profileID', array('div' => array('class' => "form-group"), 'class' => "form-control")); ?>
                         </div>
                         <div class="col-md-3 col-sm-6">
                            <?php
                                echo $this->Form->input('bride_groom', array('options' => $brideGroom,'empty' => "Select Bride/Groom",'div' => array('class' => "form-group"),'class' => "form-control", 'label' => 'Bride(Male)/Groom(Female)'));
                            ?>
                         </div>
                         <div class="col-md-3 col-sm-6">
                            <?php
                                echo $this->Form->input('marital_status', array('options' => $marital_status,'empty' => "Select marital status",'div' => array('class' => "form-group"),'class' => "form-control"));
                            ?>
                         </div>
                         <div class="col-md-3 col-sm-6">
                            <?php
                                echo $this->Form->input('manglik', array('options' => $yesNo, 'empty' => "Select Manglik",'div' => array('class' => "form-group"),'class' => "form-control"));
                            ?>
                         </div>
                         <div class="col-md-3 col-sm-6">
                            <?php
                               echo $this->Form->input('rashi', array('options' => $rashi,'empty' => "Select Rashi/ Zodiac Sign",'div' => array('class' => "form-group"),'class' => "form-control"));
                            ?>
                         </div>
                         <div class="col-md-3 col-sm-6">
                            <?php
                                 echo $this->Form->input('bornYearFrom', array('div' => array('class' => "form-group"), 'class' => "form-control","label"=>"Born Year From","id"=>"yearFrom","readonly"=>"readonly"));
                            ?>
                         </div>
                         <div class="col-md-3 col-sm-6">
                            <?php
                                 echo $this->Form->input('bornYearTo', array('div' => array('class' => "form-group"), 'class' => "form-control","label"=>"Born Year To","id"=>"yearTo","readonly"=>"readonly"));
                            ?>
                         </div>
                         <div class="col-md-3 col-sm-6">
                         &nbsp;
                         </div>
                         <div class="col-md-3 col-sm-6 text-right">
                         <label>&nbsp;</label><br/>
                            <?php
                                  echo $this->Form->button('<i class="fa fa-search"></i> Search', array('label' => false, 'div' => array('class' => ""), 'type' => "submit", 'class' => " btn btn-primary",'escape'=>false));
                            ?>
                         </div>
                    <?php echo $this->Form->end(); ?>                       
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">
                        <?php
                        if (isset($this->request->data['matrimonials']) && count($this->request->data['matrimonials'])>0) {
                            foreach ($this->request->data['matrimonials'] as $matrimonial) {
                                if (isset($matrimonial['MatrimonialAttachment']) && !empty($matrimonial['MatrimonialAttachment'])) {
                                    $option = $CommonController->getOptionValue();
                                    $path   = $option['matrimonialImagesURL'] . '/' . $matrimonial['MatrimonialAttachment'][0]['file_path'];
                                    $path   = $site_url . 'public/' . $CommonController->imageProcessor($path, 300, 300);
                                } else {
                                    $path = $site_url . 'public/default/noavatar.png';
                                }
                        ?>
                            <div class="col-md-3 col-sm-12">
                                <div class="matrimonial-box">
                                <div class="mat-pic">
                                    <?php
                                        echo $this->Html->link(
                                        $this->Html->image($path, array('alt' => $matrimonial['Matrimonial']['first_name'] . ' ' . $matrimonial['Matrimonial']['last_name'])), array('controller' => 'matrimonial', 'action' => strtolower($matrimonial['Matrimonial']['first_name'] . '-' . $matrimonial['Matrimonial']['last_name']) . '/' . $matrimonial['Matrimonial']['id']), array('escapeTitle' => false, 'title' => $matrimonial['Matrimonial']['first_name'] . ' ' . $matrimonial['Matrimonial']['last_name']));
                                    ?>
                                </div>
                                <div class="mini-details">
                                    <div class="mat-name text-center">
                                        <?php
                                        echo $this->Html->link($matrimonial['Matrimonial']['first_name'] . ' ' . $matrimonial['Matrimonial']['last_name'], array('controller' => 'matrimonial', 'action' => strtolower($matrimonial['Matrimonial']['first_name'] . '-' . $matrimonial['Matrimonial']['last_name']) . '/' . $matrimonial['Matrimonial']['id']), array('escapeTitle' => true, 'title' => $matrimonial['Matrimonial']['first_name'] . ' ' . $matrimonial['Matrimonial']['last_name'])
                                            );
        ?>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-6">Age:</div>
                                        <div class="col-md-9 col-sm-6 col-xs-6"><?php echo (date('Y') - date('Y', strtotime($matrimonial['Matrimonial']['dob']))); ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-6">Rashi:</div>
                                        <div class="col-md-9 col-sm-6 col-xs-6"><?php echo $matrimonial['Matrimonial']['rashi']; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-6">Update:</div>
                                        <div class="col-md-9 col-sm-6 col-xs-6"><?php echo date('d-m-Y', strtotime($matrimonial['Matrimonial']['modified'])); ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-6">Status:</div>
                                        <div class="col-md-9 col-sm-6 col-xs-6"><?php echo ($matrimonial['Matrimonial']['status'] == 1) ? '<span class="text-success">Active</span>' : '<span class="text-warning">Inactive</span>'; ?></div>
                                    </div>
                                    <div class="row text-center">
                                        <?php
                                        echo $this->Html->link('<i class="fa fa-eye"></i> View', array('controller' => 'matrimonial', 'action' => strtolower($matrimonial['Matrimonial']['first_name'] . '-' . $matrimonial['Matrimonial']['last_name']) . '/' . $matrimonial['Matrimonial']['id']), array('class' => 'btn btn-default btn-sm', 'escape' => false));
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         } ?>
                <?php                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 }else {  ?>
                    <div class="col-md-12 col-sm-12">
                        <div class="alert alert-warning" role="alert">
                            Sorry, we are unable to find matching matrimonial with this requirements.
                        </div>
                    </div>
                <?php }?>
            </div>
            <?php if (isset($this->request->data['matrimonials']) && count($this->request->data['matrimonials'])>0) {  ?>
            <div class="row">
                <div class="col-md-12 col-sm-12 text-right">
                    <div class="paging">
                        <ul class="pagination pagination-sm">
                            <?php
                            echo $this->Paginator->prev(
                                    ' << ', array(
                                'tag' => 'li',
                                'class' => 'prev',
                                'escape' => false,
                                    ), null, array(
                                'tag' => 'li',
                                'escape' => false,
                                'class' => 'prev disabled',
                                'disabledTag' => 'a',
                            ));
                            ?>
                            <?php
                            echo $this->Paginator->numbers(array(
                                'separator' => '',
                                'currentClass' => 'active',
                                'currentTag' => 'a',
                                'tag' => 'li',
                                'modulus'=>2
                            ));
                            ?>
                            <?php
                            echo $this->Paginator->next('>>', array(
                                'tag' => 'li',
                                'class' => 'next',
                                'escape' => false,
                                    ), null, array(
                                'tag' => 'li',
                                'escape' => false,
                                'class' => 'next disabled',
                                'disabledTag' => 'a',
                            ));
                            ?>
                        </ul>
                    </div>
                    <?php
                    echo $this->Paginator->counter(array(
                        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}'),
                    ));
                    ?>
                </div>
            </div>
            <?php } ?>
        </div>
               
    </div>
</div>
</div>
<div class="clearfix">&nbsp;</div>
</div>
<div class="clearfix"></div>
