<?php
App::import('Controller', 'Common');
$CommonController = new CommonController;
?> 
<?php  
$this->Html->css(array('file-upload/dropzone'), null, array('inline' => false));
$this->Html->script(array('file-upload/dropzone','face-detect/jquery.facedetection.min'), array('block' => 'scriptBottom'));
echo $this->Html->scriptBlock(
    'loadImage();
    loadKundali();',
    array('inline' => false,'block' => 'scriptBlock')
);
echo $this->Html->scriptBlock(
    "var mid=". $this->request->data['Matrimonial']['id']."",
    array('inline' => false,'block' => 'scriptBottom')
);
?>
<div class="no-header-image login-overlay container">
<?php echo $this->element('breadcrumb'); ?> 
    <div class=''>
        <div class="col-md-12">  
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h4>Create Matrimonial</h4>
                    <div class="row">
                        <div class="col-md-10">

                            <div class="stepwizard">
                                <div class="stepwizard-row setup-panel">
                                    <div class="stepwizard-step">
                                        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                                        <p>Basic Details</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <a href="#step-2" type="button" class="btn btn-default btn-circle" >2</a>
                                        <p>Family Details</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <a href="#step-3" type="button" class="btn btn-default btn-circle" >3</a>
                                        <p>Work &amp; Education</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <a href="#step-4" type="button" class="btn btn-default btn-circle"  >4</a>
                                        <p>Horoscope Details</p>
                                    </div>  
                                    <div class="stepwizard-step">
                                        <a href="#step-5" type="button" class="btn btn-default btn-circle"  >5</a>
                                        <p>Image &amp; Kundali</p>
                                    </div>  
                                </div>
                            </div>
                            <?php echo $this->Session->flash(); ?>
                            <?php echo $this->Form->create('Matrimonial', array('type' => 'file','onsubmit'=>'return validateMatrimonial();')); ?>
                            <?php echo $this->Form->input('id'); ?>
                                <div class="row setup-content" id="step-1">
                                    <div class="col-md-12">
                                        <h6>Basic Details</h6>
                                        <?php
                                        echo $this->Form->input('bride_groom', array('options' => $brideGroom,
                                            'empty' => "Select Bride/Groom",
                                            'div' => array('class' => "form-group"),
                                            'class' => "form-control", 'required' => 'required', 'label' => 'Bride(Male)/Groom(Female)'));

                                        echo $this->Form->input('marital_status', array('options' => $marital_status,
                                            'empty' => "Select marital status",
                                            'div' => array('class' => "form-group"),
                                            'class' => "form-control", 'required' => 'required'));
                                        echo $this->Form->input('first_name', array('div' => array('class' => "form-group"), 'class' => "form-control", 'required' => 'required'));
                                        echo $this->Form->input('last_name', array('div' => array('class' => "form-group"), 'class' => "form-control", 'required' => 'required'));
                                        echo $this->Form->input('contact', array('div' => array('class' => "form-group"), 'class' => "form-control", 'required' => 'required'));
                                        echo $this->Form->input('email', array('div' => array('class' => "form-group"), 'class' => "form-control", 'required' => 'required'));
                                        echo $this->Form->input('address', array('div' => array('class' => "form-group"), 'type' => 'textarea', 'rows' => 2, 'class' => "form-control", 'required' => 'required'));
                                        echo $this->Form->input('pin_code', array('div' => array('class' => "form-group"), 'class' => "form-control", 'required' => 'required'));
                                        echo $this->Form->input('nationality', array('div' => array('class' => "form-group"), 'class' => "form-control", 'required' => 'required'));
                                        echo $this->Form->input('placefinder', array('type' =>'text','div' => array('class' => ""), 'class' => "form-control",'empty' => 'City','title'=>'Type your city name','label'=>'City','id'=>'placefinder')); 
                                        ?>
                                        <div class="display_none">
                                          <?php  echo $this->Form->input('country', array('type' =>'text' ,'id'=>'country',)); 
                                           echo $this->Form->input('state', array('type' =>'text','id'=>'state')); 
                                            echo $this->Form->input('city', array('type' =>'text','id'=>'city',)); 
                                            echo $this->Form->input('place_id', array('type' =>'text','id'=>'place_id',));
                                          ?>
                                          </div>
                                          <?php
                                        echo $this->Form->input('bio', array('div' => array('class' => "form-group"), 'type' => 'textarea', 'rows' => 2, 'class' => "form-control", 'label' => 'About my self'));
                                        ?>
                                        <?php
                                            echo $this->Form->input('Save & Exit', array('label' => false, 'div' => array('class' => ""), 'type' => "submit", 'class' => "btn btn-primary pull-left"));
                                        ?>
                                        <button class="btn btn-primary nextBtn pull-right" type="button" >Next</button>
                                        
                                    </div>
                                </div>
                                <div class="row setup-content" id="step-2">
                                    <div class="col-md-12">
                                        <h6>Family Details</h6>
                                        <?php
                                        echo $this->Form->input('father_name', array('div' => array('class' => "form-group"), 'class' => "form-control", 'required' => 'required'));
                                        echo $this->Form->input('mother_name', array('div' => array('class' => "form-group"), 'class' => "form-control", 'required' => 'required'));
                                        echo $this->Form->input('father_occupation', array('div' => array('class' => "form-group"), 'class' => "form-control", 'required' => 'required'));
                                        echo $this->Form->input('mother_occupation', array('div' => array('class' => "form-group"), 'class' => "form-control", 'required' => 'required'));
                                        $sisterBrother = array();
                                        for ($i = 0; $i <= 10; $i++) {
                                            $sisterBrother[$i] = $i;
                                        }
                                        echo $this->Form->input('brothers', array('options' => $sisterBrother,
                                            'empty' => "Select brothers",
                                            'div' => array('class' => "form-group"),
                                            'class' => "form-control",
                                            'required' => 'required'));
                                        echo $this->Form->input('sisters', array('options' => $sisterBrother,
                                            'empty' => "Select sisters",
                                            'div' => array('class' => "form-group"),
                                            'class' => "form-control",
                                            'required' => 'required'));
                                        ?>
                                        <?php
                                        echo $this->Form->input('Save & Exit', array('label' => false, 'div' => array('class' => ""), 'type' => "submit", 'class' => "btn btn-primary pull-left"));
                                    ?>
                                    <button class="btn btn-primary nextBtn pull-right" type="button" >Next</button>
                                    </div>
                                </div>
                                <div class="row setup-content" id="step-3">
                                    <div class="col-md-12">
                                        <h6>Work &amp; Education</h6>
                                        <?php
                                        echo $this->Form->input('education', array('options' => $education,
                                            'empty' => "Select education",
                                            'div' => array('class' => "form-group"),
                                            'class' => "form-control",
                                            'required' => 'required'));
                                        echo $this->Form->input('qualification', array('div' => array('class' => "form-group"), 'class' => "form-control", 'required' => 'required'));
                                        echo $this->Form->input('employment', array('options' => $employement,
                                            'empty' => "Select employment",
                                            'div' => array('class' => "form-group"),
                                            'class' => "form-control",
                                            'required' => 'required'));
                                        echo $this->Form->input('salary', array('options' => $salary,
                                            'empty' => "Select salary",
                                            'div' => array('class' => "form-group"),
                                            'class' => "form-control",
                                            'required' => 'required'));
                                        ?>
                                         
                                         <?php
                                         echo $this->Form->input('Save & Exit', array('label' => false, 'div' => array('class' => ""), 'type' => "submit", 'class' => "btn btn-primary pull-left"));
                                     ?>
                                     <button class="btn btn-primary nextBtn pull-right" type="button" >Next</button>
                                    </div>
                                </div>
                                <div class="row setup-content" id="step-4">
                                    <div class="col-md-12">
                                        <h6>Horoscope Details</h6>
                                        <?php
                                        echo $this->Form->input('dob', array('div' => array('class' => "form-group"), 'type' => "text", 'label' => "Date of Birth", 'class' => "form-control date",'required' => 'required','id'=>'dob'));
                                        echo $this->Form->input('tob', array('div' => array('class' => "form-group"), 'type' => "text", 'label' => "Time of Birth (hh:mm:ss) (24 hrs)", 'class' => "form-control timepicker",'required' => 'required' ,'readonly'=>'readonly','id'=>'tob'));
                                        echo $this->Form->input('pob', array('div' => array('class' => "form-group"), 'class' => "form-control",'required' => 'required','label'=>"Place of Birth",'id'=>'pob'));
                                        echo $this->Form->input('mother_tongue', array('div' => array('class' => "form-group"), 'class' => "form-control",'required' => 'required'));
                                        echo $this->Form->input('drink', array('options' => $drink_smoke,
                                            'empty' => "Do you drink?",
                                            'div' => array('class' => "form-group"),
                                            'class' => "form-control",
                                            'required' => 'required'));
                                        echo $this->Form->input('smoke', array('options' => $drink_smoke,
                                            'empty' => "Do you smoke?",
                                            'div' => array('class' => "form-group"),
                                            'class' => "form-control",
                                            'required' => 'required'));
                                        echo $this->Form->input('complexion', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                        echo $this->Form->input('diet', array('options' => $diet,
                                            'empty' => "Select your diet",
                                            'div' => array('class' => "form-group"),
                                            'class' => "form-control",
                                            'required' => 'required'));
                                        echo $this->Form->input('height', array('div' => array('class' => "form-group"), 'class' => "form-control",'required' => 'required'));
                                        echo $this->Form->input('weight', array('div' => array('class' => "form-group"), 'class' => "form-control",'required' => 'required'));
                                        echo $this->Form->input('gautra', array('div' => array('class' => "form-group"), 'class' => "form-control",'required' => 'required'));
                                        echo $this->Form->input('nakshtra', array('div' => array('class' => "form-group"), 'class' => "form-control",'required' => 'required'));
                                        echo $this->Form->input('rashi', array('options' => $rashi,
                                            'empty' => "Select Rashi/ Zodiac Sign",
                                            'div' => array('class' => "form-group"),
                                            'class' => "form-control",
                                            'required' => 'required'));
                                        echo $this->Form->input('manglik', array('options' => $yesNo,
                                            'empty' => "Select Manglik",
                                            'div' => array('class' => "form-group"),
                                            'class' => "form-control",
                                            'required' => 'required'));
                                        echo $this->Form->input('blood_group', array('options' => $bloodGroup,
                                            'empty' => "Select blood group",
                                            'div' => array('class' => "form-group"),
                                            'class' => "form-control",
                                            'required' => 'required'));
                                        echo $this->Form->input('native_place', array('div' => array('class' => "form-group"), 'class' => "form-control",'required' => 'required'));
                                        echo $this->Form->input('ready_to_go_abroad', array('options' => $yesNo,
                                            'empty' => "Ready to go abroad?",
                                            'div' => array('class' => "form-group"),
                                            'class' => "form-control",
                                            'required' => 'required'));
                                        echo $this->Form->input('special_remarks', array('div' => array('class' => "form-group"), 'class' => "form-control",'type'=>'textarea'));
                                        ?>
                                        <?php
                                         echo $this->Form->input('Save & Exit', array('label' => false, 'div' => array('class' => ""), 'type' => "submit", 'class' => "btn btn-primary pull-left"));
                                        ?>
                                     <button class="btn btn-primary nextBtn pull-right" type="button" >Next</button>
                                    </div>
                                </div>
                                <?php echo $this->Form->end(); ?>
                                
                                <div class="row setup-content" id="step-5">
                                    <div class="col-md-12">
                                    <div class="heading">Images:</div>
                                        <br/>
                                        <div class="removePhoto" >
                                            <div id="multipleFace"></div>
                                            <div id="noFace"></div>
                                        </div>
                                        <div class="images-block row" id="image-block">
                                        </div>
                                        <div class="file_upload">
                                            <?php echo $this->Form->create('MatrimonialAttachment', array('type' => 'file','url' => 'uploadImages','class'=>'dropzone')); ?>
                                            <div class="display_none">
                                            <?php 
                                            echo $this->Form->input('matrimonial_id',array('value'=>$this->request->data['Matrimonial']['id'],'type'=>'text'));?>
                                            <?php echo $this->Form->input('file_type',array('value'=>'image'));?>
                                            </div>
                                            
                                                <div class="dz-message needsclick">
                                                <div><i class="fa fa-cloud-upload"></i></div>
                                                    <strong>Drop image here or click to upload.</strong><br />
                                                    <span class="note needsclick">Only images with JPG,PNG or GIF allowed.</span>
                                                </div>
                                            <?php echo $this->Form->end(); ?>
                                        </div>		
                                    </div>
                                    
                                    <div class="col-md-12">
                                    <br/><br/>
                                        <div class="heading">Kundali:</div>
                                        <br/>
                                        <div class="kundali-block row" id="kundali-block"></div>
                                        <div class="file_upload">
                                            <?php echo $this->Form->create('MatrimonialAttachment', array('type' => 'file','url' => 'uploadImages','class'=>'dropzone')); ?>
                                            <div class="display_none">
                                            <?php 
                                            echo $this->Form->input('matrimonial_id',array('value'=>$this->request->data['Matrimonial']['id'],'type'=>'text'));?>
                                            <?php echo $this->Form->input('file_type',array('value'=>'kundali'));?>
                                            </div>
                                            
                                                <div class="dz-message needsclick">
                                                <div><i class="fa fa-cloud-upload"></i></div>
                                                    <strong>Drop kundali scanned copy or click to upload.</strong><br />
                                                    <span class="note needsclick">Only images with JPG,PNG or GIF allowed.</span>
                                                </div>
                                            <?php echo $this->Form->end(); ?>
                                        </div>		
                                    </div>
                                </div>
                              
                                

                        </div>
                        <div class="col-md-2 ">
                            Promote your matrimonial in one click.
                        </div>
                        <div class="clearfix"> </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>  
</div>
<div class="clearfix"></div>
