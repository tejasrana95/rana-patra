<?php
App::import('Controller', 'Common');
$CommonController = new CommonController;
if(isset($attachmentData) && !empty($attachmentData)):
$option = $CommonController->getOptionValue();
//debug($attachmentData);
foreach ($attachmentData as $attachment):
    $attachment=$attachment['MatrimonialAttachment'];
    $url=$option['matrimonialImagesURL'];
        if($attachment['file_type']=='kundali'){
            $url=$option['matrimonialKundaliURL'];
        }
        $path = $site_url . 'public/' .$CommonController->imageProcessor($url.'/'.$attachment['file_path'], 300, 300);
        ?>
       <div class="col-md-3 col-sm-6 faceDetectClass" id="file-slug-<?php echo $attachment['id']; ?>">
           <a href="javascript:void(0);" id="delete-link-photo-<?php echo $attachment['id']; ?>" class="deleteFile" onclick="deleteFile(<?php echo $attachment['matrimonial_id']; ?>,<?php echo $attachment['id']; ?>);" title="Delete this file">X</a>
            <a href="<?php echo  $site_url . 'public' .$url.'/'.  $attachment['file_path']; ?>" class="fancybox" data-fancybox="<?php echo $attachment['file_type']; ?>" title="<?php echo $attachment['file_name']; ?>" data-parant-class="faceDetect-<?php echo $attachment['id']; ?>" data-mat-id="<?php echo $attachment['matrimonial_id']; ?>" data-image-id="<?php echo $attachment['id']; ?>">
                <div id="image-<?php echo $attachment['id']; ?>">
                    <div class="display_none removedPicture" title="This image has been removed by system due to multiple faces or no face."><i class="fa fa-ban"></i></div>
                    <img src="<?php echo $path; ?>" />
                </div>
            </a>
        </div>
    <?php 
    endforeach;
endif; ?>
<div class="display_none faceDetectWrapper" id="faceDetectWrapper">
</div>