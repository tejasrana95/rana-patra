<?php
App::import('Controller', 'Common');
$CommonController = new CommonController;
?> 
<div class="no-header-image login-overlay container">
<?php echo $this->element('breadcrumb'); ?> 
    <div class=''>
        <div class="col-md-12">  
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <h4>My Matrimonials</h4>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right">
                        <?php
                        echo $this->Html->link('Add New Matrimonial', array('controller' => 'matrimonial', 'action' => 'create'), array('escapeTitle' => true, 'title' => 'Create New matrimonial', 'class' => 'btn btn-primary btn-sm', 'escape' => false));
                        ?>
                    </div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">
                        <?php 
                        if(isset($this->request->data['matrimonials']) && !empty($this->request->data['matrimonials'])){ 
                        foreach ($this->request->data['matrimonials'] as $matrimonial): ?>
                            <?php
                            if (isset($matrimonial['MatrimonialAttachment']) && !empty($matrimonial['MatrimonialAttachment'])) {
                                $option = $CommonController->getOptionValue();
                                $path = $option['matrimonialImagesURL'].'/'.$matrimonial['MatrimonialAttachment'][0]['file_path'];
                                $path = $site_url . 'public/' . $CommonController->imageProcessor($path, 300, 300);
                            } else {
                                $path = $site_url . 'public/default/noavatar.png';
                            }
                            ?>
                            <div class="col-md-3 col-sm-12  ">
                                <div class="matrimonial-box">
                                <div class="mat-pic">
                                    <?php
                                    echo $this->Html->link(
                                            $this->Html->image($path, array('alt' => $matrimonial['Matrimonial']['first_name'] . ' ' . $matrimonial['Matrimonial']['last_name'])), array('controller' => 'matrimonial', 'action' => strtolower($matrimonial['Matrimonial']['first_name'] . '-' . $matrimonial['Matrimonial']['last_name']) . '/' . $matrimonial['Matrimonial']['id']), array('escapeTitle' => false,'title' => $matrimonial['Matrimonial']['first_name'] . ' ' . $matrimonial['Matrimonial']['last_name'])
                                    );
                                    ?>
                                </div>
                                <div class="mini-details">
                                    <div class="mat-name text-center">
                                        <?php
                                    echo $this->Html->link($matrimonial['Matrimonial']['first_name'] . ' ' . $matrimonial['Matrimonial']['last_name'], array('controller' => 'matrimonial', 'action' => strtolower($matrimonial['Matrimonial']['first_name'] . '-' . $matrimonial['Matrimonial']['last_name']) . '/' . $matrimonial['Matrimonial']['id']), array('escapeTitle' => true,'title' => $matrimonial['Matrimonial']['first_name'] . ' ' . $matrimonial['Matrimonial']['last_name'])
                                    );
                                    ?>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-6">Age:</div>
                                        <div class="col-md-9 col-sm-6 col-xs-6"><?php echo (date('Y') - date('Y', strtotime($matrimonial['Matrimonial']['dob']))); ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-6">Rashi:</div>
                                        <div class="col-md-9 col-sm-6 col-xs-6"><?php echo $matrimonial['Matrimonial']['rashi']; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-6">Update:</div>
                                        <div class="col-md-9 col-sm-6 col-xs-6"><?php echo date('d-m-Y', strtotime($matrimonial['Matrimonial']['modified'])); ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-6">Status:</div>
                                        <div class="col-md-9 col-sm-6 col-xs-6"><?php echo ($matrimonial['Matrimonial']['status'] == 1) ? '<span class="text-success">Active</span>' : '<span class="text-warning">Inactive</span>'; ?></div>
                                    </div>
                                    <div class="row text-center">
    <?php
    echo $this->Html->link('<i class="fa fa-eye"></i> View', array('controller' => 'matrimonial', 'action' => strtolower($matrimonial['Matrimonial']['first_name'] . '-' . $matrimonial['Matrimonial']['last_name']) . '/' . $matrimonial['Matrimonial']['id']), array('class' => 'btn btn-default btn-sm', 'escape' => false));
    echo '&nbsp;';
    echo $this->Html->link('<i class="fa fa-pencil"></i> Edit', array('controller' => 'matrimonial', 'action' => 'edit/' . $matrimonial['Matrimonial']['id']), array('class' => 'btn btn-primary btn-sm', 'escape' => false));
    ?>
                                    </div>
                                </div>
                            </div>
                                </div>
<?php endforeach; 
}else{  ?>
<div class="col-md-12"><div id="flashMessage" class="alert alert-danger">You do not have any matrimoanial profile. Please create it now <?php   echo $this->Html->link('Click here',  array('controller' => 'matrimonial', 'action' => 'create'), array('class' => '','escape' => false)); ?>.</div></div>
<?php } ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>  
</div>
<div class="clearfix"></div>