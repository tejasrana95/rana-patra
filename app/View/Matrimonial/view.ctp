<?php
App::import('Controller', 'Common');
$CommonController = new CommonController;
$option = $CommonController->getOptionValue();
?> 
<div class="no-header-image login-overlay container">
<?php echo $this->element('breadcrumb'); ?> 
    <div class=''>
        <div class="col-md-12">  
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h4>View Profile</h4>
                    <div class="row">
                        <div class="profile">
                            <div class="col-md-8 profile_left">
                                <h5>Profile Id : <?php echo $this->request->data['Matrimonial']['profileID'] ?></h5>
                                <div class="">
                                    <div class="col-sm-4 row">
                                        <div class="flexslider">
                                            <div class="flex-viewport" >
                                                <ul class="slides">
                                                    <?php
                                                    $imageExists = $CommonController->valueExistsInArray($this->request->data['MatrimonialAttachment'], array('file_type' => 'image'));
                                                    if (isset($imageExists) && $imageExists == false) {
                                                        $this->request->data['MatrimonialAttachment'][0] = array("file_path" => 'noavatar.jpg', 'file_type' => 'image', 'file_name' => "No Image Uploaded by User");
                                                    }
                                                    foreach ($this->request->data['MatrimonialAttachment'] as $attachment):
                                                        if ($attachment['file_type'] == "image") {
                                                            $path = $site_url . 'public/' .$CommonController->imageProcessor($option['matrimonialImagesURL'].'/'.$attachment['file_path'], 300, 300);
                                                            ?>
                                                            <li data-thumb="<?php echo $path; ?>">
                                                                <a href="<?php echo  $site_url . 'public' .$option['matrimonialImagesURL'].'/'.  $attachment['file_path'] ?>" class="fancybox" data-fancybox="group" title="<?php echo $attachment['file_name'] ?>">
                                                                    <img src="<?php echo $path; ?>" />
                                                                </a>
                                                            </li>
                                                        <?php } endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="col-sm-8 row_1">
                                        <table class="table_working_hours">
                                            <tbody>
                                                <tr class="opened_1">
                                                    <td class="day_label">Age :</td>
                                                    <td class="day_value"><?php echo (date('Y') - date('Y', strtotime($this->request->data['Matrimonial']['dob']))); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label">Height :</td>
                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['height']; ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label">Weight :</td>
                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['weight']; ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label">Marital Status :</td>
                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['marital_status']; ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label">Location :</td>
                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['placefinder']; ?></td>
                                                </tr>
                                                <tr class="closed">
                                                    <td class="day_label">Profile Created by :</td>
                                                    <td class="day_value closed"><span><?php echo $CommonController->getUserName($this->request->data['Matrimonial']['userID']); ?></span></td>
                                                </tr>
                                                <tr class="closed">
                                                    <td class="day_label">Education :</td>
                                                    <td class="day_value closed"><span><?php echo $this->request->data['Matrimonial']['education']; ?></span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                                
                                <div class="mat-details">
                                    <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                                        <ul id="myTab" class="nav nav-tabs nav-tabs1" role="tablist">
                                            <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">About Myself</a></li>
                                            <li role="presentation"><a href="#family" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">Family Details</a></li>
                                            <?php
                                            $imageExists = $CommonController->valueExistsInArray($this->request->data['MatrimonialAttachment'], array('file_type' => 'kundali'));
                                            if (isset($imageExists) && $imageExists == true) {
                                                ?>
                                                <li role="presentation"><a href="#kundali" role="tab" id="profile-tab1" data-toggle="tab" aria-controls="profile1">Kundali</a></li>
                                            <?php } ?>
                                             <li role="presentation"><a href="#contact" role="tab" id="profile-tab1" data-toggle="tab" aria-controls="profile1">Contact</a></li>
                                        </ul>
                                        <div id="myTabContent" class="tab-content ">
                                            <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
                                                <div class="tab_box">
                                                    <?php echo $this->request->data['Matrimonial']['bio']; ?>
                                                </div>
                                                <div class="basic_1">
                                                    <h5>Basics &amp; Lifestyle</h5>
                                                    <div class="col-md-6 basic_1-left row">
                                                        <table class="table_working_hours">
                                                            <tbody>
                                                                <tr class="opened_1">
                                                                    <td class="day_label">Name :</td>
                                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['first_name'] . ' ' . $this->request->data['Matrimonial']['last_name']; ?></td>
                                                                </tr>
                                                                <tr class="opened">
                                                                    <td class="day_label">Marital Status :</td>
                                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['marital_status'] ?></td>
                                                                </tr>
                                                                <tr class="opened">
                                                                    <td class="day_label">Height :</td>
                                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['height']; ?></td>
                                                                </tr>
                                                                <tr class="opened">
                                                                    <td class="day_label">Weight :</td>
                                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['weight']; ?></td>
                                                                </tr>
                                                                <tr class="closed">
                                                                    <td class="day_label">Profile Created by :</td>
                                                                    <td class="day_value closed"><span><?php echo $CommonController->getUserName($this->request->data['Matrimonial']['userID']); ?></span></td>
                                                                </tr>
                                                                <tr class="opened">
                                                                    <td class="day_label">Drink :</td>
                                                                    <td class="day_value closed"><span><?php echo $this->request->data['Matrimonial']['drink']; ?></span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-6 basic_1-left">
                                                        <table class="table_working_hours">
                                                            <tbody>
                                                                <tr class="opened_1">
                                                                    <td class="day_label">Age :</td>
                                                                    <td class="day_value"><?php echo (date('Y') - date('Y', strtotime($this->request->data['Matrimonial']['dob']))); ?> Yrs</td>
                                                                </tr>
                                                                <tr class="opened">
                                                                    <td class="day_label">Mother Tongue :</td>
                                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['mother_tongue']; ?></td>
                                                                </tr>
                                                                <tr class="opened">
                                                                    <td class="day_label">Complexion :</td>
                                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['complexion']; ?></td>
                                                                </tr>
                                                                <tr class="opened">
                                                                    <td class="day_label">Blood Group :</td>
                                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['blood_group']; ?></td>
                                                                </tr>
                                                                <tr class="closed">
                                                                    <td class="day_label">Diet :</td>
                                                                    <td class="day_value closed"><span><?php echo $this->request->data['Matrimonial']['diet']; ?></span></td>
                                                                </tr>
                                                                <tr class="closed">
                                                                    <td class="day_label">Smoke :</td>
                                                                    <td class="day_value closed"><span><?php echo $this->request->data['Matrimonial']['smoke']; ?></span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="clearfix"> </div>
                                                </div>
                                                <div class="clearfix">&nbsp;</div>
                                                <div class="basic_1">
                                                    <h5>Religious / Social &amp; Astro Background</h5>
                                                    <div class="col-md-6 basic_1-left row">
                                                        <table class="table_working_hours">
                                                            <tbody>
                                                                <tr class="opened">
                                                                    <td class="day_label">Time of Birth :</td>
                                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['tob']; ?></td>
                                                                </tr>
                                                                <tr class="opened">
                                                                    <td class="day_label">Gautra :</td>
                                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['gautra']; ?></td>
                                                                </tr>
                                                                <tr class="opened">
                                                                    <td class="day_label">Date of Birth :</td>
                                                                    <td class="day_value closed"><span><?php echo date('d-m-Y', strtotime($this->request->data['Matrimonial']['dob'])); ?></span></td>
                                                                </tr>
                                                                <tr class="opened">
                                                                    <td class="day_label">Place of Birth :</td>
                                                                    <td class="day_value closed"><span><?php echo $this->request->data['Matrimonial']['pob']; ?></span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-6 basic_1-left ">
                                                        <table class="table_working_hours">
                                                            <tbody>
                                                                <tr class="opened_1">
                                                                    <td class="day_label">Nakshtra :</td>
                                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['nakshtra']; ?></td>
                                                                </tr>
                                                                <tr class="opened">
                                                                    <td class="day_label">Manglik :</td>
                                                                    <td class="day_value"><?php echo ($this->request->data['Matrimonial']['manglik'] == 1) ? "Yes" : "No"; ?></td>
                                                                </tr>
                                                                <tr class="opened">
                                                                    <td class="day_label">Raasi :</td>
                                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['rashi']; ?></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="clearfix"> </div>
                                                </div>
                                                <div class="clearfix">&nbsp;</div>
                                                <div class="basic_1 basic_2 ">
                                                    <h5>Education &amp; Career</h5>
                                                    <div class="basic_1-left">
                                                        <table class="table_working_hours">
                                                            <tbody>
                                                                <tr class="opened">
                                                                    <td class="day_label">Education :</td>
                                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['education']; ?></td>
                                                                </tr>
                                                                <tr class="opened">
                                                                    <td class="day_label">Education Detail :</td>
                                                                    <td class="day_value"><?php echo $this->request->data['Matrimonial']['qualification']; ?></td>
                                                                </tr>
                                                                <tr class="opened">
                                                                    <td class="day_label">Occupation :</td>
                                                                    <td class="day_value closed"><?php echo $employement[$this->request->data['Matrimonial']['employment']]; ?></td>
                                                                </tr>
                                                                <tr class="opened">
                                                                    <td class="day_label">Annual Income :</td>
                                                                    <td class="day_value closed"><?php echo $this->request->data['Matrimonial']['salary']; ?></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="clearfix"> </div>
                                                </div>
                                                <?php if (trim($this->request->data['Matrimonial']['special_remarks']) && !empty(trim($this->request->data['Matrimonial']['special_remarks']))): ?>
                                                    <div class="clearfix">&nbsp;</div>
                                                    <div class="basic_1 basic_2 ">
                                                        <h5>Special Remark</h5>
                                                        <?php echo $this->request->data['Matrimonial']['special_remarks']; ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="family" aria-labelledby="profile-tab">
                                                <div class="basic_3">
                                                    <div class="basic_1 basic_2">
                                                        <h5>Family Details</h5>
                                                        <div class="col-md-6 basic_1-left row">
                                                            <table class="table_working_hours">
                                                                <tbody>
                                                                    <tr class="opened">
                                                                        <td class="day_label">Father's Name :</td>
                                                                        <td class="day_value"><?php echo $this->request->data['Matrimonial']['father_name']; ?></td>
                                                                    </tr>
                                                                    <tr class="opened">
                                                                        <td class="day_label">Father's Occupation :</td>
                                                                        <td class="day_value"><?php echo $this->request->data['Matrimonial']['father_occupation']; ?></td>
                                                                    </tr>
                                                                    <tr class="opened">
                                                                        <td class="day_label">No. of Brother :</td>
                                                                        <td class="day_value closed"><?php echo $this->request->data['Matrimonial']['brothers']; ?></td>
                                                                    </tr>
                                                                    <tr class="opened">
                                                                        <td class="day_label">Native Place :</td>
                                                                        <td class="day_value closed"><span><?php echo $this->request->data['Matrimonial']['native_place']; ?></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="col-md-6 basic_1-left ">
                                                            <table class="table_working_hours">
                                                                <tbody>
                                                                    <tr class="opened_1">
                                                                        <td class="day_label">Mother's Name :</td>
                                                                        <td class="day_value"><?php echo $this->request->data['Matrimonial']['mother_name']; ?></td>
                                                                    </tr>
                                                                    <tr class="opened">
                                                                        <td class="day_label">Mother's Occupation :</td>
                                                                        <td class="day_value"><?php echo ($this->request->data['Matrimonial']['mother_occupation'] == 1) ? "Yes" : "No"; ?></td>
                                                                    </tr>
                                                                    <tr class="opened">
                                                                        <td class="day_label">No. of Sister :</td>
                                                                        <td class="day_value"><?php echo $this->request->data['Matrimonial']['sisters']; ?></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="kundali" aria-labelledby="profile-tab1">
                                                <div class="basic_1 basic_2">
                                                    <div class="basic_1-left">
                                                        <div class="row kundali">
                                                            <?php
                                                            foreach ($this->request->data['MatrimonialAttachment'] as $attachment):
                                                                if ($attachment['file_type'] == "kundali") {
                                                                    $path = $site_url . 'public/' . $CommonController->imageProcessor($option['matrimonialKundaliURL'].'/'.$attachment['file_path'], 300, 300);
                                                                    ?>
                                                                    <div class="col-md-3 col-sm-12">
                                                                        <a href="<?php echo $site_url . 'public/' .$option['matrimonialKundaliURL'].'/'. $attachment['file_path'] ?>" class="fancybox" title="<?php echo $attachment['file_name'] ?>">
                                                                            <img src="<?php echo $path; ?>" />
                                                                        </a>
                                                                    </div>
                                                                <?php } endforeach; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="contact" aria-labelledby="profile-tab">
                                                <div class="basic_3">
                                                    <div class="basic_1 basic_2">
                                                        <h5>Contact Details</h5>
                                                        <div class="col-md-12 basic_1-left row">
                                                            <?php if($showContactTab==false){  ?>
                                                                <div class="alert alert-danger" role="alert">
                                                                    <?php echo _('Due to privacy we can not show you the contact details. Please subscribe the premium membership to get full access & access contact information.'); ?>
                                                                </div>
                                                            <?php }else{  ?>
                                                            <table class="table_working_hours">
                                                                <tbody>
                                                                    <tr class="opened">
                                                                        <td class="day_label">Contact :</td>
                                                                        <td class="day_value"><?php echo $this->request->data['Matrimonial']['contact']; ?></td>
                                                                    </tr>
                                                                    <tr class="opened">
                                                                        <td class="day_label">Email :</td>
                                                                        <td class="day_value"><a href="mailto:<?php echo $this->request->data['Matrimonial']['email']; ?>"><?php echo $this->request->data['Matrimonial']['email']; ?></td>
                                                                    </tr>
                                                                    <tr class="opened">
                                                                        <td class="day_label">Address :</td>
                                                                        <td class="day_value closed"><?php echo $this->request->data['Matrimonial']['address']; ?></td>
                                                                    </tr>
                                                                    <tr class="opened">
                                                                        <td class="day_label">Native Place :</td>
                                                                        <td class="day_value closed"><span><?php echo $this->request->data['Matrimonial']['native_place']; ?></span></td>
                                                                    </tr>
                                                                     <tr class="opened">
                                                                        <td class="day_label">Pin Code :</td>
                                                                        <td class="day_value closed"><span><?php echo $this->request->data['Matrimonial']['pin_code']; ?></span></td>
                                                                    </tr>
                                                                    <tr class="opened">
                                                                        <td class="day_label">Location :</td>
                                                                        <td class="day_value closed"><span><?php echo $this->request->data['Matrimonial']['placefinder']; ?></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 profile_right">
                                <div class="newsletter">
                                     <?php echo $this->Form->create('MatrimonialSearch', array('type' => 'get','url'=>'/matrimonial')); ?>
                                     <?php echo $this->Form->input('profileID', array('div' =>false, 'class' => "","required"=>"required","placeholder"=>"Enter Profile ID :","label"=>false)); ?>
                                        <input value="Go" type="submit">
                                    <?php echo $this->Form->end(); ?>     
                                </div>
                                <div class="view_profile similar-profile">
                                    <h5>View Similar Profiles</h5>
                                    <?php
                                    foreach ($this->request->data['similarProfile'] as $similarProfile):
                                        if (!isset($similarProfile['MatrimonialAttachment'][0])) {
                                            $similarProfile['MatrimonialAttachment'][0] = array("file_path" => 'default/noavatar.jpg', 'file_type' => 'image', 'file_name' => "No Image Uploaded by User");
                                             $path = $site_url . 'public/' . $CommonController->imageProcessor('default/noavatar.jpg', 300, 300);
                                        }else{
                                             $path = $site_url . 'public/' . $CommonController->imageProcessor($option['matrimonialImagesURL'].'/'.$similarProfile['MatrimonialAttachment'][0]['file_path'], 300, 300);
                                        }
                                        ?>
                                        <ul class="profile_item">
                                            <li class="profile_item-img">
                                            <?php
                                            echo $this->Html->link(
                                                    $this->Html->image($path, array('alt' => $similarProfile['Matrimonial']['first_name'] . ' ' . $similarProfile['Matrimonial']['last_name'])), array('controller' => 'matrimonial', 'action' => strtolower($similarProfile['Matrimonial']['first_name'] . '-' . $similarProfile['Matrimonial']['last_name']) . '/' . $similarProfile['Matrimonial']['id']), array('escapeTitle' => false, 'title' => $similarProfile['Matrimonial']['first_name'] . ' ' . $similarProfile['Matrimonial']['last_name'])
                                            );
                                            ?>
                                            </li>
                                            <li class="profile_item-desc">
                                                <div class="mat-name"><?php
                                                    echo $this->Html->link($similarProfile['Matrimonial']['first_name'] . ' ' . $similarProfile['Matrimonial']['last_name'], array('controller' => 'matrimonial', 'action' => strtolower($similarProfile['Matrimonial']['first_name'] . '-' . $similarProfile['Matrimonial']['last_name']) . '/' . $similarProfile['Matrimonial']['id']), array('escapeTitle' => false, 'title' => $similarProfile['Matrimonial']['first_name'] . ' ' . $similarProfile['Matrimonial']['last_name'])
                                                    );
                                                    ?></div>
                                                <div class="mat-basiic-detail"><?php echo (date('Y') - date('Y', strtotime($similarProfile['Matrimonial']['dob']))); ?> Yrs, 
                                                    <?php echo $similarProfile['Matrimonial']['height']; ?>
                                                </div>
                                                <div class="mat-basiic-detail"><?php
                                                    echo $this->Html->link('<h6>View Full Profile</h6>', array('controller' => 'matrimonial', 'action' => strtolower($similarProfile['Matrimonial']['first_name'] . '-' . $similarProfile['Matrimonial']['last_name']) . '/' . $similarProfile['Matrimonial']['id']), array('escapeTitle' => false, 'title' => $similarProfile['Matrimonial']['first_name'] . ' ' . $similarProfile['Matrimonial']['last_name'])
                                                    );
                                                    ?></div>
                                                
                                            </li>
                                            <div class="clearfix"> </div>
                                        </ul>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>  
</div>
<div class="clearfix"></div>