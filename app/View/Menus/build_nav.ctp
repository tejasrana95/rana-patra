<?php
App::import('Controller', 'Pages');
$PagesController = new PagesController;
App::import('Controller', 'widgets');
$WidgetsController = new WidgetsController;

if ($menu_for == "top-menu") {
    foreach ($menu_headers as $menu_header) {
        $trgt = '';
        if ($menu_header['Menu']['content_type'] == "page") {
            $pg_obj = $this->requestAction(array('controller' => 'menus', 'action' => 'get_page_name', $menu_header['Menu']['content_name']));
            $pg_name = $pg_obj['Page']['page_name'];
            $url = Router::url('/', true) . $menu_header['Menu']['url'];
        } elseif ($menu_header['Menu']['content_type'] == "custom") {
            if (!empty(trim($menu_header['Menu']['icon']))) {
                $icon = '<i class="' . $menu_header['Menu']['icon'] . '"></i> ';
            } else {
                $icon = '';
            }
            $pg_name = $icon . $menu_header['Menu']['content_name'];
            if (filter_var($menu_header['Menu']['url'], FILTER_VALIDATE_URL)) {
                $url = $menu_header['Menu']['url'];
            } else {
                $url = Router::url('/', true) . $menu_header['Menu']['url'];
            }
        } else {
            $pg_name = $menu_header['Menu']['content_name'];
            $url = "javascript:void(0)";
        }
        echo '<li><a href="' . $url . '" ' . $trgt . ' title="' . $pg_name . '">' . $pg_name . '</a></li>'; //menu "li" tag

        if (array_key_exists($menu_header['Menu']['content_name'], $aux_array)) { // rendering children menu item
            foreach ($aux_array[$menu_header['Menu']['content_name']] as $a) {
                if ($a['Menu']['content_type'] == "page") {
                    $pg_obj = $this->requestAction(array('controller' => 'menus', 'action' => 'get_page_name', $a['Menu']['content_name']));
                    $pg_name = $pg_obj['Page']['page_name'];
                    $url = Router::url('/', true) . $a['Menu']['url'];
                } else {
                    $pg_name = $a['Menu']['content_name'];
                    $url = "javascript:void(0)";
                }
                echo '<li><a href="' . $url . '" title="' . $pg_name . '">' . $pg_name . '</a></li>'; //menu "li" tag

                if (array_key_exists($a['Menu']['content_name'], $aux_array)) { // rendering children menu item
                    foreach ($aux_array[$a['Menu']['content_name']] as $b) {
                        if ($b['Menu']['content_type'] == "page") {
                            $pg_obj = $this->requestAction(array('controller' => 'menus', 'action' => 'get_page_name', $b['Menu']['content_name']));
                            $pg_name = $pg_obj['Page']['page_name'];
                            $url = Router::url('/', true) . $b['Menu']['url'];
                        } else {
                            $pg_name = $b['Menu']['content_name'];
                            $url = "javascript:void(0)";
                        }
                        echo '<li><a href="' . $url . '" title="' . $pg_name . '">' . $pg_name . '</a></li>'; //menu "li" tag

                        if (array_key_exists($b['Menu']['content_name'], $aux_array)) { // rendering children menu item
                            foreach ($aux_array[$b['Menu']['content_name']] as $c) {
                                if ($c['Menu']['content_type'] == "page") {
                                    $pg_obj = $this->requestAction(array('controller' => 'menus', 'action' => 'get_page_name', $c['Menu']['content_name']));
                                    $pg_name = $pg_obj['Page']['page_name'];
                                    $url = Router::url('/', true) . $c['Menu']['url'];
                                } else {
                                    $pg_name = $c['Menu']['content_name'];
                                    $url = "javascript:void(0)";
                                }
                                echo '<li><a href="' . $url . '" title="' . $pg_name . '">' . $pg_name . '</a></li>'; //menu "li" tag

                                if (array_key_exists($c['Menu']['content_name'], $aux_array)) { // rendering children menu item
                                    foreach ($aux_array[$c['Menu']['content_name']] as $d) {
                                        if ($d['Menu']['content_type'] == "page") {
                                            $pg_obj = $this->requestAction(array('controller' => 'menus', 'action' => 'get_page_name', $d['Menu']['content_name']));
                                            $pg_name = $pg_obj['Page']['page_name'];
                                            $url = Router::url('/', true) . $d['Menu']['url'];
                                        } else {
                                            $pg_name = $d['Menu']['content_name'];
                                            $url = "javascript:void(0)";
                                        }
                                        echo '<li><a href="' . $url . '" title="' . $pg_name . '">' . $pg_name . '</a></li>'; //menu "li" tag
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
} else if ($menu_for == "header-menu") {
    ?>
    <div class="navbar-collapse collapse floated" id="navbar-collapse">

        <ul class="nav navbar-nav navbar-with-inside clearfix navbar-right with-border">
            <?php
            foreach ($menu_headers as $menu_header) {
                if($menu_header['Menu']['login_only']==1){
                    if(isset($userData) && !empty($userData)){
                    }else{ 
                        continue; 
                    }
                }
                if ($menu_header['Menu']['content_type'] == "custom") {

                    $items = $this->requestAction(array('controller' => 'menus', 'action' => 'list_menu_item', $menu_header['Menu']['content_name']));



                    if ($menu_header['Menu']['url'] != "#" && $menu_header['Menu']['url'] != "javascript:void()") {
                        $url = parse_url($menu_header['Menu']['url']);
                        if (isset($url['scheme']) && $url['scheme'] != 'https' && $url['scheme'] != 'http') {
                            $link = Router::url('/', true) . $menu_header['Menu']['url'];
                        } else {
                            $link = $menu_header['Menu']['url'];
                        }
                    } else {
                        $link = $menu_header['Menu']['url'];
                    }

                    if (!empty(trim($menu_header['Menu']['icon']))) {
                        $icon = '<i class="' . $menu_header['Menu']['icon'] . '"></i> ';
                    } else {
                        $icon = '';
                    }
                    if (isset($items) && !empty($items)) {
                        $data_toggle = "dropdown";
                    } else {
                        $data_toggle = "";
                    }
                    $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
                    if ($_SERVER["SERVER_PORT"] != "80") {
                        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
                    } else {
                        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
                    }
                    $url_parameter = explode(Router::url('/', true), $pageURL);
                    $url_parameters = explode('/', $url_parameter[count($url_parameter) - 1]);

                    $max_menu = 0;
                    App::import('Controller', 'category');
                    $categoryController = new CategoryController;
                    $main_category = $categoryController->getcategorydata($menu_header['Menu']['content_name']);
                    $sub_category = $categoryController->getsubcategories($menu_header['Menu']['content_name']);

                    if ($pageURL != Router::url('/', true)) {


                        if ($url_parameters[0] == @$main_category['Category']['identifier']) {
                            $actives = " active";
                        } elseif (strtolower(Inflector::slug($menu_header['Menu']['content_name'], '-')) == $url_parameters[0]) {
                            $actives = " active";
                        } elseif ($menu_header['Menu']['url'] == $url_parameters[0]) {
                            $actives = " active";
                        } else {
                            $actives = " ";
                        }
                    } else {
                        $actives = " ";
                    }
                    ?>
                    <li class=" "><a class=" js-activated <?php echo $actives; ?>"  data-toggle="<?php echo $data_toggle; ?>" href="<?php echo $link; ?>" title="<?php echo $menu_header['Menu']['content_name']; ?>"><?php echo $icon; ?><?php echo $menu_header['Menu']['content_name']; ?></a>
                        <?php if (isset($items) && !empty($items)) { ?>
                            <div class=" wrap-inside-nav">
                                <div class="inside-col">
                                    <ul class="inside-nav" >
                                        <?php
                                        foreach ($items as $item) {

                                            $child = $this->requestAction(array('controller' => 'menus', 'action' => 'list_menu_item', $item['Menu']['content_name']));
                                            if ($item['Menu']['content_type'] == "customHTML") {
                                                echo '<li>' . base64_decode($item['Menu']['url']) . '</li>';
                                            } elseif ($item['Menu']['content_type'] == "custom") {
                                                $url = explode('http', $item['Menu']['url']);
                                                if (filter_var($item['Menu']['url'], FILTER_VALIDATE_URL)) {
                                                    $link = $item['Menu']['url'];
                                                } else {
                                                    $link = Router::url('/', true) . $item['Menu']['url'];
                                                }
                                                ?>
                                                <li><a href="<?php echo $link; ?>" title="<?php echo $item['Menu']['content_name']; ?>"> <?php echo $item['Menu']['content_name']; ?></a>
                                                    <?php if (isset($child) && !empty($child)) { ?>
                                                    <div class=" wrap-inside-nav-child">
                                                        <div class="inside-col-child">
                                                            <ul class="inside-nav-child" >
                                                                <?php
                                                                foreach ($child as $item) {
                                                                    if ($item['Menu']['content_type'] == "customHTML") {
                                                                        echo '<li>' . base64_decode($item['Menu']['url']) . '</li>';
                                                                    } elseif ($item['Menu']['content_type'] == "custom") {
                                                                        $url = explode('http', $item['Menu']['url']);
                                                                        if (filter_var($item['Menu']['url'], FILTER_VALIDATE_URL)) {
                                                                            $link = $item['Menu']['url'];
                                                                        } else {
                                                                            $link = Router::url('/', true) . $item['Menu']['url'];
                                                                        }
                                                                        ?>
                                                                        <li><a href="<?php echo $link; ?>" title="<?php echo $item['Menu']['content_name']; ?>"> <?php echo $item['Menu']['content_name']; ?></a>

                                                                        </li>
                                                                        <?php
                                                                    } else {
                                                                        $pageitems = $this->requestAction(array('controller' => 'menus', 'action' => 'list_menu_item', $item['Menu']['content_type'], $item['Menu']['content_name']));
                                                                        $pagelink = "";
                                                                        $main_category = $categoryController->getcategorydata($pageitems['Page']['main_category']);


                                                                        $pagelink = Router::url('/', true) . '';

                                                                        if (isset($pageitems['Page']['category']) && !empty($pageitems['Page']['category'])) {
                                                                            $sub_category = $categoryController->getsubcategories($pageitems['Page']['category']);
                                                                            $pagelink .= $main_category['Category']['identifier'];
                                                                            $pagelink .= '/' . $sub_category[0]['Category']['identifier'];
                                                                        }
                                                                        if (!empty($pageitems['Page']['sub_category'])) {
                                                                            $child_category = $categoryController->getsubcategories($pageitems['Page']['sub_category']);
                                                                            $pagelink .= '/' . $child_category['Category']['identifier'];
                                                                        }
                                                                        $pagelink .= '' . $pageitems['Page']['page_slug'];
                                                                        ?>
                                                                        <li><a href="<?php echo $pagelink; ?>" title="<?php echo $pageitems['Page']['page_name']; ?>"><?php echo $pageitems['Page']['page_name']; ?></a>

                                                                        </li>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <?php }
                                                    ?>
                                                </li>
                                                <?php
                                            } else {
                                                $pageitems = $this->requestAction(array('controller' => 'menus', 'action' => 'list_menu_item', $item['Menu']['content_type'], $item['Menu']['content_name']));
                                                $pagelink = "";
                                                $main_category = $categoryController->getcategorydata($pageitems['Page']['main_category']);


                                                $pagelink = Router::url('/', true) . '';

                                                if (isset($pageitems['Page']['category']) && !empty($pageitems['Page']['category'])) {
                                                    $sub_category = $categoryController->getsubcategories($pageitems['Page']['category']);
                                                    $pagelink .= $main_category['Category']['identifier'];
                                                    $pagelink .= '/' . $sub_category[0]['Category']['identifier'];
                                                }
                                                if (!empty($pageitems['Page']['sub_category'])) {
                                                    $child_category = $categoryController->getsubcategories($pageitems['Page']['sub_category']);
                                                    $pagelink .= '/' . $child_category['Category']['identifier'];
                                                }
                                                $pagelink .= '' . $pageitems['Page']['page_slug'];
                                                ?>
                                                <li><a href="<?php echo $pagelink; ?>" title="<?php echo $pageitems['Page']['page_name']; ?>"><?php echo $pageitems['Page']['page_name']; ?></a>
                                                     <?php if (isset($child) && !empty($child)) { ?>
                                                    <div class=" wrap-inside-nav-child">
                                                        <div class="inside-col-child">
                                                            <ul class="inside-nav-child" >
                                                                <?php
                                                                foreach ($child as $item) {
                                                                    if ($item['Menu']['content_type'] == "customHTML") {
                                                                        echo '<li>' . base64_decode($item['Menu']['url']) . '</li>';
                                                                    } elseif ($item['Menu']['content_type'] == "custom") {
                                                                        $url = explode('http', $item['Menu']['url']);
                                                                        if (filter_var($item['Menu']['url'], FILTER_VALIDATE_URL)) {
                                                                            $link = $item['Menu']['url'];
                                                                        } else {
                                                                            $link = Router::url('/', true) . $item['Menu']['url'];
                                                                        }
                                                                        ?>
                                                                        <li><a href="<?php echo $link; ?>" title="<?php echo $item['Menu']['content_name']; ?>"> <?php echo $item['Menu']['content_name']; ?></a>

                                                                        </li>
                                                                        <?php
                                                                    } else {
                                                                        $pageitems = $this->requestAction(array('controller' => 'menus', 'action' => 'list_menu_item', $item['Menu']['content_type'], $item['Menu']['content_name']));
                                                                        $pagelink = "";
                                                                        $main_category = $categoryController->getcategorydata($pageitems['Page']['main_category']);


                                                                        $pagelink = Router::url('/', true) . '';

                                                                        if (isset($pageitems['Page']['category']) && !empty($pageitems['Page']['category'])) {
                                                                            $sub_category = $categoryController->getsubcategories($pageitems['Page']['category']);
                                                                            $pagelink .= $main_category['Category']['identifier'];
                                                                            $pagelink .= '/' . $sub_category[0]['Category']['identifier'];
                                                                        }
                                                                        if (!empty($pageitems['Page']['sub_category'])) {
                                                                            $child_category = $categoryController->getsubcategories($pageitems['Page']['sub_category']);
                                                                            $pagelink .= '/' . $child_category['Category']['identifier'];
                                                                        }
                                                                        $pagelink .= '' . $pageitems['Page']['page_slug'];
                                                                        ?>
                                                                        <li><a href="<?php echo $pagelink; ?>" title="<?php echo $pageitems['Page']['page_name']; ?>"><?php echo $pageitems['Page']['page_name']; ?></a>

                                                                        </li>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <?php }
                                                    ?>
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        <?php }
                        ?>
                    </li>
                    <?php
                } elseif ($menu_header['Menu']['content_type'] == "page") {
                    $pg_obj = $this->requestAction(array('controller' => 'menus', 'action' => 'get_page_name', $menu_header['Menu']['content_name']));

                    $items = $this->requestAction(array('controller' => 'menus', 'action' => 'list_menu_item', $menu_header['Menu']['content_name']));

                    if (isset($items) && !empty($items)) {
                        $data_toggle = "dropdown";
                    } else {
                        $data_toggle = "";
                    }
                    ?>
                    <li><a href="<?php echo Router::url('/', true) . $pg_obj['Page']['page_slug']; ?>" data-toggle="<?php echo $data_toggle; ?>" title="<?php echo $pg_obj['Page']['page_name']; ?>"> <?php echo $pg_obj['Page']['page_name']; ?></a>
                        <?php if (isset($items) && !empty($items)) { ?>
                            <div class=" wrap-inside-nav">
                                <div class="inside-col">
                                    <ul class="inside-nav" >
                                        <?php
                                        foreach ($items as $item) {

                                            if ($item['Menu']['content_type'] == "customHTML") {
                                                echo '<li>' . base64_decode($item['Menu']['url']) . '</li>';
                                            } elseif ($item['Menu']['content_type'] == "custom") {
                                                $url = explode('http', $item['Menu']['url']);
                                                if (filter_var($item['Menu']['url'], FILTER_VALIDATE_URL)) {
                                                    $link = $item['Menu']['url'];
                                                } else {
                                                    $link = Router::url('/', true) . $item['Menu']['url'];
                                                }
                                                ?>
                                                <li><a href="<?php echo $link; ?>" title="<?php echo $item['Menu']['content_name']; ?>"> <?php echo $item['Menu']['content_name']; ?></a>
                                                </li>
                                                <?php
                                            } elseif ($item['Menu']['content_type'] == "insight") {
                                                $insight_type = $InsightsController->insightcategorydata($item['Menu']['content_name']);
                                                echo '<li><a href="' . Router::url('/', true) . 'insights/' . $insight_type['identifier'] . '" title="' . $insight_type['label'] . '"> ' . $insight_type['label'] . '</a></li>';
                                            } else {
                                                $pageitems = $this->requestAction(array('controller' => 'menus', 'action' => 'list_menu_item', $item['Menu']['content_type'], $item['Menu']['content_name']));
                                                $pagelink = "";
                                                $main_category = $categoryController->getcategorydata($pageitems['Page']['main_category']);
                                                $pagelink = Router::url('/', true) . 'page/' . $main_category['Category']['identifier'];
                                                if (!empty($pageitems['Page']['category'])) {
                                                    $sub_category = $categoryController->getsubcategories($pageitems['Page']['category']);
                                                    $pagelink .= '/' . $sub_category['Category']['identifier'];
                                                }
                                                if (!empty($pageitems['Page']['sub_category'])) {
                                                    $child_category = $categoryController->getsubcategories($pageitems['Page']['sub_category']);
                                                    $pagelink .= '/' . $child_category['Category']['identifier'];
                                                }
                                                $pagelink .= '/' . $pageitems['Page']['page_slug'];
                                                ?>
                                                <li><a href="<?php echo $pagelink; ?>" title="<?php echo $pageitems['Page']['page_name']; ?>"><?php echo $pageitems['Page']['page_name']; ?></a></li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        <?php }
                        ?>
                    </li>
                    <?php
                } elseif ($menu_header['Menu']['content_type'] == "category") {
                    $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
                    if ($_SERVER["SERVER_PORT"] != "80") {
                        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
                    } else {
                        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
                    }
                    $url_parameter = explode(Router::url('/', true), $pageURL);
                    $url_parameters = explode('/', $url_parameter[count($url_parameter) - 1]);

                    $max_menu = 0;
                    App::import('Controller', 'category');
                    $categoryController = new CategoryController;
                    $main_category = $categoryController->getcategorydata($menu_header['Menu']['content_name']);
                    $sub_category = $categoryController->getsubcategories($menu_header['Menu']['content_name']);

                    if ($url_parameters[0] == $main_category['Category']['identifier']) {
                        $actives = " active";
                    } else {
                        $actives = " ";
                    }
                    ?>
                    <li class=" "> <a href="#" class="js-activated <?php echo $actives; ?>" title="<?php echo $main_category['Category']['label'] ?>"><?php if (!empty($main_category['Category']['fontaws_icon'])) { ?><i class="<?php echo $main_category['Category']['fontaws_icon'] ?>"></i><?php } ?> <?php echo $main_category['Category']['label'] ?></a>

                        <div class=" wrap-inside-nav">
                            <div class="inside-col">
                                <?php
                                foreach ($sub_category as $sub_categor) {

                                    $child_category = $categoryController->getsubcategories($sub_categor['Category']['id']);

                                    if ($max_menu == 0) {
                                        $max_menu = count($child_category);
                                    } elseif ($max_menu < count($child_category)) {
                                        $max_menu = count($child_category);
                                    }
                                }
                                $blank_li = 0;
                                foreach ($sub_category as $sub_category) {

                                    $child_category = $categoryController->getsubcategories($sub_category['Category']['id']);
                                    $headinglink = "";

                                    $headinglink = Router::url('/', true) . 'page/' . $main_category['Category']['identifier'] . '/';

                                    if (isset($sub_category['Category']['is_heading']) && $sub_category['Category']['is_heading'] == '1') {
                                        $headinglink = '#';
                                    } else {
                                        $headinglink .= $sub_category['Category']['identifier'];
                                    }
                                    ?>
                                    <ul class="<?php echo $class; ?> inside-nav">
                                        <li>

                                            <p><strong><a href="<?php echo $headinglink; ?>" title="<?php echo $sub_category['Category']['label'] ?>"><?php echo $sub_category['Category']['label'] ?></a></strong>
                                            </p>
                                        </li>
                                        <?php if (!empty($sub_category['Category']['img'])) { ?>
                                            <li><a href="<?php echo $headinglink; ?>" title="<?php echo $sub_category['Category']['label'] ?>" class="removepadding">
                                                    <img src="<?php echo $sub_category['Category']['img']; ?>" title="<?php echo $sub_category['Category']['label'] ?>" alt="<?php echo $sub_category['Category']['label'] ?>" />
                                                </a></li>
                                            <?php
                                        }
                                        if (!empty($child_category) && isset($child_category)) {
                                            $countchild = count($child_category);
                                            foreach ($child_category as $child_category) {


                                                $link = Router::url('/', true) . 'page/' . $main_category['Category']['identifier'] . '/';


                                                if (!empty($sub_category['Category']['identifier'])) {
                                                    $link .= $sub_category['Category']['identifier'] . '/';
                                                }
                                                if (!empty($child_category['Category']['identifier'])) {

                                                    $link .= $child_category['Category']['identifier'];
                                                }
                                                if (isset($child_category['Category']['is_heading']) && $child_category['Category']['is_heading'] == '1') {
                                                    $newlink = $link;
                                                    $link = '#';
                                                }
                                                ?>
                                                <li ><a href="<?php echo $link ?>"><i class="fa fa-angle-right"></i> <?php echo $child_category['Category']['label'] ?></a>
                                                    <?php
                                                    if (isset($child_category['Category']['is_heading']) && $child_category['Category']['is_heading'] == '1') {
                                                        $page_links = $PagesController->get_pages_category($child_category['Category']['id'], null, 'sub_category');
                                                        if (count($page_links) > '0' && isset($page_links)) {
                                                            echo '<ul class="sublinksmenu">';
                                                            foreach ($page_links as $page_link) {
                                                                echo '<li><a href="' . $newlink . '/' . $page_link["Page"]["page_slug"] . '"><i class="fa fa-angle-right"></i> ' . $page_link['Page']['page_name'] . '</a>';
                                                            }
                                                            echo '</ul>';
                                                        }
                                                    }
                                                    ?>
                                                </li>
                                                <?php
                                            }
                                        } else {
                                            $pagedata = $PagesController->get_page_id($sub_category['Category']['identifier']);
                                            echo $this->Text->truncate(
                                                    strip_tags($pagedata['Page']['page_description']), 250, array(
                                                'ellipsis' => '...',
                                                'exact' => false,
                                                'html' => false
                                                    )
                                            );
                                        }
                                        ?>
                                        <?php
                                        if (isset($max_blank)) {
                                            if ($blank_li < $max_blank) {
                                                if ($countchild < $max_menu) {
                                                    for ($i = $countchild; $i < $max_menu; $i++) {
                                                        echo '<li>&nbsp;</li>';
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                    </ul>
                                    <?php
                                    $blank_li += 1;
                                }
                                ?>
                            </div>
                        </div>
                    </li>

                    <?php
                }
            }
            ?>
        </ul>

    </div>
    <?php
} else if ($menu_for == "footer-menu") {
    $total_footer_blocks = count($menu_headers);
    if ($total_footer_blocks == 1) {
        $main_block_class = "one_full ";
    } elseif ($total_footer_blocks == 2) {
        $main_block_class = "one_half ";
    } elseif ($total_footer_blocks == 3) {
        $main_block_class = "one_third ";
    } elseif ($total_footer_blocks == 4) {
        $main_block_class = "one_fourth ";
    } elseif ($total_footer_blocks == 5) {
        $main_block_class = "one_fifth ";
    } elseif ($total_footer_blocks == 6) {
        $main_block_class = "one_six ";
    }
    foreach ($menu_headers as $menu_header) {
        if ($menu_header['Menu']['content_type'] == "custom") {

            $items = $this->requestAction(array('controller' => 'menus', 'action' => 'list_menu_item', $menu_header['Menu']['content_name']));
            if ($menu_header['Menu']['url'] != "#" && $menu_header['Menu']['url'] != "javascript:void()") {
                $url = parse_url($menu_header['Menu']['url']);
                if (isset($url['scheme']) && $url['scheme'] != 'https' && $url['scheme'] != 'http') {
                    $link = Router::url('/', true) . $menu_header['Menu']['url'];
                } else {
                    $link = $menu_header['Menu']['url'];
                }
            } else {
                $link = $menu_header['Menu']['url'];
            }

            if (!empty(trim($menu_header['Menu']['icon']))) {
                $icon = '<i class="' . $menu_header['Menu']['icon'] . '"></i> ';
            } else {
                $icon = '';
            }
            if (isset($items) && !empty($items)) {
                $data_toggle = "dropdown";
            } else {
                $data_toggle = "";
            }
            $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
            if ($_SERVER["SERVER_PORT"] != "80") {
                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
            } else {
                $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            }
            $url_parameter = explode(Router::url('/', true), $pageURL);
            $url_parameters = explode('/', $url_parameter[count($url_parameter) - 1]);

            $max_menu = 0;
            App::import('Controller', 'category');
            $categoryController = new CategoryController;
            $main_category = $categoryController->getcategorydata($menu_header['Menu']['content_name']);
            $sub_category = $categoryController->getsubcategories($menu_header['Menu']['content_name']);
            if ($pageURL != Router::url('/', true)) {
                if (isset($main_category) && !empty($main_category)) {
                    if ($url_parameters[0] == $main_category['Category']['identifier']) {
                        $actives = " active";
                    } elseif (strtolower(Inflector::slug($menu_header['Menu']['content_name'], '-')) == $url_parameters[0]) {
                        $actives = " active";
                    } else {
                        $actives = " ";
                    }
                }
            } else {
                $actives = " ";
            }
            ?>
            <div class="<?php echo $main_block_class; ?> animate fadeInUp" data-anim-type="fadeInUp">
                <div class="qlinks">
                    <h4 class=""><?php echo $menu_header['Menu']['content_name']; ?></h4>
                    <?php if (isset($items) && !empty($items)) { ?>
                        <ul class="">
                            <?php
                            foreach ($items as $item) {
                                if ($item['Menu']['content_type'] == "customHTML") {
                                    echo '<li>' . base64_decode($item['Menu']['url']) . '</li>';
                                } elseif ($item['Menu']['content_type'] == "custom") {
                                    if (filter_var($item['Menu']['url'], FILTER_VALIDATE_URL)) {
                                        $url = $item['Menu']['url'];
                                    } else {
                                        $url = Router::url('/', true) . $item['Menu']['url'];
                                    }
                                    ?>
                                    <li><a href="<?php echo $url; ?>" title="<?php echo $item['Menu']['content_name']; ?>"><i class="fa fa-angle-right"></i> <?php echo $item['Menu']['content_name']; ?></a>
                                    </li>
                                    <?php
                                } else {
                                    if ($item['Menu']['content_type'] == 'category') {
                                        $main_category = $categoryController->getcategorydata($item['Menu']['content_name']);
                                        echo '<li><a href="' . Router::url('/', true) . '' . $main_category['Category']['identifier'] . '" title="' . $main_category['Category']['label'] . '"><i class="fa fa-angle-right"></i>  ' . $main_category['Category']['label'] . '</a></li>';
                                    } elseif ($item['Menu']['content_type'] == 'insight') {
                                        $insight_type = $InsightsController->insightcategorydata($item['Menu']['content_name']);
                                        echo '<li><a href="' . Router::url('/', true) . 'insights/' . $insight_type['identifier'] . '" title="' . $insight_type['label'] . '"><i class="fa fa-angle-right"></i>  ' . $insight_type['label'] . '</a></li>';
                                    } elseif ($item['Menu']['content_type'] == 'page') {
                                        $pageitems = $this->requestAction(array('controller' => 'menus', 'action' => 'list_menu_item', $item['Menu']['content_type'], $item['Menu']['content_name']));
                                        $pagelink = "";
                                        $main_category = $categoryController->getcategorydata($pageitems['Page']['main_category']);

                                        if (!isset($main_category['Category']['identifier'])) {
                                            $main_category['Category']['identifier'] = null;
                                        }


                                        $pagelink = Router::url('/', true);

                                        if (!empty($pageitems['Page']['category'])) {
                                            $sub_category = $categoryController->getcategorydata($pageitems['Page']['category']);
                                            if ($sub_category['Category']['identifier'] != $main_category['Category']['identifier']) {
                                                $pagelink .= $sub_category['Category']['identifier'] . '/';
                                            }
                                        }

                                        if (!empty($pageitems['Page']['sub_category'])) {
                                            $child_category = $categoryController->getcategorydata($pageitems['Page']['sub_category']);
                                            if ($child_category['Category']['identifier'] != $sub_category['Category']['identifier']) {
                                                $pagelink .= $child_category['Category']['identifier'] . '/';
                                            }
                                        }
                                        if ($pageitems['Page']['page_slug'] != $main_category['Category']['identifier']) {
                                            $pagelink .= $pageitems['Page']['page_slug'];
                                        }

                                        echo '<li><a href="' . $pagelink . '" title="' . $pageitems['Page']['page_heading'] . '"><i class="fa fa-angle-right"></i>  ' . $pageitems['Page']['page_heading'] . '</a></li>';
                                    }
                                }
                            }
                            ?>
                        </ul>
                    <?php }
                    ?>
                </div>
            </div>
            <?php
        } elseif ($menu_header['Menu']['content_type'] == "page") {
            $pg_obj = $this->requestAction(array('controller' => 'menus', 'action' => 'get_page_name', $menu_header['Menu']['content_name']));
            ?>
            <li><a href="<?php echo Router::url('/', true) . $pg_obj['Page']['page_slug']; ?>" title="<?php echo $pg_obj['Page']['page_name']; ?>"> <?php echo $pg_obj['Page']['page_name']; ?></a>
            </li>
            <?php
        } elseif ($menu_header['Menu']['content_type'] == "category") {
            $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
            if ($_SERVER["SERVER_PORT"] != "80") {
                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
            } else {
                $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            }
            $url_parameter = explode(Router::url('/', true), $pageURL);
            $url_parameters = explode('/', $url_parameter[count($url_parameter) - 1]);

            $max_menu = 0;
            App::import('Controller', 'category');
            $categoryController = new CategoryController;
            $main_category = $categoryController->getcategorydata($menu_header['Menu']['content_name']);
            $sub_category = $categoryController->getsubcategories($menu_header['Menu']['content_name']);

            if ($url_parameters[0] == $main_category['Category']['identifier']) {
                $actives = " active";
            } else {
                $actives = " ";
            }
            ?>

            <div class="<?php echo $main_block_class; ?>  animate fadeInUp" data-anim-type="fadeInUp">
                <div class="qlinks">
                    <h4 class=""><?php echo $main_category['Category']['label'] ?></h4>
                    <ul>
                        <?php
                        foreach ($sub_category as $sub_category) {

                            $child_category = $categoryController->getsubcategories($sub_category['Category']['id']);
                            $headinglink = "";

                            $headinglink = Router::url('/', true) . '' . $main_category['Category']['identifier'] . '/';

                            if (isset($sub_category['Category']['is_heading']) && $sub_category['Category']['is_heading'] == '1') {
                                $headinglink = '#';
                            } else {
                                $headinglink .= $sub_category['Category']['identifier'];
                            }
                            ?>
                            <li><a href="<?php echo $headinglink; ?>" title="<?php echo $sub_category['Category']['label'] ?>"><i class="fa fa-angle-right"></i> <?php echo $sub_category['Category']['label'] ?></a></li>

                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        <?php } elseif ($menu_header['Menu']['content_type'] == "customHTML") {
            ?>
            <div class="<?php echo $main_block_class; ?>  animate fadeInUp last" data-anim-type="fadeInUp">
                <div class="qlinks">
                    <?php echo $WidgetsController->magicKeyword(base64_decode($menu_header['Menu']['url'])); ?>
                </div>
            </div>
            <?php
        }
    }
}
?>