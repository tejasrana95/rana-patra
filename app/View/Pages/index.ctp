<?php
if (isset($media_file) && !empty($media_file)):
    ?>
    <style>.about-header {
            position: relative;
            height: 700px;
            padding-top: 300px;
            text-align: center;
            background: url(<?php echo $media_file['Media']['content']; ?>) center no-repeat;
            background-size: cover;
        }
        body{margin-top:0px}</style>
    <section class="about-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12 wow fadeInUp" data-wow-duration="2s" >
                    <h1 class="title"><?php echo $page_detail['Page']['page_name']; ?></h1>
                    <div class="path"><a href="<?php echo Router::url('/', true); ?>">Home</a> / <?php echo $page_detail['Page']['page_name']; ?></div>
                </div>
                <div class="bottom-line"></div>
            </div>
        </div>
    </section>
<?php endif; ?>
<div class="clearfixe">&nbsp;</div>
<article>
    <?php print $page_detail['Page']['content']; ?>
</article>