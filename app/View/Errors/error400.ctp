<section class="gallery-single-header">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left wow fadeIn animated" data-wow-duration="2s">
				<h1 class="title"><?php echo $message; ?></h1>
				<div class="path"><a href="<?php echo Router::url('/', true); ?>">Home</a> / Error
				</div>
			</div>
		</div>
	</div>
</section>
<div class="clearfixe">&nbsp;</div>
<article>
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left wow fadeIn animated" data-wow-duration="2s">
				<p class="text-shadow col s12">
					<?php if ($message != "The request has been black-holed") {
						?>
						<?php echo __d('cake', 'Error'); ?>
						:
						<?php printf(__d('cake', 'The requested address %s was not found on this server.'),"'{$url}'");?>
						<?php } ?>
					</p>
					<div class="debug">
						<?php
						if (Configure::read('debug') > 0):
							echo $this->element('exception_stack_trace');
						endif;
						?>
					</div>
				</div>
			</div>
		</article>