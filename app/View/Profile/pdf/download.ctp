 <?php
 App::import('Controller', 'Common');
 $CommonController = new CommonController;
 ?>
 <style>
 body { font-family: Helvetica; }
    .edit-profile{margin-top:15px;}
    p{margin:0 0 4px;}
    .text-right{text-align:right !important;}
    .cyan{background-color:#00bcd4 !important;}   
    .white-text{color:#fff !important;}
    .table-responsive{overflow-x:auto;min-height:0.01%;}
    .table{width:100%;max-width:100%;margin-bottom:29px;}
    table{background-color:transparent;}
    table{border-collapse:collapse;border-spacing:0;}
    .invoice-footer p.strong{font-weight:bold;}
    .invoice-footer p{margin:0px;}
    .z-depth-2{box-shadow:0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);}
    .green.accent-4{background-color:#00C853 !important;}
    .red.darken-4{background-color:#B71C1C !important;}
    .invoice-text{padding:30px 10px 17px 10px;}
    .white-text{color:#fff !important;}
    h4{margin-bottom:.7em;}
    h3, h4{font-weight:400;}
    h1, h2, h3, h4, h5, h6{margin:0;}
    h4, .h4{font-size:18px;padding-bottom:10px;}
    h5, h6{font-weight:600;}
    h5{font-size:25px;}
    h6, .h6{font-size:14px;}
    .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > th, .table > caption + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > td, .table > thead:first-child > tr:first-child > td{border-top:0;}
    .table > thead > tr > th{vertical-align:bottom;border-bottom:2px solid #dddddd;}
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{padding:8px;line-height:2.14285714;vertical-align:top;border-top:1px solid #dddddd;text-align:center;}
    .clearfix{margin:5px;}
    .center{text-align:center;}
    .header h5{text-align:center;font-size:26px;}
    .strong{font-weight:bold;}
    .text-muted{color: #cccccc;}
    .small{text-align: right; font-size: 11px; margin-top: 20px;}
    .alert-danger {color: #a94442;background-color: #f2dede;border-color: #ebccd1;}
    .alert {padding: 15px;margin-bottom: 20px;border: 1px solid transparent;border-radius: 4px;}
</style>
<table class="" style="width:100%;">
    <tr>
        <td class="center">
            <h5  id="">Invoices</h5>
             <img src="<?php echo $publicDir; ?>/pdflogo.jpg" alt="<?php echo $site_name.' Powered by '.$org_name; ?>">
        </td>
    </tr>
</table>
    <div class="edit-profile">       
        <div class="">
            <?php if(isset($invoices) && !empty($invoices) && $this->request->data['CustomerInvoice']['payment_status'] != "FAILED"): ?>

                <table style="width:100%">
                    <tr>
                        <td valign="top">
                            <p><span class="strong"><?php echo $this->request->data['CustomerInvoice']['first_name'] . ' ' .$this->request->data['CustomerInvoice']['last_name']  ?></span>
                                <br>
                                <span><?php echo $this->request->data['CustomerInvoice']['address'] ?></span>
                                <br>
                                <span><?php
                                    $country = $this->request->data['CustomerInvoice']['country'];
                                    $state =$this->request->data['CustomerInvoice']['state'];
                                    $city = $this->request->data['CustomerInvoice']['city'];
                                    echo $city
                                    ?>, <?php echo $state ?> 
                                    , <?php echo $country ?>
                                </span>
                                <br>
                                <span><?php echo $this->request->data['CustomerProfile']['contact'] ?></span>
                            </p>
                        </td>
                        <td class="text-right text-muted" valign="top">
                        <p><span class="strong"><?php echo $company_info['payment_company']; ?></span>
                                <br>
                                <span><?php echo $company_info['payment_address']; ?></span>
                                <br>
                                <span><?php echo $company_info['payment_contact']; ?>
                                </span>
                                <br>
                                <span class="strong"><?php echo $company_info['payment_company_url']; ?></span>
                                <br>
                                <span><?php echo $company_info['payment_mail']; ?></span>
                            </p>
                    </td>
                </tr>
            </table>
            <div class="clearfix">&nbsp;</div>
            <div id="invoice">
                    <div class="invoice-lable">

                        <table class="cyan" style="width:100%;">
                            <tr>
                                <td><h4 class="white-text invoice-text">INVOICE</h4></td>
                                <td class="white-text invoice-text">
                                    <p class="strong">Total Due</p>
                                    <h4 class="header"><?php echo $this->request->data['CustomerInvoice']['currency_code']; ?> 
                                        <?php echo $this->request->data['CustomerInvoice']['gross']; ?>
                                    </h4>
                                </td>
                                <td  class="white-text invoice-text">
                                    <p class="strong">Invoice No</p>
                                    <h4 class="header"><?php echo $this->request->data['CustomerInvoice']['txn_id']; ?></h4>
                                </td>
                                <td  class="white-text invoice-text">
                                   <p class="strong">Due Date</p>
                                   <h4 class="header"><?php echo $this->Time->format($this->request->data['CustomerInvoice']['created'], '%e.%m.%Y'); ?></h4>
                               </td>
                           </tr>
                       </table>

                   </div>

                <div class="invoice-table">
                    <div class="row">
                        <div class="">
                            <table class="table table-striped table-hover table-responsive">
                                <thead>
                                    <tr>
                                        <th data-field="no">No</th>
                                        <th data-field="item">Item</th>
                                        <th data-field="uprice">Item Number</th>
                                        <th data-field="price">Item Type</th>
                                        <th data-field="price">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td><?php echo $this->request->data['CustomerInvoice']['item_name']; ?></td>
                                        <td><?php echo $this->request->data['CustomerInvoice']['item_number']; ?></td>
                                        <td><?php echo $item_type[$this->request->data['CustomerInvoice']['item_type']]; ?></td>
                                        <td><?php echo $this->request->data['CustomerInvoice']['currency_code']; ?> 
                                            <?php echo $this->request->data['CustomerInvoice']['gross']; ?></td>
                                        </tr>

                                        <tr>
                                            <td colspan="3" class="white"></td>
                                            <td class="cyan white-text">Grand Total</td>
                                            <td class="cyan strong white-text"><?php echo $this->request->data['CustomerInvoice']['currency_code']; ?> 
                                                <?php echo $this->request->data['CustomerInvoice']['gross']; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>

                        <div class="invoice-footer">
                            <table class="" style="width:100%;">
                                <tr>
                                    <td>
                                        <p class="strong">Payment Method</p>
                                        <p><?php echo strtoupper($this->request->data['CustomerInvoice']['pay_with']); ?></p>
                                    </td>
                                    <td class="text-right">
                                        <p class="strong">Terms &amp; Condition</p>
                                         <p>Please visit: <?php echo $site_url ?></p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                        <div class=" center z-depth-2 ">
                          <div <?php
                          if (strtoupper($this->request->data['CustomerInvoice']['payment_status']) == "FAILED") {
                            echo 'class="red darken-4"';
                        } else {
                            echo 'class="green accent-4"';
                        }
                        ?>>
                        <div class="clearfix">&nbsp;</div>
                        <h6 class="header">Payment: <?php echo strtoupper($this->request->data['CustomerInvoice']['payment_status']); ?></h6>
                        <div class="clearfix">&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
        <p class="small"><em>This is computer generated invoice and no signature required.</em></p>
    <?php else: ?>
      <div id="flashMessage" class="alert alert-danger center">Invoice is not available. It might be because you are trying to print invoice for failed transaction. Please contact to support desk for more information.</div>
  <?php endif; ?>
</div>
