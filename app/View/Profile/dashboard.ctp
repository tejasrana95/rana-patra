<?php
App::import('Controller', 'Common');
$CommonController = new CommonController;

$subscriptionEndDate=$CommonController->getSubscriptionEndDate($userData['Customer']['id']);
if(isset($subscriptionEndDate) && $subscriptionEndDate!=false && $subscriptionEndDate > date('Y-m-d')){
  $subscriptionEndDateFormat=date('M, j Y',strtotime($subscriptionEndDate));
  $subscriptionMessage="Subscription type: Premium  valid till ".$subscriptionEndDateFormat;
}else{
  $subscriptionMessage="Subscription type: Free | Valid till lifetime with limited access.";
}
?>
<div class="no-header-image login-overlay container">
<?php echo $this->element('breadcrumb'); ?> 
 <div class=''>
    <div class="col-md-12"> 
    
      <div class="row">
      	<div class="col-md-6 col-sm-12">
      		<div class="row">
      			<div class="col-md-2 profile-pic">
      				<?php 
      				if(isset($userData['CustomerProfile']['profilePic']) && !empty($userData['CustomerProfile']['profilePic'])){
                if(filter_var($userData['CustomerProfile']['profilePic'], FILTER_VALIDATE_URL)){  
                 $profilePic=$userData['CustomerProfile']['profilePic'];
               }else{
                  $profilePic='/public/'.$userData['CustomerProfile']['profilePic'];
                }
      				}else{
      					$gender=$userData['CustomerProfile']['gender'];
      					if($gender==1 && !empty($userData['CustomerProfile']['address'])){
							$profilePic='/images/no-avtar-male.png';
      					}elseif($gender==0 && !empty($userData['CustomerProfile']['address'])){
      						$profilePic='/images/no-avtar-female.png';
      					}else{
      						$profilePic='/images/no-avtar.png';
      					}
      				}
      				?>
      				<img src="<?php echo $profilePic; ?>" alt="<?php echo $userData['CustomerProfile']['name']; ?>" title="<?php echo $userData['CustomerProfile']['name']; ?>">
      			</div>
      			<div class="col-md-10 user-dashboard">
      				<?php echo $userData['CustomerProfile']['name']; ?> <span class="small">(
              <a href="/profile">View profile</a> | 
              <a href="/profile/edit">Edit profile</a>
              )</span>
      				<div class="midsmall">(<?php echo $userData['Customer']['email']; ?>)</div>
              <?php 
            if(!empty($userData['CustomerProfile']['placefinder'])): ?>
            <div class="small"><i class="fa fa-map-marker"></i>
              <?php echo $userData['CustomerProfile']['placefinder']; ?>
            </div>
          <?php  endif; ?>
      			</div>
      		</div>
			
      	</div>
      	<div class="col-md-6 col-sm-12 text-right">
      		<?php 
            echo $this->Html->link('<i class="fa fa-key"></i> Password',  array('controller' => 'profile', 'action' => 'password'), array('class' => 'btn btn-info btn-sm','escape' => false));
            echo '&nbsp;';
            echo $this->Html->link('<i class="fa fa-credit-card"></i> Subscription',  array('controller' => 'profile', 'action' => 'subscription'), array('class' => 'btn btn-success btn-sm','escape' => false));
            echo '&nbsp;';
            echo $this->Html->link('<i class="fa fa-sign-out"></i> Logout',  array('controller' => 'user', 'action' => 'logout'), array('class' => 'btn btn-danger btn-sm','escape' => false));
         ?>
      		<div class="small padding-10"><?php echo $subscriptionMessage; ?></div>
      	</div>
      </div>
    </div>
  </div>
  <div class="clearfix">&nbsp;</div>
	<?php  if(empty($userData['CustomerProfile']['address']) || empty($userData['CustomerProfile']['contact']) || empty($userData['CustomerProfile']['city']) || empty($userData['CustomerProfile']['state']) || empty($userData['CustomerProfile']['country']) ){ ?>
  <div class="col-md-12"><div id="flashMessage" class="alert alert-danger">It's seems your profile is not complete. Please complete it now <?php   echo $this->Html->link('Click here',  array('controller' => 'profile', 'action' => 'edit'), array('class' => '','escape' => false)); ?>.</div></div>
  <?php } ?>
 
  <div class="clearfix">&nbsp;</div>
  <div class="">
  <div class="col-sm-6">
          <!-- Widget -->
          <div class="widget">
            <div class="widget-content padding-30 bg-blue-600">
              <div class="widget-watermark darker font-size-60 margin-15"><i class="fa fa-heartbeat" aria-hidden="true"></i></div>
              <div class="counter counter-md counter-inverse text-left">
                <div class="counter-number-group">
                    <?php  echo $this->Html->link('<span class="counter-number">'.$stats['Matrimonial'].'</span>'
                            . ' <span class="counter-number-related text-capitalize">Matrimonial</span>',
                            array('controller' => 'matrimonial', 'action' => 'myview'), array('class' => '','escape' => false)); 
                    ?>
                </div>
                <div class="counter-label text-capitalize">Submitted</div>
              </div>
            </div>
          </div>
          <!-- End Widget -->
        </div>
        <div class="col-sm-6">
          <!-- Widget -->
          <div class="widget">
            <div class="widget-content padding-30 bg-red-600">
              <div class="widget-watermark darker font-size-60 margin-15"><i class="fa fa-file-text" aria-hidden="true"></i></div>
              <div class="counter counter-md counter-inverse text-left">
                <div class="counter-number-group">
                    <?php  echo $this->Html->link('<span class="counter-number">'.$stats['Article'].'</span>'
                            . ' <span class="counter-number-related text-capitalize">Article</span>',
                            array('controller' => 'article', 'action' => 'myview'), array('class' => '','escape' => false)); 
                    ?>
                </div>
                <div class="counter-label text-capitalize">Submitted</div>
              </div>
            </div>
          </div>
          <!-- End Widget -->
        </div>
        <div class="col-sm-6">
          <!-- Widget -->
          <div class="widget">
            <div class="widget-content padding-30 bg-green-600">
              <div class="widget-watermark darker font-size-60 margin-15"><i class="fa fa-calendar" aria-hidden="true"></i></div>
              <div class="counter counter-md counter-inverse text-left">
                <div class="counter-number-group">
                    <?php  echo $this->Html->link('<span class="counter-number">'.$stats['Event'].'</span>'
                            . ' <span class="counter-number-related text-capitalize">Event</span>',
                            array('controller' => 'event', 'action' => 'myview'), array('class' => '','escape' => false)); 
                    ?>
                </div>
                <div class="counter-label text-capitalize">Submitted</div>
              </div>
            </div>
          </div>
          <!-- End Widget -->
        </div>
        <div class="col-sm-6">
          <!-- Widget -->
          <div class="widget">
            <div class="widget-content padding-30 bg-purple-600">
              <div class="widget-watermark darker font-size-60 margin-15"><i class="fa fa-newspaper-o" aria-hidden="true"></i></div>
              <div class="counter counter-md counter-inverse text-left">
                <div class="counter-number-group">
                    <?php  echo $this->Html->link('<span class="counter-number">'.$stats['News'].'</span>'
                            . ' <span class="counter-number-related text-capitalize">News</span>',
                            array('controller' => 'news', 'action' => 'myview'), array('class' => '','escape' => false)); 
                    ?>
                </div>
                <div class="counter-label text-capitalize">Submitted</div>
              </div>
            </div>
          </div>
          <!-- End Widget -->
        </div>
    </div>
    </div>
<div class="clearfix"></div>