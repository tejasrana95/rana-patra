 <?php
 App::import('Controller', 'Common');
 $CommonController = new CommonController;
 $subscriptionEndDate=$CommonController->getSubscriptionEndDate($userData['Customer']['id']);
 if(isset($subscriptionEndDate) && $subscriptionEndDate!=false && $subscriptionEndDate > date('Y-m-d')){
   $subscriptionEndDateFormat=date('M, j Y',strtotime($subscriptionEndDate));
   $subscriptionMessage="Subscription type: Premium  valid till ".$subscriptionEndDateFormat;
 }else{
   $subscriptionMessage="Subscription type: Free | Valid till lifetime with limited access.";
 }
 ?>
 <div class="no-header-image login-overlay container">
 <?php echo $this->element('breadcrumb'); ?> 
   <div class=''>
    <div class="col-md-12">  
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="row">
            <div class="col-md-2 profile-pic">
              <?php 
              if(isset($userData['CustomerProfile']['profilePic']) && !empty($userData['CustomerProfile']['profilePic'])){
                if(filter_var($userData['CustomerProfile']['profilePic'], FILTER_VALIDATE_URL)){  
                 $profilePic=$userData['CustomerProfile']['profilePic'];
               }else{
                  $profilePic='/public/'.$userData['CustomerProfile']['profilePic'];
                }
              }else{
               $gender=$userData['CustomerProfile']['gender'];
               if($gender==1 && !empty($userData['CustomerProfile']['address'])){
                $profilePic='/images/no-avtar-male.png';
              }elseif($gender==0 && !empty($userData['CustomerProfile']['address'])){
                $profilePic='/images/no-avtar-female.png';
              }else{
                $profilePic='/images/no-avtar.png';
              }

            }
            ?>
            <img src="<?php echo $profilePic; ?>" alt="<?php echo $userData['CustomerProfile']['name']; ?>" title="<?php echo $userData['CustomerProfile']['name']; ?>">
          </div>
          <div class="col-md-10 user-dashboard">
            <?php echo $userData['CustomerProfile']['name']; ?> <span class="small">(
            <a href="/profile">View profile</a> | 
            <a href="/profile/edit">Edit profile</a>
            )</span>
            <div class="midsmall">(<?php echo $userData['Customer']['email']; ?>)</div>
            <?php 
            if(!empty($userData['CustomerProfile']['placefinder'])): ?>
            <div class="small"><i class="fa fa-map-marker"></i>
              <?php echo $userData['CustomerProfile']['placefinder']; ?>
            </div>
          <?php  endif; ?>
        </div>
      </div>
      
    </div>
    <div class="col-md-6 col-sm-12 text-right">
      <?php 
      echo $this->Html->link('<i class="fa fa-key"></i> Password',  array('controller' => 'profile', 'action' => 'password'), array('class' => 'btn btn-info btn-sm','escape' => false));
      echo '&nbsp;';
      echo $this->Html->link('<i class="fa fa-credit-card"></i> Subscription',  array('controller' => 'profile', 'action' => 'subscription'), array('class' => 'btn btn-success btn-sm','escape' => false));
      echo '&nbsp;';
      echo $this->Html->link('<i class="fa fa-sign-out"></i> Logout',  array('controller' => 'user', 'action' => 'logout'), array('class' => 'btn btn-danger btn-sm','escape' => false));
      ?>
      	<div class="small padding-10"><?php echo $subscriptionMessage; ?></div>
    </div>
  </div>

</div>
</div>
<div class="clearfix">&nbsp;</div>
<?php echo $this->Session->flash(); ?>
<div class="clearfix">&nbsp;</div>
<div class="">
  <div class="row">
    <div class="col-sm-12 col-md-3 profile-tab-left">
      <ul>
        <li class=""><?php 
          echo $this->Html->link('<i class="fa fa-user"></i> Profile',  array('controller' => 'profile', 'action' => 'index'), array('escape' => false,'class'=>''));
          ?></li>
          <li class=""><?php 
            echo $this->Html->link('<i class="fa fa-pencil"></i> Edit Profile',  array('controller' => 'profile', 'action' => 'edit'), array('escape' => false));
            ?></li>
            <li class=""><?php 
              echo $this->Html->link('<i class="fa fa-key"></i> Password',  array('controller' => 'profile', 'action' => 'password'), array('escape' => false));
              ?></li>
              <li class="active"><?php 
                echo $this->Html->link('<i class="fa fa-credit-card"></i> Subscription / invoices',  array('controller' => 'profile', 'action' => 'subscription'), array('escape' => false));
                ?></li>
              </ul>
            </div>
            <div class="col-sm-12 col-md-9 profile-tab-right">
              <div class="header col-md-12 col-sm-12">
                <h5 class="" id="">Invoices</h5>
              </div>
              <div class="col-md-12 col-sm-12 edit-profile">       
                <div class="">
                <?php if(isset($invoices) && !empty($invoices)): ?>
                  <div class="text-right">
                 <?php  echo $this->Html->link('<i class="fa fa-download"></i> Download',  array('controller' => 'profile', 'action' => 'invoice',$this->request->data['CustomerInvoice']['id'],'download','ext' => 'pdf'), array('class' => 'btn btn-info btn-sm','escape' => false,'target' => '_blank'));  ?>
                 <?php  echo $this->Html->link('<i class="fa fa-credit-card"></i> Purchase/Renew Subscription',  array('controller' => 'payment', 'action' => 'subscription'), array('class' => 'btn btn-info btn-sm','escape' => false)); ?>
                  </div>
                <div id="invoice">
        <div class="card-panel">
            <div class="invoice-header">
            <img src="<?php echo $site_url; ?>images/rana-Community.png" alt="<?php echo $site_name.' Powered by '.$org_name; ?>">
                <div class="row section">

                    <div class="col-sm-12 col-md-6">
                        
                        <p>
                        <span class="invoice-icon"><i class="fa fa-user cyan-text"></i></span><br/>
                        <span class="strong"><?php echo $this->request->data['CustomerInvoice']['first_name'] . ' '.$this->request->data['CustomerInvoice']['last_name']; ?></span>
                            <br>
                            <span><?php echo $this->request->data['CustomerInvoice']['address'] ?></span>
                            <br>
                            <span><?php
                                $country = $this->request->data['CustomerInvoice']['country'];
                                $state = $this->request->data['CustomerInvoice']['state'];
                                $city =$this->request->data['CustomerInvoice']['city'];
                                echo $city
                                ?>, <?php echo $state ?> 
                                , <?php echo $country ?>
                            </span>
                            <br>
                            <span><?php echo $this->request->data['CustomerProfile']['contact'] ?></span>
                        </p>
                    </div>

                    <div class="col-sm-12 col-md-6">
                        <div class="invoce-company-address text-right text-muted">
                        
                            <span class="invoice-icon"><i class="fa fa-building cyan-text"></i></span>

                            <p><span class="strong"><?php echo $company_info['payment_company']; ?></span>
                                <br>
                                <span><?php echo $company_info['payment_address']; ?></span>                                
                                <br>
                                <span><?php echo $company_info['payment_contact']; ?></span>
                            </p>
                        </div>

                        <div class="invoce-company-contact text-right text-muted">
                            <span class="invoice-icon"><i class="fa fa-globe cyan-text"></i></span>
                            <p><span class="strong"><?php echo $company_info['payment_company_url']; ?></span>
                                <br>
                                <span><?php echo $company_info['payment_mail']; ?></span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="invoice-lable">
                <div class="row cyan">
                    <div class="col-sm-12 col-md-3 cyan">
                        <h4 class="white-text invoice-text">INVOICE</h4>
                    </div>
                    <div class="col-sm-12 col-md-9 invoice-brief cyan white-text">
                        <div class="row">
                            <div class="col-sm-12 col-md-3">
                                <p class="strong">Total Due</p>
                                <h4 class="header"><?php echo $this->request->data['CustomerInvoice']['currency_code']; ?> 
                                    <?php echo $this->request->data['CustomerInvoice']['gross']; ?>
                                </h4>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <p class="strong">Invoice No</p>
                                <h4 class="header"><?php echo $this->request->data['CustomerInvoice']['txn_id']; ?></h4>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <p class="strong">Due Date</p>
                                <h4 class="header"><?php echo $this->Time->format($this->request->data['CustomerInvoice']['created'], '%e.%m.%Y'); ?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="invoice-table">
                <div class="row">
                    <div class="">
                        <table class="table table-striped table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th data-field="no">No</th>
                                    <th data-field="item">Item</th>
                                    <th data-field="uprice">Item Number</th>
                                    <th data-field="price">Item Type</th>
                                    <th data-field="price">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1.</td>
                                    <td><?php echo $this->request->data['CustomerInvoice']['item_name']; ?></td>
                                    <td><?php echo $this->request->data['CustomerInvoice']['item_number']; ?></td>
                                    <td><?php echo $item_type[$this->request->data['CustomerInvoice']['item_type']]; ?></td>
                                    <td><?php echo $this->request->data['CustomerInvoice']['currency_code']; ?> 
                                        <?php echo $this->request->data['CustomerInvoice']['gross']; ?></td>
                                </tr>

                                <tr>
                                    <td colspan="3" class="white"></td>
                                    <td class="cyan white-text">Grand Total</td>
                                    <td class="cyan strong white-text"><?php echo $this->request->data['CustomerInvoice']['currency_code']; ?> 
                                        <?php echo $this->request->data['CustomerInvoice']['gross']; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

            <div class="invoice-footer">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <p class="strong">Payment Method</p>
                        <p><?php echo strtoupper($this->request->data['CustomerInvoice']['pay_with']); ?></p>
                        </div>
                        <div class="col-sm-12 col-md-6 text-right">
                        <p class="strong">Terms &amp; Condition</p>
                        <ul>
                            <li>Please visit: <?php echo $site_url ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class=" center z-depth-2 ">
              <div <?php
              if (strtoupper($this->request->data['CustomerInvoice']['payment_status']) == "FAILED") {
                echo 'class="red darken-4"';
              } else {
                echo 'class="green accent-4"';
              }
              ?>>
              <div class="clearfix">&nbsp;</div>
              <h6 class="header">Payment: <?php echo strtoupper($this->request->data['CustomerInvoice']['payment_status']); ?></h6>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div>
      </div>
                <?php else: ?>
                  <div id="flashMessage" class="alert alert-warning">It seems you do not have premium subscriptions.</div>
                <?php endif; ?>
                </div>
               
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>   
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
