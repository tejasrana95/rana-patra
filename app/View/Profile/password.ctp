 <?php
 App::import('Controller', 'Common');
 $CommonController = new CommonController;
 $subscriptionEndDate=$CommonController->getSubscriptionEndDate($userData['Customer']['id']);
 if(isset($subscriptionEndDate) && $subscriptionEndDate!=false && $subscriptionEndDate > date('Y-m-d')){
   $subscriptionEndDateFormat=date('M, j Y',strtotime($subscriptionEndDate));
   $subscriptionMessage="Subscription type: Premium  valid till ".$subscriptionEndDateFormat;
 }else{
   $subscriptionMessage="Subscription type: Free | Valid till lifetime with limited access.";
 }
 ?>
 <div class="no-header-image login-overlay container">
 <?php echo $this->element('breadcrumb'); ?> 
   <div class=''>
    <div class="col-md-12">  
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="row">
            <div class="col-md-2 profile-pic">
              <?php 
              if(isset($userData['CustomerProfile']['profilePic']) && !empty($userData['CustomerProfile']['profilePic'])){
                if(filter_var($userData['CustomerProfile']['profilePic'], FILTER_VALIDATE_URL)){  
                 $profilePic=$userData['CustomerProfile']['profilePic'];
               }else{
                  $profilePic='/public/'.$userData['CustomerProfile']['profilePic'];
                }
              }else{
               $gender=$userData['CustomerProfile']['gender'];
               if($gender==1 && !empty($userData['CustomerProfile']['address'])){
                $profilePic='/images/no-avtar-male.png';
              }elseif($gender==0 && !empty($userData['CustomerProfile']['address'])){
                $profilePic='/images/no-avtar-female.png';
              }else{
                $profilePic='/images/no-avtar.png';
              }

            }
            ?>
            <img src="<?php echo $profilePic; ?>" alt="<?php echo $userData['CustomerProfile']['name']; ?>" title="<?php echo $userData['CustomerProfile']['name']; ?>">
          </div>
          <div class="col-md-10 user-dashboard">
            <?php echo $userData['CustomerProfile']['name']; ?> <span class="small">(
            <a href="/profile">View profile</a> | 
            <a href="/profile/edit">Edit profile</a>
            )</span>
            <div class="midsmall">(<?php echo $userData['Customer']['email']; ?>)</div>
            <?php 
            if(!empty($userData['CustomerProfile']['placefinder'])): ?>
            <div class="small"><i class="fa fa-map-marker"></i>
              <?php echo $userData['CustomerProfile']['placefinder']; ?>
            </div>
          <?php  endif; ?>
        </div>
      </div>
      
    </div>
    <div class="col-md-6 col-sm-12 text-right">
      <?php 
      echo $this->Html->link('<i class="fa fa-key"></i> Password',  array('controller' => 'profile', 'action' => 'password'), array('class' => 'btn btn-info btn-sm','escape' => false));
      echo '&nbsp;';
      echo $this->Html->link('<i class="fa fa-credit-card"></i> Subscription',  array('controller' => 'profile', 'action' => 'subscription'), array('class' => 'btn btn-success btn-sm','escape' => false));
      echo '&nbsp;';
      echo $this->Html->link('<i class="fa fa-sign-out"></i> Logout',  array('controller' => 'user', 'action' => 'logout'), array('class' => 'btn btn-danger btn-sm','escape' => false));
      ?>
      	<div class="small padding-10"><?php echo $subscriptionMessage; ?></div>
    </div>
  </div>

</div>
</div>
<div class="clearfix">&nbsp;</div>
<?php echo $this->Session->flash();
if(!empty($userData['Customer']['account_type'])){
 ?>
<div id="flashMessage" class="alert alert-warning">It seems you create your account or login through <?php echo $userData['Customer']['account_type'] ?>. So you might not able to change your password. We strongly recommend you to login with site credentials to change your password.</div>
<?php } ?>
<div class="clearfix">&nbsp;</div>
<div class="">
  <div class="row">
    <div class="col-sm-12 col-md-3 profile-tab-left">
      <ul>
        <li class=""><?php 
          echo $this->Html->link('<i class="fa fa-user"></i> Profile',  array('controller' => 'profile', 'action' => 'index'), array('escape' => false,'class'=>''));
          ?></li>
          <li class=""><?php 
            echo $this->Html->link('<i class="fa fa-pencil"></i> Edit Profile',  array('controller' => 'profile', 'action' => 'edit'), array('escape' => false));
            ?></li>
            <li class="active"><?php 
              echo $this->Html->link('<i class="fa fa-key"></i> Password',  array('controller' => 'profile', 'action' => 'password'), array('escape' => false));
              ?></li>
              <li><?php 
                echo $this->Html->link('<i class="fa fa-credit-card"></i> Subscription',  array('controller' => 'profile', 'action' => 'subscription'), array('escape' => false));
                ?></li>
              </ul>
            </div>
            <div class="col-sm-12 col-md-9 profile-tab-right">
              <div class="header col-md-12 col-sm-12">
                <h5 class="" id="">Change Password</h5>
              </div>
              <?php echo $this->Form->create('Customer'); ?>
              <?php echo $this->Form->input('id'); ?>
              <div class="col-md-12 col-sm-12 edit-profile">       
                <div class="row">
                  <div class="col-md-3 col-sm-12">Current Password</div>
                  <?php  echo $this->Form->input('oldpassword', array('div' => array('class' => "col-md-9 col-sm-12"), 'class' => "form-control",'type'=>'password', 'empty' => '','placeholder'=>'Current Password','title'=>'Current Password','label'=>false)); ?>
                </div>
                <div class="row">
                  <div class="col-md-3 col-sm-12">New Password</div>
                  <?php  echo $this->Form->input('password', array('div' => array('class' => "col-md-9 col-sm-12"), 'class' => "form-control",'type'=>'password', 'empty' => '','placeholder'=>'New Password','title'=>'New Password','label'=>false)); ?>
                </div>
                <div class="row">
                  <div class="col-md-3 col-sm-12">Confirm Password</div>
                  <?php  echo $this->Form->input('confirm_password', array('div' => array('class' => "col-md-9 col-sm-12"), 'class' => "form-control",'type'=>'password', 'empty' => '','placeholder'=>'Confirm Password','title'=>'Confirm Password','label'=>false)); ?>
                </div>
              <div class="clearfix">&nbsp;</div>
              <div class="row">
              <?php
              echo $this->Form->input('Save', array('label' => false, 'div' => array('class' => "col-md-4 col-sm-12"), 'type' => "submit", 'class' => "btn btn-success btn-block"));
              ?>
              <?php echo $this->Form->end(); ?>
            </div>
            </div>
          </div>   
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
