 <?php
 App::import('Controller', 'Common');
 $CommonController = new CommonController;
 $subscriptionEndDate=$CommonController->getSubscriptionEndDate($userData['Customer']['id']);
 if(isset($subscriptionEndDate) && $subscriptionEndDate!=false && $subscriptionEndDate > date('Y-m-d')){
   $subscriptionEndDateFormat=date('M, j Y',strtotime($subscriptionEndDate));
   $subscriptionMessage="Subscription type: Premium  valid till ".$subscriptionEndDateFormat;
 }else{
   $subscriptionMessage="Subscription type: Free | Valid till lifetime with limited access.";
 }
 ?>
 <div class="no-header-image login-overlay container">
 <?php echo $this->element('breadcrumb'); ?> 
   <div class=''>
    <div class="col-md-12">  
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="row">
            <div class="col-md-2 profile-pic">
              <?php 
              if(isset($userData['CustomerProfile']['profilePic']) && !empty($userData['CustomerProfile']['profilePic'])){
                if(filter_var($userData['CustomerProfile']['profilePic'], FILTER_VALIDATE_URL)){  
                 $profilePic=$userData['CustomerProfile']['profilePic'];
               }else{
                  $profilePic='/public/'.$userData['CustomerProfile']['profilePic'];
                }
              }else{
               $gender=$userData['CustomerProfile']['gender'];
               if($gender==1 && !empty($userData['CustomerProfile']['address'])){
                $profilePic='/images/no-avtar-male.png';
              }elseif($gender==0 && !empty($userData['CustomerProfile']['address'])){
                $profilePic='/images/no-avtar-female.png';
              }else{
                $profilePic='/images/no-avtar.png';
              }

            }
            ?>
            <img src="<?php echo $profilePic; ?>" alt="<?php echo $userData['CustomerProfile']['name']; ?>" title="<?php echo $userData['CustomerProfile']['name']; ?>">
          </div>
          <div class="col-md-10 user-dashboard">
            <?php echo $userData['CustomerProfile']['name']; ?> <span class="small">(
            <a href="/profile">View profile</a> | 
            <a href="/profile/edit">Edit profile</a>
            )</span>
            <div class="midsmall">(<?php echo $userData['Customer']['email']; ?>)</div>
            <?php 
            if(!empty($userData['CustomerProfile']['placefinder'])): ?>
            <div class="small"><i class="fa fa-map-marker"></i>
              <?php echo $userData['CustomerProfile']['placefinder']; ?>
            </div>
          <?php  endif; ?>
        </div>
      </div>
      
    </div>
    <div class="col-md-6 col-sm-12 text-right">
      <?php 
      echo $this->Html->link('<i class="fa fa-key"></i> Password',  array('controller' => 'profile', 'action' => 'password'), array('class' => 'btn btn-info btn-sm','escape' => false));
      echo '&nbsp;';
      echo $this->Html->link('<i class="fa fa-credit-card"></i> Subscription',  array('controller' => 'profile', 'action' => 'subscription'), array('class' => 'btn btn-success btn-sm','escape' => false));
      echo '&nbsp;';
      echo $this->Html->link('<i class="fa fa-sign-out"></i> Logout',  array('controller' => 'user', 'action' => 'logout'), array('class' => 'btn btn-danger btn-sm','escape' => false));
      ?>
      	<div class="small padding-10"><?php echo $subscriptionMessage; ?></div>
    </div>
  </div>

</div>
</div>
<div class="clearfix">&nbsp;</div>
<?php echo $this->Session->flash(); ?>
<div class="clearfix">&nbsp;</div>
<div class="">
  <div class="row">
    <div class="col-sm-12 col-md-3 profile-tab-left">
      <ul>
        <li class=""><?php 
          echo $this->Html->link('<i class="fa fa-user"></i> Profile',  array('controller' => 'profile', 'action' => 'index'), array('escape' => false,'class'=>''));
          ?></li>
          <li class="active"><?php 
            echo $this->Html->link('<i class="fa fa-pencil"></i> Edit Profile',  array('controller' => 'profile', 'action' => 'edit'), array('escape' => false));
            ?></li>
            <li><?php 
              echo $this->Html->link('<i class="fa fa-key"></i> Password',  array('controller' => 'profile', 'action' => 'password'), array('escape' => false));
              ?></li>
              <li><?php 
                echo $this->Html->link('<i class="fa fa-credit-card"></i> Subscription',  array('controller' => 'profile', 'action' => 'subscription'), array('escape' => false));
                ?></li>
              </ul>
            </div>
            <div class="col-sm-12 col-md-9 profile-tab-right">
              <div class="header col-md-12 col-sm-12">
                <h5 class="" id="">Edit Profile</h5>
              </div>
              <?php echo $this->Form->create('CustomerProfile', array('type' => 'file','onsubmit'=>'return validateProfile();')); ?>
              <?php echo $this->Form->input('id'); ?>
              <div class="col-md-12 col-sm-12 edit-profile">       
                <div class="row">
                  <div class="col-md-2 col-sm-12">Name</div>
                  <?php  echo $this->Form->input('name', array('div' => array('class' => "col-md-10 col-sm-12"), 'class' => "form-control", 'empty' => '','placeholder'=>'E.g Tejas Rana','title'=>'Please enter your name','label'=>false)); ?>
                </div>
                <div class="row">
                  <div class="col-md-2 col-sm-12">Gender</div>
                  <div class="col-md-10 col-sm-12 gender">
                   <?php $options = array('1' => 'Male', '0' => 'Female');
                   $attributes = array('legend' => false);
                   echo $this->Form->radio('gender', $options, $attributes); ?>
                 </div>
               </div>
               <div class="row">
                <div class="col-md-2 col-sm-12">Address</div>
                <div class="col-md-10 col-sm-12">
                  <?php  echo $this->Form->input('address', array( 'class' => "form-control", 'empty' => '','placeholder'=>'E.g H-505 ABC avenue near silver plaza','title'=>'Please enter your address','label'=>false,)); ?>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-2 col-sm-12">City</div>
                <div class="col-md-10 col-sm-12 required">
                <?php  echo $this->Form->input('placefinder', array('div' => false, 'class' => "form-control", 'empty' => '','placeholder'=>'Type your city name','title'=>'Type your city name','label'=>false,'id'=>'placefinder')); ?>
                 
                  <div class="display_none">
                    <?php  echo $this->Form->input('country', array('type' =>'text' ,'id'=>'country',)); ?>
                    <?php  echo $this->Form->input('state', array('type' =>'text','id'=>'state')); ?>
                    <?php  echo $this->Form->input('city', array('type' =>'text','id'=>'city',)); ?>
                    <?php  echo $this->Form->input('place_id', array('type' =>'text','id'=>'place_id',)); ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-2 col-sm-12">Contact</div>
                <?php  echo $this->Form->input('contact', array('div' => array('class' => "col-md-10 col-sm-12"), 'class' => "form-control", 'empty' => '','placeholder'=>'E.g 9876543210','title'=>'Please enter your contact number','label'=>false)); ?>
              </div>
              <div class="row">
                <div class="col-md-2 col-sm-12">Newsletter</div>
                <?php echo $this->Form->input('newsLatter', array('div' => array('class' => "col-md-10 col-sm-12 checkbox"), 'type'=>'checkbox','empty' => '','label'=>'Yes! I want to receive newsletter.','title'=>'Yes! I want to receive newsletter')); ?>
              </div>

              <div class="row profile-pic">
               <div class="col-md-2 col-sm-12">Profile picture</div>
               <div class="col-md-10 col-sm-12">
               <div class="row col-md-4 col-sm-12">
                <img src="<?php echo $profilePic; ?>" alt="<?php echo $userData['CustomerProfile']['name']; ?>" title="<?php echo $userData['CustomerProfile']['name']; ?>">
                 <?php
                echo $this->Form->input('other.profile_pic', array('div' => array('class' => ""), 'type' => "file", 'label' => false));
                ?> 
              </div>
              <div class="col-md-8 col-sm-12 profile-pic-view">
                <div id="uploadCropPreview" style="display: none"></div>
                <div id="uploadCropPreview-hint" style="display: none">Please crop your image by zooming or moving it.</div>
                <div class="" style="display: none">
                  <?php
                  echo $this->Form->input('cropImage.imagebase64', array('id' => "imagebase64", 'type' => 'text'));
                  ?>
                </div>
              </div>
            </div>
          </div>
              <div class="clearfix">&nbsp;</div>
              <div class="clearfix">&nbsp;</div>  
              <div class="col-md-4 col-sm-12">
              <button type="button" class="btn btn-success btn-block upload-result" id="upload-result" >Save</button>     
              </div>     
              <?php
              //echo $this->Form->input('Save', array('label' => false, 'div' => array('class' => "col-md-4 col-sm-12"), 'type' => "submit", 'class' => "btn btn-success btn-block"));
              ?>
              <?php echo $this->Form->end(); ?>
            </div>
          </div>   
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
