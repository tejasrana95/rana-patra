<header class="header">
    <nav id="nav" class="navbar <?php echo (isset($navClass)) ? $navClass : ""; ?> navbar-default navbar-fixed-top" role="navigation">
        <div class="container relative-nav-container">
            <a class="toggle-button visible-xs-block" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-navicon"></i>
            </a>
            <a class="navbar-brand scroll" href="<?php echo Router::url('/', true); ?>">
                <img class="normal-logo hidden-xs" src="<?php echo Router::url('/', true); ?>images/rana-Community.png" alt="logo" width="40" height="44"/>
                <img class="inverse-logo hidden-xs" src="<?php echo Router::url('/', true); ?>images/rana-Community.png" alt="logo" width="40" height="44"/>
                <img class="visible-xs-block" src="<?php echo Router::url('/', true); ?>images/rana-Community.png" width="40" height="44" alt="logo"/>
            </a>
            <ul class="nav navbar-nav navbar-right nav-icons wrap-user-control">
                <li><a id="search-open" href="#fakelink"><i class="fa fa-search"></i></a></li>
                <li class="open-signup">
                 <?php if($loggedIn): ?>
                    <a href="javascript:void(1);"><i class="fa fa-user"></i></a>
                <?php else :  ?>
                    <a href="/login"><i class="fa fa-user"></i></a>
                <?php endif; ?>
                <div class="wrap-login-signup">
                    <?php if($loggedIn): ?>
                        <div class="signup-content">
                            <h5 class="user-profile-name">Hello, <?php echo $userData['CustomerProfile']['name'] ?> </h5>
                            <?php 
                            echo $this->Html->link('<i class="fa fa-home"></i> Dashboard',  '/dashboard', array('class' => 'btn btn-default btn-sm','escape' => false));
                            echo $this->Html->link('<i class="fa fa-eye"></i> View Profile',  array('controller' => 'profile', 'action' => 'index'), array('class' => 'btn btn-default btn-sm','escape' => false));
                            echo $this->Html->link('<i class="fa fa-pencil"></i> Edit Profile',  array('controller' => 'profile', 'action' => 'edit'), array('class' => 'btn btn-primary btn-sm','escape' => false));
                            echo $this->Html->link('<i class="fa fa-key"></i> Change Password',  array('controller' => 'profile', 'action' => 'password'), array('class' => 'btn btn-info btn-sm','escape' => false));
                            echo $this->Html->link('<i class="fa fa-credit-card"></i> Subscription',  array('controller' => 'profile', 'action' => 'subscription'), array('class' => 'btn btn-success btn-sm','escape' => false));
                            ?>
                          
                            <span class="or-sep">or</span>   
                            <?php 
                             echo $this->Html->link('<i class="fa fa-sign-out"></i> Logout',  array('controller' => 'user', 'action' => 'logout'), array('class' => 'btn btn-danger btn-sm','escape' => false));
                             ?>                                     
                        </div>
                    <?php else :  ?>
                        <div class="signup-content">
                            <h5>Quick Login</h5>
                            <a onclick="window.open('<?php echo $facebookUrl; ?>', '_blank', 'location=yes,height=370,width=620,scrollbars=yes,status=yes')" class="btn btn-block btn-social btn-sm btn-facebook">
                                <span class="fa fa-facebook"></span> Sign in with Facebook
                            </a>
                            <a onclick="window.open('<?php echo $googleUrl; ?>', '_blank', 'location=yes,height=370,width=620,scrollbars=yes,status=yes')" class="btn btn-block btn-social btn-sm btn-google">
                                <span class="fa fa-google"></span> Sign in with Google
                            </a>
                            <span class="or-sep">or</span>                                        
                            <a href="/login" class="btn btn-success btn-sm">Login with existing account</a>
                            <a href="/login/register" class="btn btn-primary btn-sm">Create new account</a>
                        </div>
                    <?php endif; ?>
                    </div>
                </li>
            </ul>


            <?php
echo $this->requestAction(array(
    'controller' => 'menus',
    'action'     => 'build_nav',
    "header-menu",
), array('return'));
?>

        </div>
        </div>
        <div class="navbar-search">
            <div class="container">
                <form action="<?php echo Router::url('/', true); ?>search/" method="get" autocomplete="on">
                    <div class="input-group">


                        <input id="search" name="search" placeholder="Type your search..." type="text" class="form-control" autocomplete="off">
                        <span class="input-group-btn">
                            <button type="reset" class="btn search-close" id="search-close">
                                <i class="fa fa-close"></i>
                            </button>
                        </span>

                    </div>
                </form>
            </div>
        </div>
    </nav>
</header><!--/.header -->