<section class="contacts">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <article class="contacts-block bottom-border">
                    <h5>Contacts</h5>
                    <ul>
                        <li>
                            <p><i class="fa fa-map-marker"></i><?php echo @$option['address']; ?></p>
                        </li>
                        <li>
                            <p><i class="fa fa-phone"></i><?php echo @$option['telephone']; ?></p>
                        </li>
                        <li>
                            <p><i class="fa fa-envelope"></i><a href="mailto:<?php echo @$option['contact_us_id']; ?>"><?php echo @$option['contact_us_id']; ?></a> </p>
                        </li>
                    </ul>
                </article>
            </div>
            <div class="col-md-4">
                <article class="twitter-block bottom-border">
                    <h5 class="offset">Twitter</h5>
                    <div class="twitter-wrap offset left-border">
                        <div class="tweets-list-container tweets-list-container-with-logo logo twitter-demo"></div>
                    </div>
                </article>
            </div>
            <div class="col-md-4">
                <article class="subscribe-info">
                    <h5 class="offset">Subscribe</h5>
                    <div class="subscribe-wrap offset left-border">
                        <form>
                            <label for="contacts-email">Be aware of news</label>
                            <div class="form-group has-feedback"><input type="email" class="form-control " id="contacts-email" placeholder="Enter email"><i class="fa fa-pencil-square fa-lg form-control-feedback"></i></div>
                        </form>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>
<section class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p><?php echo @$option['copyright_text']; ?></p>
            </div>
            <div class="col-md-6">
                <ul class="copyright-links list-inline">
                    <li><a href="<?php echo @$option['facebook_link']; ?>"><i class="fa fa-facebook fa-lg"></i></a></li>
                    <li><a href="<?php echo @$option['twitter_link']; ?>"><i class="fa fa-twitter fa-lg"></i></a></li>
                    <li><a href="<?php echo @$option['youtube_link']; ?>"><i class="fa fa-youtube-play fa-lg"></i></a></li>
                    <li><a href="<?php echo @$option['google_plus_link']; ?>"><i class="fa fa-google-plus fa-lg"></i></a></li>
                    <li><a href="<?php echo @$option['linkedin_link']; ?>"><i class="fa fa-linkedin fa-lg"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
