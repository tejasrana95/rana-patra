<head>
    <title><?php echo @$title_for_layout; ?></title><meta name="description" content="<?php echo (isset($description_for_layout) && !empty($description_for_layout) ? $description_for_layout : '') ?>" /><meta name="keywords" content="<?php echo (isset($keywords_for_layout) && !empty($keywords_for_layout) ? $keywords_for_layout : '') ?>" /><?php echo $this->Html->charset(); ?>
    <?php
    echo $this->Html->meta('icon');
    echo $this->Html->css('imageCrop/croppie');
    echo $this->Html->css('bootstrap-social');
    echo $this->Html->css('flexslider');
    echo $this->Html->css('bootstrap-datetimepicker.min');
    echo $this->Html->css('master');
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <script>
        var base_url = "<?php echo Router::url('/', true); ?>";
    </script>
</head>