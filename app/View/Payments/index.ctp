<?php
 App::import('Controller', 'Common');
 $CommonController = new CommonController;
 $this->Html->script(array('money-exchange/money.min'), array('block' => 'scriptBottom'));
 echo $this->Html->scriptBlock(
    'var amount = '.json_encode($amount).'',
    array('inline' => false,'block' => 'scriptBlock')
 );
?>
<div class="no-header-image login-overlay container">
<?php echo $this->element('breadcrumb'); ?> 
    <div class="col-md-12">
        <div class="row">
            <div class="header col-md-12 col-sm-12">
                <h5 class="" id=""><?php echo $title_for_layout ?></h5>
            </div>
            <div class="col-sm-12 col-md-12">
            <?php echo $this->Session->flash(); ?>
                <?php echo $this->Form->create('CustomerInvoice', array('url'=>'/payments/checkout','onsubmit'=>'return validateCheckout();')); ?>
                <div class="row">
                    <fieldset class="col-md-6 col-sm-12">
                        <legend class="">Subscription / Payment Type</legend>
                        <?php
                            echo $this->Form->input('item_type', array('options' => $item_type,'empty' => "Select Subscription/Payment Type",'div' => array('class' => "form-group"),'class' => "form-control",'required' => 'required'));
                            echo $this->Form->input('item_name', array('options' =>$item_name,'empty' => "Select Subscription/Payment Sub-Type",'div' => array('class' => "form-group"),'class' => "form-control",'required' => 'required','onchange'=>'fetchPrice();' ,'id'=>'item_name'));
                            echo $this->Form->input('no_of_months', array('options' => array_combine(range(1, 12), range(1, 12))
                            ,'empty' => "Select Month",'div' => array('class' => "form-group"),'class' => "form-control",'required' => 'required','id'=>'no_of_month','onchange'=>'fetchPrice();' )); ?><?php
                            echo $this->Form->input('currency_code', array('options' => $currency,'empty' => "Select Currency type",'label'=>'Currency','div' => array('class' => "form-group"),'class' => "form-control",'required' => 'required','id'=>'mc_currency','onchange'=>'fetchPrice();')); ?>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-addon" id="currencyCode">INR</div>
                                            <?php  echo $this->Form->input('mc_gross', array('div' => false,'label'=>false, 'class' => "form-control",'required' => 'required','id'=>'mc_gross','disabled' => 'disabled')); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-addon" id="">INR</div>
                                            <?php echo $this->Form->input('actual_amount_inr', array('div' => false,'label'=>false, 'class' => "form-control",'disabled' => 'disabled','id'=>"actual_amount_inr")); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php  echo $this->Form->input('pay_with', array('options' => $payment_gateway,'empty' => "Pay with",'div' => array('class' => "form-group"),'class' => "form-control",'required' => 'required','id'=>"payment-gateway")); ?>
                    </fieldset> 
                    <fieldset class="col-md-6 col-sm-12">
                        <legend class="">Personal Information</legend>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <?php echo $this->Form->input('first_name', array('div' => false, 'class' => "form-control",'required' => 'required')); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                        <?php echo $this->Form->input('last_name', array('div' => false, 'class' => "form-control",'required' => 'required')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                            echo $this->Form->input('payer_email', array('div' => array('class' => "form-group"),'type'=>'email', 'class' => "form-control",'required' => 'required'));
                            echo $this->Form->input('address', array('div' => array('class' => "form-group"),'type'=>'textarea','rows'=>3, 'class' => "form-control",'required' => 'required'));
                            echo $this->Form->input('zip', array('div' => array('class' => "form-group"),'type'=>'text', 'class' => "form-control",'required' => 'required'));
                            echo $this->Form->input('placefinder', array('type' =>'text','div' => array('class' => ""), 'class' => "form-control",'empty' => 'City','title'=>'Type your city name','label'=>'City','id'=>'placefinder')); ?>
                            <div class="display_none">
                                <?php  echo $this->Form->input('country', array('type' =>'text' ,'id'=>'country',)); ?>
                                <?php  echo $this->Form->input('state', array('type' =>'text','id'=>'state')); ?>
                                <?php  echo $this->Form->input('city', array('type' =>'text','id'=>'city',)); ?>
                                <?php  echo $this->Form->input('place_id', array('type' =>'text','id'=>'place_id',)); ?>
                            </div>
                    </fieldset>               
                </div>
                <?php echo $this->Form->input('Proceed >>', array('label' => false, 'div' => array('class' => "text-right"), 'type' => "submit", 'class' => "btn btn-primary")); ?>
                <div class="text-right">
                    <?php echo _("By clicking on proceed you agree with the terms and conditions."); ?>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>