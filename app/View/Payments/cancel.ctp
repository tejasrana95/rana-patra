<?php
?>
<div class="no-header-image login-overlay container">
<?php echo $this->element('breadcrumb'); ?> 
    <div class="col-md-12">
        <div class="row">
            <div class="header col-md-12 col-sm-12">
                <h5 class="" id="">&nbsp;</h5>
            </div>
            <div class="col-sm-12 col-md-12">
            <?php echo $this->Session->flash(); ?>
                <div class="row ">
                    <div class="text-center"><h3>Transaction cancelled by user.</h3><em>If you are facing any issue. Please, contact to our Customer Support Team.</em></div>
                    <?php if (isset($reason)) : ?>
                        <div class="text-cetn"><em><?php echo $reason; ?></em></div>
                    <?php endif; ?>
                </div>
                
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
