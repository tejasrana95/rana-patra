<?php
 App::import('Controller', 'Common');
 $CommonController = new CommonController;
 $this->Html->script(array('money-exchange/money.min'), array('block' => 'scriptBottom'));
 echo $this->Html->scriptBlock(
    "setTimeout(function() {
        $('#autosubmit').trigger('click');
    }, 5);
    ",
    array('inline' => false,'block' => 'scriptBlock')
 );
?>
<div class="no-header-image login-overlay container">
<?php echo $this->element('breadcrumb'); ?> 
    <div class="col-md-12">
        <div class="row">
            <div class="header col-md-12 col-sm-12">
                <h5 class="" id="">&nbsp;</h5>
            </div>
            <div class="col-sm-12 col-md-12">
            <?php echo $this->Session->flash(); ?>
                <div class="row ">
                <div class="text-center"><h3>Please wait. Redirecting you to PayTM gateway.</h3><em>(Do not close this browser or press back.)</em></div>
                <div class="display_none">
                <?php echo $this->Form->create('CustomerInvoice', array('url'=>PAYTM_TXN_URL)); ?>
                    <fieldset class="col-md-12 col-sm-12">
                        <input type="text" name="MID" value="<?php echo PAYTM_MERCHANT_MID; ?>">
                        <input type="text" name="WEBSITE" value="<?php echo PAYTM_MERCHANT_WEBSITE; ?>">
                       
                        <input type="text" name="CHANNEL_ID" value="WEB">
                        <input type="text" name="INDUSTRY_TYPE_ID" value="Retail">
                        <input type="text" name="TXN_AMOUNT" value="<?php echo $this->request->data['CustomerInvoice']['actual_amount_inr']; ?>">
                       
                        <input type="text" name="CUST_ID" value="<?php echo $this->request->data['CustomerInvoice']['userID']; ?>">
                        <input type="text" name="ORDER_ID" value="<?php echo $this->request->data['CustomerInvoice']['item_number']; ?>">
                        <input type="hidden" name="CHECKSUMHASH" value="<?php echo $this->request->data['CustomerInvoice']['checkSum']; ?>">
                    </fieldset>     
                        <?php echo $this->Form->input('Proceed >>', array('label' => false, 'div' => array('class' => "text-right"), 'type' => "submit",'id'=>'autosubmit', 'class' => "btn btn-primary ")); ?>         
                    <?php echo $this->Form->end(); ?>          
                </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>