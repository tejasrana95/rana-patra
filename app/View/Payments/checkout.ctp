<?php
 App::import('Controller', 'Common');
 $CommonController = new CommonController;
 $this->Html->script(array('money-exchange/money.min'), array('block' => 'scriptBottom'));
 echo $this->Html->scriptBlock(
    "setTimeout(function() {
        $('#autosubmit').trigger('click');
    }, 5);
    ",
    array('inline' => false,'block' => 'scriptBlock')
 );
?>
<div class="no-header-image login-overlay container">
<?php echo $this->element('breadcrumb'); ?> 
    <div class="col-md-12">
        <div class="row">
            <div class="header col-md-12 col-sm-12">
                <h5 class="" id="">&nbsp;</h5>
            </div>
            <div class="col-sm-12 col-md-12">
            <?php echo $this->Session->flash(); ?>
                <div class="row ">
                <div class="text-center"><h3>Please wait. Redirecting you to payment gateway.</h3><em>(Do not close this browser or press back.)</em></div>
                <div class="display_none">
                <?php echo $this->Form->create('CustomerInvoice', array('url'=>$option['paypal_link'])); ?>
                    <fieldset class="col-md-12 col-sm-12">
                        <input type="text" name="business" value="<?php echo $option['paypal']; ?>">
                        <input type="text" name="cmd" value="_xclick">
                        <input type='text' name='cancel_return' value='<?php echo $option['site_url'] ?>payments/cancel/<?php echo $this->request->data['CustomerInvoice']['lastID']; ?>'>
                        <input type='text' name='return' value='<?php echo $option['site_url'] ?>payments/success/<?php echo $this->request->data['CustomerInvoice']['lastID']; ?>'>
                        <input type='text' name='notify_url' value='<?php echo $option['site_url'] ?>payments/ipn/<?php echo $this->request->data['CustomerInvoice']['lastID']; ?>'>            
                        <input type="text" name="item_name" value="<?php echo $this->request->data['CustomerInvoice']['item_name']; ?>">
                        <input type="text" name="item_number" value="<?php echo $this->request->data['CustomerInvoice']['item_number']; ?>">
                        <input type="text" name="amount" value="<?php echo $this->request->data['CustomerInvoice']['amount']; ?>">
                        <input type="text" name="currency_code" value="<?php echo $this->request->data['CustomerInvoice']['currency_code']; ?>">
                        <input type="text" name="address_override" value="1">
                        <input type="text" name="first_name" value="<?php echo $this->request->data['CustomerInvoice']['first_name']; ?>">
                        <input type="text" name="last_name" value="<?php echo $this->request->data['CustomerInvoice']['last_name']; ?>">
                        <input type="text" name="address1" value="<?php echo $this->request->data['CustomerInvoice']['address']; ?>">
                        <input type="text" name="city" value="<?php echo $this->request->data['CustomerInvoice']['city']; ?>">
                        <input type="text" name="state" value="<?php echo $this->request->data['CustomerInvoice']['state']; ?>">
                        <input type="text" name="country" value="<?php echo $this->request->data['CustomerInvoice']['country']; ?>">
                        <input type="text" name="payer_email" value="<?php echo $this->request->data['CustomerInvoice']['payer_email']; ?>">
                        <input type="text" name="zip" value="<?php echo $this->request->data['CustomerInvoice']['zip']; ?>">
                    </fieldset>     
                        <?php echo $this->Form->input('Proceed >>', array('label' => false, 'div' => array('class' => "text-right"), 'type' => "submit",'id'=>'autosubmit', 'class' => "btn btn-primary ")); ?>         
                    <?php echo $this->Form->end(); ?>          
                </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>