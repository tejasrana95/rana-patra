<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');
App::import('Controller', 'Common');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array();
    public $components = array('Paginator', 'Security');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
        );

    function beforeRender() {
        parent::beforeRender();
    }

    public function beforeFilter() {
        parent::beforeFilter();

        // Allow
    }

    /**
     * Displays a view
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     * 	or MissingViewException in debug mode.
     */
    public function index($page_slug = '404') {
        $commonController = new CommonController();
        $this->loadModel('Media');
        if (!isset($page_slug)) {
           throw new NotFoundException();
        }
        // getting page detail by slug using cakephp findBy<fieldname> method
            $condition = array('Page.page_slug' => $page_slug, 'Page.status' => INFI_ACTIVE_STATUS);
       
        $page_detail = $this->Page->find('first', array('conditions' => $condition));
      
       
        if (!empty($page_detail)) {
             $this->layout = $page_detail['Page']['layout'];
            // set to detail to view render
            //setting page title for layout

            if (!empty(trim($page_detail['Page']['page_title']))) {
                $this->set('title_for_layout', $page_detail['Page']['page_title']);
            } else {
                $this->set('title_for_layout', $page_detail['Page']['page_name']);
            }
            $this->set('keywords_for_layout', $page_detail['Page']['page_keywords']);
            $this->set('description_for_layout', trim($page_detail['Page']['page_description']));

            $this->set('widgets', unserialize($page_detail['Page']['widgets']));
            if (!empty($page_detail['Page']['content'])) {

                $replaceContent = $commonController->magicKeyword($page_detail['Page']['content'], $page_detail['Page']['category'], $page_detail['Page']['sub_category'], $page_detail['Page']['id']);
                $page_detail['Page']['content'] = $replaceContent;
            }
            if (!empty($page_detail['Page']['banner_text'])) {

                $replaceContent = $commonController->magicKeyword($page_detail['Page']['banner_text']);
                $page_detail['Page']['banner_text'] = $replaceContent;
            }

            $mediaFile = $this->Media->find('first', array('conditions' => array('Media.media_id' => $page_detail['Page']['id'])));

            //breadcrum detail
            $this->set('page_slug', $page_slug);
            $this->set('main_category', $page_detail['Page']['main_category']);
            $this->set('category', $page_detail['Page']['category']);
            $this->set('sub_category', $page_detail['Page']['sub_category']);

            //page
            $this->set('layout', $page_detail['Page']['layout']);
            $this->set('media_file', $mediaFile);
            $this->set('page_detail', $page_detail);
            $this->set('currentpage', $page_slug);
            $this->set('navbarClass',array());
        } else {
            throw new NotFoundException();
        }
    }

    public function display() {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = $title_for_layout = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (!empty($path[$count - 1])) {
            $title_for_layout = Inflector::humanize($path[$count - 1]);
        }
        $this->set(compact('page', 'subpage', 'title_for_layout'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingViewException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }

}
