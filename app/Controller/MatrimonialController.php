<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'Common');

/**
 * Matrimonial Controller
 *
 * @property Article $Article
 * @property PaginatorComponent $Paginator
 */
class MatrimonialController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Auth', 'RequestHandler','Security','Paginator');
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Security->unlockedActions = array('uploadImages');
        
        $this->layout = 'matrimonial_layout';
    }

    public $uses = array('Matrimonial', 'MatrimonialAttachment');

    public function index()
    {
        $this->set('page_slug', 'matrimonial');
        $this->set('keywords_for_layout', 'Search / Browse Matrimonials');
        $this->set('description_for_layout', 'Search / Browse Matrimonial');
        $this->set('title_for_layout', 'Search / Browse Matrimonial');
        $this->set('navbarClass', array('navClass' => 'white-navbar'));
        $query=array();
        if ($this->request->is(array('get'))) {
            $query=$this->request->query;
            $this->request->data['MatrimonialSearch']=$query;
        }
        $this->Paginator->settings = $this->paginate;
      

        $condition=array('Matrimonial.status' => 1);
        if (!empty($query)) {
            foreach ($query as $key => $value) {
                if ($key=="name" && !empty(trim($value))) {
                    $condition['OR']=array('Matrimonial.first_name LIKE'=>'%'.$value.'%','Matrimonial.last_name LIKE'=>'%'.$value.'%',);
                }
                if (!empty(trim($value)) && $key!="bornYearFrom" && $key!="bornYearTo" &&  $key!="name") {
                    $condition['AND']['Matrimonial.'.$key]=$value;
                }
            }
            if (isset($query['bornYearFrom']) && !empty(trim($query['bornYearFrom'])) && isset($query['bornYearTo']) && !empty(trim($query['bornYearTo']))) {
                 $condition['Matrimonial.dob >= ']=$query['bornYearFrom']."-01-01";
                 $condition['Matrimonial.dob <= ']=$query['bornYearTo']."-12-31";
            } elseif (isset($query['bornYearFrom']) && !empty(trim($query['bornYearFrom'])) && isset($query['bornYearTo']) && empty(trim($query['bornYearTo']))) {
                $condition['Matrimonial.dob >= ']=$query['bornYearFrom']."-01-01";
            } elseif (isset($query['bornYearFrom']) && empty(trim($query['bornYearFrom'])) && isset($query['bornYearTo']) && !empty(trim($query['bornYearTo']))) {
                $condition['Matrimonial.dob <= ']=$query['bornYearTo']."-12-31";
            }
        }
        $this->Matrimonial->hasMany['MatrimonialAttachment']['conditions'] = array('MatrimonialAttachment.file_type' => 'image');
        $options = array(
            'conditions' => $condition,
            'contain' => array('MatrimonialAttachment'),
            'limit' => 20,
            'order' => array(
                'Matrimonial.modified' => 'desc'
            )
        );
        $this->Paginator->settings = $options;
        $allMatrimonials = $this->Paginator->paginate('Matrimonial');
        //if user search with profile id and we got only one result then we directly redirecting user to that profile instead of showing single result.
        if (isset($query['profileID']) && !empty(trim($query['profileID'])) && count($allMatrimonials)==1){
            $temp=explode('-',$allMatrimonials[0]['Matrimonial']['profileID']);
            $matId=$temp[count($temp)-1];
            unset($temp[count($temp)-1]);
            $slug=implode('-',$temp);
            $this->redirect('/matrimonial/'.$slug.'/'.$matId);
        }
        $this->request->data['matrimonials'] = $allMatrimonials;
        $this->set('breadcrumb',array('/matrimonial'=>'Matrimonials'));
    }

    public function myview()
    {
        $userId = $this->Auth->user('id');
        $this->set('page_slug', 'matrimonial-myview');
        $this->set('keywords_for_layout', 'My Matrimonials - List of all matrimonials which created by me');
        $this->set('description_for_layout', 'My Matrimonials - List of all matrimonials which created by me');
        $this->set('title_for_layout', 'My Matrimonials - List of all matrimonials which created by me');
        $this->set('navbarClass', array('navClass' => 'white-navbar'));

        $allMatrimonials = $this->Matrimonial->getAllMatrimonials($userId, false, 'image');
        $this->request->data['matrimonials'] = $allMatrimonials;
        $this->set('breadcrumb',array('/matrimonial'=>'Matrimonials','/matrimonial/myview'=>'My Matrimonials'));
    }

    public function view($username, $mid)
    {
        $userId = $this->Auth->user('id');
        $this->set('page_slug', 'matrimonial-view');
        $allMatrimonials = $this->Matrimonial->viewMatrimonial($mid, 0);
        if (!isset($allMatrimonials['Matrimonial']['id']) || !$this->Matrimonial->exists($allMatrimonials['Matrimonial']['id'])) {
            throw new NotFoundException(__('Invalid page'));
        }
        $this->set('keywords_for_layout', 'View matrimonial profile of ' . $allMatrimonials['Matrimonial']['bride_groom'] . ' ' . $allMatrimonials['Matrimonial']['first_name'] . ' ' . $allMatrimonials['Matrimonial']['last_name']);
        $this->set('description_for_layout', 'View matrimonial profile of ' . $allMatrimonials['Matrimonial']['bride_groom'] . ' ' . $allMatrimonials['Matrimonial']['first_name'] . ' ' . $allMatrimonials['Matrimonial']['last_name']);
        $this->set('title_for_layout', 'View matrimonial profile of ' . $allMatrimonials['Matrimonial']['bride_groom'] . ' ' . $allMatrimonials['Matrimonial']['first_name'] . ' ' . $allMatrimonials['Matrimonial']['last_name']);
        $this->set('navbarClass', array('navClass' => 'white-navbar'));
        $this->request->data = $allMatrimonials;
        $showContactTab=false;

        $commonController=new CommonController();
        $isPremium=$commonController->checkPremiumMember($userId);
        
        if($allMatrimonials['Matrimonial']['userID']==$userId || $isPremium==true){
            $showContactTab=true;
        }
        $this->set('showContactTab', $showContactTab);
        //similar
        $similarMatrimonial = $this->Matrimonial->similarMatrimonial($allMatrimonials, 5, 'image');
        $this->request->data['similarProfile'] = $similarMatrimonial;
        $this->set('breadcrumb',array('/matrimonial'=>'Matrimonials',''=>$allMatrimonials['Matrimonial']['first_name'] . ' ' . $allMatrimonials['Matrimonial']['last_name']));
    }
    
    public function create()
    {

        $userId = $this->Auth->user('id');
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Matrimonial']['userID'] = $userId;
            $lastId=$this->Matrimonial->save($this->request->data);
            if ($lastId) {
                $this->Session->setFlash(__('Your matrimonial has been added successfully.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "edit/".$lastId['Matrimonial']['id']));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $this->set('page_slug', 'matrimonial-add');
        $this->set('keywords_for_layout', 'Create matrimonial, add new matrimonial');
        $this->set('description_for_layout', 'Create matrimonial, add new matrimonial');
        $this->set('title_for_layout', 'Add new matrimonial');
        $this->set('navbarClass', array('navClass' => 'white-navbar'));
        $this->set('breadcrumb',array('/matrimonial'=>'Matrimonials',''=>'Add new matrimonial'));
    }
    public function edit($id)
    {
            $userId = $this->Auth->user('id');
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Matrimonial']['userID'] = $userId;
            $lastId=$this->Matrimonial->save($this->request->data);
            if ($lastId) {
                $this->Session->setFlash(__('Your matrimonial has been added successfully.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "edit/".$lastId['Matrimonial']['id']));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
            $this->request->data=$this->Matrimonial->viewMatrimonial($id, $userId);
           
        if (!isset($this->request->data) || empty( $this->request->data)) {
            throw new NotFoundException(__('Invalid page'));
        }
            $this->request->data['MatrimonialAttachment']['matrimonial_id']=$this->request->data['Matrimonial']['id'];
            $this->set('page_slug', 'matrimonial-add');
            $this->set('keywords_for_layout', 'Edit matrimonial');
            $this->set('description_for_layout', 'Edit matrimonial');
            $this->set('title_for_layout', 'Edit matrimonial');
            $this->set('navbarClass', array('navClass' => 'white-navbar'));
            $this->set('breadcrumb',array('/matrimonial'=>'Matrimonials',''=>'Edit matrimonial'));
    }

    public function uploadImages()
    {
        $userId = $this->Auth->user('id');
        $this->layout ="";
        if (in_array($this->action, array("post"))) {
            $this->Security->validatePost = false;
        }
        if ($this->request->is(array('post', 'put')) && isset($_FILES) && !empty($_FILES) && isset($this->request->data['MatrimonialAttachment']['matrimonial_id']) && isset($this->request->data['MatrimonialAttachment']['file_type'])) {
            $commonController = new CommonController();
            $options=$commonController->getOptionValue();
            $this->request->data['MatrimonialAttachment']['file']=$_FILES['file'];
            $this->request->data['MatrimonialAttachment']['file_name']=$_FILES['file']['name'];
            $ext = strtolower(pathinfo($this->request->data['MatrimonialAttachment']['file']['name'], PATHINFO_EXTENSION));
                
            $imagePath = $this->request->data['MatrimonialAttachment']['file']['name'] . '-' . uniqid() . '.' . $ext;
            $filePath=$options['matrimonialImages'];
            if ($this->request->data['MatrimonialAttachment']['file_type']=='kundali') {
                $filePath=$options['matrimonialKundali'];
            }
            $path = $filePath .'/'. $imagePath;
            try {
                @move_uploaded_file($this->request->data['MatrimonialAttachment']['file']['tmp_name'], $path);
                $this->request->data['MatrimonialAttachment']['file_path'] = $imagePath;
                if ($this->MatrimonialAttachment->save($this->request->data)) {
                    $output=array('previewElement'=>true);
                } else {
                    $output=array('_removeLink'=>true);
                }
            } catch (Exception $e) {
                $output=array('_removeLink'=>$e->getMessage());
            }
        }
        $this->set('output', $output);
    }
    function loadImages($mid, $file_type = "image")
    {
        $userId = $this->Auth->user('id');
        $this->layout ="";
        $attachmentData=$this->MatrimonialAttachment->getAttachments($mid, $file_type);
        $this->set('attachmentData', $attachmentData);
    }
    function deleteImages($mid, $file_id)
    {
        $userId = $this->Auth->user('id');
        $this->layout ="";
        $commonController = new CommonController();
        $options=$commonController->getOptionValue();
        $attachmentData=$this->Matrimonial->viewMatrimonial($mid, $userId);

        if (isset($attachmentData['Matrimonial']['userID']) && $attachmentData['Matrimonial']['userID']==$userId) {
            $fileInfo = $this->MatrimonialAttachment->find('first', array('conditions' => array('MatrimonialAttachment.id'=>$file_id)));
            $filePath=$options['matrimonialImages'];
            if ($fileInfo['MatrimonialAttachment']['file_type']=='kundali') {
                $filePath=$options['matrimonialKundali'];
            }
            if (file_exists($filePath  .'/'. $fileInfo['MatrimonialAttachment']['file_path'])) {
                @unlink($filePath  .'/'. $fileInfo['MatrimonialAttachment']['file_path']);
                @unlink($filePath  .'/thumbs/'. $fileInfo['MatrimonialAttachment']['file_path']);
            }
            $this->MatrimonialAttachment->id = $file_id;
            $this->MatrimonialAttachment->delete();
            exit();
        }
    }
}
