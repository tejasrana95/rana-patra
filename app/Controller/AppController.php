<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('CakeTime', 'Utility');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package             app.Controller
 * @link                http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $helpers = array(
        'Cache',
        'Html',
        'Form',
        'Js',
        'Session',
        'Time',
        'Text');
  
public $components = array(
        'Session',
        'RequestHandler',
        //'DebugKit.Toolbar',
        'Security' => array(
            'csrfExpires' => '+1 hour'
        )
    );

    function beforeRender() {
            //error page
            if ($this->name == 'CakeError') {
                $this->layout = 'error_layout';
            }
            parent::beforeRender();


            $front_layout = array(
                'with_left_bar' => "With left side bar",
                'full_width_layout' => 'Full width Layout',
                'landing_page' => 'Landing Page',
                'history_page' => 'History Page',
                'partner_speak' => 'Partner Speak',
                'parallax_layout' => 'Parallax layout',
                'investor_layout' => 'Investor layout',
                );
            $this->set('front_layout', $front_layout);

            $item_type = array(
                '1' => "Matrimonial",
                // '2' => 'Donate',
                // '3' => 'Gift Voucher',
                // '4' => 'Other',
                );
            $this->set('item_type', $item_type);
            
            $item_name = array(
                'PRO-MEMBER' => "PREMIUM MEMBERSHIP",
                'MATRIMONIAL-AD' => 'MATRIMONIAL AD',
                /*'PRO-MEMBER' => 'PREMIUM MEMBERSHIP',
                'ADVERTISEMENT' => 'ADVERTISEMET',
                'DONATE'=>'DONATE',
                'GIFT-VOUCHER'=>'GIFT-VOUCHER',
                'OTHER'=>'OTHER'*/
                );
            $this->set('item_name', $item_name);

            $amount=array('PRO-MEMBER'=>500,'MATRIMONIAL-AD'=>250);
            $this->set('amount',$amount);

            $process_by = array(
                '0' => "Self",
                '1' => 'Agent',
                '2' => 'Auto',
                '3' => 'Other',
                );
            $this->set('process_by', $process_by);

            $currency = array(
                'INR' => 'India Rupee',
                'USD' => 'United States Dollar',
                'CAD' => 'Canada Dollar',
                'GBP' => 'United Kingdom Pound',
                'JPY' => 'Japan Yen',
                'AUD' => 'Australia Dollar',
                'NZD' => 'New Zealand Dollar'
                );
    $this->set('currency', $currency);

    $brideGroom = array(
        'Bride' => "Bride",
        'Groom' => 'Groom',
        );
    $this->set('brideGroom', $brideGroom);

    $marital_status = array(
        'Single' => "Single",
        'Widowed' => 'Widowed',
        'Separated' => 'Separated',
        'Divorced' => 'Divorced',
        );
    $this->set('marital_status', $marital_status);

    $rashi = array(
        'Aries' => "Mesh (Aries)",
        'Taurus' => "Vrushabh (Taurus)",
        'Gemini' => "Mithun (Gemini)",
        'Cancer' => "Kark (Cancer)",
        'Leo' => "Sinh (Leo)",
        'Virgo' => "Kanya (Virgo)",
        'Libra' => "Tula (Libra)",
        'Scorpius' => "Vrushchik (Scorpius)",
        'Sagittarius' => "Dhanu (Sagittarius)",
        'Capricornus' => "Makar (Capricornus)",
        'Aquarius' => "Kumbha (Aquarius)",
        'Pisces' => "Meen (Pisces)",
        );
    $this->set('rashi', $rashi);

    $yesNo = array(
        '0' => "No",
        '1' => "Yes",
        );
    $this->set('yesNo', $yesNo);

    $employement = array(
            '0' => "Unemployment",
            '1' => "Study is going on",
            '2' => "Salaried Person",
            '3' => "Businessman",
            '4' => "Intern",
            '5' => "Freelancer",
            '6' => "Self Employment",
            '7' => "Other",
        );
    $this->set('employement', $employement);

    $salary = array(
        'Less than 1,00,000' => "Less than 1,00,000",
        '1,00,000 - 1,50,000' => "1,00,000 - 1,50,000",
        '1,51,000 - 2,00,000' => "1,51,000 - 2,00,000",
        '2,00,000 - 2,50,000' => "2,00,000 - 2,50,000",
        '2,51,000 - 3,00,000' => "2,51,000 - 3,00,000",
        '3,00,000 - 3,50,000' => "3,00,000 - 3,50,000",
        '3,51,000 - 4,00,000' => "3,51,000 - 4,00,000",
        '4,00,000 - 4,50,000' => "4,00,000 - 4,50,000",
        '4,51,000 - 5,00,000' => "4,51,000 - 5,00,000",
        '5,00,000 - 5,50,000' => "5,00,000 - 5,50,000",
        '5,51,000 - 6,00,000' => "5,51,000 - 6,00,000",
        '6,00,000 - 6,50,000' => "6,00,000 - 6,50,000",
        '6,51,000 - 7,00,000' => "6,51,000 - 7,00,000",
        '7,00,000 - 7,50,000' => "7,00,000 - 7,50,000",
        '7,51,000 - 8,00,000' => "7,51,000 - 8,00,000",
        '8,00,000 - 8,50,000' => "8,00,000 - 8,50,000",
        '8,51,000 - 9,00,000' => "8,51,000 - 9,00,000",
        '9,00,000 - 9,50,000' => "9,00,000 - 9,50,000",
        '9,51,000 - 10,00,000' => "9,51,000 - 10,00,000",
        '10,00,000 and Above' => "10,00,000 and Above",
        'Dont want to disclose' => "Dont want to disclose",
        );
    $this->set('salary', $salary);

    $bloodGroup = array(
        'A+' => "A+",
        'A-' => "A-",
        'B+' => "B+",
        'B-' => "B-",
        'AB+' => "AB+",
        'AB-' => "AB-",
        'O+' => "O+",
        'O-' => "O-",
        );
    $this->set('bloodGroup', $bloodGroup);

    $education = array(
        'DOCTORATE - PHD' => "DOCTORATE - PHD",
        'DOCTOR - MBBS' => "DOCTOR - MBBS",
        'CHARTERED ACCOUNT - CA' => "CHARTERED ACCOUNT - CA",
        'Architect (B Arch)' => "Architect (B Arch)",
        'POST GRADUATE' => "POST GRADUATE",
        'DEGREE - GRADUATION' => "DEGREE - GRADUATION",
        'CERTIFICATE COURSE' => "CERTIFICATE COURSE",
        'DIPLOMA - NON ENGINEERING' => "DIPLOMA - NON ENGINEERING",
        'DIPLOMA ENGINEER' => "DIPLOMA ENGINEER",
        '12TH PASS - HSC' => "12TH PASS - HSC",
        '10TH PASS - SSC' => "10TH PASS - SSC",
        'NON MATRIC' => "NON MATRIC",
        'None of these' => "None of these",
        );
    $this->set('education', $education);

    $article_layout = array(
        'standard_article_layout' => "Standard Article Layout",
        );
    $this->set('article_layout', $article_layout);

    $payment_gateway = array(
        'paypal' => "paypal", 'cash' => "cash",
        );
    $this->set('payment_gateway', $payment_gateway);

    $diet = array(
        'Pure Vegetarian' => 'Pure Vegetarian',
        'Non Vegetarian' =>'Non Vegetarian',
        'Occasionally Non Veg' =>'Occasionally Non Veg',
        'Don\'t want to disclose' =>'Don\'t want to disclose',
        );
    $this->set('diet', $diet);
    
    $drink_smoke = array(
        'Yes' => 'Yes',
        'No' =>'No',
        'Occasionally' =>'Occasionally',
        'Don\'t want to disclose' =>'Don\'t want to disclose',
        );
    $this->set('drink_smoke', $drink_smoke);
    
    $payment_gateway=array('paypal'=>"PayPal","paytm"=>"PayTM");
    $this->set("payment_gateway",$payment_gateway);
    $this->set('thumb_width', 100);
    $this->set('thumb_height', 100);
    $options = $this->Option->find('first', array('conditions' => array('Option.meta_key' => 'site_url')));
    $this->set('site_url', rtrim($options['Option']['meta_value'], '/') . '/');
            //user role]
     $this->set('site_name', "Rana Patra");
     $this->set('org_name', "Infi Technology");

}
    
    public function beforeFilter() {
        App::import('Controller', 'User');
        $userController=new UserController;
        $this->layout = "default";

        $this->loadModel('Option');
        $this->loadModel('Customer');
        $this->loadModel('CustomerProfile');
        $site_url = $this->Option->find('first', array(
            'conditions' => array('Option.meta_key' => "site_url")));
        $publicDir = $this->Option->find('first', array(
            'conditions' => array('Option.meta_key' => "publicDir")));
        $option = $this->Option->find('all');
        $newOption = array();
        foreach ($option as $opt) {
            $newOption[$opt['Option']['meta_key']] = $opt['Option']['meta_value'];
        }

        $public_dir = rtrim($publicDir['Option']['meta_value'], DS);
        $urlPostFix = explode(DS, $public_dir);
        $urlPostFix_result = $urlPostFix[count($urlPostFix) - 1];
        $siteurl = rtrim($site_url['Option']['meta_value'], '/');
        $this->set('option', $newOption);
        $this->set('site_url', $siteurl);
        $this->set('publicDir', $public_dir);
        $this->set('publicDir', $urlPostFix_result);
        
        $position = array('sidebar' => "Sidebar", 'top_wrapper' => "Top Wrapper", "top_fullwidth" => "Top Fullwidth", "bottom_wrapper" => "Bottom Wrapper", "bottom_fullwidth" => "Bottom Fullwidth", "footer_script" => "Footer Script");
        $this->set('position', $position);
        $this->Session->write('site_url', $siteurl.'/');           
        $this->Session->write('publicDir', $urlPostFix_result);   
        $this->set('facebookUrl', $userController->fblogin());
        $this->set('googleUrl', $userController->googlelogin());
        if ($this->Session->read('Auth.User')){
            $userData=$this->Customer->find('first',array('conditions'=>array('Customer.id'=>$this->Session->read('Auth.User.id'))));
            $userProfileData=$this->CustomerProfile->find('first',array('conditions'=>array('CustomerProfile.userID'=>$this->Session->read('Auth.User.id'))));
            if(isset($userProfileData['CustomerProfile'])){
                $userData['CustomerProfile']=$userProfileData['CustomerProfile'];
            }else{
                $userData['CustomerProfile']['gender']='';
                $userData['CustomerProfile']['name']=$userData['Customer']['email'];
                $userData['CustomerProfile']['address']='';
                $userData['CustomerProfile']['contact']='';
                $userData['CustomerProfile']['newsLatter']='';
            }
            
            $this->set('userData', $userData);
            $this->set('loggedIn', true);
        }else{
            $this->set('loggedIn', false);
        }
    }

}
    