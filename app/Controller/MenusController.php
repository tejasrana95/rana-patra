<?php

App::uses('AppController', 'Controller');

/**
 * Menus Controller
 *
 * @property Partner $Menus
 * @property PaginatorComponent $Paginator
 */
class MenusController extends AppController {

    public $uses = array('Menu', 'Page');

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * function to render top,header and footer menus
     *
     */
    public function build_nav($menu_for = null) {
// validating menu structure before rendering
        $all_menu_items = $this->Menu->find('all', array(
            "conditions" => array("menu_for" => $menu_for),
            "order" => "Menu.sort_order asc"
        ));


        foreach ($all_menu_items as $item) {

            if (!self::check_status($item)) {
                self::inactive_menu_item($item['Menu']['id']);
            } else if (!$item['Menu']['is_active'] && self::check_status($item)) {
                $this->Menu->read(null, $item['Menu']['id']);
                $this->Menu->set(array(
                    'is_active' => INFI_ACTIVE_STATUS
                ));
                $this->Menu->save();
            }
        }

        //getting menu headers
        $menu_headers = $this->Menu->find('all', array(
            "conditions" => array("parent_id" => 0, "menu_for" => $menu_for, 'is_active' => INFI_ACTIVE_STATUS,), // 0 indicates menu headers
            "order" => "Menu.sort_order asc"
        ));
        // getting all those nodes which have at least one child menu item
        $all_parents = $this->Menu->find('all', array("conditions" => array("menu_for" => $menu_for, 'is_active' => INFI_ACTIVE_STATUS),
            'fields' => array('DISTINCT Menu.parent_id'),));

        $aux_array = array(); // auxiliary array to store all parents and their children menu items

        foreach ($all_parents as $parent) { // getting children of all elements in $all_paernt array
            $a = $this->Menu->find('all', array("conditions" => array("parent_id" => $parent['Menu']['parent_id']
                    , "menu_for" => $menu_for, 'is_active' => INFI_ACTIVE_STATUS),));


            $aux_array[$parent['Menu']['parent_id']] = $a; // this array has all parents and their children regardless of their hierarchy
        }



        $this->set('menu_headers', $menu_headers);
        $this->set('all_parents', $all_parents);
        $this->set('aux_array', $aux_array);
        $this->set('menu_for', $menu_for);

        //$this->set('child_navigation',$siteChildDbLinks);
        //$this->viewPath = 'Menus';
        //debug($aux_array);
        $this->render('build_nav');
    }

    /**
     * function to check staus of content while rendering menu
     *
     */
    public function check_status($item = null) {
        $this->loadModel('Page');
        $page_status = false;
        $type = $item['Menu']['content_type'];
        $id = $item['Menu']['content_name'];
        $count = 0;
        if ($type == "page") {
            $page_obj = $this->Page->find('first', array(
                'conditions' => array('Page.id' => $id)
            ));
            if (isset($page_obj['Page']['status']))
                $page_status = true;
        }else if ($type == "custom") {
            $page_status = true;
        } else if ($type == "category") {
            $page_status = true;
        } else if ($type == "customHTML") {
            $page_status = true;
        }
        //debug($page_status );debug($type ." ".$id);
        return $page_status;
    }

    /**
     * function to get type of menu
     *
     */
    public function inactive_menu_item($id = null) {
        if ($id != null) {
            $item_to_inactive = $this->Menu->find('first', array(
                "conditions" => array("id" => $id), // to deactivate checkboxes which are already in menu
            ));

            //debug($item_to_inactive['Menu']['content_name']);
            $children = $this->Menu->find('all', array(
                "conditions" => array("parent_id" => $item_to_inactive['Menu']['content_name'],
                    "menu_for" => $item_to_inactive['Menu']['menu_for']
                ),
            ));
            //debug($children);

            foreach ($children as $child) {
                //debug($child);
                $this->Menu->read(null, $child['Menu']['id']);
                $this->Menu->set(array(
                    'parent_id' => $item_to_inactive['Menu']['parent_id'],
                ));
                //  $this->Menu->save();
            }

            //debug($children);
            $this->Menu->read(null, $item_to_inactive['Menu']['id']);
            $this->Menu->set(array(
                'is_active' => false
            ));
            // $this->Menu->save();
        }
    }

    /**
     * function to get name a page
     *
     */
    public function get_page_name($id = null) {
        $page_obj = $this->Page->find('first', array(
            'conditions' => array('Page.id' => $id)
        ));
        //if(isset($page_obj['Page']['page_name'])) return $page_obj['Page']['page_name'];
        if (isset($page_obj))
            return $page_obj;
    }

    /**
     * function to get list of items in a particular model
     *
     */
    public function list_menu_item($contentType = null, $pageid = null) {
        if ($contentType == "page") {
            $page_obj = $this->Page->find('first', array(
                'conditions' => array('Page.id' => $pageid)
            ));
            //if(isset($page_obj['Page']['page_name'])) return $page_obj['Page']['page_name'];
            if (isset($page_obj))
                return $page_obj;
        } else {
            return $this->Menu->find('all', array(
                        'conditions' => array(
                            'Menu.parent_id' => $contentType,
                        ),
            ));
        }
    }

}
