<?php

App::uses('AppController', 'Controller');
include_once APP . 'Lib' . DS . 'elfinder' . DS . 'php' . DS . 'autoload.php';

class FileManagerController extends AppController {

    public $name = 'FileManager';
    public $uses = array();
    public $components = array('RequestHandler', 'Auth');
    public $helpers = array('Js', 'Html');

    public function access($attr, $path, $data, $volume) {
        return strpos(basename($path), '.') === 1       // if file/folder begins with '.' (dot)
                ? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
                : null;                                    // else elFinder decide it itself
    }

    public function beforeFilter() {

        $this->Auth->allow();

        parent::beforeFilter();

        $this->Security->csrfCheck = false;
        $this->Security->validatePost = false;
    }

    public function opts($userid = null) {
        if ($userid != null) {
            $this->loadModel('User');
            $this->loadModel('UserRole');
            $role_id = $this->User->find('first', array('conditions' => array('User.id' => $userid)));
            $role = $this->UserRole->find('first', array('conditions' => array('UserRole.id' => $role_id['User']['role_id'])));
            $is_admin = $role['UserRole']['is_admin'];
        } else {
            $is_admin = 0;
        }
        if ($is_admin == 1) {
            $disable = "";
        } else {
            $disable['disabled'] = array('rename', 'rm');
        }



        $this->loadModel('Option');
        $publicDir = $this->Option->find('first', array(
            'conditions' => array('Option.meta_key' => "publicDir")));
        $site_url = $this->Option->find('first', array(
            'conditions' => array('Option.meta_key' => "site_url")));


        if (!file_exists($publicDir['Option']['meta_value'])) {
            App::uses('Folder', 'Utility');
            App::uses('File', 'Utility');
            $dir = new Folder($publicDir['Option']['meta_value'], true, 0755);
        }

        $public_dir = rtrim($publicDir['Option']['meta_value'], '/');
        $urlPostFix = explode('/', $public_dir);
        $urlPostFix_result = $urlPostFix[count($urlPostFix) - 1];
        $siteurl = rtrim($site_url['Option']['meta_value'], '/');


        $opts = array(
            'debug' => false,
            'roots' => array(
                array(
                    'driver' => 'LocalFileSystem', // driver for accessing file system (REQUIRED)
                    'path' => $public_dir . '/', // path to files (REQUIRED)
                    'URL' => $siteurl . '/' . $urlPostFix_result . '/', // URL to files (REQUIRED)
                    'accessControl' => 'access'                     // disable and hide dot starting files (OPTIONAL)
                )
            )
        );

        return $opts;
    }

    public function index($userid = null) {
        $this->loadModel('Option');
        $adminUrl = $this->Option->find('first', array(
            'conditions' => array('Option.meta_key' => "admin_url")));
        $this->layout = '';

        if (isset($_GET['cmd']) || isset($_POST)) {
            // run elFinder
            header("Access-Control-Allow-Origin: " . rtrim($adminUrl['Option']['meta_value'], '/'));
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Allow-Headers: X-Requested-With");

            $connector = new elFinderConnector(new elFinder($this->opts($userid)));
            $connector->run();
        } else {
            $this->redirect(array('controller' => "errors", "action" => '404'));
        }
    }

    public function imageDispatcher() {

        if (isset($_GET['imageURL'])) {
            $imageURL = urldecode($_GET['imageURL']);
        } else {
            die();
        }
        if (isset($_GET['pix'])) {
            $pix = $_GET['pix'];
        } else {
            $pix = 32;
        }

        $extension = pathinfo($imageURL, PATHINFO_EXTENSION);
        if ($extension == 'png' || $extension == 'PNG') {
            header('Content-type: image/png');
        } elseif ($extension == 'gif' || $extension == 'GIF') {
            header('Content-type: image/gif');
        } else {
            header('Content-type: image/jpeg');
        }
        if ($imageURL) {


            $image_path = $imageURL;

            $image_size = getimagesize($image_path);
            $image_width = $image_size[0];
            $image_height = $image_size[1];

            $new_size = ($image_width + $image_height) / ($image_width * ($image_height / $pix));
            $new_width = $image_width * $new_size;
            $new_height = $image_height * $new_size;

            $new_image = imagecreatetruecolor($new_width, $new_height);
            if ($extension == 'png' || $extension == 'PNG') {
                $old_image = imagecreatefrompng($image_path);
            } elseif ($extension == 'gif' || $extension == 'GIF') {
                $old_image = imagecreatefromgif($image_path);
            } else {
                $old_image = imagecreatefromjpeg($image_path);
            }

            imagecopyresized($new_image, $old_image, 0, 0, 0, 0, $new_width, $new_height, $image_width, $image_height);
            if ($extension == 'png' || $extension == 'PNG') {
                imagepng($new_image);
            } elseif ($extension == 'gif' || $extension == 'GIF') {
                imagegif($new_image);
            } else {
                imagejpeg($new_image);
            }
        }
        die();
    }

}
