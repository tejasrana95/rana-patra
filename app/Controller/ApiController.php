<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');
include_once APP . 'Lib' . DS . 'twitter' . DS . 'twitteroauth.php';

/**
 * CakePHP CommonController
 * @author Tejas.Rana
 */
class ApiController extends AppController {

//    defining all model required in this controller
    public $uses = array(
        'Option',
    );

    function getConnectionWithAccessToken($cons_key, $cons_secret, $oauth_token, $oauth_token_secret) {
        $connection = new TwitterOAuth($cons_key, $cons_secret, $oauth_token, $oauth_token_secret);
        return $connection;
    }

    function relativeTime($time) {
        $delta = strtotime('+2 hours') - strtotime($time);
        if ($delta < 2 * MINUTE) {
            return "1 min ago";
        }
        if ($delta < 45 * MINUTE) {
            return floor($delta / MINUTE) . " min ago";
        }
        if ($delta < 90 * MINUTE) {
            return "1 hour ago";
        }
        if ($delta < 24 * HOUR) {
            return floor($delta / HOUR) . " hours ago";
        }
        if ($delta < 48 * HOUR) {
            return "yesterday";
        }
        if ($delta < 30 * DAY) {
            return floor($delta / DAY) . " days ago";
        }
        if ($delta < 12 * MONTH) {
            $months = floor($delta / DAY / 30);
            return $months <= 1 ? "1 month ago" : $months . " months ago";
        } else {
            $years = floor($delta / DAY / 365);
            return $years <= 1 ? "1 year ago" : $years . " years ago";
        }
    }

    public function twitter($notweets = null) {

        $option = $this->Option->find('all');
        $newOption = array();
        foreach ($option as $opt) {
            $newOption[$opt['Option']['meta_key']] = $opt['Option']['meta_value'];
        }

        $connection = self::getConnectionWithAccessToken($newOption['twitter_consumer_key'], $newOption['twitter_consumer_secret'], $newOption['twitter_access_token'], $newOption['twitter_access_token_secret']);
        $twitteruser = $newOption['twitter_user'];

        if ($notweets == null) {
            $notweets = $newOption['tweet_limit'];
        }
// if username isn't array check if user separated usernames with comma
        if (!is_array($twitteruser))
            $twitteruser = explode(',', trim($twitteruser));

        if (count($twitteruser) > 1) {

            $all_tweets = array();
            $remainder = $notweets % count($twitteruser);
            $tweets_per_user = floor($notweets / count($twitteruser));

            // split tweets per user so it doesn't exceed limit
            foreach ($twitteruser as $user) {
                if ($remainder > 0) {
                    $notweets = $tweets_per_user + $remainder;
                } else {
                    $notweets = $tweets_per_user;
                }

                // get tweets
                $tweets[] = $connection->get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=" . $user . "&count=" . $notweets);
            }

            // merge into one array
            $all_tweets = $tweets[0];
            for ($i = 1; $i < count($tweets); $i++) {
                $all_tweets = array_merge($all_tweets, $tweets[$i]);
            }
        } else {
            $all_tweets = $connection->get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=" . $twitteruser[0] . "&count=" . $notweets);
        }
       return $all_tweets;
    }

    function tweet_html_text($tweet) {
        $text = $tweet->text;
        $user = $tweet->user;
        $created_at = $tweet->created_at;

        // hastags
        $linkified = array();
        foreach ($tweet->entities->hashtags as $hashtag) {
            $hash = $hashtag->text;

            if (in_array($hash, $linkified)) {
                continue; // do not process same hash twice or more
            }
            $linkified[] = $hash;

            // replace single words only, so looking for #Google we wont linkify >#Google<Reader
            $text = preg_replace('/#\b' . $hash . '\b/', sprintf('<a href="https://twitter.com/search?q=%%23%2$s&src=hash">#%1$s</a>', $hash, urlencode($hash)), $text);
        }

        // user_mentions
        $linkified = array();
        foreach ($tweet->entities->user_mentions as $userMention) {
            $name = $userMention->name;
            $screenName = $userMention->screen_name;

            if (in_array($screenName, $linkified)) {
                continue; // do not process same user mention twice or more
            }
            $linkified[] = $screenName;

            // replace single words only, so looking for @John we wont linkify >@John<Snow
            $text = preg_replace('/@\b' . $screenName . '\b/', sprintf('<a href="https://www.twitter.com/%1$s" title="%2$s">@%1$s</a>', $screenName, $name), $text);
        }

        // urls
        $linkified = array();
        foreach ($tweet->entities->urls as $url) {
            $url = $url->url;

            if (in_array($url, $linkified)) {
                continue; // do not process same url twice or more
            }
            $linkified[] = $url;

            $text = str_replace($url, sprintf('<a href="%1$s">%1$s</a>', $url), $text);
        }
        $timeago = self::relativeTime($created_at);
        $tweet = '<p>' . $text . '</p>
        <footer>@' . $user->screen_name . '</a><cite> ' . $timeago . '</cite></footer>';
        return $tweet;
    }

}
