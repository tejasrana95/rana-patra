<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'Api');
App::import('Controller', 'Blog');
App::import('Controller', 'Common');
/**
 * Widgets Controller
 *
 * @property Widget $Widget
 * @property PaginatorComponent $Paginator
 */
class WidgetsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Security');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
        );
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Security->enabled = false;
        $this->Security->csrfCheck = false;
        $this->Security->validatePost = false;
    }

    public function index($widget_for = null, $name = false, $position = null) {
        $widget_detail = array();
        if (isset($widget_for) && !empty($widget_for)) {
            if (isset($position) && $position) {
                $condition = array('Widget.identifier' => $widget_for,
                    'Widget.status' => INFI_ACTIVE_STATUS,
                    "Widget.position" => $position);
            } else {
                $condition = array('Widget.identifier' => $widget_for,
                    'Widget.status' => INFI_ACTIVE_STATUS);
            }
            $widget_detail = $this->Widget->find('first', array(
                'conditions' => $condition,
                'fields' => array(
                    'Widget.name',
                    'Widget.value'
                    )
                ));
            if (empty($widget_detail) || !isset($widget_detail)) {
                if (isset($position) && $position) {
                    $condition = array('Widget.identifier' => $widget_for,
                        'Widget.status' => INFI_ACTIVE_STATUS,
                        "Widget.position" => $position);
                } else {
                    $condition = array('Widget.identifier' => $widget_for,
                        'Widget.status' => INFI_ACTIVE_STATUS);
                }
                $widget_detail = $this->Widget->find('first', array(
                    'conditions' => $condition,
                    'fields' => array(
                        'Widget.name',
                        'Widget.value'
                        )
                    ));
            }
        }

        if (isset($name) && $name) {
            $widget_details = $widget_detail;
        } else {
            $widget_details['Widget']['value'] = $this->magicKeyword($widget_detail['Widget']['value']);
        }

        $this->set('widget_detail', $widget_details);
    }

    public function magicKeyword($data, $id = null, $currentId = null, $pageid = null) {

        $uniqueID = "[UNIQUEID]";
        $tmpOldStrLength = strlen($uniqueID);
        $offset = 0;
        $i = 1;
        while (($offset = strpos($data, $uniqueID, $offset)) !== false) {
            if ($i % 2 == 0) {
                $n = $i - 1;
            } else {
                $n = $i;
            }
            $data = substr_replace($data, $n, $offset, $tmpOldStrLength);
            $i++;
        }
        $newdata = $data;
        $newdata = str_replace('[[RIGHTBAR][RIGHTBAR]]', '[[RIGHTBAR][' . $id . '/' . $currentId . ']]', $data);
        $newdata = str_replace('[[SUBCATEGORY][SUBCATEGORY]]', '[[SUBCATEGORY][' . $id . '/' . $currentId . ']]', $newdata);
        $newdata = str_replace('[[CAREER][OPENINGS]]', '[[CAREER][' . $pageid . '/' . $currentId . ']]', $newdata);
        return preg_replace_callback("/\[(\[[A-Z]+\])\[(.*?)\]\]/", array(&$this, 'magicKeywordCallback'), $data);
    }

    public function magicKeywordCallback(&$matches) {

        if (count($matches) == 3) {
            if ($matches[1] == "[PAGE]") {
                return $this->requestAction(array(
                    'controller' => "Pages",
                    "action" => 'get_page',
                    $matches[2]
                    ), array('return'));
            } elseif ($matches[1] == "[WIDGET]") {
                return $this->requestAction(array(
                    'controller' => "Widgets",
                    "action" => 'index',
                    $matches[2]
                    ), array('return'));
            } elseif ($matches[1] == "[GALLERY]") {

                $otherdata = explode('/', $matches[2]);
                if (isset($otherdata[0])) {
                    $matches[2] = $otherdata[0];
                }
                if (isset($otherdata[1])) {
                    $matches[3] = $otherdata[1];
                } else {
                    $matches[3] = null;
                }


                return $this->requestAction(array(
                    'controller' => "Gallery",
                    "action" => 'getgallery',
                    $matches[2], $matches[3]
                    ), array('return'));
            }  elseif ($matches[1] == "[CAREER]") {
                $otherdata = explode('/', $matches[2]);
                $matches[2] = $otherdata[0];
                $matches[3] = $otherdata[1];
                return $this->requestAction(array(
                    'controller' => "careers",
                    "action" => 'index', $matches[2], $matches[3]
                    ), array('return'));
            } 
        }
    }

    public function getwidget($widget_identifirs, $position = null) {
       // Configure::write('Cache.disable', true);
       // Configure::write('Cache.check', false);
        if (is_array($widget_identifirs)) {
            $widget_details = array();
            if (isset($widget_identifirs) && !empty($widget_identifirs)) {
                if (isset($position) && $position) {
                    $all_widgets = $this->Widget->find('all', array(
                        'conditions' => array('Widget.status' => INFI_ACTIVE_STATUS, "Widget.position" => $position),
                        'order' => array('Widget.sort asc'),
                        ));

                    foreach ($all_widgets as $all_widget) {
                        if (in_array($all_widget['Widget']['identifier'], $widget_identifirs)) {
                            if (!empty($all_widget)) {
                                $all_widget['Widget']['value'] = $this->magicKeyword($all_widget['Widget']['value']);
                                $widget_details[] = $all_widget;
                            }
                        }
                    }
                }
            }
            return $widget_details;
        }
    }
    function renderWidget($widget_identifirs){
       return $this->magicKeyword($widget_identifirs);
   }

   function twitter(){
       $Api = new ApiController;
       $twitterdata= $Api->twitter(5);
       $this->layout="";
       $this->set('twitterdata',$twitterdata);
    }
    function blogs(){
        $blog = new BlogController;
        $allArticles=$blog->getArticles(0,1,3); 
        $this->layout="";
        $this->set('blogContent',$this->renderWidget('[[WIDGET][homepage-blog]]'));
        $this->set('allArticles',$allArticles);
    }
    function testimonials(){
        $CommonController = new CommonController;
        $allTestimonials=$CommonController->getTestimonials(); 
        $this->layout="";
        $this->set('testimonialContent',$this->renderWidget('[[WIDGET][homepage-user-review]]'));
        $this->set('allTestimonials',$allTestimonials);
    }

}
