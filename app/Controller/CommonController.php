<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP CommonController
 * @author Tejas.Rana
 */
class CommonController extends AppController {
    //    defining all model required in this controller
    public $uses = array(
        'Testimonial',
        'Article',
        'ArticleCategory',
        'Page',
        'Option',
        'Matrimonial',
        'Customer',
        'CustomerProfile',
        'CustomerInvoice',
        'User',
        'Session',
    );

    public function breadcrumb($array) {
        $keys = array();
        $buildBreadcrumb = array();
        foreach ($array as $key => $value) {
            $keys[] = $key;
            $buildBreadcrumb[] = '<a href="' . implode('/', $keys) . '">' . $value . '</a>';
        }
        return implode(' / ', $buildBreadcrumb);
    }

    public function magicKeyword($data, $id = null, $currentId = null, $pageid = null) {

        $uniqueID = "[UNIQUEID]";
        $tmpOldStrLength = strlen($uniqueID);
        $offset = 0;
        $i = 1;
        while (($offset = strpos($data, $uniqueID, $offset)) !== false) {
            if ($i % 2 == 0) {
                $n = $i - 1;
            } else {
                $n = $i;
            }
            $data = substr_replace($data, $n, $offset, $tmpOldStrLength);
            $i++;
        }

        $newdata = str_replace('[[RIGHTBAR][RIGHTBAR]]', '[[RIGHTBAR][' . $id . '/' . $currentId . ']]', $data);
        $newdata = str_replace('[[SUBCATEGORY][SUBCATEGORY]]', '[[SUBCATEGORY][' . $id . '/' . $currentId . ']]', $newdata);
        $newdata = str_replace('[[CAREERS][OPENINGS]]', '[[CAREERS][' . $pageid . '/' . $currentId . ']]', $newdata);
        $newdata = str_replace('[[LATESTHINKING][LATESTHINKING]]', '[[LATESTHINKING][' . $pageid . ']]', $newdata);
        return preg_replace_callback("/\[(\[[A-Z]+\])\[(.*?)\]\]/", array(&$this, 'magicKeywordCallback'), $newdata);
    }

    public function magicKeywordCallback(&$matches) {
        if (count($matches) == 3) {
            if ($matches[1] == "[PAGE]") {
                return $this->requestAction(array(
                            'controller' => "Pages",
                            "action" => 'get_page',
                            $matches[2]
                                ), array('return'));
            } elseif ($matches[1] == "[WIDGET]") {
                return $this->requestAction(array(
                            'controller' => "Widgets",
                            "action" => 'index',
                            $matches[2]
                                ), array('return'));
            } elseif ($matches[1] == "[GALLERY]") {
                $otherdata = explode('/', $matches[2]);
                if (isset($otherdata[0])) {
                    $matches[2] = $otherdata[0];
                }
                if (isset($otherdata[1])) {
                    $matches[3] = $otherdata[1];
                } else {
                    $matches[3] = null;
                }

                return $this->requestAction(array(
                            'controller' => "Gallery",
                            "action" => 'getgallery',
                            $matches[2], $matches[3]
                                ), array('return'));
            } elseif ($matches[1] == "[TEAM]") {
                return $this->requestAction(array(
                            'controller' => "Teams",
                            "action" => 'getteam',
                            $matches[2]
                                ), array('return'));
            } elseif ($matches[1] == "[CUSTOMERLOGO]") {
                return $this->requestAction(array(
                            'controller' => "customerlogos",
                            "action" => 'index',
                            $matches[2]
                                ), array('return'));
            } elseif ($matches[1] == "[NEWS]") {
                $otherdata = explode('/', $matches[2]);
                $matches[2] = $otherdata[0];
                $matches[3] = $otherdata[1];
                return $this->requestAction(array(
                            'controller' => "News",
                            "action" => 'getnews',
                            $matches[2], $matches[3]
                                ), array('return'));
            } elseif ($matches[1] == "[TESTIMONIAL]") {
                $otherdata = explode('/', $matches[2]);
                $matches[2] = $otherdata[0];
                $matches[3] = $otherdata[1];
                return $this->requestAction(array(
                            'controller' => "testimonials",
                            "action" => 'gettestimonial',
                            $matches[2], $matches[3]
                                ), array('return'));
            } elseif ($matches[1] == "[RIGHTBAR]") {
                $otherdata = explode('/', $matches[2]);
                $matches[2] = $otherdata[0];
                $matches[3] = $otherdata[1];

                return $this->requestAction(array(
                            'controller' => "pages",
                            "action" => 'rightbar',
                            $matches[2], $matches[3]
                                ), array('return'));
            } elseif ($matches[1] == "[SUBCATEGORY]") {
                $otherdata = explode('/', $matches[2]);
                $matches[2] = $otherdata[0];
                $matches[3] = $otherdata[1];
                return $this->requestAction(array(
                            'controller' => "pages",
                            "action" => 'subcategory',
                            $matches[2], $matches[3]
                                ), array('return'));
            } elseif ($matches[1] == "[PARTNERSPEAK]") {
                $otherdata = explode('/', $matches[2]);
                $matches[2] = $otherdata[0];
                $matches[3] = $otherdata[1];

                return $this->requestAction(array(
                            'controller' => "pages",
                            "action" => 'getpartnerspeak',
                            $matches[2], $matches[3]
                                ), array('return'));
            } elseif ($matches[1] == "[CUSTOMERSPEAK]") {
                $otherdata = explode('/', $matches[2]);
                $matches[2] = $otherdata[0];
                if (isset($otherdata[1])) {
                    $matches[3] = $otherdata[1];
                } else {
                    $matches[3] = "";
                }

                return $this->requestAction(array(
                            'controller' => "pages",
                            "action" => 'getcustomerspeaks',
                            $matches[2], $matches[3]
                                ), array('return'));
            } elseif ($matches[1] == "[LATESTINHR]") {
                $otherdata = explode('/', $matches[2]);
                $matches[2] = $otherdata[0];
                $matches[3] = $otherdata[1];

                return $this->requestAction(array(
                            'controller' => "pages",
                            "action" => 'getlatest_in_hr',
                            $matches[2], $matches[3]
                                ), array('return'));
            } elseif ($matches[1] == "[CAREERS]") {
                $otherdata = explode('/', $matches[2]);
                $matches[2] = $otherdata[0];
                $matches[3] = $otherdata[1];
                return $this->requestAction(array(
                            'controller' => "career",
                            "action" => 'index', $matches[2], $matches[3]
                                ), array('return'));
            } elseif ($matches[1] == "[LATESTHINKING]") {
                return $this->requestAction(array(
                            'controller' => "pages",
                            "action" => 'latest_thinking',
                            $matches[2]
                                ), array('return'));
            }
        }
    }

    public function GenerateDownloadLink($file_id = null) {
        if (!empty($file_id)) {
            $site_url = $this->Option->find('first', array('conditions' => array('Option.meta_key' => "site_url")));
            $siteurl = rtrim($site_url['Option']['meta_value'], '/');

            $options = array('conditions' => array('Download.' . $this->Download->primaryKey => $file_id));
            $file = $this->Download->find('first', $options);

            $link = $siteurl . '/common/download/' . base64_encode($file['Download']['id']) . '/' . md5($_SESSION['Auth']['User']['WebServiceData']['username']) . '/' . md5($file['Download']['id']);
            return $link;
        }
    }

    public function download($file_id = null, $username = null, $secureFileId = null) {
        $this->layout = "";
        if (!empty($file_id) && !empty($username) && !empty($secureFileId)) {

            if ($username == md5($_SESSION['Auth']['User']['WebServiceData']['username']) && md5(base64_decode($file_id)) == $secureFileId) {
                $options = array('conditions' => array('Download.' . $this->Download->primaryKey => base64_decode($file_id)));
                $file = $this->Download->find('first', $options);
                if (function_exists('curl_version')) {
                    $ch = curl_init($file['Download']['file_path']);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_exec($ch);
                    # get the content type
                    $mimetype = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

                    header("Content-Type: $mimetype");
                }
                $opts = array(
                    'http' => array(
                        'method' => "GET",
                        'header' => "Accept-language: en\r\n" .
                        "Cookie: foo=bar\r\n"
                    )
                );

                $context = stream_context_create($opts);

                // Open the file using the HTTP headers set above
                $file_open = @file_get_contents(str_replace(' ', '%20', $file['Download']['file_path']), false, $context);

                if (isset($file_open) && !empty($file_open)) {
                    echo $file_open;
                } else {
                    $this->redirect($file['Download']['file_path']);
                }
            } else {
                header("HTTP/1.0 404 Not Found");
            }
        } else {
            header("HTTP/1.0 404 Not Found");
        }
    }

    function getTestimonials() {
        $all = $this->Testimonial->find('all', array('order' => array('modified DESC'),));
        return $all;
    }

    function truncate($text, $length, $suffix = '', $isHTML = true) {
        $i = 0;
        $simpleTags = array('br' => true, 'hr' => true, 'input' => true, 'image' => true, 'link' => true, 'meta' => true);
        $tags = array();
        if ($isHTML) {
            preg_match_all('/<[^>]+>([^<]*)/', $text, $m, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);
            foreach ($m as $o) {
                if ($o[0][1] - $i >= $length)
                    break;
                $t = substr(strtok($o[0][0], " \t\n\r\0\x0B>"), 1);
// test if the tag is unpaired, then we mustn't save them
                if ($t[0] != '/' && (!isset($simpleTags[$t])))
                    $tags[] = $t;
                elseif (end($tags) == substr($t, 1))
                    array_pop($tags);
                $i += $o[1][1] - $o[0][1];
            }
        }

// output without closing tags
        $output = substr($text, 0, $length = min(strlen($text), $length + $i));
// closing tags
        $output2 = (count($tags = array_reverse($tags)) ? '</' . implode('></', $tags) . '>' : '');

// Find last space or HTML tag (solving problem with last space in HTML tag eg. <span class="new">)

        $splitted = preg_split('/<.*>| /', $output, -1, PREG_SPLIT_OFFSET_CAPTURE);
        $last_item = end($splitted);
        $pos = end($last_item);
// Append closing tags to output
        $output .= $output2;

// Get everything until last space
        $one = substr($output, 0, $pos);
// Get the rest
        $two = substr($output, $pos, (strlen($output) - $pos));
// Extract all tags from the last bit
        preg_match_all('/<(.*?)>/s', $two, $tags);
// Add suffix if needed
        if (strlen($text) > $length) {
            $one .= $suffix;
        }
// Re-attach tags
        $output = $one . implode($tags[0]);

//added to remove unnecessary closure
        $output = str_replace('</!-->', '', $output);

        return $output;
    }

    function urlGenrator($id, $type) {
        if (isset($type) && isset($id)) {
            if ($type == "Article") {
                $url = Router::url('/', true) . 'blog/';
                if (is_int($id)) {

                    $article = $this->Article->find('first', array(
                        'conditions' => array('Article.id' => $id)
                    ));
                } else {
                    $article = $this->Article->find('first', array(
                        'conditions' => array('Article.article_slug' => $id)
                    ));
                }
                if (isset($article['Article']['main_category'])) {
                    $main_category = $this->ArticleCategory->find('first', array(
                        'conditions' => array('ArticleCategory.id' => $article['Article']['main_category'])
                    ));
                    $url .= $main_category['ArticleCategory']['identifier'] . '/';
                }
                if (isset($article['Article']['category'])) {
                    $category = $this->ArticleCategory->find('first', array(
                        'conditions' => array('ArticleCategory.id' => $article['Article']['category'])
                    ));
                    $url .= $category['ArticleCategory']['identifier'] . '/';
                }
                if (isset($article['Article']['sub_category'])) {
                    $sub_category = $this->ArticleCategory->find('first', array(
                        'conditions' => array('ArticleCategory.id' => $article['Article']['sub_category'])
                    ));
                    $url .= $sub_category['ArticleCategory']['identifier'] . '/';
                }
                $url .= $article['Article']['article_slug'];
                return $url;
            } elseif ($type == "ArticleCategory") {
                $url = Router::url('/', true) . 'blog/category/';
                if (is_int($id)) {
                    $category = $this->ArticleCategory->find('first', array(
                        'conditions' => array('ArticleCategory.id' => $id)
                    ));
                } else {
                    $category = $this->ArticleCategory->find('first', array(
                        'conditions' => array('ArticleCategory.identifier' => $id)
                    ));
                }
                if ($category['ArticleCategory']['parent'] != 0) {
                    $parentCategory = $this->ArticleCategory->find('first', array(
                        'conditions' => array('ArticleCategory.id' => $category['ArticleCategory']['parent'])
                    ));
                    $url .= $parentCategory['ArticleCategory']['identifier'] . '/';
                }
                $url .= $category['ArticleCategory']['identifier'];
                return $url;
            }
        }
    }

    function getUserName($id, $isAdmin = null) {
        if (isset($isAdmin) && $isAdmin == 1) {
            $username = $this->User->find('first', array(
                'conditions' => array('User.id' => $id)
            ));

            return ($username['User']['username']);
        } else {
            $CustomerProfile = $this->CustomerProfile->find('first', array(
                'conditions' => array('CustomerProfile.userID' => $id)
            ));

            return ($CustomerProfile['CustomerProfile']['name']);
        }
    }

    function getUserProfile($id, $isAdmin = null) {
        if (isset($isAdmin) && $isAdmin == 1) {
            $username = $this->User->find('first', array(
                'conditions' => array('User.id' => $id)
            ));
            return ($username['User']['pic']);
        } else {
            $CustomerProfile = $this->CustomerProfile->find('first', array(
                'conditions' => array('CustomerProfile.userID' => $id)
            ));
            return ($CustomerProfile['CustomerProfile']['profilePic']);
        }              

    }


    public function imageProcessor($url, $width = 200, $height = 200) {
        $publicDir = $this->Option->find('first', array(
            'conditions' => array('Option.meta_key' => "publicDir")));
        $public_dir = rtrim($publicDir['Option']['meta_value'], DS);
        $urlPostFix = explode(DS, $public_dir);
        $urlPostFix_result = $urlPostFix[count($urlPostFix) - 1];
        $image = $urlPostFix_result . '/' . $url;
        $destinationPath = explode('/', $image);
        unset($destinationPath[count($destinationPath) - 1]);
        $destinationPath = implode('/', $destinationPath);
        $destinationPath = $destinationPath . '/thumbs/';
        $filename = explode('/', $image);
        $filename = $filename[count($filename) - 1];
        if (file_exists($destinationPath) == false) {
            mkdir($destinationPath, 0777);
        }
        if (file_exists($destinationPath . $filename)) {
            $path = str_replace($urlPostFix_result, '', $destinationPath . $filename);
            $path = ltrim($path, '/');
            return $path;
        } elseif (file_exists($image) && !file_exists($destinationPath . $filename)) {
            $arr_image_details = getimagesize($image); // pass id to thumb name
            $original_width = $arr_image_details[0];
            $original_height = $arr_image_details[1];
            if ($original_width > $original_height) {
                $new_width = $width;
                $new_height = intval($original_height * $new_width / $original_width);
            } else {
                $new_height = $height;
                $new_width = intval($original_width * $new_height / $original_height);
            }
            $dest_x = intval(($width - $new_width) / 2);
            $dest_y = intval(($height - $new_height) / 2);
            if ($arr_image_details[2] == IMAGETYPE_GIF) {
                $imgt = "ImageGIF";
                $imgcreatefrom = "ImageCreateFromGIF";
            }
            if ($arr_image_details[2] == IMAGETYPE_JPEG) {
                $imgt = "ImageJPEG";
                $imgcreatefrom = "ImageCreateFromJPEG";
            }
            if ($arr_image_details[2] == IMAGETYPE_PNG) {
                $imgt = "ImagePNG";
                $imgcreatefrom = "ImageCreateFromPNG";
            }
            if ($imgt) {
                $old_image = $imgcreatefrom($image);
                $new_image = imagecreatetruecolor($width, $height);
                $white = imagecolorallocate($new_image, 255, 255, 255);
                imagefill($new_image, 0, 0, $white);
                imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
                $imgt($new_image, $destinationPath . '/' . $filename);
            }
            $path = str_replace($urlPostFix_result, '', $destinationPath . $filename);
            $path = ltrim($path, '/');
            return $path;
        } else {
            return false;
        }
    }

    function valueExistsInArray($array, $search) {
        $array = json_encode($array);
        $search = json_encode($search);
        $search = str_replace('{', "", $search);
        $search = str_replace('}', "", $search);
        if (strpos($array, $search) !== false) {
            return true;
        } else {
            return false;
        }
    }
    function placeFinder($keyword=false){
        if($keyword==false){
            $keyword=$this->request->query('placeFinder');
        }
        $view=($this->request->query('view')=='pre') ? 'pre' : 'json' ;
        if (empty($keyword) || $keyword==false) {
            throw new NotFoundException(__('Invalid page'));
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://maps.googleapis.com/maps/api/place/autocomplete/json?types=(cities)&libraries=places&language=en_GB&key=AIzaSyD0dVnF3UoMuy5Mmc1tPyi72KgXR6132q4&input='.$keyword,
            CURLOPT_USERAGENT => 'placeFinder_api_infi'
        ));
        $resp = json_decode(curl_exec($curl));
        curl_close($curl);
        $places=array();
        if(isset($resp->status) && $resp->status=="OK"){
            foreach($resp->predictions as $prediction){                
                if(!isset($prediction->terms[2])){
                    $prediction->terms[2]=(object) array('offset'=>$prediction->terms[1]->offset,'value'=>$prediction->terms[1]->value);
                    $prediction->terms[1]->offset=" ";
                    $prediction->terms[1]->value=" ";
                }

                $places[]=array('location'=>$prediction->description,'place_id'=>$prediction->place_id,'terms'=>$prediction->terms);
            }
            $places['status']="OK";
        }else{
            $places=array('status'=>'ZERO_RESULTS');
        }
        $this->layout = "";
        $this->set("view",$view);
        if($view=='json'){
            $this->set("placeArray",json_encode($places));
        }elseif($view=='pre'){
            $this->set("placeArray",($places));
        }
    }

    function getOptionValue(){
        $option = $this->Option->find('all');
        $newOption = array();
        foreach ($option as $opt) {
            $newOption[$opt['Option']['meta_key']] = $opt['Option']['meta_value'];
        }
        return $newOption;
    }
    function fetchCurrency(){
        $cSession = curl_init(); 
        //step2
        curl_setopt($cSession,CURLOPT_URL,"http://api.fixer.io/latest?base=INR");
        curl_setopt($cSession,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($cSession,CURLOPT_HEADER, false); 
        //step3
        $result=curl_exec($cSession);
        //step4
        curl_close($cSession);
        
       return json_decode($result,true);
    }

    function getSubscriptionEndDate($userId){
        $insertedInvoice=$this->CustomerInvoice->find('first', array('conditions' => array('CustomerInvoice.payment_status' => "SUCCESS",'CustomerInvoice.userID'=>$userId),'order' => array('invoice_to DESC')));
        if(isset($insertedInvoice['CustomerInvoice']['invoice_to'])){
            return $insertedInvoice['CustomerInvoice']['invoice_to'];
        }else{
            return false;
        }
    }
    function checkPremiumMember($userId){
        $expiryDate=$this->getSubscriptionEndDate($userId);
        if($expiryDate!==false){
            $currentDate=strtotime(date('Y-m-d'));
            $expiryDate=strtotime($expiryDate);
            if($expiryDate>=$currentDate){
                return true;
            }else{
                return false;
            }
        }else
        {
            return false;
        }
    }
}
