<?php

App::uses('AppController', 'Controller');

/**
 * Widgets Controller
 *
 * @property Widget $Widget
 * @property PaginatorComponent $Paginator
 */
class CategoryController extends AppController
{

    /**
     *  method
     *
     * @return void
     */
    public function getcategory($categoryid)
    {

        if (isset($categoryid) && !empty($categoryid)) {
            if (is_numeric($categoryid)) {
                $category_info = $this->Category->find('first', array(
                    'conditions' => array('Category.id' => $categoryid),
                ));
            } else {
                $category_info = $this->Category->find('first', array(
                    'conditions' => array('Category.identifier' => $categoryid),
                ));
            }
        }
        $this->set('category_info', $category_info);
    }

    public function getcategorydata($categoryid)
    {

        if (isset($categoryid) && !empty($categoryid)) {
            if (is_numeric($categoryid)) {
                $category_info = $this->Category->find('first', array(
                    'conditions' => array('Category.id' => $categoryid, 'Category.reserved !=' => 1),
                ));
            } else {
                $category_info = $this->Category->find('first', array(
                    'conditions' => array('Category.identifier' => $categoryid, 'Category.reserved !=' => 1),
                ));
            }
            return $category_info;
        }
    }

    public function getsubcategories($categoryid)
    {
        $category_info = "";
        if (isset($categoryid) && !empty($categoryid)) {
            if (is_numeric($categoryid)) {
                $category_info = $this->Category->find('all', array(
                    'conditions' => array('Category.parent' => $categoryid, 'Category.reserved !=' => 1),
                    'order'      => array('Category.sort_order asc'),
                ));
            }
            return $category_info;
        }
    }

}
