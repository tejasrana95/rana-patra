<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'Common');
/**
 * Articles Controller
 *
 * @property Article $Article
 * @property PaginatorComponent $Paginator
 */
class ProfileController extends AppController {
    /**
     * Components
     *
     * @var array
     */

    public $components = array('Auth','RequestHandler');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->layout='full_width_layout';
    }

    public $uses = array('Customer','CustomerProfile','CustomerInvoice','Article','Matrimonial','Option');

    public function dashboard() { 
        $userId = $this->Auth->user('id');
        $this->loadModel('Matrimonial');
        $this->loadModel('Event');
        $this->loadModel('News');
        $this->loadModel('Article');
        $userData=array();
        $this->loadModel('Matrimonial');
        $userData['Matrimonial'] = $this->Matrimonial->getAllMatrimonials($userId,true);
        $userData['Event'] = $this->Event->getAllEvents($userId,true);
        $userData['News'] = $this->News->getAllNews($userId,true);
        $userData['Article'] = $this->Article->getAllArticle($userId,true);
        $this->set('stats',$userData);
        $this->set('page_slug', 'Dashboard');
        $this->set('keywords_for_layout','User panel');
        $this->set('description_for_layout', 'User Panel');
        $this->set('title_for_layout', 'Dashboard');
        $this->set('navbarClass', array('navClass'=>'white-navbar'));
        $this->set('breadcrumb',array('/dashboard'=>'Dashboard'));
    }

    public function index() { 
        $this->set('page_slug', 'view_profile');
        $this->set('keywords_for_layout','Profile');
        $this->set('description_for_layout', 'Profile');
        $this->set('title_for_layout', 'Profile');
        $this->set('navbarClass', array('navClass'=>'white-navbar'));
        $this->set('breadcrumb',array('/profile'=>'Profile'));
    }
    public function edit() { 
        $userId = $this->Auth->user('id');
         if ($this->request->is(array('post', 'put'))) {
            $publicDir = $this->Option->find('first', array('conditions' => array('Option.meta_key' => 'publicDir')));
            $publicDir = rtrim($publicDir['Option']['meta_value'], '/') . '/';
            $error = false;
            $imagePath = "";
            //Image Cropping
            $valid_exts = array('jpeg', 'jpg', 'png', 'gif');
            $max_file_size = 1024 * 2048; #2000kb
            $nw = $nh = 200; # image with # height
            $success=1;
            if (isset($this->request->data['cropImage']['imagebase64']) && !empty($this->request->data['cropImage']['imagebase64']) && $this->request->data['cropImage']['imagebase64'] !="data:,") {
                    $data=$this->request->data['cropImage']['imagebase64'];
                    list($type, $data) = explode(';', $data);
                    list(, $data)      = explode(',', $data);
                    $ext=explode('/',$type);
                    $ext=$ext[count($ext)-1];
                    $data = base64_decode($data);
                    $imageName = time().'.'.$ext;
                    $tempUsername=str_replace('.','_',$this->Auth->user('username'));
                    $tempUsername=explode('@',$tempUsername);
                    $imagePath = 'userProfileImages/' . $tempUsername[0] . '-' .$userId  .'-' .uniqid()  . '.' . $ext;
                    $path = $publicDir . $imagePath;
                    @unlink($publicDir.$userdata['CustomerProfile']['profilePic']);
                    $success= file_put_contents($path, $data);
                    $this->request->data['CustomerProfile']['profilePic'] = $imagePath;
            }
            if ($success) {
                $this->request->data['CustomerProfile']['userID']=$userId;
                if ($this->CustomerProfile->save($this->request->data)) {
                    $this->Session->setFlash(__('Your profile has been updated successfully.'), 'default', array('class' => 'alert alert-success'));
                    $this->redirect(array('action' => "edit"));
                } else {
                    $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
                }
                
            } else {
                $this->Session->setFlash(__('Following error occurs, file is too small or large.'), 'default', array('class' => 'alert alert-danger'));
            }
            
        }

        $userData = $this->CustomerProfile->find('first', array('conditions' => array('userID' => $userId)));

        $this->request->data=$userData;
        $this->set('page_slug', 'edit_profile');
        $this->set('keywords_for_layout','Edit Profile');
        $this->set('description_for_layout', 'Edit Profile');
        $this->set('title_for_layout', 'Edit Profile');
        $this->set('navbarClass', array('navClass'=>'white-navbar'));
        $this->set('breadcrumb',array('/profile'=>'Profile','/edit'=>'Edit Profile'));
    }
    public function password() { 
        $userId = $this->Auth->user('id');
        
        if ($this->request->is('post') || $this->request->is('put')) {
            App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
            $passwordHasher = new SimplePasswordHasher();
            $userData = $this->Customer->find('first', array('conditions' => array('id' => $userId)));
            if($userData['Customer']['password']==$passwordHasher->hash($this->request->data['Customer']['oldpassword'])){ 
                $this->Customer->set($this->request->data);
                if ($this->Customer->validates()) {
                    $this->Customer->id = $this->Auth->user('id');
                    if ($this->Customer->saveField('password', $this->data['Customer']['confirm_password'])) {
                        $this->Session->setFlash(__('Password changed successfully.'), 'default', array('class' => "alert alert-success"));
                        $this->redirect(array('action' => "password"));
                    } else {
                        $this->Session->setFlash(__('Unable to change password,following error occurred.'), 'default', array('class' => 'alert alert-danger'));
                    }
                } else {
                    $this->Session->setFlash(__('Unable to change password,following error occurred.'), 'default', array('class' => 'alert alert-danger'));
                }
            }else{
                $this->Session->setFlash(__('Unable to authenticate your current password. Please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }

        $this->request->data['Customer']['id']=$userId;
        $this->set('page_slug', 'password');
        $this->set('keywords_for_layout','Change Password');
        $this->set('description_for_layout', 'Change Password');
        $this->set('title_for_layout', 'Change Password');
        $this->set('navbarClass', array('navClass'=>'white-navbar'));
        $this->set('breadcrumb',array('/profile'=>'Profile','/password'=>'Password'));
    }
    public function subscription() { 
        $userId = $this->Auth->user('id');
        $invoices=$this->CustomerInvoice->find('all',array('conditions'=>array('userID'=>$userId),'order'=>array('created desc')));

        $this->set('invoices',$invoices);
        $this->set('page_slug', 'subscription');
        $this->set('keywords_for_layout','Manage Subscription');
        $this->set('description_for_layout', 'Manage Subscription');
        $this->set('title_for_layout', 'Manage Subscription');
        $this->set('navbarClass', array('navClass'=>'white-navbar'));
        $this->set('breadcrumb',array('/profile'=>'Profile','/subscription'=>'Subscription'));
    }

    public function invoice($id,$download=false) { 
        ini_set('memory_limit', '512M');
        $userId = $this->Auth->user('id');
        $invoices=$this->CustomerInvoice->find('first',array('conditions'=>array('userID'=>$userId,'id'=>$id),'order'=>array('created desc')));
         if (empty($invoices)) {
            throw new NotFoundException();
        }
        $this->request->data=$invoices;
        $options = array('conditions' => array('userID' => $this->request->data['CustomerInvoice']['userID']));
        $CustomerProfile = $this->CustomerProfile->find('first', $options);
        $this->request->data['CustomerProfile'] = $CustomerProfile['CustomerProfile'];

        $options = array('conditions' => array($this->Customer->primaryKey =>$userId));
        $Customer = $this->Customer->find('first', $options);
        $this->request->data['Customer'] = $Customer['Customer'];

        $options = array('conditions' => array('meta_key' => "payment_company"), 'fields' => array('meta_value'));
        $payment_company = $this->Option->find('first', $options);

        $options = array('conditions' => array('meta_key' => "payment_mail"), 'fields' => array('meta_value'));
        $payment_mail = $this->Option->find('first', $options);

        $options = array('conditions' => array('meta_key' => "payment_address"), 'fields' => array('meta_value'));
        $payment_address = $this->Option->find('first', $options);

        $options = array('conditions' => array('meta_key' => "payment_contact"), 'fields' => array('meta_value'));
        $payment_contact = $this->Option->find('first', $options);
        
         $options = array('conditions' => array('meta_key' => "payment_company_url"), 'fields' => array('meta_value'));
        $payment_company_url = $this->Option->find('first', $options);

        $this->set('company_info', array('payment_mail' => $payment_mail['Option']['meta_value'],
            'payment_address' => $payment_address['Option']['meta_value'],
            'payment_contact' => $payment_contact['Option']['meta_value'],
            'payment_company' => $payment_company['Option']['meta_value'],
            'payment_company_url'=>$payment_company_url['Option']['meta_value'],
            ));
        $this->set('payment_mail', $payment_mail['Option']['meta_value']);

        

        $this->set('invoices',$invoices);
        $this->set('page_slug', 'Invoice');
        $this->set('keywords_for_layout','View Invoice');
        $this->set('description_for_layout', 'View Invoice');
        $this->set('title_for_layout', 'View Invoice');
        $this->set('navbarClass', array('navClass'=>'white-navbar'));
        if($download!=false){
            $this->layout="pdf";
            $this->render('download');
        }
        $this->set('breadcrumb',array('/profile'=>'Profile','/invoice'=>'Invoice'));
    }


}
?>