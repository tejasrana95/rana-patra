<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'Common');
App::import('Vendor', 'PayTM', array('file'=>'PayTM'.DS.'paytm.php'));
/**
 * Articles Controller
 *
 * @property Article $Article
 * @property PaginatorComponent $Paginator
 */
class PaymentsController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Auth', 'RequestHandler','Security');

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->layout = 'full_width_layout';
        $this->Auth->allow('ipn');
        if($this->RequestHandler->isPost() && ($this->action == 'paytm' ||$this->action == 'ipn' )){
        $this->Security->validatePost = false;
        $this->Security->enabled = false;
        $this->Security->csrfCheck = false;
        }
    }

    public $uses = array('Customer', 'CustomerProfile', 'CustomerInvoice', 'Article', 'Matrimonial', 'Option');

    public function subscription()
    {
        $userId = $this->Auth->user('id');
        $this->loadModel('Matrimonial');
        $this->loadModel('Event');
        $this->loadModel('News');
        $this->loadModel('Article');
        $userData = array();
        $userData['Matrimonial'] = $this->Matrimonial->find('count', array('conditions' => array('Matrimonial.userID' => $userId)));
        $userData['Event'] = $this->Event->find('count', array('conditions' => array('Event.userId' => $userId, 'Event.isAdmin = 0')));
        $userData['News'] = $this->News->find('count', array('conditions' => array('News.userId' => $userId, 'News.isAdmin = 0')));
        $userData['Article'] = $this->Article->find('count', array('conditions' => array('Article.userId' => $userId, 'Article.isAdmin = 0')));
        $this->set('stats', $userData);


        $this->set('page_slug', 'payment');
        $this->set('keywords_for_layout', 'Purchase / Renew Subscription');
        $this->set('description_for_layout', 'Purchase / Renew Subscription');
        $this->set('title_for_layout', 'Purchase / Renew Subscription');
        $this->set('navbarClass', array('navClass' => 'white-navbar'));
    }

    public function index()
    {
        $userId = $this->Auth->user('id');
        $this->set('page_slug', 'payment');
        $this->set('keywords_for_layout', 'Payment');
        $this->set('description_for_layout', 'Payment');
        $this->set('title_for_layout', 'Payment / Subscription');
        $this->set('navbarClass', array('navClass' => 'white-navbar'));
        $this->set('breadcrumb', array('/payment'=>'Payment'));
    }
    public function checkout()
    {
        $userId = $this->Auth->user('id');
        $this->set('page_slug', 'payment');
        $this->set('keywords_for_layout', 'Payment');
        $this->set('description_for_layout', 'Payment');
        $this->set('title_for_layout', 'Payment / Subscription');
        $this->set('navbarClass', array('navClass' => 'white-navbar'));
        $this->set('breadcrumb', array('/payment'=>'Payment','/payments/checkout'=>'Checkout'));
        if ($this->request->is(array('post'))) {
            $commonController = new CommonController();
            $results=$commonController->fetchCurrency();
            $amount=array('PRO-MEMBER'=>500,'MATRIMONIAL-AD'=>250);
            $amount=$amount[$this->request->data['CustomerInvoice']['item_name']];
            if ($this->request->data['CustomerInvoice']['currency_code']=="INR") {
                $amountConverted=$amount;
            } else {
                $amountConverted=$amount*($results['rates'][$this->request->data['CustomerInvoice']['currency_code']]);
            }
            $totalAmount=$amountConverted*$this->request->data['CustomerInvoice']['no_of_months'];
            $this->set('securityToken', base64_encode(json_encode($this->request->data)));
            $this->request->data['CustomerInvoice']['amount']=$totalAmount;
            $this->request->data['CustomerInvoice']['gross']=$totalAmount;
            $this->request->data['CustomerInvoice']['userID'] = $userId;
            $this->request->data['CustomerInvoice']['process_by'] = 0;
            $this->request->data['CustomerInvoice']['system_id'] = $userId;
            $this->request->data['CustomerInvoice']['actual_amount_inr'] =  $amount*$this->request->data['CustomerInvoice']['no_of_months'];
            $this->request->data['CustomerInvoice']['payment_status'] = 'PENDING';
            $this->request->data['CustomerInvoice']['item_number']=$this->request->data['CustomerInvoice']['item_name'].'-'.date('Ymdhis').'-'.ucwords(substr($this->request->data['CustomerInvoice']['first_name'], 0, 1).substr($this->request->data['CustomerInvoice']['last_name'], 0, 1));
           

            if ($this->request->data['CustomerInvoice']['pay_with']=="paytm") {
                $commonController = new CommonController();
                $options=$commonController->getOptionValue();
                define('PAYTM_REFUND_URL', 'https://'. $options['paytm_url'].'/oltp/HANDLER_INTERNAL/REFUND');
                define('PAYTM_STATUS_QUERY_URL', 'https://'. $options['paytm_url'].'/oltp/HANDLER_INTERNAL/TXNSTATUS');
                define('PAYTM_STATUS_QUERY_NEW_URL', 'https://'. $options['paytm_url'].'/oltp/HANDLER_INTERNAL/getTxnStatus');
                define('PAYTM_TXN_URL', 'https://'. $options['paytm_url'].'/oltp-web/processTransaction');
                if ($options['paytm_merchant_website']=="WEB_STAGING") {
                    define('PAYTM_ENVIRONMENT', 'TEST');
                } else {
                    define('PAYTM_ENVIRONMENT', 'PROD');
                }
                define('PAYTM_MERCHANT_KEY', $options['paytm_merchant_key']); 
                define('PAYTM_MERCHANT_MID', $options['paytm_mid']); 
                define('PAYTM_MERCHANT_WEBSITE', $options['paytm_merchant_website']);
                

                $paramList["MID"] = PAYTM_MERCHANT_MID;
                $paramList["ORDER_ID"] =  $this->request->data['CustomerInvoice']['item_number'];
                $paramList["CUST_ID"] = $userId;
                $paramList["INDUSTRY_TYPE_ID"] = 'Retail';
                $paramList["CHANNEL_ID"] ='WEB';
                $paramList["TXN_AMOUNT"] =  $amount*$this->request->data['CustomerInvoice']['no_of_months'];
                $paramList["WEBSITE"] = PAYTM_MERCHANT_WEBSITE;
                $paytm=new paytm();
                $checkSum =$paytm->getChecksumFromArray($paramList, PAYTM_MERCHANT_KEY);
                $this->request->data['CustomerInvoice']['checkSum']=$checkSum;
                $this->render('paytm');
            }
            $lastId=$this->CustomerInvoice->save($this->request->data);
            if ($lastId) {
                $this->request->data['CustomerInvoice']['lastID']=$lastId['CustomerInvoice']['id'];
            } else {
                $this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
                $this->redirect(array('action' => "/index"));
            }
        } else {
            $this->Session->setFlash(__('You can not access checkout page directly. Please fill this form to proceed to checkout.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "/index"));
        }
    }
    public function ipn($id)
    {
        $commonController = new CommonController();
        $options=$commonController->getOptionValue();

        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
                  $keyval = explode ('=', $keyval);
            if (count($keyval) == 2) {
                $myPost[$keyval[0]] = urldecode($keyval[1]);
            }
        }
// read the post from PayPal system and add 'cmd'
        $req = 'cmd=_notify-validate';
        if (function_exists('get_magic_quotes_gpc')) {
                   $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                  $value = urlencode($value);
            }
                   $req .= "&$key=$value";
        }
        $ch = curl_init($options['paypal_ipn']);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        if (!($res = curl_exec($ch))) {
            // error_log("Got " . curl_error($ch) . " when processing IPN data");
            curl_close($ch);
            exit;
        }
        curl_close($ch);
        if (strcmp ($res, "VERIFIED") == 0) {
            $status_paypal=($_POST['payment_status']=="Completed") ?'SUCCESS' :'FAILED';
            $this->request->data['CustomerInvoice']['payment_status']=$status_paypal;
            
        } elseif (strcmp ($res, "INVALID") == 0) {
            $status_paypal=($_POST['payment_status']=="Completed") ?'SUCCESS' :'FAILED';
            $this->request->data['CustomerInvoice']['payment_status']=$status_paypal;
        }
        $this->request->data['CustomerInvoice']['id'] = $id;
        $this->request->data['CustomerInvoice']['txn_id']=$_POST['txn_id'];
        $this->request->data['CustomerInvoice']['ipn_data']=serialize($_POST);
        $lastId=$this->CustomerInvoice->save($this->request->data);
        if($this->request->data['CustomerInvoice']['payment_status']=="SUCCESS"){
            $this->updateInvoice($id);
        }
        header("HTTP/1.1 200 OK");
        echo 'SUCCESS';
        exit();
    }

    public function success($id)
    {
        $userId = $this->Auth->user('id');
        $this->request->data['CustomerInvoice']['id'] = $id;
        $this->request->data['CustomerInvoice']['payment_status'] = 'SUCCESS';
        $lastId=$this->CustomerInvoice->save($this->request->data);

        $this->set('page_slug', 'success');
        $this->set('keywords_for_layout', 'Success');
        $this->set('description_for_layout', 'Success');
        $this->set('title_for_layout', 'Payment Success');
        $this->set('navbarClass', array('navClass' => 'white-navbar'));
        $this->set('breadcrumb', array('/payment'=>'Payment','/payment/checkout'=>'Checkout','/payment/success'=>'Success'));
    }
    public function cancel($id)
    {
        $userId = $this->Auth->user('id');
        $this->request->data['CustomerInvoice']['id'] = $id;
        $this->request->data['CustomerInvoice']['payment_status'] = 'FAILED';
        $lastId=$this->CustomerInvoice->save($this->request->data);

        $this->set('page_slug', 'cancel');
        $this->set('keywords_for_layout', 'Cancel');
        $this->set('description_for_layout', 'Cancel');
        $this->set('title_for_layout', 'Payment Cancel');
        $this->set('navbarClass', array('navClass' => 'white-navbar'));
        $this->set('breadcrumb', array('/payment'=>'Payment','/payment/checkout'=>'Checkout','/payment/cancel'=>'Cancel'));
    }
    public function paytm()
    {
        $userId = $this->Auth->user('id');
        $insertedInvoice=$this->CustomerInvoice->find('first', array('conditions' => array('CustomerInvoice.item_number' => $_POST['ORDERID'])));
        $this->request->data['CustomerInvoice']['id'] = $insertedInvoice['CustomerInvoice']['id'];
        $this->request->data['CustomerInvoice']['txn_id'] = $_POST['TXNID'];
        
        $commonController = new CommonController();
        $options=$commonController->getOptionValue();
        $paytmChecksum = "";
        $paramList = array();
        $isValidChecksum = "FALSE";
        $paramList = $_POST;
        $paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg
        $paytm=new paytm();
        $isValidChecksum = $paytm->verifychecksum_e($paramList, $options['paytm_merchant_key'], $paytmChecksum); //will return TRUE or FALSE string.
        $this->request->data['CustomerInvoice']['ipn_data']=serialize($_POST);
        
        $this->set('navbarClass', array('navClass' => 'white-navbar'));
        $this->set('page_slug', 'failed');
        $this->set('keywords_for_layout', 'failed');
        $this->set('description_for_layout', 'failed');
        $this->set('title_for_layout', 'Payment Failed');
        $this->set('breadcrumb', array('/payment'=>'Payment','/payment/checkout'=>'Checkout','/payment/cancel'=>'Failed'));
        if ($isValidChecksum == "TRUE") {
            if ($_POST["STATUS"] == "TXN_SUCCESS") {
                $this->request->data['CustomerInvoice']['payment_status'] = 'SUCCESS';
                $this->set('page_slug', 'success');
                $this->set('keywords_for_layout', 'Success');
                $this->set('description_for_layout', 'Success');
                $this->set('title_for_layout', 'Payment Success');
                $this->set('breadcrumb', array('/payment'=>'Payment','/payment/checkout'=>'Checkout','/payment/success'=>'Success'));
                $lastId=$this->CustomerInvoice->save($this->request->data);
                $this->updateInvoice($insertedInvoice['CustomerInvoice']['id']);
                $this->render('success');
            } else {
                $this->request->data['CustomerInvoice']['payment_status'] = 'FAILED';
                $lastId=$this->CustomerInvoice->save($this->request->data);
                $this->render('cancel');
            }
        } else {
            $this->request->data['CustomerInvoice']['payment_status'] = 'FAILED';
            $lastId=$this->CustomerInvoice->save($this->request->data);
            $this->set('reason', "Checksum mismatched.");
            $this->render('cancel');
        }
    }

    public function updateInvoice($currentID){
        $userId = $this->Auth->user('id');
        $insertedInvoice=$this->CustomerInvoice->find('first', array('conditions' => array('userID' => $userId,'payment_status' => "SUCCESS",'invoice_to >' => date('Y-m-d'),'item_name' => 'PRO-MEMBER','id !='=>$currentID),'order' => array('invoice_to DESC')));
        $currentInvoice=$this->CustomerInvoice->find('first', array('conditions' => array('id' => $currentID,'userID' => $userId,)));
        if(isset($insertedInvoice['CustomerInvoice']) && !empty($insertedInvoice['CustomerInvoice'])){
            if(empty($insertedInvoice['CustomerInvoice']['invoice_to']) || $insertedInvoice['CustomerInvoice']['invoice_to']==null || $insertedInvoice['CustomerInvoice']['invoice_to'] < date('Y-m-d')){
                $insertedInvoice['CustomerInvoice']['invoice_to']=date('Y-m-d');
            }
            $invoice_to = date("Y-m-d", strtotime("+".$currentInvoice['CustomerInvoice']['no_of_months']." month", strtotime($insertedInvoice['CustomerInvoice']['invoice_to'])));
            $invoice_from = date("Y-m-d", strtotime("+1 day", strtotime($insertedInvoice['CustomerInvoice']['invoice_to'])));
        }else{
            $invoice_to = date("Y-m-d", strtotime("+".$currentInvoice['CustomerInvoice']['no_of_months']." month", strtotime(date('Y-m-d'))));
            $invoice_from = date("Y-m-d");
        }
        $this->request->data['CustomerInvoice']['id'] =$currentID;
        $this->request->data['CustomerInvoice']['invoice_from'] =$invoice_from;
        $this->request->data['CustomerInvoice']['invoice_to'] =$invoice_to;                
        $lastId=$this->CustomerInvoice->save($this->request->data);

        $userData=$this->Customer->find('first', array('conditions' => array('id' => $userId)));
        $option = $this->Option->find('all');
        $allOption = array();
        foreach ($option as $opt) {
            $allOption[$opt['Option']['meta_key']] = $opt['Option']['meta_value'];
        }
        $sitename = $allOption['site_name'];
        $site_url = $allOption['site_url'];
        $admin_mail = $allOption['admin_mail'];
        $copyright_text =$allOption["copyright_text"];
        $email = new CakeEmail('smtp');
        $email->template('common');
        $email->emailFormat('html');
        $email->from(array($admin_mail => $sitename));
        $email->to(array($userData['Customer']['email']));
        $email->subject('Payment Received | Rana Community');
        $email->viewVars(array(
            'site_name' => $sitename,
            'site_url' => $site_url,
            'heading' => 'Thanks for your payment',
            'content' => '<p>Thank you for your payment. We have received your amount. You can download the invoice by logging into the account. Feel free to contact us to our sales team or customer support team.</p>',
            'options'=>$allOption, 
            'copyright_text' => $copyright_text, 
        ));
        $send=$email->send();
    }

}
