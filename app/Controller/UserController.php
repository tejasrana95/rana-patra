<?php

App::uses('AppController', 'Controller');
App::import('Vendor', 'Facebook',array('file'=>'Facebook'.DS.'autoload.php'));
App::import('Vendor', 'Google',array('file'=>'Google'.DS.'vendor'.DS.'autoload.php'));
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

/**
 * Articles Controller
 *
 * @property Article $Article
 * @property PaginatorComponent $Paginator
 */
class UserController extends AppController {

    /**
     * Components
     *
     * @var array
     */

    public $components = array(
       'Auth' => array(
            'authenticate' => array(
                'Form' => array(
                    'userModel' => 'Customer',
                    'fields' => array(
                        'username' => 'email',
                        'password' => 'password'
                        )
                    )
                )
            )
    );

     public function beforeFilter() {
        parent::beforeFilter();
        // $this->Security->validatePost = false;
        // $this->Security->enabled = false;
        // $this->Security->csrfCheck = false;
        $this->Auth->allow('login', 'logout','register','confirmationLinkGenerator','confirm','fblogin','fbcallback','googlelogin','googlecallback','reset','password');
    }

    public $uses = array('Customer','CustomerProfile','Option');

	public function logout() {
            $this->Session->setFlash(__('You are logged out successfully.'), 'default', array('class' => 'alert alert-success'));
            return $this->redirect($this->Auth->logout());
	}

    public function reset(){
        if ($this->Session->read('Auth.User')){
            $this->redirect('/dashboard');
        }

        if ($this->request->is('post')) {

            $conditions = array(
                'Customer.email' =>$this->request->data['Customer']['email'],
                );

            if ($this->Customer->hasAny($conditions)){
                
                $userData = $this->Customer->find('first', array('conditions' => array('email' => $this->request->data['Customer']['email'])));
                $confirmationLink=$this->confirmationLinkGenerator($userData['Customer']['id'],true);
                $option = $this->Option->find('all');
                $allOption = array();
                foreach ($option as $opt) {
                    $allOption[$opt['Option']['meta_key']] = $opt['Option']['meta_value'];
                }
                $sitename = $allOption['site_name'];
                $site_url = $allOption['site_url'];
                $admin_mail = $allOption['admin_mail'];
                $copyright_text =$allOption["copyright_text"];
                $email = new CakeEmail('smtp');
                $email->template('reset');
                $email->emailFormat('html');
                $email->from(array($admin_mail => $sitename));
                $email->to(array($this->request->data['Customer']['email']));
                $email->subject('Reset your password. Rana Patra | Rana Community');
                $email->viewVars(array(
                    'site_name' => $sitename,
                    'site_url' => $site_url,
                    'heading' => 'Alert from Rana Patra',
                    'content' => '<p>We are mailing for you because we get a request to reset your password. If you didn\'t make that request please ignore this mail your password will not changes. But if you made this request please follow below instructions.</p>',
                    'options'=>$allOption,
                    'confirmationLink' => $confirmationLink, 
                    'copyright_text' => $copyright_text, 
                ));
                $send=$email->send();
                $this->Session->setFlash(__('A confirmation mail has been sent to your email id. Please confirm your email id to regenerate new password.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "login"));
            } else {
                $this->Session->setFlash(__('Account not found. Are you sure you registered with this ('.$this->request->data['Customer']['email'].') email id?'), 'default', array('class' => 'alert alert-danger'));
            }

        }

            $this->layout='full_width_layout';
            $this->set('page_slug', 'Reset');
            $this->set('keywords_for_layout','Reset password');
            $this->set('description_for_layout', 'Please enter your mail id to reset your password');
            $this->set('title_for_layout', 'Reset password');
            $this->set('navbarClass', array('navClass'=>'white-navbar'));
    }

    public function login() { 

        if ($this->Session->read('Auth.User')){
            $this->redirect('/dashboard');
        }

        if ($this->request->is('post')) {
            $userinfo = $this->Customer->find('first', array('conditions' => array('Customer.email' => $this->request->data['Customer']['email'])));
            if(isset($userinfo['Customer'])){
                if( $userinfo['Customer']['status']==1){ 

                    if ($this->Auth->login()) {
                        return $this->redirect('/dashboard');
                    } else {
                       $this->Session->setFlash(__('It\'s seems you enter wrong email id or password. Please try again.'), 'default', array('class' => 'alert alert-danger'));
                       $this->redirect('/login');
                   }
               }else{
                   $this->Session->setFlash(__('Your account is not yet activated because you didn\'t verify your email id. Please check your email account and verify your email id.'), 'default', array('class' => 'alert alert-danger'));
                   $this->redirect('/login');
               }
           }else{
            $this->Session->setFlash(__('We are unable to find your account in our system. Are you sure you register with '.$this->request->data['Customer']['email'].' email id?'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect('/login');
        }
        }

        $this->layout='full_width_layout';
        $this->set('page_slug', 'login');
        $this->set('keywords_for_layout','Login page, signup, signin, forget password');
        $this->set('description_for_layout', 'Please login or create account for full access of Rana patra');
        $this->set('title_for_layout', 'Login');
        $this->set('navbarClass', array('navClass'=>'white-navbar'));
    }

    function register(){
        if ($this->Session->read('Auth.User')){
            $this->redirect('/dashboard');
        }
    	if ($this->request->is('post')) {
    		$this->request->data['Customer']['username']=$this->request->data['Customer']['email'];
    		$this->request->data['Customer']['status']=0;
            $this->Customer->create();
            if ($this->Customer->save($this->request->data)) {
            	$lastId=$this->Customer->getLastInsertID();
                $this->CustomerProfile->create();
                $this->request->data['CustomerProfile']['userID']=$lastId;
                $this->CustomerProfile->validator()->remove('gender');
                $this->CustomerProfile->validator()->remove('address');
                $this->CustomerProfile->validator()->remove('contact');
                $this->CustomerProfile->validator()->remove('placefinder');
                $this->CustomerProfile->validator()->remove('state');
                $this->CustomerProfile->validator()->remove('city');
                $this->CustomerProfile->validator()->remove('country');
                $this->CustomerProfile->save($this->request->data);
            	$confirmationLink=$this->confirmationLinkGenerator($lastId);
                $option = $this->Option->find('all');
                $allOption = array();
                foreach ($option as $opt) {
                    $allOption[$opt['Option']['meta_key']] = $opt['Option']['meta_value'];
                }
                $sitename = $allOption['site_name'];
                $site_url = $allOption['site_url'];
                $admin_mail = $allOption['admin_mail'];
                $copyright_text =$allOption["copyright_text"];
                $email = new CakeEmail('smtp');
                $email->template('signup');
                $email->emailFormat('html');
                $email->from(array($admin_mail => $sitename));
                $email->to(array($this->request->data['Customer']['email']));
                $email->subject('Verify your account. Rana Patra | Rana Community');
                $email->viewVars(array(
                    'site_name' => $sitename,
                    'site_url' => $site_url,
                    'heading' => 'Welcome to '.$sitename,
                    'content' => '<p>Welcome to '.$sitename.'. We are very excited to see you as a part of member of '.$sitename.'.</p>',
                    'options'=>$allOption,
                    'confirmationLink' => $confirmationLink,
                    'copyright_text' => $copyright_text, 
                ));
                $send=$email->send();
                $this->Session->setFlash(__('Account created. Please confirm your email id.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "login"));
            } else {
                $this->Session->setFlash(__('Something went wrong, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }


    	$this->layout='full_width_layout';
    	$this->set('page_slug', 'Register with Rana Patra');
    	$this->set('keywords_for_layout','Signup page, signup, signin, forget password');
    	$this->set('description_for_layout', 'Please create account for full access of Rana patra');
    	$this->set('title_for_layout', 'Register');
    	$this->set('navbarClass', array('navClass'=>'white-navbar'));   	
    	
    }


    function password($string=''){
       if ($this->Session->read('Auth.User')){
            $this->redirect('/dashboard');
        }
          
        if ( isset($this->request->data) && !empty($this->request->data)) {
            $userinfo = $this->Customer->find('first', array('conditions' => array('Customer.id' => $this->request->data['Customer']['id'])));
            if($userinfo['Customer']['email']!= $this->request->data['Customer']['email']){
                 $this->Session->setFlash(__('Something went wrong, please try again.'), 'default', array('class' => 'alert alert-danger'));
                $this->redirect(array('action' => "login"));
            }
            $this->request->data['Customer']['status']=1;
            unset($this->request->data['Customer']['email']);
            if ($this->Customer->save($this->request->data)) {
                $option = $this->Option->find('all');
                $allOption = array();
                foreach ($option as $opt) {
                    $allOption[$opt['Option']['meta_key']] = $opt['Option']['meta_value'];
                }
                $sitename = $allOption['site_name'];
                $site_url = $allOption['site_url'];
                $admin_mail = $allOption['admin_mail'];
                $copyright_text =$allOption["copyright_text"];
                
                $email = new CakeEmail('smtp');
                $email->template('password_changed');
                $email->emailFormat('html');
                $email->from(array($admin_mail => $sitename));
                $email->to(array($userinfo['Customer']['email']));
                $email->subject('Your Password has been changed. Rana Patra | Rana Community');
                $email->viewVars(array(
                'site_name' => $sitename,
                'site_url' => $site_url,
                'heading' => 'Alert from Rana Patra',
                'content' => '<p>This is an alert email. We detect that you recently change your password.</p><p>If you didn\'t change password then please reset your password. Or if you change it then please ignore this mail.</p>',
                'options'=>$allOption,
                'copyright_text' => $copyright_text, 
                ));
                $send=$email->send();
                $this->Session->setFlash(__('Your password has been changed successfully. Please login with your new password.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "login"));
            } else {
                $this->Session->setFlash(__('There is an error in password.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        if($string!="" && !empty($string)){ 
            $id=$this->confirm($string,true);
            if (!$this->Customer->exists($id)) { 
              $this->Session->setFlash(__('We are not able to identify your email id. Are you sure you register with that email id?'), 'default', array('class' => 'alert alert-danger'));
              $this->redirect(array('action' => "login/reset"));
          }
          if(is_numeric($id) && $id!=false){
            $userData = $this->Customer->find('first', array('conditions' => array('id' => $id)));
            unset($userData['Customer']['password']);
            $this->request->data=$userData;
            $this->layout='full_width_layout';
            $this->set('page_slug', 'Reset your password');
            $this->set('keywords_for_layout','Reset password');
            $this->set('description_for_layout', 'Please enter your new password');
            $this->set('title_for_layout', 'Reset password');
            $this->set('navbarClass', array('navClass'=>'white-navbar'));    
        }else{
           $this->Session->setFlash(__('We are not able to identify your email id. Are you sure you register with that email id?'), 'default', array('class' => 'alert alert-danger'));
           $this->redirect("/login/reset");
       }   
   }else{
       $this->Session->setFlash(__('You do not have sufficient permissions to access this page.'), 'default', array('class' => 'alert alert-danger'));
       $this->redirect("/login");
   } 

}
    function confirmationLinkGenerator($id,$reset=false){
    	
    	if (!$this->Customer->exists($id)) {
            return false;
        }
        	$userinfo = $this->Customer->find('first', array('conditions' => array('Customer.id' => $id)));
			$site_url = CakeSession::read('site_url'); 	
        	$email=$userinfo['Customer']['email'];
        	$id=$userinfo['Customer']['id'];
        	$created=$userinfo['Customer']['created'];
        	$new_time = date("Y-m-d H:i:s", strtotime('+2 hours', strtotime($created)));
        	$new_time=strtotime($new_time);
            $linkUrl=$site_url.'login/confirm/';

            if($reset==true){
                $this->request->data['Customer']['status'] =  $userinfo['Customer']['status'];
                $this->Customer->id = $id;
                $this->Customer->save($this->request->data);

                $created=$userinfo['Customer']['modified'];
                $new_time = date("Y-m-d H:i:s", strtotime('+2 hours', strtotime($created)));
                $new_time=strtotime($new_time);
                $linkUrl=$site_url.'login/password/';
            }


        	$code=md5($email).'-'.base64_encode('Tejas'.$id).'-'.sha1($email).'-'.base64_encode($new_time).'-'.md5(sha1($new_time));
        	

        	$url=$linkUrl.$code;
        	return $url;
       
    }



     function confirm($string,$validate=false){

        if ($this->Session->read('Auth.User')){
            $this->redirect('/dashboard');
        }
     	$stringData=explode('-',$string);
		$id=str_replace('Tejas','',base64_decode($stringData[1]));
		if (!$this->Customer->exists($id)) {
			$this->Session->setFlash(__('We are not able to identify your email id. Are you sure you register with that email id?'), 'default', array('class' => 'alert alert-danger'));
		}

		$userinfo = $this->Customer->find('first', array('conditions' => array('Customer.id' => $id)));
		$email=$userinfo['Customer']['email'];
		$linkTime=base64_decode($stringData[3]);
		if($stringData[0]==md5($email) && $stringData[2]==sha1($email) && $stringData[4]==md5(sha1($linkTime))){
			$now=strtotime(date("Y-m-d H:i:s"));
           
			if($now<=$linkTime){
                if($validate==true){
                    return $id;     
                }else{
                    $this->request->data['Customer']['status'] =  1;
                    $this->Customer->id = $id;
                    $this->Customer->save($this->request->data);


                    $option = $this->Option->find('all');
                    $allOption = array();
                    foreach ($option as $opt) {
                        $allOption[$opt['Option']['meta_key']] = $opt['Option']['meta_value'];
                    }
                    $sitename = $allOption['site_name'];
                    $site_url = $allOption['site_url'];
                    $admin_mail = $allOption['admin_mail'];
                    $copyright_text =$allOption["copyright_text"];
                    $email = new CakeEmail('smtp');
                    $email->template('common');
                    $email->emailFormat('html');
                    $email->from(array($admin_mail => $sitename));
                    $email->to(array($userinfo['Customer']['email']));
                    $email->subject('Email verified. Rana Patra | Rana Community');
                    $email->viewVars(array(
                        'site_name' => $sitename,
                        'site_url' => $site_url,
                        'heading' => 'Alert from Rana Patra',
                        'content' => '<p>This is a notification mail. Your email id is verified. Now you can log in with your email and password.</p>',
                        'options'=>$allOption, 
                        'copyright_text' => $copyright_text, 
                    ));
                    $send=$email->send();
                    $this->Session->setFlash(__('Your email id verified successfully. Please login with your password.'), 'default', array('class' => "alert alert-success"));
			         return $this->redirect('/login');
                }
			}else{
				$this->Session->setFlash(__('You confirmation link is expired. Please reset your password to activate and set new password.'), 'default', array('class' => 'alert alert-danger'));
                if($validate==true){
                    return $this->redirect('/login/reset');
                }else{
                    return $this->redirect('/login/register');
                }
			}
		}else{
			$this->Session->setFlash(__('We are not able to authenticate this confirmation link. Please reset your password to regenerate new confirmation link.'), 'default', array('class' => 'alert alert-danger'));
			 if($validate==true){
                    return false;  
            }else{
                    return $this->redirect('/login/register');
            }
		} 
		
    }

    function fblogin(){
    	Configure::load('facebook');
    	$appId=Configure::read('Facebook.appId');
    	$app_secret=Configure::read('Facebook.secret');
    	$fb = new Facebook\Facebook([
		  'app_id' => $appId, // Replace {app-id} with your app id
		  'app_secret' =>  $app_secret,
		  'default_graph_version' => 'v2.2',
		  ]);
        CakeSession::delete('FBRLH_state');
		$site_url = CakeSession::read('site_url'); 

    	$helper = $fb->getRedirectLoginHelper();
    	$permissions = ['email','user_birthday','public_profile']; 
    	
    	$loginUrl = $helper->getLoginUrl($site_url.'login/fbcallback', $permissions);

    	return $loginUrl.'&display=popup';
    }

    function fbcallback(){
    	Configure::load('facebook');
    	$appId=Configure::read('Facebook.appId');
    	$app_secret=Configure::read('Facebook.secret');
         CakeSession::delete('FBRLH_state');

         if(isset($_GET['state'])){
            CakeSession::write('FBRLH_state',$_GET['state']);
         }
 

    	$fb = new Facebook\Facebook([
		  'app_id' => $appId, // Replace {app-id} with your app id
		  'app_secret' =>  $app_secret,
		  'default_graph_version' => 'v2.4',
		  ]);
    	$permissions = ['email','user_birthday','public_profile']; 
    	$error=false;
    	$message="";
    	$helper = $fb->getRedirectLoginHelper($permissions);
               
    	try {
    		$accessToken = $helper->getAccessToken();
    	} catch(Facebook\Exceptions\FacebookResponseException $e) {
    		$error=true;
    		$message= 'Graph returned an error: ' . $e->getMessage();
    	} catch(Facebook\Exceptions\FacebookSDKException $e) {
    		$error=true;
    		$message=  'Facebook SDK returned an error: ' . $e->getMessage();
    	}
    	if (! isset($accessToken)) {
    		if ($helper->getError()) {
    			$error=true;
    			$message=  "Error: " . $helper->getError() . "<br/>";
    			$message .=  "Error Code: " . $helper->getErrorCode() . "<br/>";
    			$message .= "Error Reason: " . $helper->getErrorReason() . "<br/>";
    			$message .= "Error Description: " . $helper->getErrorDescription() . "<br/>";
    		} else {
    			$error=true;
    			$message= 'Bad request';
    		}
    	}

    	if($error==0){ 
    		$oAuth2Client = $fb->getOAuth2Client();
    		$tokenMetadata = $oAuth2Client->debugToken($accessToken);
    		$tokenMetadata->validateAppId($appId); 
    		$tokenMetadata->validateExpiration();
    		try {
    			$response = $fb->get('/me?fields=id,name,email,birthday,gender,picture',(string) $accessToken);
    			$userNode = $response->getGraphUser();

    			$userData=$response->getDecodedBody();

    			$conditions = array(
    				'Customer.email' => $userData['email'],
    				);
    			if (!$this->Customer->hasAny($conditions)){

    				$this->Customer->create();
    				$this->CustomerProfile->create();
    				$this->request->data['Customer']['username'] =  $userData['email'];
    				$this->request->data['Customer']['password'] =  $userData['id'].'Tejas'.$userData['email'];
    				$this->request->data['Customer']['confirm_password'] =  $this->request->data['Customer']['password'];
    				$this->request->data['Customer']['email'] =  $userData['email'];
    				$this->request->data['Customer']['status'] =1;
    				$this->request->data['Customer']['account_type'] = 'fb';
    				$this->request->data['Customer']['access_token'] =(string) $accessToken;

    				$customerProfile=$this->Customer->save($this->request->data);
    				$this->request->data['CustomerProfile']['userID'] =  $this->Customer->getLastInsertID();
    				$this->request->data['CustomerProfile']['name'] =  $userData['name'];
    				//$this->request->data['CustomerProfile']['profilePic'] =  $userData['picture']['data']['url'];
    				$this->request->data['CustomerProfile']['gender'] =  ($userData['gender']=='male') ? 1:0;

    				if ($this->CustomerProfile->save($this->request->data) && $customerProfile) {
                        //after create
                       $option = $this->Option->find('all');
                        $allOption = array();
                        foreach ($option as $opt) {
                            $allOption[$opt['Option']['meta_key']] = $opt['Option']['meta_value'];
                        }
                        $sitename = $allOption['site_name'];
                        $site_url = $allOption['site_url'];
                        $admin_mail = $allOption['admin_mail'];
                        $copyright_text =$allOption["copyright_text"];
                        $email = new CakeEmail('smtp');
                        $email->template('social_login');
                        $email->emailFormat('html');
                        $email->from(array($admin_mail => $sitename));
                        $email->to(array($this->request->data['Customer']['email']));
                        $email->subject('Welcome to Rana Patra | Rana Community');
                        $email->viewVars(array(
                            'site_name' => $sitename,
                            'site_url' => $site_url,
                            'heading' => 'Welcome to '.$sitename,
                            'content' => '<p>Thank you for login with Facebook. We are very happy to see you. Our community is powered by you. And we hope you will find all your need here. With this community, you can find interesting articles, matrimonials, event and news information.</p>',
                            'options'=>$allOption, 
                            'copyright_text' => $copyright_text, 
                        ));
                        $send=$email->send();

                        $loginData = $this->Customer->find('first', array('conditions' => array('Customer.id' => $this->Customer->getLastInsertID())));
                        
                        $this->Session->write('Auth.User.id',$this->Customer->getLastInsertID() );
                        $this->Session->write('Auth.User.username',$loginData['Customer']['username']);
                        $this->Session->write('Auth.User.email',$loginData['Customer']['email'] );
                        $this->Session->write('Auth.User.status',$loginData['Customer']['status'] );
                        $this->Session->write('Auth.User.account_type',$loginData['Customer']['account_type'] );
                        $this->Session->write('Auth.User.access_token',$loginData['Customer']['access_token'] );
                        $this->Session->write('Auth.User.created',$loginData['Customer']['created'] );
                        $this->Session->write('Auth.User.modified',$loginData['Customer']['modified']);
    				} else {
    					$this->Session->setFlash(__('Something went wrong please try again.'), 'default', array('class' => 'alert alert-danger'));
    					$this->Session->write('errorMessage',$message);
    					$this->redirect('/login?error=1');
    				}
    			}else{
    				$customerData=$this->Customer->find('first',array('conditions'=>$conditions));

    				$this->request->data['Customer']['account_type'] = 'fb';
    				$this->request->data['Customer']['access_token'] =(string) $accessToken;

    				$this->request->data['CustomerProfile']['name'] =  $userData['name'];
    				//$this->request->data['CustomerProfile']['profilePic'] =  $userData['picture']['data']['url'];

    				$customerData=$customerData['Customer'];

    				$this->Customer->id = $customerData['id'];
					$this->Customer->save($this->request->data);

					$conditions = array(
    					'CustomerProfile.userID' => $customerData['id'],
    				);

					$customerProfileData=$this->CustomerProfile->find('first',array('conditions'=>$conditions));

					$this->CustomerProfile->id = $customerProfileData['CustomerProfile']['id'];
					$this->CustomerProfile->save($this->request->data);


                    $this->Session->write('Auth.User.id',$this->Customer->id );
                    $this->Session->write('Auth.User.username',$customerData['username']);
                    $this->Session->write('Auth.User.email',$customerData['email'] );
                    $this->Session->write('Auth.User.status',$customerData['status'] );
                    $this->Session->write('Auth.User.account_type',$customerData['account_type'] );
                    $this->Session->write('Auth.User.access_token',$customerData['access_token'] );
                    $this->Session->write('Auth.User.created',$customerData['created'] );
                    $this->Session->write('Auth.User.modified',$customerData['modified']);
    			}

    		} catch(Facebook\Exceptions\FacebookResponseException $e) {
    			$error=true;
    			$message=  'Graph returned an error: ' . $e->getMessage();
    			$this->Session->write('errorMessage',$message);
    		} catch(Facebook\Exceptions\FacebookSDKException $e) {
    			$error=true;
    			$message=  'Facebook SDK returned an error: ' . $e->getMessage();
    			$this->Session->write('errorMessage',$message);
    		}
    	}else{
    		$this->Session->write('errorMessage',$message);
    	}
    	$this->set('error',$error);
    	$this->layout=false;
    }



    function googlelogin(){
    	Configure::load('facebook');
    	$client_id=Configure::read('Google.client_id');
    	$client_secret=Configure::read('Google.client_secret');
    	$site_url = CakeSession::read('site_url'); 

		$client = new Google_Client();
		$client->setClientId($client_id);
		$client->setClientSecret($client_secret);

		$client->setRedirectUri($site_url.'login/googlecallback');
		$client->addScope("email");
		$client->addScope("profile");

		$service = new Google_Service_Oauth2($client);

 		$authUrl = $client->createAuthUrl();
    	return $authUrl;
    }

    function googlecallback(){
    	Configure::load('facebook');
    	$client_id=Configure::read('Google.client_id');
    	$client_secret=Configure::read('Google.client_secret');
    	$site_url = CakeSession::read('site_url'); 
    	if (isset($_GET['code'])) {
    		$error=false;
    		$client = new Google_Client();
    		$client->setClientId($client_id);
			$client->setClientSecret($client_secret);
			$client->setRedirectUri($site_url.'login/googlecallback');
		  	$client->authenticate($_GET['code']);
		  	$client->getAccessToken();
 			$client->setAccessToken($client->getAccessToken());
			
			$service = new Google_Service_Oauth2($client);

 			$user = $service->userinfo->get(); //get user info 
		 // header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
 			

 			$conditions = array(
    				'Customer.email' => $user->email,
					);
    			if (!$this->Customer->hasAny($conditions)){

    				$this->Customer->create();
    				$this->CustomerProfile->create();
    				$this->request->data['Customer']['username'] =   $user->email;
    				$this->request->data['Customer']['password'] =  $user->id.'Tejas'.$user->email;
    				$this->request->data['Customer']['confirm_password'] =  $this->request->data['Customer']['password'];
    				$this->request->data['Customer']['email'] =  $user->email;
    				$this->request->data['Customer']['status'] =1;
    				$this->request->data['Customer']['account_type'] = 'google';
    				$this->request->data['Customer']['access_token'] =	$_GET['code'];

    				$customerProfile=$this->Customer->save($this->request->data);
    				$this->request->data['CustomerProfile']['userID'] =  $this->Customer->getLastInsertID();;
    				$this->request->data['CustomerProfile']['name'] =  $user->name;
    				$this->request->data['CustomerProfile']['profilePic'] =  $user->picture;
    				$this->request->data['CustomerProfile']['gender'] =  ($user->gender=='male') ? 1:0;

    				if ($this->CustomerProfile->save($this->request->data) && $customerProfile) {
                        //after create
                        $option = $this->Option->find('all');
                        $allOption = array();
                        foreach ($option as $opt) {
                            $allOption[$opt['Option']['meta_key']] = $opt['Option']['meta_value'];
                        }
                        $sitename = $allOption['site_name'];
                        $site_url = $allOption['site_url'];
                        $admin_mail = $allOption['admin_mail'];
                        $copyright_text =$allOption["copyright_text"];
                        $email = new CakeEmail('smtp');
                        $email->template('social_login');
                        $email->emailFormat('html');
                        $email->from(array($admin_mail => $sitename));
                        $email->to(array($this->request->data['Customer']['email']));
                        $email->subject('Welcome to Rana Patra | Rana Community');
                        $email->viewVars(array(
                            'site_name' => $sitename,
                            'site_url' => $site_url,
                            'heading' => 'Welcome to '.$sitename,
                            'content' => '<p>Thank you for login with Google. We are very happy to see you. Our community is powered by you. And we hope you will find all your need here. With this community, you can find interesting articles, matrimonials, event and news information.</p>',
                            'options'=>$allOption, 
                            'copyright_text' => $copyright_text, 
                        ));
                        $send=$email->send();

                        $loginData = $this->Customer->find('first', array('conditions' => array('Customer.id' => $this->Customer->getLastInsertID())));
                        
                        $this->Session->write('Auth.User.id',$this->Customer->getLastInsertID() );
                        $this->Session->write('Auth.User.username',$loginData['Customer']['username']);
                        $this->Session->write('Auth.User.email',$loginData['Customer']['email'] );
                        $this->Session->write('Auth.User.status',$loginData['Customer']['status'] );
                        $this->Session->write('Auth.User.account_type',$loginData['Customer']['account_type'] );
                        $this->Session->write('Auth.User.access_token',$loginData['Customer']['access_token'] );
                        $this->Session->write('Auth.User.created',$loginData['Customer']['created'] );
                        $this->Session->write('Auth.User.modified',$loginData['Customer']['modified']);

    				} else {
    					$this->Session->setFlash(__('Something went wrong please try again.'), 'default', array('class' => 'alert alert-danger'));
    					$this->Session->write('errorMessage',$message);
    				//	$this->redirect('/login?error=1');
    				}
    			}else{
    				$customerData=$this->Customer->find('first',array('conditions'=>$conditions));

    				$this->request->data['Customer']['account_type'] = 'google';
    				$this->request->data['Customer']['access_token'] =$_GET['code'];

    				$this->request->data['CustomerProfile']['name'] =   $user->name;
    				$this->request->data['CustomerProfile']['profilePic'] =   $user->picture;

    				$customerData=$customerData['Customer'];

    				$this->Customer->id = $customerData['id'];
					$this->Customer->save($this->request->data);

					$conditions = array(
    					'CustomerProfile.userID' => $customerData['id'],
    				);

					$customerProfileData=$this->CustomerProfile->find('first',array('conditions'=>$conditions));

					$this->CustomerProfile->id = $customerProfileData['CustomerProfile']['id'];
					$this->CustomerProfile->save($this->request->data);

                    $this->Session->write('Auth.User.id',$this->Customer->id );
                    $this->Session->write('Auth.User.username',$customerData['username']);
                    $this->Session->write('Auth.User.email',$customerData['email'] );
                    $this->Session->write('Auth.User.status',$customerData['status'] );
                    $this->Session->write('Auth.User.account_type',$customerData['account_type'] );
                    $this->Session->write('Auth.User.access_token',$customerData['access_token'] );
                    $this->Session->write('Auth.User.created',$customerData['created'] );
                    $this->Session->write('Auth.User.modified',$customerData['modified']);
    			}

		  	$this->set('error',$error);
    		$this->layout=false;
		}else{
			$error=true;
    		$message=  'Access denied by user';
    		$this->Session->write('errorMessage',$message);
    		$this->set('error',$error);
    		$this->layout=false;
		}
    }
}
?>