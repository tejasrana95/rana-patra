<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');
App::import('Vendor', 'ClassUpload', array('file' => 'class.upload.php'));

/**
 * Articles Controller
 *
 * @property Article $Article
 * @property PaginatorComponent $Paginator
 */
class BlogController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Security');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
        );
    public $uses = array('Article','ArticleCategory');
    // function call before any view rendor 
    // function used to add and override function feautres
    function beforeRender() {
        parent::beforeRender();
    }

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Security->validatePost = true;
        // Allow 
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        //end check permission

        $this->Paginator->settings = $this->paginate;
        $this->Article->recursive = 0;
$this->set('title', 'Blog');
        if(isset($_GET['term'])){
         $this->Paginator->settings = array(
            'conditions' => array("OR" => array(
                "Article.article_title LIKE" => '%'.addslashes($_GET['term']).'%',
                "Article.content LIKE" => '%'.addslashes($_GET['term']).'%',
                )),
            );
         $this->set('title', 'Search result for '.$_GET['term']);
     }
     $articles = $this->Paginator->paginate('Article');
     $this->layout = "common_layout";
     $url=rtrim(Router::url('/', true),'/');
     $breadcrumb=array($url=>'Home','blog'=>'Blog');
     $this->set('breadcrumb',$breadcrumb);
     $this->set('blogs', $articles);
     $this->set('title_for_layout', 'Blog');
     $this->set('article_name', 'Blog');
     $this->set('navbarClass', array('navClass' => 'white-navbar'));
 }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($main_category, $category=null, $subcategory=null, $slug=null) {
        //check permission
       $url=rtrim(Router::url('/', true),'/');

       $breadcrumb=array($url=>'Home','blog'=>'Blog');

       if(isset($main_category) && isset($category) && isset($subcategory) && isset($slug)){

        $main_category_id = $this->ArticleCategory->find('first', array(
            'fields' => array('ArticleCategory.id','ArticleCategory.label'),
            'conditions' => array('ArticleCategory.identifier' => $main_category)
            ));

        $category_id = $this->ArticleCategory->find('first', array(
            'fields' => array('ArticleCategory.id','ArticleCategory.label'),
            'conditions' => array('ArticleCategory.identifier' => $category)
            ));

        $subcategory_id = $this->ArticleCategory->find('first', array(
            'fields' => array('ArticleCategory.id','ArticleCategory.label'),
            'conditions' => array('ArticleCategory.identifier' => $subcategory)
            ));


        $options = array('conditions' => array('Article.main_category' => $main_category_id['ArticleCategory']['id'],
            'Article.category' => $category_id['ArticleCategory']['id'],
            'Article.sub_category' => $subcategory_id['ArticleCategory']['id'],
            'Article.article_slug' => $slug
            ));


        $breadcrumb['category/'.$main_category]=$main_category_id['ArticleCategory']['label'];
        $breadcrumb[$category]=$category_id['ArticleCategory']['label'];
        $breadcrumb[$subcategory]=$subcategory_id['ArticleCategory']['label'];

    }elseif(isset($main_category) && isset($category) && isset($subcategory)){

       $main_category_id = $this->ArticleCategory->find('first', array(
        'fields' => array('ArticleCategory.id','ArticleCategory.label'),
        'conditions' => array('ArticleCategory.identifier' => $main_category)
        ));

       $category_id = $this->ArticleCategory->find('first', array(
        'fields' => array('ArticleCategory.id','ArticleCategory.label'),
        'conditions' => array('ArticleCategory.identifier' => $category)
        ));

       $options = array('conditions' => array('Article.main_category' => $main_category_id['ArticleCategory']['id'],
        'Article.category' => $category_id['ArticleCategory']['id'],
        'Article.article_slug' => $subcategory
        ));

       $breadcrumb['category/'.$main_category]=$main_category_id['ArticleCategory']['label'];
       $breadcrumb[$category]=$category_id['ArticleCategory']['label'];
   }elseif(isset($main_category) && isset($category)){
       $main_category_id = $this->ArticleCategory->find('first', array(
        'fields' => array('ArticleCategory.id','ArticleCategory.label'),
        'conditions' => array('ArticleCategory.identifier' => $main_category)
        ));

       $options = array('conditions' => array('Article.main_category' => $main_category_id['ArticleCategory']['id'],
        'Article.article_slug' => $category
        ));

       $breadcrumb['category/'.$main_category]=$main_category_id['ArticleCategory']['label'];
   }elseif(isset($main_category)){
    $options = array('conditions' => array('Article.article_slug' => $main_category));
}else{
   throw new NotFoundException();
}

$article=$this->Article->find('first', $options);
$this->set('blog',$article );

$breadcrumb[$article['Article']['article_slug']]=$article['Article']['article_name'];

$this->layout = "full_width_layout";
$this->set('page_slug', $article['Article']['article_slug']);
        //debug($articles);
        //die();
$this->set('breadcrumb',$breadcrumb);
$this->set('title_for_layout', $article['Article']['article_name']);
$this->set('keywords_for_layout', $article['Article']['article_keywords']);
$this->set('description_for_layout', $article['Article']['article_description']);
$this->set('article_name', $article['Article']['article_name']);
$this->set('title', $article['Article']['article_name']);
$this->set('navbarClass', array('navClass' => 'white-navbar'));
}

    /**
     * add method
     *
     * @return void
     */
    public function addComment($copyData = null) {
        $userId = $_SESSION['Auth']['User']['id'];
        $isAdmin = 1;
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('article', 'add');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
                ));
        }
        //end check permission

        $this->loadModel('ArticleCategory');
        $this->loadModel('Widget');

        if ($this->request->is('post')) {

            $this->Article->create();

            $this->request->data['Article']['userId'] = $userId;
            $this->request->data['Article']['isAdmin'] = $isAdmin;
            if ($this->Article->save($this->request->data)) {
                //saving file location
                $this->Session->setFlash(__('The article has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }


        $subcates = array();
        $parentcates = array();


        $this->set('parentcates', $parentcates);
        $this->set('subcates', $subcates);
        $this->set('title_for_layout', 'Add New Article');
        $this->set('article_name', 'Add New Article');
        $this->set('parent_article_name', 'List Articles');
        $this->set('article_title', 'Add New Article');
    }
    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('article', 'delete');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
                ));
        }
        //end check permission
        $this->Article->id = $id;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }
        //method allow for action 
        $this->request->allowMethod('post', 'delete');
        //delete media 
        $this->loadModel('Media');
        $media_detail = $this->Media->find('first', array('conditions' => array(
            'Media.media_id' => $id,
            'Media.used_for' => 'Article'
            )));
        if (isset($media_detail) && !empty($media_detail)) {
            $this->Media->id = $media_detail['Media']['id'];
            $this->Media->delete();
        }
        //end
        if ($this->Article->delete()) {
            $this->Session->setFlash(__('The article has been deleted.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => "index"));
        } else {
            $this->Session->setFlash(__('The article could not be deleted, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index"));
        }
    }


    /**
     * home article content method
     *
     */
    //get image file method
    public function get_file($id = null) {
        if (isset($id)) {
            $this->loadModel('Media');
            $file_detail = $this->Article->find('first', array(
                'conditions' => array(
                    'Article.id' => $id
                    )));
            $file = $this->Media->find('first', array('conditions' => array(
                'Media.media_id' => $id,
                'Media.used_for' => 'Article')));
            $this->autoRender = false;
            if (!empty($file)) {
                return $file;
            } else {
                //$this->redirect('index');
            }
        }
    }

    function getArticles($onlyadmin=0,$featured=1,$limit=3){
        $options=array();
        if(isset($onlyadmin) && $onlyadmin==1){
            $options['conditions']['Article.isAdmin']=1;
        }
        if(isset($featured) && $featured==1){
            $options['conditions']['Article.featured']=1;
        }
        $options['conditions']['Article.status']=1;
        $options['order']=array('created'=>'desc');
        if($limit!="all"){
            $options['limit']=$limit;
        }

        $articles=$this->Article->find('all', $options);
        return $articles;
    }
    
    function category(){
        $AllParentArticleCategory = $this->ArticleCategory->find('all', array(
            'conditions' => array('ArticleCategory.parent' => 0)
            ));
        $cateogriesTree=array();
        $i=0;
        foreach($AllParentArticleCategory as $parentCategories){
            $cateogriesTree[$i]=$parentCategories['ArticleCategory'];
            
            $AllSubChildArticleCategory = $this->ArticleCategory->find('all', array(
                'conditions' => array('ArticleCategory.parent' => $parentCategories['ArticleCategory']['id'])
                )); 
            
            foreach($AllSubChildArticleCategory as $childArticle){

                $cateogriesTree[$i]['child'][]=$childArticle['ArticleCategory'];
            }
            $i++;
        }
        $this->layout="";
        $this->set('categories',$cateogriesTree);
    }
    function categoryblog($main_category=null,$category=null,$subcategory=null){
        if(isset($main_category)){
            $main_category_id = $this->ArticleCategory->find('first', array(
                'fields' => array('ArticleCategory.id','ArticleCategory.label'),
                'conditions' => array('ArticleCategory.identifier' => $main_category)
                ));

            $this->Paginator->settings = array(
                'conditions' => array('Article.main_category' => $main_category_id['ArticleCategory']['id']),
                );

            
            if(isset($category)){
                $category_id = $this->ArticleCategory->find('first', array(
                    'fields' => array('ArticleCategory.id','ArticleCategory.label'),
                    'conditions' => array('ArticleCategory.identifier' => $category)
                    ));
                $this->Paginator->settings['conditions']['Article.category'] = $category_id['ArticleCategory']['id'];
            }
            if(isset($subcategory)){
                $subcategory_id = $this->ArticleCategory->find('first', array(
                 'fields' => array('ArticleCategory.id','ArticleCategory.label'),
                 'conditions' => array('ArticleCategory.identifier' => $subcategory)
                 ));
                $this->Paginator->settings['conditions']['Article.sub_category'] = $subcategory_id['ArticleCategory']['id'];
            }
            
            //$this->Paginator->settings = $this->paginate;
            $this->Article->recursive = 0;


            $articles = $this->Paginator->paginate('Article');

            $this->layout = "common_layout";
            $url=rtrim(Router::url('/', true),'/');
            $breadcrumb=array($url=>'Home','blog'=>'Blog');
            $breadcrumb['category/'.$main_category]=$main_category_id['ArticleCategory']['label'];

            if(isset($category)){
                $breadcrumb[$category]=$category_id['ArticleCategory']['label'];
            }

            if(isset($subcategory)){
                $breadcrumb[$subcategory]=$subcategory_id['ArticleCategory']['label'];
            }

            $this->set('breadcrumb',$breadcrumb);

            $this->set('blogs', $articles);
            $this->set('title_for_layout', 'Blog');
            $this->set('article_name', 'Blog');
            $this->set('title', 'Blog');

            $this->layout = "common_layout";
            $this->render('index');
        }else{
            throw new NotFoundException();
        }

    }
    function latestBlog($onlyadmin=0,$featured=1,$limit=3){
        $allArticles=$this->getArticles(0,1,5); 
        $this->layout="";
        $this->set('allArticles',$allArticles);
    }


}
