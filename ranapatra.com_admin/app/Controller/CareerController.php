<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');
App::import('Vendor', 'ClassUpload', array('file' => 'class.upload.php'));

/**
 * EmployeeSpeaks Controller
 *
 * @property EmployeeSpeak $EmployeeSpeak
 * @property PaginatorComponent $Paginator
 */
class CareerController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
        'order' => array(
            'Career.sort_order' => 'asc'
        )
    );

    function beforeRender() {
        parent::beforeRender();
    }

    /**
     * index method
     *
     * @return void
     */
    public function index($lang = null) {
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('career', 'index');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        $this->loadModel('Language');
        $this->Paginator->settings = $this->paginate;
        $this->Career->recursive = 0;

        //language
        $language = $this->Language->find('all', array(
            'conditions' => array('Language.status' => "1"), 'order' => array('Language.default DESC')));

        if (!isset($lang)) {
            $langcode = $language[0]['Language']['iso_code'];
        } else {
            $langcode = $this->params['pass'][0];
        }
        $Career = $this->Paginator->paginate('Career', array('Career.lang' => $langcode));
        $this->set('Langauges', $language);
        $this->set('currentlangpage', $langcode);
        $this->set('Careers', $Career);
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($lang = null, $id = null) {
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('career', 'index');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        if (!$this->Career->exists($id)) {
            throw new NotFoundException(__('Invalid Career'));
        }
        $options = array('conditions' => array('Career.' . $this->Career->primaryKey => $id));
        $this->set('Career', $this->Career->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('career', 'add');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        $this->loadModel('Language');
        if ($this->request->is('post')) {
            $this->Career->create();
            $this->request->data['Career']['created'] = date('Y-m-d H:i:s');

            if ($this->Career->save($this->request->data)) {
                $this->Session->setFlash(__('The Career has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index/" . $this->request->data['Career']['lang']));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $language = $this->Language->find('all', array(
            'conditions' => array('Language.status' => "1"), 'order' => array('Language.default DESC')));

        $dropdownlang = array();
        foreach ($language as $langs) {
            $dropdownlang[$langs['Language']['iso_code']] = $langs['Language']['lang_name'];
        }
        $language = $this->Language->find('all', array(
            'conditions' => array('Language.status' => "1"), 'order' => array('Language.default DESC')));


        $this->set('Langauges', $language);
        $this->set('dropdownlang', $dropdownlang);
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($lang = null, $id = null) {
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('career', 'edit');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        $this->loadModel('Language');
        if (!$this->Career->exists($id)) {
            throw new NotFoundException(__('Invalid Career'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Career->save($this->request->data)) {
                $this->Session->setFlash(__('The Career has been updated.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index/" . $this->request->data['Career']['lang']));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $options = array('conditions' => array('Career.' . $this->Career->primaryKey => $id));
        $this->request->data = $this->Career->find('first', $options);
        //language
        $language = $this->Language->find('all', array(
            'conditions' => array('Language.status' => "1"), 'order' => array('Language.default DESC')));

        $dropdownlang = array();
        foreach ($language as $langs) {
            $dropdownlang[$langs['Language']['iso_code']] = $langs['Language']['lang_name'];
        }
        $language = $this->Language->find('all', array(
            'conditions' => array('Language.status' => "1"), 'order' => array('Language.default DESC')));

        $this->set('Langauges', $language);
        $this->set('dropdownlang', $dropdownlang);
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($lang = null, $id = null) {
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('career', 'delete');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        $this->Career->id = $id;
        if (!$this->Career->exists()) {
            throw new NotFoundException(__('Invalid Career'));
        }
        //method allow for action 
        $this->request->allowMethod('post', 'delete');
        if ($this->Career->delete()) {
            $this->Session->setFlash(__('The Career has been deleted.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => "index/" . $lang));
        } else {
            $this->Session->setFlash(__('The Career could not be deleted, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index/" . $lang));
        }
    }

    /**
     * inactive method
     *
     * function to change status inactive
     * 
     */ public function inactive($lang = null, $id = null) {
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('career', 'review');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        $this->Career->id = $id;
        if (!$this->Career->exists()) {
            throw new NotFoundException(__('Invalid Career'));
        }
        $this->request->allowMethod('post');
        if ($this->Career->saveField('status', NEO_INACTIVE_STATUS)) {
            $this->Session->setFlash('Career has been inactived.', 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index/' . $lang));
        } else {
            $this->Session->setFlash(__('Career activation failed, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index/" . $lang));
        }
    }

    /**
     * active method
     *
     * function to change status to active
     */
    public function active($lang = null, $id = null) {
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('career', 'review');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        $this->Career->id = $id;
        if (!$this->Career->exists()) {
            throw new NotFoundException(__('Invalid Career'));
        }
        $this->request->allowMethod('post');
        if ($this->Career->saveField('status', NEO_ACTIVE_STATUS)) {
            $this->Session->setFlash('Career has been actived.', 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index/' . $lang));
        } else {
            $this->Session->setFlash(__('Career activation failed, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index/" . $lang));
        }
    }

    /**
     * update_order method
     *
     * function to change order 
     */
    public function update_order($lang, $direction, $id, $sort_order) {
        
        $this->Career->find('first', array("conditions" => array("Career.sort_order= (" . $sort_order . ")")));
        if ($direction == "down") {
            $newSortOrder = $sort_order + 1;
        } else if ($direction == "up") {
            $newSortOrder = $sort_order - 1;
        }
        $result = $this->Career->find('first', array("conditions" => array("Career.sort_order= " . $newSortOrder . "")));
        $tranid = $result['Career']['id'];

        $this->Career->id = $id;
        $this->Career->saveField('sort_order', $newSortOrder);

        $this->Career->id = $tranid;
        $this->Career->saveField('sort_order', $sort_order);

        $this->redirect('index/' . $lang);
    }

}
