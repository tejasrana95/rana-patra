<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');
App::import('Controller', 'Common');

/**
 * Customers Controller
 *
 * @property Customer $Customer
 * @property PaginatorComponent $Paginator
 */
class CustomerInvoicesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Qimage', 'RequestHandler');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
        'order' => array(
            'CustomerInvoice.id' => 'desc',
        ),
    );

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow customers to register and logout.
        $this->Security->csrfCheck = false;
        $this->Security->validatePost = false;
        $this->Security->validatePost = false;
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard',
            ));
        }
        //end check permission
        $this->loadModel('UserRole');
        $this->Paginator->settings = $this->paginate;
        $this->CustomerInvoice->recursive = 0;
        $this->set('CustomerInvoice', $this->Paginator->paginate());
        $userID = $_SESSION['Auth']['User']['id'];
        $this->set("adminuserID", $userID);
        $this->set('title_for_layout', 'Invoices');

        $this->set('page_name', 'Invoices');
        $this->set('parent_page_name', 'Invoices');
        $this->set('page_title', 'List Invoices');
    }

    /**
     * view method
     *
     * @param  string              $id
     * @throws NotFoundException
     * @return void
     */
    public function view($id = null) {
        //check permission
        $this->loadModel('UserRole');
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard',
            ));
        }
        $this->loadModel('Customer');
        $this->loadModel('CustomerProfile');
        $this->loadModel('Option');
        //end check permission


        if (!$this->CustomerInvoice->exists($id)) {
            throw new NotFoundException(__('Invalid Invoice'));
        }

        $userID = $_SESSION['Auth']['User']['id'];

        $options = array('conditions' => array($this->CustomerInvoice->primaryKey => $id));
        $this->request->data = $this->CustomerInvoice->find('first', $options);

        $options = array('conditions' => array('userID' => $this->request->data['CustomerInvoice']['userID']));
        $CustomerProfile = $this->CustomerProfile->find('first', $options);
        $this->request->data['CustomerProfile'] = $CustomerProfile['CustomerProfile'];

        $options = array('conditions' => array($this->Customer->primaryKey => $this->request->data['CustomerInvoice']['userID']));
        $Customer = $this->Customer->find('first', $options);
        $this->request->data['Customer'] = $Customer['Customer'];

        $options = array('conditions' => array('meta_key' => "payment_company"), 'fields' => array('meta_value'));
        $payment_company = $this->Option->find('first', $options);

        $options = array('conditions' => array('meta_key' => "payment_mail"), 'fields' => array('meta_value'));
        $payment_mail = $this->Option->find('first', $options);

        $options = array('conditions' => array('meta_key' => "payment_address"), 'fields' => array('meta_value'));
        $payment_address = $this->Option->find('first', $options);

        $options = array('conditions' => array('meta_key' => "payment_contact"), 'fields' => array('meta_value'));
        $payment_contact = $this->Option->find('first', $options);
        
         $options = array('conditions' => array('meta_key' => "payment_company_url"), 'fields' => array('meta_value'));
        $payment_company_url = $this->Option->find('first', $options);

        $this->set('company_info', array('payment_mail' => $payment_mail['Option']['meta_value'],
            'payment_address' => $payment_address['Option']['meta_value'],
            'payment_contact' => $payment_contact['Option']['meta_value'],
            'payment_company' => $payment_company['Option']['meta_value'],
            'payment_company_url'=>$payment_company_url['Option']['meta_value'],
            ));
        $this->set('payment_mail', $payment_mail['Option']['meta_value']);
        $userID = $_SESSION['Auth']['User']['id'];
        $this->set("adminuserID", $userID);

        $this->set('title_for_layout', 'Invoices');
        $this->set('page_name', 'View Invoices');
        $this->set('parent_page_name', 'List Invoices');
        $this->set('page_title', 'View Invoices');
    }

    
    
    public function printinvoice($id = null) {
        //check permission
        $this->layout="print";
        $this->loadModel('UserRole');
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard',
            ));
        }
        $this->loadModel('Customer');
        $this->loadModel('CustomerProfile');
        $this->loadModel('Option');
        //end check permission


        if (!$this->CustomerInvoice->exists($id)) {
            throw new NotFoundException(__('Invalid Invoice'));
        }

        $userID = $_SESSION['Auth']['User']['id'];

        $options = array('conditions' => array($this->CustomerInvoice->primaryKey => $id));
        $this->request->data = $this->CustomerInvoice->find('first', $options);

        $options = array('conditions' => array('userID' => $this->request->data['CustomerInvoice']['userID']));
        $CustomerProfile = $this->CustomerProfile->find('first', $options);
        $this->request->data['CustomerProfile'] = $CustomerProfile['CustomerProfile'];

        $options = array('conditions' => array($this->Customer->primaryKey => $this->request->data['CustomerInvoice']['userID']));
        $Customer = $this->Customer->find('first', $options);
        $this->request->data['Customer'] = $Customer['Customer'];

        $options = array('conditions' => array('meta_key' => "payment_company"), 'fields' => array('meta_value'));
        $payment_company = $this->Option->find('first', $options);

        $options = array('conditions' => array('meta_key' => "payment_mail"), 'fields' => array('meta_value'));
        $payment_mail = $this->Option->find('first', $options);

        $options = array('conditions' => array('meta_key' => "payment_address"), 'fields' => array('meta_value'));
        $payment_address = $this->Option->find('first', $options);

        $options = array('conditions' => array('meta_key' => "payment_contact"), 'fields' => array('meta_value'));
        $payment_contact = $this->Option->find('first', $options);
        
         $options = array('conditions' => array('meta_key' => "payment_company_url"), 'fields' => array('meta_value'));
        $payment_company_url = $this->Option->find('first', $options);

        $this->set('company_info', array('payment_mail' => $payment_mail['Option']['meta_value'],
            'payment_address' => $payment_address['Option']['meta_value'],
            'payment_contact' => $payment_contact['Option']['meta_value'],
            'payment_company' => $payment_company['Option']['meta_value'],
            'payment_company_url'=>$payment_company_url['Option']['meta_value'],
            ));
        $this->set('payment_mail', $payment_mail['Option']['meta_value']);
        $userID = $_SESSION['Auth']['User']['id'];
        $this->set("adminuserID", $userID);

        $this->set('title_for_layout', 'Invoices');
        $this->set('page_name', 'View Invoices');
        $this->set('parent_page_name', 'List Invoices');
        $this->set('page_title', 'View Invoices');
    }
    /**
     * add method
     *
     * @return void
     */
    public function add() {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard',
            ));
        }
        //end check permission
        $this->loadModel('Customer');
        $this->loadModel('Option');
        if ($this->request->is('post')) {
            $userID = $_SESSION['Auth']['User']['id'];
            if ($this->request->data['CustomerInvoice']['process_by'] == 1) {
                $this->request->data['CustomerInvoice']['system_id'] = $userID;
            } else {
                $this->request->data['CustomerInvoice']['system_id'] = 0;
            }
            $this->CustomerInvoice->create();
            if ($this->CustomerInvoice->save($this->request->data)) {
                $this->Session->setFlash(__('The Invoice has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }

        $options = array('conditions' => array('Customer.status' => 1), 'fields' => array('Customer.id', 'Customer.username'), 'order' => array('Customer.username ASC'));
        $Customers = $this->Customer->find('list', $options);

        $options = array('conditions' => array('meta_key' => "payment_mail"), 'fields' => array('meta_value'));
        $payment_mail = $this->Option->find('first', $options);

        $this->set('payment_mail', $payment_mail['Option']['meta_value']);
        $this->set('customers', $Customers);
        $userID = $_SESSION['Auth']['User']['id'];
        $this->set("adminuserID", $userID);

        $this->set('title_for_layout', 'Invoices');
        $this->set('page_name', 'Add Invoices');
        $this->set('parent_page_name', 'List Invoices');
        $this->set('page_title', 'Add Invoices');
    }

    /**
     * edit method
     *
     * @param  string              $id
     * @throws NotFoundException
     * @return void
     */
    public function edit($id = null) {
        //check permission
        $this->loadModel('UserRole');
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard',
            ));
        }
        $this->loadModel('Customer');
        $this->loadModel('Option');
        //end check permission


        if (!$this->CustomerInvoice->exists($id)) {
            throw new NotFoundException(__('Invalid Invoice'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $userID = $_SESSION['Auth']['User']['id'];
            if ($this->request->data['CustomerInvoice']['process_by'] == 1) {
                $this->request->data['CustomerInvoice']['system_id'] = $userID;
            } else {
                $this->request->data['CustomerInvoice']['system_id'] = 0;
            }
            if ($this->CustomerInvoice->save($this->request->data)) {
                $this->Session->setFlash(__('The invoice has been updated.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $userID = $_SESSION['Auth']['User']['id'];

        $options = array('conditions' => array($this->CustomerInvoice->primaryKey => $id));
        $this->request->data = $this->CustomerInvoice->find('first', $options);



        $options = array('conditions' => array('Customer.status' => 1), 'fields' => array('Customer.id', 'Customer.username'), 'order' => array('Customer.username ASC'));
        $Customers = $this->Customer->find('list', $options);

        $options = array('conditions' => array('meta_key' => "payment_mail"), 'fields' => array('meta_value'));
        $payment_mail = $this->Option->find('first', $options);

        $this->set('payment_mail', $payment_mail['Option']['meta_value']);
        $this->set('customers', $Customers);
        $userID = $_SESSION['Auth']['User']['id'];
        $this->set("adminuserID", $userID);

        $this->set('title_for_layout', 'Invoices');
        $this->set('page_name', 'Edit Invoices');
        $this->set('parent_page_name', 'List Invoices');
        $this->set('page_title', 'Edit Invoices');
    }

}
