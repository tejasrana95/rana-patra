<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');

/**
 * Pages Controller
 *
 * @property ArticleCategory $ArticleCategory
 * @property PaginatorComponent $Paginator
 */
class ArticleCategoryController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
        'order' => array(
            'ArticleCategory.parent' => 'asc')
    );

    // function call before any view rendor 
    // function used to add and override function feautres
    function beforeRender() {
        parent::beforeRender();
    }

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow 
    }

    /**
     * index method
     *
     * @return void
     */
    public function index($all = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('articlecategory', 'index');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Paginator->settings = $this->paginate;
        $this->ArticleCategory->recursive = 0;
        

        $categories = $this->Paginator->paginate('ArticleCategory');
        
        $this->set('categories', $categories);
          $this->set('title_for_layout', 'Categories');
        $this->set('page_name', 'Categories');
        $this->set('page_title', 'Categories');
     
    }

    /**
     * Parent articlecategory name method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function articlecategorydata($id) {
        if (isset($id) && $id != 0) {
            $parentname = $this->ArticleCategory->find('first', array(
                'conditions' => array('ArticleCategory.id' => $id)));
            return $parentname['ArticleCategory'];
        } else {
            return array();
        }
    }

    /**
     * add method
     *
     * @return void
     */
    public function add($copyData = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('articlecategory', 'add');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        if ($this->request->is('post')) {
            $this->ArticleCategory->create();
            if ($this->request->data['ArticleCategory']['parent'] == "" || $this->request->data['ArticleCategory']['parent'] == '0') {
                $this->request->data['ArticleCategory']['parent'] = $this->request->data['ArticleCategory']['main_parent'];
            }
            if ($this->ArticleCategory->save($this->request->data)) {
                $this->Session->setFlash(__('The article category has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
         $this->set('title_for_layout', 'Add Article Category');
        $this->set('page_name', 'Add Article Category');
        $this->set('parent_page_name', 'List Article Category');
        $this->set('page_title', 'Add Article Category');
      
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('articlecategory', 'edit');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission

        if (!$this->ArticleCategory->exists($id)) {
            throw new NotFoundException(__('Invalid page'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->ArticleCategory->id = $id;

            if ($this->request->data['ArticleCategory']['sub_parent'] != "" && $this->request->data['ArticleCategory']['sub_parent'] != '0' && $this->request->data['ArticleCategory']['parent'] != '0' && $this->request->data['ArticleCategory']['parent'] != '') {
                $this->request->data['ArticleCategory']['parent'] = $this->request->data['ArticleCategory']['sub_parent'];
            }
            if ($this->ArticleCategory->save($this->request->data)) {
                $this->Session->setFlash(__('The article category has been updated.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
      
        //get parent data
        $parentcat = $this->ArticleCategory->find('all', array(
            'conditions' => array('ArticleCategory.parent' => "0"), 'order' => array('ArticleCategory.label ASC')));
        $parents = array();
        $parents["0"] = "Top";
        foreach ($parentcat as $parent) {
            $parents[$parent['ArticleCategory']['id']] = $parent['ArticleCategory']['label'];
        }
        $subparent['parent'] = "";
        $mainparent['id'] = "";

        $this->set('parentCate', $parents);
        //get content for id
        $articlecategorydata = $this->requestAction('articlecategory/articlecategorydata/' . $id);

        if ($articlecategorydata['parent'] != 0) {
            $subparent = $this->requestAction('articlecategory/articlecategorydata/' . $articlecategorydata['parent']);
        }
        if ($subparent['parent'] != 0) {
            $mainparent = $this->requestAction('articlecategory/articlecategorydata/' . $subparent['parent']);
        } else {
            $mainparent = $subparent;
            $subparent['id'] = "0";
        }


        //get parent data
        if (isset($mainparent['id'])) {
            $condition = array('ArticleCategory.parent' => $mainparent['id'], 'ArticleCategory.id !=' => $id);
            $subparentcat = $this->ArticleCategory->find('all', array(
                'conditions' => $condition, 'order' => array('ArticleCategory.label ASC')));
            $subparents = array();
            $subparents["0"] = "Sub Parent";
            foreach ($subparentcat as $subparn) {
                $subparents[$subparn['ArticleCategory']['id']] = $subparn['ArticleCategory']['label'];
            }
        } else {
            $subparents[0] = 'Top';
            $mainparent['id'] = 0;
        }
       
        $this->set('allsubparent', $subparents);
        $this->set('mainparent', $mainparent);
        $this->set('subparent', $subparent);
        $this->set('articlecategory', $articlecategorydata);
        $this->set('title_for_layout', 'Edit Article Category');
        $this->set('page_name', 'Edit Article Category');
        $this->set('parent_page_name', 'List Article Category');
        $this->set('page_title', 'Edit Article Category');
       
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('articlecategory', 'delete');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->ArticleCategory->id = $id;
        if (!$this->ArticleCategory->exists()) {
            throw new NotFoundException(__('Invalid Article Category'));
        }
        //method allow for action 
        $this->request->allowMethod('post', 'delete');
        if ($this->ArticleCategory->delete()) {
            $this->Session->setFlash(__('The Article Category has been deleted.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => "index"));
        } else {
            $this->Session->setFlash(__('The Article Category could not be deleted, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index"));
        }
    }

//parent articlecategory fetch
    public function parentcate($default = null) {
        
            $parentname = $this->ArticleCategory->find('all', array(
                'conditions' => array('ArticleCategory.parent' => '0'),
                'order' => array('ArticleCategory.label ASC')));
            if (isset($parentname) && !empty($parentname)) {
                echo '<option label="" value="">Select Parent ArticleCategory</option>';
                if ($default == "all") {
                    echo '<option label="" value="0">All</option>';
                } elseif ($default != null) {
                    echo '<option label="" value="0">Top</option>';
                }
                foreach ($parentname as $parentname) {
                    echo '<option label="" value="' . $parentname['ArticleCategory']['id'] . '"';
                    echo ' >' . $parentname['ArticleCategory']['label'] . '</option>';
                }
            } else {
                if ($default == "all") {
                    echo '<option label="" value="">Select Article Category</option>';
                    echo '<option label="" value="0">All</option>';
                } elseif ($default != null) {
                     echo '<option label="" value="">Select Article Category</option>';
                    echo '<option label="" value="0">Top</option>';
                } else {
                    echo '<option label="" value="">No data available </option>';
                }
            }
        die();
    }

    //sub articlecategory fetch
    public function subcate($parentid = null, $currentId = null, $forarticlecategory = null) {
        if ($parentid != null) {
            if ($parentid == 0) {
                echo '<option label="" value="0">Top</option>';
            } else {
                $condition = array('ArticleCategory.parent' => $parentid);
                if ($currentId != null) {
                    $condition['ArticleCategory.id !='] = $currentId;
                }
                $parentname = $this->ArticleCategory->find('all', array(
                    'conditions' => $condition,
                    'order' => array('ArticleCategory.label ASC')));
                if (isset($parentname) && !empty($parentname)) {
                    echo '<option label="" value="">Select Sub Article Category</option>';
                    if ($forarticlecategory != null) {
                        echo '<option label="" value="">Sub Parent</option>';
                    }
                    foreach ($parentname as $parentname) {

                        echo '<option label="" value="' . $parentname['ArticleCategory']['id'] . '"';
                        echo ' >' . $parentname['ArticleCategory']['label'] . '</option>';
                    }
                } else {
                    if ($forarticlecategory != null) {
                        echo '<option label="" value="">Sub Parent</option>';
                    } else {
                        echo '<option label="" value="">No data available </option>';
                    }
                }
            }
        } else {
            echo '<option label="" value="">No data available </option>';
        }
        die();
    }

    public function getarticlecategorydata($articlecategoryid) {

        if (isset($articlecategoryid) && !empty($articlecategoryid)) {
            if (is_numeric($articlecategoryid)) {
                $articlecategory_info = $this->ArticleCategory->find('first', array(
                    'conditions' => array('ArticleCategory.id' => $articlecategoryid),
                ));
            } else {
                $articlecategory_info = $this->ArticleCategory->find('first', array(
                    'conditions' => array('ArticleCategory.identifier' => $articlecategoryid),
                ));
            }
            return $articlecategory_info;
        }
    }

    public function getchild($articlecategoryid) {

        if (isset($articlecategoryid) && !empty($articlecategoryid)) {
            if (is_numeric($articlecategoryid)) {
                $articlecategory_info = $this->ArticleCategory->find('first', array(
                    'conditions' => array('ArticleCategory.parent' => $articlecategoryid),
                ));
            } 
            return $articlecategory_info;
        }
    }

}
