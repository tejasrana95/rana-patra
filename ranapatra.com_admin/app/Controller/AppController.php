<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('CakeTime', 'Utility');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $helpers = array(
        'Cache',
        'Html',
        'Form',
        'Js',
        'Session',
        'Time',
        'Text');
    public $components = array(
        'Session',
        'Auth' => array(
        ),
        'RequestHandler',
        'DebugKit.Toolbar',
        'Security' => array(
            'csrfExpires' => '+1 hour'
        )
    );

    function beforeRender() {
        //error page
        if ($this->name == 'CakeError') {
            	$this->layout='error';
        }
        parent::beforeRender();


        $front_layout = array(
            'home' => "Home Page",
            'full_width_layout' => 'Full width Layout',
            'contact_layout' => 'Contact page',
        );
        $this->set('front_layout', $front_layout);

        $item_type = array(
            '1' => "Matrimonial",
            '2' => 'Donation',
            '3' => 'Gift Voucher',
            '4' => 'Other',
        );
        $this->set('item_type', $item_type);

        $process_by = array(
            '0' => "Self",
            '1' => 'Agent',
            '2' => 'Auto',
            '3' => 'Other',
        );
        $this->set('process_by', $process_by);

        $currency = array(
            'ALL' => 'Albania Lek',
            'AFN' => 'Afghanistan Afghani',
            'ARS' => 'Argentina Peso',
            'AWG' => 'Aruba Guilder',
            'AUD' => 'Australia Dollar',
            'AZN' => 'Azerbaijan New Manat',
            'BSD' => 'Bahamas Dollar',
            'BBD' => 'Barbados Dollar',
            'BDT' => 'Bangladeshi taka',
            'BYR' => 'Belarus Ruble',
            'BZD' => 'Belize Dollar',
            'BMD' => 'Bermuda Dollar',
            'BOB' => 'Bolivia Boliviano',
            'BAM' => 'Bosnia and Herzegovina Convertible Marka',
            'BWP' => 'Botswana Pula',
            'BGN' => 'Bulgaria Lev',
            'BRL' => 'Brazil Real',
            'BND' => 'Brunei Darussalam Dollar',
            'KHR' => 'Cambodia Riel',
            'CAD' => 'Canada Dollar',
            'KYD' => 'Cayman Islands Dollar',
            'CLP' => 'Chile Peso',
            'CNY' => 'China Yuan Renminbi',
            'COP' => 'Colombia Peso',
            'CRC' => 'Costa Rica Colon',
            'HRK' => 'Croatia Kuna',
            'CUP' => 'Cuba Peso',
            'CZK' => 'Czech Republic Koruna',
            'DKK' => 'Denmark Krone',
            'DOP' => 'Dominican Republic Peso',
            'XCD' => 'East Caribbean Dollar',
            'EGP' => 'Egypt Pound',
            'SVC' => 'El Salvador Colon',
            'EEK' => 'Estonia Kroon',
            'EUR' => 'Euro Member Countries',
            'FKP' => 'Falkland Islands (Malvinas) Pound',
            'FJD' => 'Fiji Dollar',
            'GHC' => 'Ghana Cedis',
            'GIP' => 'Gibraltar Pound',
            'GTQ' => 'Guatemala Quetzal',
            'GGP' => 'Guernsey Pound',
            'GYD' => 'Guyana Dollar',
            'HNL' => 'Honduras Lempira',
            'HKD' => 'Hong Kong Dollar',
            'HUF' => 'Hungary Forint',
            'ISK' => 'Iceland Krona',
            'INR' => 'India Rupee',
            'IDR' => 'Indonesia Rupiah',
            'IRR' => 'Iran Rial',
            'IMP' => 'Isle of Man Pound',
            'ILS' => 'Israel Shekel',
            'JMD' => 'Jamaica Dollar',
            'JPY' => 'Japan Yen',
            'JEP' => 'Jersey Pound',
            'KZT' => 'Kazakhstan Tenge',
            'KPW' => 'Korea (North) Won',
            'KRW' => 'Korea (South) Won',
            'KGS' => 'Kyrgyzstan Som',
            'LAK' => 'Laos Kip',
            'LVL' => 'Latvia Lat',
            'LBP' => 'Lebanon Pound',
            'LRD' => 'Liberia Dollar',
            'LTL' => 'Lithuania Litas',
            'MKD' => 'Macedonia Denar',
            'MYR' => 'Malaysia Ringgit',
            'MUR' => 'Mauritius Rupee',
            'MXN' => 'Mexico Peso',
            'MNT' => 'Mongolia Tughrik',
            'MZN' => 'Mozambique Metical',
            'NAD' => 'Namibia Dollar',
            'NPR' => 'Nepal Rupee',
            'ANG' => 'Netherlands Antilles Guilder',
            'NZD' => 'New Zealand Dollar',
            'NIO' => 'Nicaragua Cordoba',
            'NGN' => 'Nigeria Naira',
            'NOK' => 'Norway Krone',
            'OMR' => 'Oman Rial',
            'PKR' => 'Pakistan Rupee',
            'PAB' => 'Panama Balboa',
            'PYG' => 'Paraguay Guarani',
            'PEN' => 'Peru Nuevo Sol',
            'PHP' => 'Philippines Peso',
            'PLN' => 'Poland Zloty',
            'QAR' => 'Qatar Riyal',
            'RON' => 'Romania New Leu',
            'RUB' => 'Russia Ruble',
            'SHP' => 'Saint Helena Pound',
            'SAR' => 'Saudi Arabia Riyal',
            'RSD' => 'Serbia Dinar',
            'SCR' => 'Seychelles Rupee',
            'SGD' => 'Singapore Dollar',
            'SBD' => 'Solomon Islands Dollar',
            'SOS' => 'Somalia Shilling',
            'ZAR' => 'South Africa Rand',
            'LKR' => 'Sri Lanka Rupee',
            'SEK' => 'Sweden Krona',
            'CHF' => 'Switzerland Franc',
            'SRD' => 'Suriname Dollar',
            'SYP' => 'Syria Pound',
            'TWD' => 'Taiwan New Dollar',
            'THB' => 'Thailand Baht',
            'TTD' => 'Trinidad and Tobago Dollar',
            'TRY' => 'Turkey Lira',
            'TRL' => 'Turkey Lira',
            'TVD' => 'Tuvalu Dollar',
            'UAH' => 'Ukraine Hryvna',
            'GBP' => 'United Kingdom Pound',
            'UGX' => 'Uganda Shilling',
            'USD' => 'United States Dollar',
            'UYU' => 'Uruguay Peso',
            'UZS' => 'Uzbekistan Som',
            'VEF' => 'Venezuela Bolivar',
            'VND' => 'Viet Nam Dong',
            'YER' => 'Yemen Rial',
            'ZWD' => 'Zimbabwe Dollar'
        );
        $this->set('currency', $currency);

        $brideGroom = array(
            'Bride' => "Bride",
            'Groom' => 'Groom',
        );
        $this->set('brideGroom', $brideGroom);

        $marital_status = array(
            'Single' => "Single",
            'Widowed' => 'Widowed',
            'Separated' => 'Separated',
            'Divorced' => 'Divorced',
        );
        $this->set('marital_status', $marital_status);

        $rashi = array(
            'Aries' => "Mesh (Aries)",
            'Taurus' => "Vrushabh (Taurus)",
            'Gemini' => "Mithun (Gemini)",
            'Cancer' => "Kark (Cancer)",
            'Leo' => "Sinh (Leo)",
            'Virgo' => "Kanya (Virgo)",
            'Libra' => "Tula (Libra)",
            'Scorpius' => "Vrushchik (Scorpius)",
            'Sagittarius' => "Dhanu (Sagittarius)",
            'Capricornus' => "Makar (Capricornus)",
            'Aquarius' => "Kumbha (Aquarius)",
            'Pisces' => "Meen (Pisces)",
        );
        $this->set('rashi', $rashi);

        $yesNo = array(
            '0' => "No",
            '1' => "Yes",
        );
        $this->set('yesNo', $yesNo);

        $employement = array(
            '0' => "Unemployment",
            '1' => "Study is going on",
            '2' => "Salaried Person",
            '3' => "Businessman",
            '4' => "Intern",
            '5' => "Freelancer",
            '6' => "Self Employment",
            '7' => "Other",
        );
        $this->set('employement', $employement);

        $diet = array(
        'Pure Vegetarian' => 'Pure Vegetarian',
        'Non Vegetarian' =>'Non Vegetarian',
        'Occasionally Non Veg' =>'Occasionally Non Veg',
        'Don\'t want to disclose' =>'Don\'t want to disclose',
        );
    $this->set('diet', $diet);
    
    $drink_smoke = array(
        'Yes' => 'Yes',
        'No' =>'No',
        'Occasionally' =>'Occasionally',
        'Don\'t want to disclose' =>'Don\'t want to disclose',
        );
    $this->set('drink_smoke', $drink_smoke);
        
        $salary = array(
            'Less than 1,00,000' => "Less than 1,00,000",
            '1,00,000 - 1,50,000' => "1,00,000 - 1,50,000",
            '1,51,000 - 2,00,000' => "1,51,000 - 2,00,000",
            '2,00,000 - 2,50,000' => "2,00,000 - 2,50,000",
            '2,51,000 - 3,00,000' => "2,51,000 - 3,00,000",
            '3,00,000 - 3,50,000' => "3,00,000 - 3,50,000",
            '3,51,000 - 4,00,000' => "3,51,000 - 4,00,000",
            '4,00,000 - 4,50,000' => "4,00,000 - 4,50,000",
            '4,51,000 - 5,00,000' => "4,51,000 - 5,00,000",
            '5,00,000 - 5,50,000' => "5,00,000 - 5,50,000",
            '5,51,000 - 6,00,000' => "5,51,000 - 6,00,000",
            '6,00,000 - 6,50,000' => "6,00,000 - 6,50,000",
            '7,51,000 - 8,00,000' => "7,51,000 - 8,00,000",
            '8,00,000 - 8,50,000' => "8,00,000 - 8,50,000",
            '8,51,000 - 9,00,000' => "8,51,000 - 9,00,000",
            '9,00,000 - 9,50,000' => "9,00,000 - 9,50,000",
            '9,51,000 - 10,00,000' => "9,51,000 - 10,00,000",
            '10,00,000 and Above' => "10,00,000 and Above",
            'Dont want to disclose' => "Dont want to disclose",
        );
        $this->set('salary', $salary);

        $bloodGroup = array(
            'A+' => "A+",
            'A-' => "A-",
            'B+' => "B+",
            'B-' => "B-",
            'AB+' => "AB+",
            'AB-' => "AB-",
            'O+' => "O+",
            'O-' => "O-",
        );
        $this->set('bloodGroup', $bloodGroup);

        $education = array(
            'DOCTORATE - PHD' => "DOCTORATE - PHD",
            'DOCTOR - MBBS' => "DOCTOR - MBBS",
            'CHARTERED ACCOUNT - CA' => "CHARTERED ACCOUNT - CA",
            'Architect (B Arch)' => "Architect (B Arch)",
            'POST GRADUATE' => "POST GRADUATE",
            'DEGREE - GRADUATION' => "DEGREE - GRADUATION",
            'CERTIFICATE COURSE' => "CERTIFICATE COURSE",
            'DIPLOMA - NON ENGINEERING' => "DIPLOMA - NON ENGINEERING",
            'DIPLOMA ENGINEER' => "DIPLOMA ENGINEER",
            '12TH PASS - HSC' => "12TH PASS - HSC",
            '10TH PASS - SSC' => "10TH PASS - SSC",
            'NON MATRIC' => "NON MATRIC",
            'None of these' => "None of these",
        );
        $this->set('education', $education);

        $article_layout = array(
            'standard_article_layout' => "Standard Article Layout",
        );
        $this->set('article_layout', $article_layout);

        $payment_gateway = array(
            'paypal' => "paypal", 'cash' => "cash",
        );
        $this->set('payment_gateway', $payment_gateway);


        $this->set('thumb_width', 100);
        $this->set('thumb_height', 100);
        //user role
        if (isset($_SESSION['Auth']['User']['id'])) {
            $user_id = $_SESSION['Auth']['User']['id'];
            $this->loadModel('User');
            $this->loadModel('Option');
            $this->loadModel('UserRole');
            $role_id = $this->User->find('first', array('conditions' => array('User.id' => $user_id)));
            $role = $this->UserRole->find('first', array('conditions' => array('UserRole.id' => $role_id['User']['role_id'])));
            $is_admin = $role['UserRole']['is_admin'];
            $roles = unserialize($role['UserRole']['roles']);
            $this->set('is_admin', $is_admin);
            $this->set('roles', $roles);
            $this->set('userID', $_SESSION['Auth']['User']['id']);

            $options = $this->Option->find('first', array('conditions' => array('Option.meta_key' => 'site_url')));
            $this->set('site_url', rtrim($options['Option']['meta_value'], '/') . '/');
            $this->set('site_name', "Rana Patra");
            $this->set('org_name', "Infi Technology");
        }
    }

    public function beforeFilter() {
        /*
          checking for admin section and redirect to login page if session not set
         */

        //set layout for admin for default
        $this->layout = "admin_layout";

        $this->loadModel('Option');
        $site_url = $this->Option->find('first', array(
            'conditions' => array('Option.meta_key' => "site_url")));
        $publicDir = $this->Option->find('first', array(
            'conditions' => array('Option.meta_key' => "publicDir")));

        $public_dir = rtrim($publicDir['Option']['meta_value'], DS);
        $urlPostFix = explode(DS, $public_dir);
        $urlPostFix_result = $urlPostFix[count($urlPostFix) - 1];
        $siteurl = rtrim($site_url['Option']['meta_value'], '/');


        $this->set('site_url', $siteurl);
        $this->set('publicDir', $public_dir);
        $this->set('publicDir', $urlPostFix_result);

        $this->set('news_status', $this->news_status);

        $position = array('sidebar' => "Sidebar", 'top_wrapper' => "Top Wrapper", "top_fullwidth" => "Top Fullwidth", "bottom_wrapper" => "Bottom Wrapper", "bottom_fullwidth" => "Bottom Fullwidth", "footer_script" => "Footer Script",'None'=>'None');
        $this->set('position', $position);
    }

}
