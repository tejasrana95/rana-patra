<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');

/**
 * Widgets Controller
 *
 * @property Widget $Widget
 * @property PaginatorComponent $Paginator
 */
class WidgetsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
        'order' => array(
            'Widget.created' => 'desc'
        )
    );

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Paginator->settings = $this->paginate;
        $this->Widget->recursive = 0;

        $widgets = $this->Paginator->paginate('Widget');
        $this->set('widgets', $widgets);
        $this->set('title_for_layout', 'Widget');
        $this->set('page_name', 'Widget');
        $this->set('page_title', 'Widget');
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        if (!$this->Widget->exists($id)) {
            throw new NotFoundException(__('Invalid widget'));
        }
        $options = array('conditions' => array('Widget.' . $this->Widget->primaryKey => $id));
        $this->set('widget', $this->Widget->find('first', $options));
        $this->set('title_for_layout', 'View Widget');
        $this->set('page_name', 'View Widget');
        $this->set('page_title', 'View Widget');
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        if ($this->request->is('post')) {
            $this->Widget->create();
            if ($this->Widget->save($this->request->data)) {
                $this->Session->setFlash(__('The widget has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index"));
            } else {
                $this->Session->setFlash(__('The widget could not be saved. Please, try again.'));
            }
        }
        $this->set('title_for_layout', 'Add Widget');
        $this->set('page_name', 'Add Widget');
        $this->set('page_title', 'Add Widget');
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        if (!$this->Widget->exists($id)) {
            throw new NotFoundException(__('Invalid widget'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Widget->save($this->request->data)) {
                $this->Session->setFlash(__('The widget has been updated.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index/"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $options = array('conditions' => array('Widget.' . $this->Widget->primaryKey => $id));
        $this->request->data = $this->Widget->find('first', $options);
        $this->set('title_for_layout', 'Edit Widget');
        $this->set('page_name', 'Edit Widget');
        $this->set('page_title', 'Edit Widget');
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Widget->id = $id;
        if (!$this->Widget->exists()) {
            throw new NotFoundException(__('Invalid widget'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Widget->delete()) {
            $this->Session->setFlash(__('The widget has been deleted.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => "index"));
        } else {
            $this->Session->setFlash(__('The widget could not be deleted, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index/"));
        }
        return $this->redirect(array('action' => 'index/'));
    }

    /**
     * inactive method
     *
     * function to change status inactive
     */
    public function inactive($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Widget->id = $id;
        if (!$this->Widget->exists()) {
            throw new NotFoundException(__('Invalid wdget'));
        }
        $this->request->allowMethod('post');
        if ($this->Widget->saveField('status', INFI_INACTIVE_STATUS)) {
            $this->Session->setFlash('Widget has been inactived.', 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => "index/"));
        } else {
            $this->Session->setFlash(__('Widget activation failed, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index/"));
        }
    }

    /**
     * active method
     *
     * function to change status to active
     */
    public function active($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Widget->id = $id;
        if (!$this->Widget->exists()) {
            throw new NotFoundException(__('Invalid widget'));
        }
        $this->request->allowMethod('post');
        if ($this->Widget->saveField('status', INFI_ACTIVE_STATUS)) {
            $this->Session->setFlash('Widget has been actived.', 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => "index/"));
        } else {
            $this->Session->setFlash(__('Widget activation failed, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index/"));
        }
    }

    public function widgetlist() {

        $widgets = $this->Widget->find('all', array('conditions' => array('Widget.position !=' => "", 'Widget.status' => INFI_ACTIVE_STATUS)));
        $widgetarr = array();
        if (!empty($widgets)) {
            echo '<option label="" value="">Select Widget</option>';
            foreach ($widgets as $widgets) {
                echo '<option label="" value="' . $widgets['Widget']['identifier'] . '"';
                echo ' >' . $widgets['Widget']['name'] . '</option>';
            }
        } else {
            echo '<option label="" value="">No Widget available</option>';
        }
        die();
    }

}
