<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');

/**
 * Options Controller
 *
 * @property Option $Option
 * @property PaginatorComponent $Paginator
 */
class OptionsController extends AppController {

    //before filter
    public function beforeFilter() {
        
    }

    public function index() {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            if ($this->Option->saveAll($this->request->data['Option'])) {
                $this->Session->setFlash(__('Options has been updated.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect('index');
            }
        } else {
            //getting record for edit part
            $option_detail = $this->Option->find('all', array(
                'fields' => array(
                    'meta_key',
                    'meta_value',
                    'field_type',
                    'id'
                )
            ));
            $this->set('title_for_layout', 'Site options');
            $this->set('page_name', 'Site options');
            $this->set('page_title', 'Settings');
            $this->request->data = $option_detail;
        }
    }

    public function get_value($key = null) {
        return $this->Option->find('first', array(
                    "conditions" => array("meta_key" => $key), // 0 indicates menu headers
        ));
    }

}
