<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');
App::import('Controller', 'Common');

/**
 * Customers Controller
 *
 * @property Customer $Customer
 * @property PaginatorComponent $Paginator
 */
class CustomersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Qimage', 'RequestHandler');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
        'order' => array(
            'Customer.created' => 'desc'
        )
    );

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow customers to register and logout.
        $this->Security->csrfCheck = false;
        $this->Security->validatePost = false;
        $this->Security->validatePost = false;
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->loadModel('UserRole');
        $this->Paginator->settings = $this->paginate;
        $this->Customer->recursive = 0;
        $this->set('customers', $this->Paginator->paginate());
        $userID = $_SESSION['Auth']['User']['id'];
        $this->set("adminuserID", $userID);
        $this->set('title_for_layout', 'Customers');
        $this->set('page_name', 'Customers');
        $this->set('page_title', 'Customers');
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'customers', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->loadModel('UserRole');
        if (!$this->Customer->exists($id)) {
            throw new NotFoundException(__('Invalid Customer'));
        }
        $userID = $_SESSION['Auth']['User']['id'];

        $this->loadModel('Option');
        $this->loadModel('CustomerProfile');
        $this->loadModel('CustomerInvoice');
        $options = array('conditions' => array('Customer.' . $this->Customer->primaryKey => $id));
        $optionProfile = array('conditions' => array('CustomerProfile.userID' => $id));
        $Customer = $this->Customer->find('first', $options);
        $CustomerProfile = $this->CustomerProfile->find('first', $optionProfile);
        $this->request->data['Customer'] = $Customer['Customer'];
        $this->request->data['CustomerProfile'] = $CustomerProfile['CustomerProfile'];

        $this->request->data['Customer']['password'] = "";

        $CommonController = new CommonController;
        $country = $CommonController->country($CustomerProfile['CustomerProfile']['country']);
        $state = $CommonController->state($this->request->data['CustomerProfile']['country'], $CustomerProfile['CustomerProfile']['state']);
        $city = $CommonController->city($this->request->data['CustomerProfile']['state'], $CustomerProfile['CustomerProfile']['city']);


        //invoice
        $optionInvoice = array('conditions' => array('CustomerInvoice.userID' => $id), 'order' => array('CustomerInvoice.created' => 'desc'));
        $CustomerInvoice = $this->CustomerInvoice->find('all', $optionInvoice);
        //end

        $this->set("CustomerInvoice", $CustomerInvoice);
        $this->set("country", $country);
        $this->set("state", $state);
        $this->set("city", $city);
        $this->set("adminuserID", $userID);
        $this->set('title_for_layout', 'Customers');
        $this->set('page_name', 'Customer');
        $this->set('page_title', 'Customer');
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        if ($this->request->is('post')) {

            //Image Cropping
            $valid_exts = array('jpeg', 'jpg', 'png', 'gif');
            $max_file_size = 200 * 1024; #200kb
            $nw = $nh = 200; # image with # height
            $this->loadModel('Option');
            $this->loadModel('CustomerProfile');
            $publicDir = $this->Option->find('first', array('conditions' => array('Option.meta_key' => 'publicDir')));
            $publicDir = rtrim($publicDir['Option']['meta_value'], '/') . '/';
            $error = false;
            $imagePath = "";
            if (isset($this->request->data['other']['profile_pic'])) {
                if (!$this->request->data['other']['profile_pic']['error'] && $this->request->data['other']['profile_pic']['size'] < $max_file_size) {
                    $ext = strtolower(pathinfo($this->request->data['other']['profile_pic']['name'], PATHINFO_EXTENSION));
                    if (in_array($ext, $valid_exts)) {
                        $imagePath = 'userProfileImages/' . $this->request->data['Customer']['username'] . '-' . uniqid() . '.' . $ext;
                        $path = $publicDir . $imagePath;
                        $size = getimagesize($this->request->data['other']['profile_pic']['tmp_name']);

                        $x = (int) $this->request->data['cropImage']['x'];
                        $y = (int) $this->request->data['cropImage']['y'];
                        $w = (int) $this->request->data['cropImage']['w'] ? $this->request->data['cropImage']['w'] : $size[0];
                        $h = (int) $this->request->data['cropImage']['h'] ? $this->request->data['cropImage']['h'] : $size[1];

                        $data = file_get_contents($this->request->data['other']['profile_pic']['tmp_name']);
                        $vImg = imagecreatefromstring($data);
                        $dstImg = imagecreatetruecolor($nw, $nh);
                        imagecopyresampled($dstImg, $vImg, 0, 0, $x, $y, $nw, $nh, $w, $h);
                        imagejpeg($dstImg, $path);
                        imagedestroy($dstImg);
                    }
                } else {
                    $this->Session->setFlash(__('Following error occurs, file is too small or large.'), 'default', array('class' => 'alert alert-danger'));
                    $error = true;
                }
            }

            if (!$error) {
                $this->Customer->create();
                $this->CustomerProfile->create();


                if ($this->Customer->save($this->request->data)) {
                    $this->request->data['CustomerProfile']['profilePic'] = $imagePath;
                    $this->request->data['CustomerProfile']['userID'] = $this->Customer->id;
                    if ($this->CustomerProfile->save($this->request->data)) {
                        $this->Session->setFlash(__('The customer has been saved.'), 'default', array('class' => 'alert alert-success'));
                        $this->redirect(array('action' => "index"));
                    } else {
                        $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
                    }
                } else {
                    $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
                }
            } else {
                $this->Session->setFlash(__('Following error occurs, file is too small or large.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
      

        $userID = $_SESSION['Auth']['User']['id'];
        $this->set("adminuserID", $userID);
       
        $this->set('title_for_layout', 'Add Customer');
        $this->set('page_name', 'Add Customer');
        $this->set('page_title', 'Add Customer');
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        $this->loadModel('Option');
        $this->loadModel('CustomerProfile');
        //end check permission
        $this->loadModel('UserRole');

        if (!$this->Customer->exists($id)) {
            throw new NotFoundException(__('Invalid customer'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $publicDir = $this->Option->find('first', array('conditions' => array('Option.meta_key' => 'publicDir')));
            $publicDir = rtrim($publicDir['Option']['meta_value'], '/') . '/';
            $error = false;
            $imagePath = "";
            //Image Cropping
            $valid_exts = array('jpeg', 'jpg', 'png', 'gif');
            $max_file_size = 200 * 1024; #200kb
            $nw = $nh = 200; # image with # height
            if (isset($this->request->data['other']['profile_pic'])) {
                if (!empty($this->request->data['other']['profile_pic']['error']) && $this->request->data['other']['profile_pic']['size'] < $max_file_size) {
                    $ext = strtolower(pathinfo($this->request->data['other']['profile_pic']['name'], PATHINFO_EXTENSION));
                    if (in_array($ext, $valid_exts)) {
                        $imagePath = 'userProfileImages/' . $this->request->data['Customer']['username'] . '-' . uniqid() . '.' . $ext;
                        $path = $publicDir . $imagePath;
                        $size = getimagesize($this->request->data['other']['profile_pic']['tmp_name']);

                        $x = (int) $this->request->data['cropImage']['x'];
                        $y = (int) $this->request->data['cropImage']['y'];
                        $w = (int) $this->request->data['cropImage']['w'] ? $this->request->data['cropImage']['w'] : $size[0];
                        $h = (int) $this->request->data['cropImage']['h'] ? $this->request->data['cropImage']['h'] : $size[1];

                        $data = file_get_contents($this->request->data['other']['profile_pic']['tmp_name']);
                        $vImg = imagecreatefromstring($data);
                        $dstImg = imagecreatetruecolor($nw, $nh);
                        imagecopyresampled($dstImg, $vImg, 0, 0, $x, $y, $nw, $nh, $w, $h);
                        imagejpeg($dstImg, $path);
                        imagedestroy($dstImg);
                        $this->request->data['CustomerProfile']['profilePic'] = $imagePath;
                    }
                } else {
                    $this->Session->setFlash(__('Following error occurs, file is too small or large.'), 'default', array('class' => 'alert alert-danger'));
                    $error = true;
                }
            }


            if (!$error) {
                if ($this->Customer->save($this->request->data)) {

                    // $this->request->data['CustomerProfile']['userID'] = $this->Customer->id;
                    if ($this->CustomerProfile->save($this->request->data)) {
                        $this->Session->setFlash(__('The customer has been saved.'), 'default', array('class' => 'alert alert-success'));
                        $this->redirect(array('action' => "index"));
                    } else {
                        $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
                    }
                } else {
                    $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
                }
            } else {
                $this->Session->setFlash(__('Following error occurs, file is too small or large.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $userID = $_SESSION['Auth']['User']['id'];

        $options = array('conditions' => array('Customer.' . $this->Customer->primaryKey => $id));
        $optionProfile = array('conditions' => array('CustomerProfile.userID' => $id));
        $Customer = $this->Customer->find('first', $options);
        $CustomerProfile = $this->CustomerProfile->find('first', $optionProfile);
        if(isset($CustomerProfile) && !empty($CustomerProfile)){        
            $this->request->data['CustomerProfile'] = $CustomerProfile['CustomerProfile'];
        }
        $this->request->data['Customer'] = $Customer['Customer'];
        $this->request->data['Customer']['password'] = "";

        $this->set("adminuserID", $userID);
        $this->set('title_for_layout', 'Customers');
        $this->set('page_name', 'Customer');
        $this->set('page_title', 'Customer');
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Customer->id = $id;
        if (!$this->Customer->exists()) {
            throw new NotFoundException(__('Invalid customer'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Customer->delete()) {
            $this->Session->setFlash(__('The customer has been deleted.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => "index"));
        } else {
            $this->Session->setFlash(__('The customer could not be deleted, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index"));
        }
    }

    public function inactive($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('customer', 'review');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Customer->id = $id;
        if (!$this->Customer->exists()) {
            throw new NotFoundException(__('Invalid Customer'));
        }
        $this->request->allowMethod('post');
        if ($this->Customer->saveField('status', INFI_INACTIVE_STATUS)) {
            $this->Session->setFlash('Customer has been inactived.', 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('Customer activation failed, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index"));
        }
    }

    /**
     * active method
     *
     * function to change status to active
     */
    public function active($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('customer', 'review');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Customer->id = $id;
        if (!$this->Customer->exists()) {
            throw new NotFoundException(__('Invalid Customer'));
        }
        $this->request->allowMethod('post');
        if ($this->Customer->saveField('status', INFI_ACTIVE_STATUS)) {
            $this->Session->setFlash('Customer has been actived.', 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('Customer activation failed, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index"));
        }
    }

    public function getCustomerInfo($userID = null, $type = null) {
        $customerinfo = $this->Customer->find('first', array('conditions' => array('Customer.id' => $userID)));
        if (isset($type)) {
            $this->layout = "";
            $this->set('type', $type);
            $this->set('customerinfo', $customerinfo);
        } else {
            return $customerinfo;
            die();
        }
    }

}
