<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');
App::import('Controller', 'Users');
App::import('Vendor', 'ClassUpload', array('file' => 'class.upload.php'));

/**
 * AdminRequest Controller
 *
 * @property AdminRequest $AdminRequest
 * @property PaginatorComponent $Paginator
 */
class CommonController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
        'order' => array(
            'AdminRequest.readed' => 'asc',
            'AdminRequest.proceed' => 'asc'
        )
    );

    // function call before any view rendor 
    // function used to add and override function feautres
    /* 	function beforeRender() {
      parent::beforeRender();
      }
      public function beforeFilter() {
      parent::beforeFilter();
      // Allow
      } */

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->redirect(array('action' => "index/"));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function country($return = null) {
        $this->loadModel('Country');
        $this->layout = "";

        $options = array('conditions' => array('Country.type' => "CO"), 'fields' => array('Country.id', 'Country.local_name'), 'order' => array('Country.local_name ASC'));
        if (isset($return) && $return == true && !is_numeric($return)) {
            $all = "list";
        } elseif (is_numeric($return)) {
            $options['conditions']['Country.id'] = $return;
            $all = "first";
        } else {
            $all = "all";
        }


        $country = $this->Country->find($all, $options);

        if (isset($return)) {
            return $country;
        } else {
            $this->set('countries', $country);
        }
    }

    /**
     * add method
     *
     * @return void
     */
    public function state($countryId, $return = null) {
        $this->loadModel('Country');
        $this->layout = "";
        $options = array('conditions' => array('Country.type' => "RE", 'Country.in_location' => $countryId), 'fields' => array('Country.id', 'Country.local_name'), 'order' => array('Country.local_name ASC'));
        if (isset($return) && $return == true && !is_numeric($return)) {
            $all = "list";
        } elseif (is_numeric($return)) {
            $options['conditions']['Country.id'] = $return;
            $all = "first";
        } else {
            $all = "all";
        }
        $country = $this->Country->find($all, $options);
        if (isset($return)) {
            return $country;
        } else {
            $this->set('countries', $country);
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function city($stateId, $return = null) {
        $this->loadModel('Country');
        $this->layout = "";
        $options = array('conditions' => array('Country.type' => "CI", 'Country.in_location' => $stateId), 'fields' => array('Country.id', 'Country.local_name'), 'order' => array('Country.local_name ASC'));
        if (isset($return) && $return == true && !is_numeric($return)) {
            $all = "list";
        } elseif (is_numeric($return)) {
            $options['conditions']['Country.id'] = $return;
            $all = "first";
        } else {
            $all = "all";
        }
        $country = $this->Country->find($all, $options);
        if (isset($return)) {
            return $country;
        } else {
            $this->set('countries', $country);
        }
    }

    function placeFinder($keyword=false){
        if($keyword==false){
            $keyword=$this->request->query('placeFinder');
        }
        $view=($this->request->query('view')=='pre') ? 'pre' : 'json' ;
        if (empty($keyword) || $keyword==false) {
            throw new NotFoundException(__('Invalid page'));
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://maps.googleapis.com/maps/api/place/autocomplete/json?types=(cities)&libraries=places&language=en_GB&key=AIzaSyD0dVnF3UoMuy5Mmc1tPyi72KgXR6132q4&input='.$keyword,
            CURLOPT_USERAGENT => 'placeFinder_api_infi'
        ));
        $resp = json_decode(curl_exec($curl));
        curl_close($curl);
        $places=array();
        if(isset($resp->status) && $resp->status=="OK"){
            foreach($resp->predictions as $prediction){                
                if(!isset($prediction->terms[2])){
                    $prediction->terms[2]=(object) array('offset'=>$prediction->terms[1]->offset,'value'=>$prediction->terms[1]->value);
                    $prediction->terms[1]->offset=" ";
                    $prediction->terms[1]->value=" ";
                }

                $places[]=array('location'=>$prediction->description,'place_id'=>$prediction->place_id,'terms'=>$prediction->terms);
            }
            $places['status']="OK";
        }else{
            $places=array('status'=>'ZERO_RESULTS');
        }
        $this->layout = "";
        $this->set("view",$view);
        if($view=='json'){
            $this->set("placeArray",json_encode($places));
        }elseif($view=='pre'){
            $this->set("placeArray",($places));
        }
    }
}
