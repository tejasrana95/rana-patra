<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');
App::import('Controller', 'Users');

/**
 * AdminRequest Controller
 *
 * @property AdminRequest $AdminRequest
 * @property PaginatorComponent $Paginator
 */
class AdminRequestsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
        'order' => array(
            'AdminRequest.readed' => 'asc',
            'AdminRequest.proceed' => 'asc'
            )
        );

    // function call before any view rendor 
    // function used to add and override function feautres
    /* 	function beforeRender() {
      parent::beforeRender();
      }
      public function beforeFilter() {
      parent::beforeFilter();
      // Allow
  } */

    /**
     * index method
     *
     * @return void
     */
    public function index($lang = null) {
        $UserRolesController = new UserRolesController;
        $user_role = $UserRolesController->getPermission();
        $this->Paginator->settings = $this->paginate;
        $this->AdminRequest->recursive = 0;
        $all = $this->AdminRequest->find('all');
        if ($user_role['is_admin'] != 1) {
            $this->Paginator->settings = array(
                'conditions' => array('AdminRequest.userID' => $_SESSION['Auth']['User']['id']),
                );
        }
        $admin_requests = $this->Paginator->paginate('AdminRequest');
        $this->set('admin_requests', $admin_requests);
        $this->set('title_for_layout', 'Request to admin');
        $this->set('page_name', 'Request to admin');
        $this->set('page_title', 'Request to admin');
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->AdminRequest->exists($id)) {
            throw new NotFoundException(__('Invalid Admin Requests'));
        }
        $options = array('conditions' => array('AdminRequest.id' => $id));
        $this->set('adminrequests', $this->AdminRequest->find('first', $options));

        $this->set('title_for_layout', 'View Request to admin');
        $this->set('page_name', 'View Request to admin');
        $this->set('parent_page_name', 'Request to admin');
        $this->set('page_title', 'View Request to admin');
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->AdminRequest->create();
            $this->request->data['AdminRequest']['userID'] = $_SESSION['Auth']['User']['id'];
            $this->request->data['AdminRequest']['proceed'] = 0;
            $this->request->data['AdminRequest']['readed'] = 0;
            if ($this->AdminRequest->save($this->request->data)) {
                $this->Session->setFlash(__('The Admin Request has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index/"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $requesttypedrop = array('1' => 'Delete Request', '2' => 'Edit Request', '3' => 'Approval Request');
        $this->set('requesttypedrop', $requesttypedrop);
        $this->set('title_for_layout', 'Add Request to admin');
        $this->set('page_name', 'Add Request to admin');
        $this->set('parent_page_name', 'Request to admin');
        $this->set('page_title', 'Add Request to admin');
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->AdminRequest->exists($id)) {
            throw new NotFoundException(__('Invalid Admin Request'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['AdminRequest']['readed'] = 1;
            if ($this->AdminRequest->save($this->request->data)) {
                $this->Session->setFlash(__('The Admin Request has been updated.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index/"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $options = array('conditions' => array('AdminRequest.' . $this->AdminRequest->primaryKey => $id));
        $this->request->data = $this->AdminRequest->find('first', $options);
        $proceeddrop = array('0' => 'Pending', '1' => 'Reject', '2' => 'Approve');
        $requesttypedrop = array('1' => 'Delete Request', '2' => 'Edit Request', '3' => 'Approval Request');
        $this->set('requesttypedrop', $requesttypedrop);
        $this->set('proceeddrop', $proceeddrop);

        $this->set('title_for_layout', 'Edit Request to admin');
        $this->set('page_name', 'Edit Request to admin');
        $this->set('parent_page_name', 'Request to admin');
        $this->set('page_title', 'Edit Request to admin');
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->AdminRequest->id = $id;
        if (!$this->AdminRequest->exists()) {
            throw new NotFoundException(__('Invalid customerlogo'));
        }
        //method allow for action 
        $this->request->allowMethod('post', 'delete');
        //end
        if ($this->AdminRequest->delete()) {

            $this->Session->setFlash(__('The Admin Request has been deleted.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => "index/"));
        } else {
            $this->Session->setFlash(__('The Admin Request could not be deleted, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index/"));
        }
    }

    public function getAdminRequest() {
        $allrequest = $this->AdminRequest->find('all', array('conditions' => array('AdminRequest.readed' => 0), 'order' => array('AdminRequest.id desc')));
        return $allrequest;
    }

    public function getNotifications() {
        $this->loadModel('Award');
        $this->loadModel('Career');
        $this->loadModel('Customerlogo');
        $this->loadModel('CustomerSpeak');
        $this->loadModel('Event');
        $this->loadModel('Gallery');
        $this->loadModel('History');
        $this->loadModel('Infographic');
        $this->loadModel('Insight');
        $this->loadModel('LatestInHr');
        $this->loadModel('Mediakit');
        $this->loadModel('News');
        $this->loadModel('Page');
        $this->loadModel('Partnerspeak');
        $this->loadModel('Team');
        $notifications = array();


        //award notification
        $awards = $this->Award->find('all', array('conditions' => array('Award.status !=' => NEO_ACTIVE_STATUS,
            'Award.created >' => date('Y-m-d', strtotime("-4 weeks"))
            )));

        foreach ($awards as $award) {
            $notifications[] = array('title' => $award['Award']['award_title'], 'section' => 'Awards', 'link' => Router::url('/', true) . 'awards/edit/' . $award['Award']['lang'] . '/' . $award['Award']['id']);
        }
        //end award
        //career
        $Careers = $this->Career->find('all', array('conditions' => array('Career.status !=' => NEO_ACTIVE_STATUS,
            'Career.created >' => date('Y-m-d', strtotime("-4 weeks"))
            )));

        foreach ($Careers as $Career) {
            $notifications[] = array('title' => $Career['Career']['position'], 'section' => 'Career', 'link' => Router::url('/', true) . 'career/edit/' . $Career['Career']['lang'] . '/' . $Career['Career']['id']);
        }
        //end career
        //Customerlogo
        $Customerlogos = $this->Customerlogo->find('all', array('conditions' => array('Customerlogo.status !=' => NEO_ACTIVE_STATUS,
            'Customerlogo.created >' => date('Y-m-d', strtotime("-4 weeks"))
            )));

        foreach ($Customerlogos as $Customerlogo) {
            $notifications[] = array('title' => $Customerlogo['Customerlogo']['title'], 'section' => 'Customer logo', 'link' => Router::url('/', true) . 'customerlogos/edit/' . $Customerlogo['Customerlogo']['id']);
        }
        //end Customerlogo
        //CustomerSpeak
        $CustomerSpeaks = $this->CustomerSpeak->find('all', array('conditions' => array('CustomerSpeak.status !=' => NEO_ACTIVE_STATUS,
            'CustomerSpeak.created >' => date('Y-m-d', strtotime("-4 weeks"))
            )));

        foreach ($CustomerSpeaks as $CustomerSpeak) {
            $notifications[] = array('title' => $CustomerSpeak['CustomerSpeak']['heading'], 'section' => 'Customer Speak', 'link' => Router::url('/', true) . 'customer_speaks/edit/' . $CustomerSpeak['CustomerSpeak']['lang'] . '/' . $CustomerSpeak['CustomerSpeak']['id']);
        }
        //end CustomerSpeak
        //Event
        $Events = $this->Event->find('all', array('conditions' => array('Event.status !=' => NEO_ACTIVE_STATUS,
            'Event.created >' => date('Y-m-d', strtotime("-4 weeks"))
            )));

        foreach ($Events as $Event) {
            $notifications[] = array('title' => $Event['Event']['event_title'], 'section' => 'Event', 'link' => Router::url('/', true) . 'events/edit/' . $Event['Event']['lang'] . '/' . $Event['Event']['id']);
        }
        //end Event
        //Gallery
        $Galleries = $this->Gallery->find('all', array('conditions' => array('Gallery.status !=' => NEO_ACTIVE_STATUS,
            'Gallery.created >' => date('Y-m-d', strtotime("-4 weeks"))
            )));

        foreach ($Galleries as $Gallery) {
            $notifications[] = array('title' => $Gallery['Gallery']['heading'], 'section' => 'Gallery', 'link' => Router::url('/', true) . 'gallery/edit/' . $Gallery['Gallery']['lang'] . '/' . $Gallery['Gallery']['id']);
        }
        //end Gallery
        //History
        $Histories = $this->History->find('all', array('conditions' => array('History.status !=' => NEO_ACTIVE_STATUS,
            'History.created >' => date('Y-m-d', strtotime("-4 weeks"))
            )));

        foreach ($Histories as $History) {
            $notifications[] = array('title' => $History['History']['year'], 'section' => 'History', 'link' => Router::url('/', true) . 'history/edit/' . $History['History']['lang'] . '/' . $History['History']['id']);
        }
        //end History
        //Infographic
        $Infographics = $this->Infographic->find('all', array('conditions' => array('Infographic.status !=' => NEO_ACTIVE_STATUS,
            'Infographic.created >' => date('Y-m-d', strtotime("-4 weeks"))
            )));

        foreach ($Infographics as $Infographic) {
            $notifications[] = array('title' => $Infographic['Infographic']['heading'], 'section' => 'Infographic', 'link' => Router::url('/', true) . 'infographics/edit/' . $Infographic['Infographic']['lang'] . '/' . $Infographic['Infographic']['id']);
        }
        //end Infographic
        //Insight
        $Insights = $this->Insight->find('all', array('conditions' => array('Insight.status !=' => NEO_ACTIVE_STATUS,
            'Insight.created >' => date('Y-m-d', strtotime("-4 weeks"))
            )));

        foreach ($Insights as $Insight) {
            $notifications[] = array('title' => $Insight['Insight']['title'], 'section' => 'Insight', 'link' => Router::url('/', true) . 'insights/edit/' . $Insight['Insight']['lang'] . '/' . $Insight['Insight']['id']);
        }
        //end Insight
        //LatestInHr
        $LatestInHrs = $this->LatestInHr->find('all', array('conditions' => array('LatestInHr.status !=' => NEO_ACTIVE_STATUS,
            'LatestInHr.created >' => date('Y-m-d', strtotime("-4 weeks"))
            )));

        foreach ($LatestInHrs as $LatestInHr) {
            $notifications[] = array('title' => $LatestInHr['LatestInHr']['heading'], 'section' => 'Latest in Hr', 'link' => Router::url('/', true) . 'latest_in_hrs/edit/' . $LatestInHr['LatestInHr']['lang'] . '/' . $LatestInHr['LatestInHr']['id']);
        }
        //end LatestInHr
        //Mediakit
        $Mediakits = $this->Mediakit->find('all', array('conditions' => array('Mediakit.status !=' => NEO_ACTIVE_STATUS,
            'Mediakit.created >' => date('Y-m-d', strtotime("-4 weeks"))
            )));

        foreach ($Mediakits as $Mediakit) {
            $notifications[] = array('title' => $Mediakit['Mediakit']['title'], 'section' => 'Media kit', 'link' => Router::url('/', true) . 'mediakits/edit/' . $Mediakit['Mediakit']['id']);
        }
        //end Mediakit
        //News
        $Newses = $this->News->find('all', array('conditions' => array('News.status !=' => NEO_ACTIVE_STATUS,
            'News.created >' => date('Y-m-d', strtotime("-4 weeks"))
            )));

        foreach ($Newses as $News) {
            $notifications[] = array('title' => $News['News']['news_title'], 'section' => 'News', 'link' => Router::url('/', true) . 'news/edit/' . $News['News']['lang'] . '/' . $News['News']['id']);
        }
        //end News
        //Page
        $Pages = $this->Page->find('all', array('conditions' => array('Page.status !=' => NEO_ACTIVE_STATUS,
            'Page.created >' => date('Y-m-d', strtotime("-4 weeks"))
            )));

        foreach ($Pages as $Page) {
            $notifications[] = array('title' => $Page['Page']['page_name'], 'section' => 'Page', 'link' => Router::url('/', true) . 'pages/edit/' . $Page['Page']['lang'] . '/' . $Page['Page']['id']);
        }
        //end page  
        //Partnerspeak
        $Partnerspeaks = $this->Partnerspeak->find('all', array('conditions' => array('Partnerspeak.status !=' => NEO_ACTIVE_STATUS,
            'Partnerspeak.created >' => date('Y-m-d', strtotime("-4 weeks"))
            )));

        foreach ($Partnerspeaks as $Partnerspeak) {
            $notifications[] = array('title' => $Partnerspeak['Partnerspeak']['heading'], 'section' => 'Partner speak', 'link' => Router::url('/', true) . 'partnerspeaks/edit/' . $Partnerspeak['Partnerspeak']['lang'] . '/' . $Partnerspeak['Partnerspeak']['id']);
        }
        //end Partnerspeak  
         //Team
        $Teams = $this->Team->find('all', array('conditions' => array('Team.status !=' => NEO_ACTIVE_STATUS,
            'Team.created >' => date('Y-m-d', strtotime("-4 weeks"))
            )));

        foreach ($Teams as $Team) {
            $notifications[] = array('title' => $Team['Team']['name'], 'section' => 'Team', 'link' => Router::url('/', true) . 'teams/edit/' . $Team['Team']['lang'] . '/' . $Team['Team']['id']);
        }
        //end Team  
        return $notifications;
    }

}
