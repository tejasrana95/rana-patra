<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');

/**
 * DataSheets Controller
 *
 * @property DataSheet $DataSheet
 * @property PaginatorComponent $Paginator
 */
class MenusController extends AppController {

    public $uses = array(
        'Page',
        'Menu',
        'News',
    );

    public function index( $menu_for = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission     = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard',
            ));
        }
        //end check permission
     
        $this->loadModel('Category');
       


        //if($menu_for==null)$menu_for="header-menu";
        if (isset($this->request->data['select_menu_type'])) {
            $menu_for = $this->request->data['select_menu_type'];
        } else {
            $menu_for = "header-menu";
        }

        //fetching pages objects to display on left side
        $pages = $this->Page->find('list', array(
            'fields'     => array('Page.id'),
            'conditions' => array('Page.status' => INFI_ACTIVE_STATUS),
        ));
        // to render the previously define menu
        $menu_headers = $this->Menu->find('all', array(
            "conditions" => array("parent_id" => 0, "menu_for" => $menu_for), // 0 indicates menu headers
            "order"      => "Menu.sort_order asc",
        ));
        $all_parents = $this->Menu->find('all', array(
            "conditions" => array("menu_for" => $menu_for),
            'fields'     => array('DISTINCT Menu.parent_id'),
        ));
        $aux_array = array(); // auxiliary array to store all parents and their children menu items

        $content_list = $this->Menu->find('list', array(
            'fields'     => array('Menu.content_name'),
            "conditions" => array("content_type" => "model", "menu_for" => $menu_for), // to deactivate checkboxes which are already in menu
        ));
        $all_menu_items = $this->Menu->find('all', array("conditions" => array("menu_for" => $menu_for)));
        foreach ($all_parents as $parent) {
            $a = $this->Menu->find('all', array(
                "conditions" => array("parent_id" => $parent['Menu']['parent_id'], "menu_for" => $menu_for),
            ));
            $aux_array[$parent['Menu']['parent_id']] = $a;
        }
        $page_menu_list = $this->Menu->find('list', array(
            'fields'     => array('Menu.content_name'),
            "conditions" => array("content_type" => "page", "menu_for" => $menu_for), // to deactivate checkboxes which are already in menu
        ));

        //categories
        //parent Category
        $parentcate = $this->Category->find('all', array(
            'conditions' => array('Category.parent' => 0),
            'order'      => array('Category.label ASC'),
        ));

        $category_list = $this->Menu->find('list', array(
            'fields'     => array('Menu.content_name'),
            "conditions" => array("content_type" => "category", "menu_for" => $menu_for), // to deactivate checkboxes which are already in menu
        ));

       
        $this->set('page_menu_list', $page_menu_list); // pages which are already there in menu table
        $this->set('menu_headers', $menu_headers);
        $this->set('all_parents', $all_parents);
        $this->set('aux_array', $aux_array);
        $this->set('navigation', $menu_headers);
        $this->set('menu_for', $menu_for);
        $this->set('hierarchy', 1);
        $this->set('all_menu_items', $all_menu_items);
        $this->set('menu_pages', $this->Menu->menu_type);
        $this->set('categories', $parentcate);
        $this->set('category_list', $category_list);
       
        $this->set('pages', $pages); // all pages in the database
     
        $this->set('content_list', $content_list);
        $this->set('title_for_layout', 'Menus');
        $this->set('page_name', 'Menu');
        $this->set('page_title', 'Menu');
    }
 public function save_menu() {
        if ($this->request->is('post')) {
            $menu_for = $this->request->data['Page']['menu_for'];
            //debug($this->request->data);
            //debug(json_decode($this->request->data['Page']['nestable-output'], true));
            $menu_items = json_decode($this->request->data['Page']['nestable-output'], true);
            if ($menu_items == null)
                return $this->redirect(array('controller' => 'menus', 'action' => 'index',));
            $hierarchy_order = 1;
            if (!empty($menu_items)) {

                //	$this->Menu->deleteAll(array('1 = 1'), false);
                $this->Menu->deleteAll(array('Menu.menu_for' => $menu_for), false);
                foreach ($menu_items as $k => $v) {
                    $this->Menu->create();
                 
                    $this->Menu->set('parent_id', 0);
                    $this->Menu->set('hierarchy', $hierarchy_order);
                    $this->Menu->set('content_type', $v['type']);
                    $this->Menu->set('icon', @$v['icon']);
                    $this->Menu->set('content_name', $v['id']);
                    $this->Menu->set('login_only', $v['login']);
                    //$this->MenuLink->set('sort_order',$parentSortorder);
                    $this->Menu->set('menu_for', $menu_for);
                    if ($v['type'] == "model") {
                        $this->Menu->set('url', $v['id']);
                    } else if ($v['type'] == "page") {
                        $page_obj = $this->Page->find('first', array(
                            'conditions' => array('Page.id' => $v['id'])
                        ));

                        $this->Menu->set('url', $page_obj['Page']['page_slug']);
                    } else if ($v['type'] == "custom") {
                        //URL for custom
                        $this->Menu->set('url', $v['url']);
                    } else if ($v['type'] == "customHTML") {
                        $this->Menu->set('url', $v['url']);
                    }


                    $this->Menu->save(array('updated' => false));
                    if (!empty($v['children'])) {
                        self::save_children( $v['children'], $v['id'], $hierarchy_order + 1, $menu_for);
                    }
                }
                //self::main_menu('header-menu'); // rendering menus
                return $this->redirect(
                                array('controller' => 'menus', 'action' => 'index',  $menu_for)
                );
            }
        } else {
            $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
        }
    }
    /**
     * function to save children menu structure into database
     *
     */
    public function save_children( $childrenArray = null, $parent = null, $hierarchy_order, $menu_for) {
        //first generation children
        if ($childrenArray) {
            foreach ($childrenArray as $child) {
                $this->Menu->create();
             
                $this->Menu->set('parent_id', $parent);
                $this->Menu->set('hierarchy', $hierarchy_order);
                $this->Menu->set('content_type', $child['type']);
                $this->Menu->set('content_name', $child['id']);
                $this->Menu->set('login_only', $child['login']);
                $this->Menu->set('icon', @$child['icon']);
                $this->Menu->set('menu_for', $menu_for);
                if ($child['type'] == "model") {
                    $this->Menu->set('url', $child['id']);
                } else if ($child['type'] == "page") {
                    $page_obj = $this->Page->find('first', array(
                        'conditions' => array('Page.id' => $child['id']),
                    ));
                    $this->Menu->set('url', @$page_obj['Page']['page_slug']);
                } else if ($child['type'] == "custom") {
                    $this->Menu->set('url', $child['url']);
                } else if ($child['type'] == "customHTML") {
                    $this->Menu->set('url', $child['url']);
                }
                //$this->Menu->set('menu_id',$this->request->data['Menu']['menu_id']);
                $this->Menu->save(array('updated' => false));

                //2nd generation
                if (isset($child['children'])) {
                    foreach ($child['children'] as $chld) {
                        $this->Menu->create();
                       
                        $this->Menu->set('parent_id', $child['id']);
                        $this->Menu->set('hierarchy', $hierarchy_order + 1);
                        $this->Menu->set('content_type', $chld['type']);
                        $this->Menu->set('content_name', $chld['id']);
                        $this->Menu->set('icon', @$chld['icon']);
                        $this->Menu->set('login_only', $chld['login']);
                        $this->Menu->set('menu_for', $menu_for);
                        if ($chld['type'] == "model") {
                            $this->Menu->set('url', $chld['id']);
                        } else if ($chld['type'] == "page") {
                            $page_obj = $this->Page->find('first', array(
                                'conditions' => array('Page.id' => $chld['id']),
                            ));
                            $this->Menu->set('url', $page_obj['Page']['page_slug']);
                        } else if ($chld['type'] == "custom") {
                            $this->Menu->set('url', $chld['url']);
                        } else if ($chld['type'] == "customHTML") {
                            $this->Menu->set('url', $chld['url']);
                        }
                        //$this->Menu->set('menu_id',$this->request->data['Menu']['menu_id']);
                        $this->Menu->save(array('updated' => false));

                        //3rd generation children
                        if (isset($chld['children'])) {
                            foreach ($chld['children'] as $ch) {
                                $this->Menu->create();
                               
                                $this->Menu->set('parent_id', $chld['id']);
                                $this->Menu->set('hierarchy', $hierarchy_order + 2);
                                $this->Menu->set('content_type', $ch['type']);
                                $this->Menu->set('content_name', $ch['id']);
                                $this->Menu->set('icon', @$ch['icon']);
                                $this->Menu->set('login_only', $ch['login']);
                                $this->Menu->set('menu_for', $menu_for);
                                if ($ch['type'] == "model") {
                                    $this->Menu->set('url', $ch['id']);
                                } else if ($ch['type'] == "page") {
                                    $page_obj = $this->Page->find('first', array(
                                        'conditions' => array('Page.id' => $ch['id']),
                                    ));
                                    $this->Menu->set('url', $page_obj['Page']['page_slug']);
                                } else if ($ch['type'] == "custom") {
                                    $this->Menu->set('url', $ch['url']);
                                } else if ($ch['type'] == "customHTML") {
                                    $this->Menu->set('url', $ch['url']);
                                }
                                //$this->Menu->set('menu_id',$this->request->data['Menu']['menu_id']);
                                $this->Menu->save(array('updated' => false));

                                //4th generation children
                                if (isset($ch['children'])) {
                                    foreach ($ch['children'] as $c) {

                                        $this->Menu->create();
                                      
                                        $this->Menu->set('parent_id', $ch['id']);
                                        $this->Menu->set('hierarchy', $hierarchy_order + 3);
                                        $this->Menu->set('content_type', $c['type']);
                                        $this->Menu->set('content_name', $c['id']);
                                        $this->Menu->set('icon', @$c['icon']);
                                        $this->Menu->set('login_only', $c['login']);
                                        $this->Menu->set('menu_for', $menu_for);
                                        if ($c['type'] == "model") {
                                            $this->Menu->set('url', $c['id']);
                                        } else if ($c['type'] == "page") {
                                            $page_obj = $this->Page->find('first', array(
                                                'conditions' => array('Page.id' => $c['id']),
                                            ));
                                            $this->Menu->set('url', $page_obj['Page']['page_slug']);
                                        } else if ($c['type'] == "custom") {
                                            $this->Menu->set('url', $c['url']);
                                        } else if ($c['type'] == "customHTML") {
                                            $this->Menu->set('url', $c['url']);
                                        }
                                        //$this->Menu->set('menu_id',$this->request->data['Menu']['menu_id']);
                                        $this->Menu->save(array('updated' => false));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
 public function check_status($type = null, $id = null) {
        $page_status = false;
        $count = 0;
        if ($type == "model") {
            if ($id == "Services")
                $count = $this->Service->find('count');
            if ($id == "Events")
                $count = $this->Event->find('count');
            if ($id == "Whitepapers")
                $count = $this->WhitePaper->find('count');
            if ($id == "Datasheets")
                $count = $this->DataSheet->find('count');
            if ($id == "News")
                $count = $this->News->find('count');
            if ($id == "Partners")
                $count = $this->Partner->find('count');
            if ($id == "Newsletters")
                $count = $this->NewsLetter->find('count');
            if ($id == "Webinars")
                $count = $this->Webinar->find('count');
            if ($id == "Customers")
                $count = $this->Customer->find('count');
            if ($count != 0)
                $page_status = true;
            //debug($count);
        }else if ($type == "page") {
            $page_obj = $this->Page->find('first', array(
                'conditions' => array('Page.id' => $id)
            ));
            if ($page_obj['Page']['status'])
                $page_status = true;
        }else if ($type == "custom") {
            $page_status = true;
        } else if ($type == "customHTML") {
            $page_status = true;
        } else if ($type == "category") {
            $page_status = true;
        } else if ($type == "insight") {
            $page_status = true;
        }
        //debug($page_status );debug($type ." ".$id);
        return $page_status;
    }

    /**
     * edit method
     *
     * @param  string              $id
     * @throws NotFoundException
     * @return void
     */
    public function edit( $id = null) {
      
        if (!$this->Menu->exists($id)) {
            throw new NotFoundException(__('Invalid Menu'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Menu->id                     = $id;
            $this->request->data['Menu']['url'] = base64_encode($this->request->data['Menu']['url']);
            if ($this->Menu->save($this->request->data)) {
                $this->Session->setFlash(__('The menu has been updated.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index/"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $menudata = $this->Menu->find('first', array(
            'conditions' => array('Menu.id' => $id)));
        if ($menudata['Menu']['content_type'] == "customHTML") {
            $menudata['Menu']['url'] = base64_decode($menudata['Menu']['url']);
        }
        $this->request->data = $menudata;
    }

 
 
 public function get_page_name($id = null) {
        $page_obj = $this->Page->find('first', array(
            'conditions' => array('Page.id' => $id)
        ));
        return $page_obj['Page']['page_name'];
    }
    /**
     * function to get type of menu
     *
     */
 
public function delete_menu_item($id = null) {

        if ($id != null) {
            $item_to_delete = $this->Menu->find('first', array(
                "conditions" => array("id" => $id), // to deactivate checkboxes which are already in menu
            ));

            //debug($item_to_delete['Menu']['content_name']);
            $children = $this->Menu->find('all', array(
                "conditions" => array("parent_id" => $item_to_delete['Menu']['content_name'],
                    "menu_for" => $item_to_delete['Menu']['menu_for']
                ), // to deactivate checkboxes which are already in menu
            ));
            //debug($children);

            foreach ($children as $child) {
                //debug($child);	
                $this->Menu->read(null, $child['Menu']['id']);
                $this->Menu->set(array(
                    'parent_id' => $item_to_delete['Menu']['parent_id'],
                ));
                $this->Menu->save();
            }

            //debug($children);				
            $this->Menu->delete($id);
            return $this->redirect(array('controller' => 'menus', 'action' => 'index',  $item_to_delete['Menu']['menu_for']));
        }
    }

}
