<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');

/**
 * UserRoles Controller
 *
 * @property UserRole $UserRole
 * @property PaginatorComponent $Paginator
 */
class UserRolesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
        'order' => array(
            'UserRole.id' => 'desc'
        )
    );

    public function beforeFilter() {
        parent::beforeFilter();
// Allow userroles to register and logout.
        $this->Auth->allow('add', 'logout');
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Paginator->settings = $this->paginate;
        $this->UserRole->recursive = 0;
        $this->set('userRoles', $this->Paginator->paginate());
        $this->set('title_for_layout', 'User Roles');
        $this->set('page_name', 'User Roles');
        $this->set('page_title', 'User Roles');
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        if (!$this->UserRole->exists($id)) {
            throw new NotFoundException(__('Invalid userrole'));
        }
        $options = array('conditions' => array('UserRole.' . $this->UserRole->primaryKey => $id));
        $userRoles = $this->UserRole->find('first', $options);
        $userRoles['UserRole']['roles'] = unserialize($userRoles['UserRole']['roles']);
        $this->set('userrole', $userRoles);
        $this->set('title_for_layout', 'User Roles');
        $this->set('page_name', 'User Roles');
        $this->set('page_title', 'User Roles');
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        if ($this->request->is('post')) {
            $this->request->data['UserRole']['roles'] = serialize($this->request->data['UserRole']['roles']);
            $this->UserRole->create();
            if ($this->UserRole->save($this->request->data)) {
                $this->Session->setFlash(__('The user role has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $this->set('title_for_layout', 'User Roles');
        $this->set('page_name', 'User Roles');
        $this->set('page_title', 'User Roles');
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        if (!$this->UserRole->exists($id)) {
            throw new NotFoundException(__('Invalid user role'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['UserRole']['roles'] = serialize($this->request->data['UserRole']['roles']);
            if ($this->UserRole->save($this->request->data)) {
                $this->Session->setFlash(__('The user role has been updated.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        } else {
            $options = array('conditions' => array('UserRole.' . $this->UserRole->primaryKey => $id));
            $this->request->data = $this->UserRole->find('first', $options);
            $this->request->data['UserRole']['roles'] = unserialize($this->request->data['UserRole']['roles']);
        }
        $this->set('title_for_layout', 'User Roles');
        $this->set('page_name', 'User Roles');
        $this->set('page_title', 'User Roles');
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->UserRole->id = $id;
        if (!$this->UserRole->exists()) {
            throw new NotFoundException(__('Invalid user role'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->UserRole->delete()) {
            $this->Session->setFlash(__('The user role has been deleted.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => "index"));
        } else {
            $this->Session->setFlash(__('The user role could not be deleted, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index"));
        }
    }

    public function checkPermission($controller = null, $action = null) {

        if (isset($_SESSION['Auth']['User']['id'])) {

            $user_id = $_SESSION['Auth']['User']['id'];
            $this->loadModel('User');
            $this->loadModel('UserRole');
            $role_id = $this->User->find('first', array('conditions' => array('User.id' => $user_id)));
            $role = $this->UserRole->find('first', array('conditions' => array('UserRole.id' => $role_id['User']['role_id'])));
            $is_admin = $role['UserRole']['is_admin'];
            $roles = unserialize($role['UserRole']['roles']);
            if ($controller != null && $action != null) {
                if ($is_admin != 1) {
                    if ($action == 'index') {
                        if ($roles[$controller]['add'] == 1 || $roles[$controller]['edit'] == 1 || $roles[$controller]['delete'] == 1) {
                            return true;
                        } else {
                            return false;
                        }
                    } elseif ($action == 'review') {
                        if ($roles[$controller][$action] != 1) {
                            return true;
                        } else {
                            return false;
                        }
                    } elseif ($roles[$controller][$action] == 1) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return true;
                }
            } elseif (isset($_SESSION['Auth']['User']['id']) && $is_admin == 1) {
                return true;
            }
        } else {
            return false;
        }
    }

    public function getPermission($userID = null) {
        if ($userID == null) {
            $userID = $_SESSION['Auth']['User']['id'];
        }
        $this->loadModel('User');
        $this->loadModel('UserRole');
        $role_id = $this->User->find('first', array('conditions' => array('User.id' => $userID)));
        $role = $this->UserRole->find('first', array('conditions' => array('UserRole.id' => $role_id['User']['role_id'])));
        $is_admin = $role['UserRole']['is_admin'];
        $roles = unserialize($role['UserRole']['roles']);
        return array('is_admin' => $is_admin, 'roles' => $roles);
    }

}
