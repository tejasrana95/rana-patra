<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');
App::import('Controller', 'Users');

/**
 * Testimonial Controller
 *
 * @property Testimonial $Testimonial
 * @property PaginatorComponent $Paginator
 */
class TestimonialsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
        'order' => array(
            'Testimonial.readed' => 'asc',
            'Testimonial.proceed' => 'asc'
            )
        );

    // function call before any view rendor 
    // function used to add and override function feautres
    /*  function beforeRender() {
      parent::beforeRender();
      }
      public function beforeFilter() {
      parent::beforeFilter();
      // Allow
  } */

    /**
     * index method
     *
     * @return void
     */
    public function index($lang = null) {
        $UserRolesController = new UserRolesController;
        $user_role = $UserRolesController->getPermission();
        $this->Paginator->settings = $this->paginate;
        $this->Testimonial->recursive = 0;
        $all = $this->Testimonial->find('all');
        $admin_requests = $this->Paginator->paginate('Testimonial');
        $this->set('testimonails', $admin_requests);
        $this->set('title_for_layout', 'Testimonial');
        $this->set('page_name', 'Testimonial');
        $this->set('page_title', 'Testimonial');
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Testimonial->exists($id)) {
            throw new NotFoundException(__('Invalid data'));
        }
        $options = array('conditions' => array('Testimonial.id' => $id));
        $this->set('Testimonials', $this->Testimonial->find('first', $options));

        $this->set('title_for_layout', 'View Testimonial');
        $this->set('page_name', 'View Testimonial');
        $this->set('parent_page_name', 'Testimonial');
        $this->set('page_title', 'View Testimonial');
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Testimonial->create();
            if ($this->Testimonial->save($this->request->data)) {
                $this->Session->setFlash(__('The Testimonial has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index/"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $this->set('title_for_layout', 'Add Testimonial');
        $this->set('page_name', 'Add Testimonial');
        $this->set('parent_page_name', 'Testimonial');
        $this->set('page_title', 'Add Testimonial');
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Testimonial->exists($id)) {
            throw new NotFoundException(__('Invalid Testimonial'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Testimonial->save($this->request->data)) {
                $this->Session->setFlash(__('The Testimonial has been updated.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index/"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $options = array('conditions' => array('Testimonial.' . $this->Testimonial->primaryKey => $id));
        $this->request->data = $this->Testimonial->find('first', $options);
       
        $this->set('title_for_layout', 'Edit Testimonial');
        $this->set('page_name', 'Edit Testimonial');
        $this->set('parent_page_name', 'Testimonial');
        $this->set('page_title', 'Edit Testimonial');
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Testimonial->id = $id;
        if (!$this->Testimonial->exists()) {
            throw new NotFoundException(__('Invalid customerlogo'));
        }
        //method allow for action 
        $this->request->allowMethod('post', 'delete');
        //end
        if ($this->Testimonial->delete()) {

            $this->Session->setFlash(__('The Testimonial has been deleted.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => "index/"));
        } else {
            $this->Session->setFlash(__('The Testimonial could not be deleted, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index/"));
        }
    }
}