<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');
App::import('Vendor', 'ClassUpload', array('file' => 'class.upload.php'));

/**
 * Pages Controller
 *
 * @property Page $Page
 * @property PaginatorComponent $Paginator
 */
class PagesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Security');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
    );

    // function call before any view rendor 
    // function used to add and override function feautres
    function beforeRender() {
        parent::beforeRender();
    }

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Security->validatePost = false;
        // Allow 
    }

    /**
     * index method
     *
     * @return void
     */
    public function index($all = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('page', 'index');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
       
        $this->Paginator->settings = $this->paginate;
        $this->Page->recursive = 0;
      

        $pages = $this->Paginator->paginate('Page');

    
        $this->set('pages', $pages);
         $this->set('title_for_layout', 'Pages');
        $this->set('page_name', 'Pages');
        $this->set('page_title', 'Pages');
     
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('page', 'index');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        if (!$this->Page->exists($id)) {
            throw new NotFoundException(__('Invalid page'));
        }
        $options = array('conditions' => array('Page.' . $this->Page->primaryKey => $id));
        $this->set('page', $this->Page->find('first', $options));
            $this->set('title_for_layout', 'View Page');
        $this->set('page_name', 'View Page');
        $this->set('parent_page_name', 'List Page');
        $this->set('page_title', 'View Page');
    }

    /**
     * add method
     *
     * @return void
     */
    public function add($copyData = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('page', 'add');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
       
        $this->loadModel('Category');
        $this->loadModel('Widget');
       
        if ($this->request->is('post')) {

            $this->Page->create();

            $this->request->data['Page']['widgets'] = serialize($this->request->data['Page']['widgets']);
           
            if ($this->Page->save($this->request->data)) {
                //saving file location
                if (isset($this->request->data['Page']['banner_image_file']) &&
                        !empty($this->request->data['Page']['banner_image_file'])) {
                    $this->loadModel('Media');
                    $this->Media->create();
                    $data['Media']['content'] = $this->request->data['Page']['banner_image_file'];
                    $data['Media']['media_id'] = $this->Page->id;
                    $data['Media']['number'] = 1;
                    $mime_type = explode('.', $this->data['Page']['banner_image_file']);
                    $data['Media']['type'] = $mime_type[count($mime_type) - 1];
                    $data['Media']['used_for'] = 'Page';
                    $this->Media->save($data);
                }
                $this->Session->setFlash(__('The page has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }

     
        $subcates = array();
        $parentcates = array();
       
    
        $this->set('parentcates', $parentcates);
        $this->set('subcates', $subcates);
          $this->set('title_for_layout', 'Add New Page');
        $this->set('page_name', 'Add New Page');
        $this->set('parent_page_name', 'List Pages');
        $this->set('page_title', 'Add New Page');
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit( $id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('page', 'edit');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
    
        $this->loadModel('Category');
        $this->loadModel('Widget');
      
        if (!$this->Page->exists($id)) {
            throw new NotFoundException(__('Invalid page'));
        }
        if ($this->request->is(array('post', 'put'))) {

            //widget to serialize
            $this->request->data['Page']['widgets'] = serialize($this->request->data['Page']['widgets']);
           

            if ($this->Page->save($this->request->data)) {
                if (isset($this->request->data['Page']['banner_image_file']) && !empty($this->request->data['Page']['banner_image_file'])) {
                    $file_content = ($this->data['Page']['banner_image_file']);
                    $this->loadModel('Media');
                    $this->Media->create();
                    $data['Media']['content'] = $file_content;
                    $data['Media']['media_id'] = $this->Page->id;
                    $media_detail = $this->Media->find('first', array('conditions' => array(
                            'used_for' => 'Page',
                            'media_id' => $this->Page->id)));
                    if (isset($media_detail) && !empty($media_detail)) {
                        $data['Media']['id'] = $media_detail['Media']['id'];
                    }
                    $data['Media']['number'] = 1;
                    
                    $mime_type = explode('.', $this->data['Page']['banner_image_file']);
                    $data['Media']['type'] = $mime_type[count($mime_type) - 1];
                    $data['Media']['used_for'] = 'Page';
                    $this->Media->save($data);
                }
              
                $this->Session->setFlash(__('The page has been updated.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index/"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $options = array('conditions' => array('Page.' . $this->Page->primaryKey => $id));
        $pageData = $this->Page->find('first', $options);
        $this->request->data = $pageData;
       
   

        //widget
        $widgets = $this->Widget->find('all', array('conditions' => array( 'Widget.position !=' => "", 'Widget.status' => INFI_ACTIVE_STATUS)));

        $dropdownwidget = array();
        $selectedwidget = array();
        foreach ($widgets as $widget) {
            $dropdownwidget[$widget['Widget']['identifier']] = $widget['Widget']['name'];
        }

        $editwidgets = unserialize($pageData['Page']['widgets']);

        //edit widget
      
     
        //parent Category
        $parentcate = $this->Category->find('all', array(
            'conditions' => array('Category.parent' => 0),
            'order' => array('Category.label ASC')
        ));
        $parentcates = array();
        foreach ($parentcate as $parentcate) {
            $parentcates[$parentcate['Category']['id']] = $parentcate['Category']['label'];
        }
        //get sub parent
        $subparent = $this->Category->find('all', array(
            'conditions' => array('Category.parent' => $pageData['Page']['main_category']),
            'order' => array('Category.label ASC')
        ));
        $subparents = array();
        foreach ($subparent as $subparent) {
            $subparents[$subparent['Category']['id']] = $subparent['Category']['label'];
        }

        //get sub category
        $subcate = $this->Category->find('all', array(
            'conditions' => array('Category.parent' => $pageData['Page']['category']),
            'order' => array('Category.label ASC')
        ));
        $subcates = array();
        foreach ($subcate as $subcate) {
            $subcates[$subcate['Category']['id']] = $subcate['Category']['label'];
        }


        //other category
        //get sub parent
        $other_subparents = array();
        if (!empty($pageData['Page']['other_main_category'])) {
            $subparent = $this->Category->find('all', array(
                'conditions' => array('Category.parent' => $pageData['Page']['other_main_category']),
                'order' => array('Category.label ASC')
            ));
            foreach ($subparent as $subparent) {
                $other_subparents[$subparent['Category']['id']] = $subparent['Category']['label'];
            }
        }



        //get sub category
        $other_subcates = array();
        if (!empty($pageData['Page']['other_category'])) {
            $subcate = $this->Category->find('all', array(
                'conditions' => array('Category.parent' => $pageData['Page']['other_category']),
                'order' => array('Category.label ASC')
            ));

            foreach ($subcate as $subcate) {
                $other_subcates[$subcate['Category']['id']] = $subcate['Category']['label'];
            }
        }



      
        


     
        $this->set('dropdownwidget', $dropdownwidget);
        $this->set('selectedwidget', $editwidgets);

        $this->set('parentcates', $parentcates);
        $this->set('subparents', $subparents);
        $this->set('subcates', $subcates);
         $this->set('title_for_layout', 'Edit Page');
       $this->set('page_name', 'Edit Page');
        $this->set('parent_page_name', 'List Pages');
        $this->set('page_title', 'Edit Page');
     
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete( $id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('page', 'delete');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Page->id = $id;
        if (!$this->Page->exists()) {
            throw new NotFoundException(__('Invalid page'));
        }
        //method allow for action 
        $this->request->allowMethod('post', 'delete');
        //delete media 
        $this->loadModel('Media');
        $media_detail = $this->Media->find('first', array('conditions' => array(
                'Media.media_id' => $id,
                'Media.used_for' => 'Page'
        )));
        if (isset($media_detail) && !empty($media_detail)) {
            $this->Media->id = $media_detail['Media']['id'];
            $this->Media->delete();
        }
        //end
        if ($this->Page->delete()) {
            $this->Session->setFlash(__('The page has been deleted.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => "index"));
        } else {
            $this->Session->setFlash(__('The page could not be deleted, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index"));
        }
    }

    /**
     * inactive method
     *
     * function to change status inactive
     */
    public function inactive( $id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('page', 'review');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Page->id = $id;
        if (!$this->Page->exists()) {
            throw new NotFoundException(__('Invalid page'));
        }
        $this->request->allowMethod('post');
        if ($this->Page->saveField('status', INFI_INACTIVE_STATUS)) {
            $this->Session->setFlash('Page has been inactived.', 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('Page activation failed, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index"));
        }
    }

    /**
     * active method
     *
     * function to change status to active
     */
    public function active($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('page', 'review');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Page->id = $id;
        if (!$this->Page->exists()) {
            throw new NotFoundException(__('Invalid page'));
        }
        $this->request->allowMethod('post');
        if ($this->Page->saveField('status', INFI_ACTIVE_STATUS)) {
            $this->Session->setFlash('Page has been actived.', 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('Page activation failed, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index"));
        }
    }

    /**
     * home page content method
     *
     */
    public function home() {
        $this->layout = 'front_layout';
    }

    //get image file method
    public function get_file($id = null) {
        if (isset($id)) {
            $this->loadModel('Media');
            $file_detail = $this->Page->find('first', array(
                'conditions' => array(
                    'Page.id' => $id
            )));
            $file = $this->Media->find('first', array('conditions' => array(
                    'Media.media_id' => $id,
                    'Media.used_for' => 'Page')));
            $this->autoRender = false;
            if (!empty($file)) {
                return $file;
            } else {
                //$this->redirect('index');
            }
        }
    }

}
