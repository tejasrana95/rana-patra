<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');

/**
 * Pages Controller
 *
 * @property Category $Category
 * @property PaginatorComponent $Paginator
 */
class CategoryController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
        'order' => array(
            'Category.parent' => 'asc')
    );

    // function call before any view rendor 
    // function used to add and override function feautres
    function beforeRender() {
        parent::beforeRender();
    }

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow 
    }

    /**
     * index method
     *
     * @return void
     */
    public function index($all = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('category', 'index');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Paginator->settings = $this->paginate;
        $this->Category->recursive = 0;
        

        $categories = $this->Paginator->paginate('Category');
        
        $this->set('categories', $categories);
          $this->set('title_for_layout', 'Categories');
        $this->set('page_name', 'Categories');
        $this->set('page_title', 'Categories');
     
    }

    /**
     * Parent category name method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function categorydata($id) {
        if (isset($id) && $id != 0) {
            $parentname = $this->Category->find('first', array(
                'conditions' => array('Category.id' => $id)));
            return $parentname['Category'];
        } else {
            return array();
        }
    }

    /**
     * add method
     *
     * @return void
     */
    public function add($copyData = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('category', 'add');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        if ($this->request->is('post')) {
            $this->Category->create();
            if ($this->request->data['Category']['parent'] == "" || $this->request->data['Category']['parent'] == '0') {
                $this->request->data['Category']['parent'] = $this->request->data['Category']['main_parent'];
            }
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
         $this->set('title_for_layout', 'Add Category');
        $this->set('page_name', 'Add Category');
        $this->set('parent_page_name', 'List Category');
        $this->set('page_title', 'Add Category');
      
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('category', 'edit');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission

        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid page'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Category->id = $id;

            if ($this->request->data['Category']['sub_parent'] != "" && $this->request->data['Category']['sub_parent'] != '0' && $this->request->data['Category']['parent'] != '0' && $this->request->data['Category']['parent'] != '') {
                $this->request->data['Category']['parent'] = $this->request->data['Category']['sub_parent'];
            }
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been updated.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
      
        //get parent data
        $parentcat = $this->Category->find('all', array(
            'conditions' => array('Category.parent' => "0"), 'order' => array('Category.label ASC')));
        $parents = array();
        $parents["0"] = "Top";
        foreach ($parentcat as $parent) {
            $parents[$parent['Category']['id']] = $parent['Category']['label'];
        }
        $subparent['parent'] = "";
        $mainparent['id'] = "";

        $this->set('parentCate', $parents);
        //get content for id
        $categorydata = $this->requestAction('category/categorydata/' . $id);

        if ($categorydata['parent'] != 0) {
            $subparent = $this->requestAction('category/categorydata/' . $categorydata['parent']);
        }
        if ($subparent['parent'] != 0) {
            $mainparent = $this->requestAction('category/categorydata/' . $subparent['parent']);
        } else {
            $mainparent = $subparent;
            $subparent['id'] = "0";
        }


        //get parent data
        if (isset($mainparent['id'])) {
            $condition = array('Category.parent' => $mainparent['id'], 'Category.id !=' => $id);
            $subparentcat = $this->Category->find('all', array(
                'conditions' => $condition, 'order' => array('Category.label ASC')));
            $subparents = array();
            $subparents["0"] = "Sub Parent";
            foreach ($subparentcat as $subparn) {
                $subparents[$subparn['Category']['id']] = $subparn['Category']['label'];
            }
        } else {
            $subparents[0] = 'Top';
            $mainparent['id'] = 0;
        }
       
        $this->set('allsubparent', $subparents);
        $this->set('mainparent', $mainparent);
        $this->set('subparent', $subparent);
        $this->set('category', $categorydata);
        $this->set('title_for_layout', 'Edit Category');
        $this->set('page_name', 'Edit Category');
        $this->set('parent_page_name', 'List Category');
        $this->set('page_title', 'Edit Category');
       
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('category', 'delete');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Category->id = $id;
        if (!$this->Category->exists()) {
            throw new NotFoundException(__('Invalid Category'));
        }
        //method allow for action 
        $this->request->allowMethod('post', 'delete');
        if ($this->Category->delete()) {
            $this->Session->setFlash(__('The Category has been deleted.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => "index"));
        } else {
            $this->Session->setFlash(__('The Category could not be deleted, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index"));
        }
    }

//parent category fetch
    public function parentcate($default = null) {
        
            $parentname = $this->Category->find('all', array(
                'conditions' => array('Category.parent' => '0'),
                'order' => array('Category.label ASC')));
            if (isset($parentname) && !empty($parentname)) {
                echo '<option label="" value="">Select Parent Category</option>';
                if ($default == "all") {
                    echo '<option label="" value="0">All</option>';
                } elseif ($default != null) {
                    echo '<option label="" value="0">Top</option>';
                }
                foreach ($parentname as $parentname) {
                    echo '<option label="" value="' . $parentname['Category']['id'] . '"';
                    echo ' >' . $parentname['Category']['label'] . '</option>';
                }
            } else {
                if ($default == "all") {
                    echo '<option label="" value="">Select Category</option>';
                    echo '<option label="" value="0">All</option>';
                } elseif ($default != null) {
                     echo '<option label="" value="">Select Category</option>';
                    echo '<option label="" value="0">Top</option>';
                } else {
                    echo '<option label="" value="">No data available </option>';
                }
            }
        die();
    }

    //sub category fetch
    public function subcate($parentid = null, $currentId = null, $forcategory = null) {
        if ($parentid != null) {
            if ($parentid == 0) {
                echo '<option label="" value="0">Top</option>';
            } else {
                $condition = array('Category.parent' => $parentid);
                if ($currentId != null) {
                    $condition['Category.id !='] = $currentId;
                }
                $parentname = $this->Category->find('all', array(
                    'conditions' => $condition,
                    'order' => array('Category.label ASC')));
                if (isset($parentname) && !empty($parentname)) {
                    echo '<option label="" value="">Select Sub Category</option>';
                    if ($forcategory != null) {
                        echo '<option label="" value="">Sub Parent</option>';
                    }
                    foreach ($parentname as $parentname) {

                        echo '<option label="" value="' . $parentname['Category']['id'] . '"';
                        echo ' >' . $parentname['Category']['label'] . '</option>';
                    }
                } else {
                    if ($forcategory != null) {
                        echo '<option label="" value="">Sub Parent</option>';
                    } else {
                        echo '<option label="" value="">No data available </option>';
                    }
                }
            }
        } else {
            echo '<option label="" value="">No data available </option>';
        }
        die();
    }

    public function getcategorydata($categoryid) {

        if (isset($categoryid) && !empty($categoryid)) {
            if (is_numeric($categoryid)) {
                $category_info = $this->Category->find('first', array(
                    'conditions' => array('Category.id' => $categoryid),
                ));
            } else {
                $category_info = $this->Category->find('first', array(
                    'conditions' => array('Category.identifier' => $categoryid),
                ));
            }
            return $category_info;
        }
    }

    public function getchild($categoryid) {

        if (isset($categoryid) && !empty($categoryid)) {
            if (is_numeric($categoryid)) {
                $category_info = $this->Category->find('first', array(
                    'conditions' => array('Category.parent' => $categoryid),
                ));
            } 
            return $category_info;
        }
    }

}
