<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');
App::import('Vendor', 'ClassUpload', array('file' => 'class.upload.php'));

/**
 * Articles Controller
 *
 * @property Article $Article
 * @property PaginatorComponent $Paginator
 */
class ArticlesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Security');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
    );

    // function call before any view rendor 
    // function used to add and override function feautres
    function beforeRender() {
        parent::beforeRender();
    }

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Security->validatePost = false;
        // Allow 
    }

    /**
     * index method
     *
     * @return void
     */
    public function index($all = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('article', 'index');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission

        $this->Paginator->settings = $this->paginate;
        $this->Article->recursive = 0;


        $articles = $this->Paginator->paginate('Article');


        $this->set('articles', $articles);
        $this->set('title_for_layout', 'Articles');
        $this->set('article_name', 'Articles');
        $this->set('article_title', 'Articles');
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('article', 'index');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        if (!$this->Article->exists($id)) {
            throw new NotFoundException(__('Invalid article'));
        }
        $options = array('conditions' => array('Article.' . $this->Article->primaryKey => $id));
        $this->set('article', $this->Article->find('first', $options));
        $this->set('title_for_layout', 'View Article');
        $this->set('article_name', 'View Article');
        $this->set('parent_article_name', 'List Article');
        $this->set('article_title', 'View Article');
    }

    /**
     * add method
     *
     * @return void
     */
    public function add($copyData = null) {
        $userID = $_SESSION['Auth']['User']['id'];
        $isAdmin = 1;
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('article', 'add');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission

        $this->loadModel('ArticleCategory');
        $this->loadModel('Widget');

        if ($this->request->is('post')) {

            $this->Article->create();

            $this->request->data['Article']['userID'] = $userID;
            $this->request->data['Article']['isAdmin'] = $isAdmin;
            if ($this->Article->save($this->request->data)) {
                //saving file location
                $this->Session->setFlash(__('The article has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }


        $subcates = array();
        $parentcates = array();


        $this->set('parentcates', $parentcates);
        $this->set('subcates', $subcates);
        $this->set('title_for_layout', 'Add New Article');
        $this->set('article_name', 'Add New Article');
        $this->set('parent_article_name', 'List Articles');
        $this->set('article_title', 'Add New Article');
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('article', 'edit');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission

        $this->loadModel('ArticleCategory');
        $this->loadModel('Widget');

        if (!$this->Article->exists($id)) {
            throw new NotFoundException(__('Invalid article'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Article->save($this->request->data)) {

                $this->Session->setFlash(__('The article has been updated.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index/"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $options = array('conditions' => array('Article.' . $this->Article->primaryKey => $id));
        $articleData = $this->Article->find('first', $options);
        $this->request->data = $articleData;



        //edit widget
        //parent ArticleCategory
        $parentcate = $this->ArticleCategory->find('all', array(
            'conditions' => array('ArticleCategory.parent' => 0),
            'order' => array('ArticleCategory.label ASC')
        ));
        $parentcates = array();
        foreach ($parentcate as $parentcate) {
            $parentcates[$parentcate['ArticleCategory']['id']] = $parentcate['ArticleCategory']['label'];
        }
        //get sub parent
        $subparent = $this->ArticleCategory->find('all', array(
            'conditions' => array('ArticleCategory.parent' => $articleData['Article']['main_category']),
            'order' => array('ArticleCategory.label ASC')
        ));
        $subparents = array();
        foreach ($subparent as $subparent) {
            $subparents[$subparent['ArticleCategory']['id']] = $subparent['ArticleCategory']['label'];
        }

        //get sub articlecategory
        $subcate = $this->ArticleCategory->find('all', array(
            'conditions' => array('ArticleCategory.parent' => $articleData['Article']['category']),
            'order' => array('ArticleCategory.label ASC')
        ));
        $subcates = array();
        foreach ($subcate as $subcate) {
            $subcates[$subcate['ArticleCategory']['id']] = $subcate['ArticleCategory']['label'];
        }


        //other articlecategory
        //get sub parent
        $other_subparents = array();
        if (!empty($articleData['Article']['other_main_articlecategory'])) {
            $subparent = $this->ArticleCategory->find('all', array(
                'conditions' => array('ArticleCategory.parent' => $articleData['Article']['other_main_articlecategory']),
                'order' => array('ArticleCategory.label ASC')
            ));
            foreach ($subparent as $subparent) {
                $other_subparents[$subparent['ArticleCategory']['id']] = $subparent['ArticleCategory']['label'];
            }
        }



        //get sub articlecategory
        $other_subcates = array();
        if (!empty($articleData['Article']['other_articlecategory'])) {
            $subcate = $this->ArticleCategory->find('all', array(
                'conditions' => array('ArticleCategory.parent' => $articleData['Article']['other_articlecategory']),
                'order' => array('ArticleCategory.label ASC')
            ));

            foreach ($subcate as $subcate) {
                $other_subcates[$subcate['ArticleCategory']['id']] = $subcate['ArticleCategory']['label'];
            }
        }


        $this->set('parentcates', $parentcates);
        $this->set('subparents', $subparents);
        $this->set('subcates', $subcates);
        $this->set('title_for_layout', 'Edit Article');
        $this->set('article_name', 'Edit Article');
        $this->set('parent_article_name', 'List Articles');
        $this->set('article_title', 'Edit Article');
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('article', 'delete');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Article->id = $id;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }
        //method allow for action 
        $this->request->allowMethod('post', 'delete');
        //delete media 
        $this->loadModel('Media');
        $media_detail = $this->Media->find('first', array('conditions' => array(
                'Media.media_id' => $id,
                'Media.used_for' => 'Article'
        )));
        if (isset($media_detail) && !empty($media_detail)) {
            $this->Media->id = $media_detail['Media']['id'];
            $this->Media->delete();
        }
        //end
        if ($this->Article->delete()) {
            $this->Session->setFlash(__('The article has been deleted.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => "index"));
        } else {
            $this->Session->setFlash(__('The article could not be deleted, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index"));
        }
    }

    /**
     * inactive method
     *
     * function to change status inactive
     */
    public function inactive($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('article', 'review');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Article->id = $id;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }
        $this->request->allowMethod('post');
        if ($this->Article->saveField('status', INFI_INACTIVE_STATUS)) {
            $this->Session->setFlash('Article has been inactived.', 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('Article activation failed, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index"));
        }
    }

    /**
     * active method
     *
     * function to change status to active
     */
    public function active($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission('article', 'review');
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->Article->id = $id;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }
        $this->request->allowMethod('post');
        if ($this->Article->saveField('status', INFI_ACTIVE_STATUS)) {
            $this->Session->setFlash('Article has been actived.', 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->Session->setFlash(__('Article activation failed, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index"));
        }
    }

    /**
     * home article content method
     *
     */
    public function home() {
        $this->layout = 'front_layout';
    }

    //get image file method
    public function get_file($id = null) {
        if (isset($id)) {
            $this->loadModel('Media');
            $file_detail = $this->Article->find('first', array(
                'conditions' => array(
                    'Article.id' => $id
            )));
            $file = $this->Media->find('first', array('conditions' => array(
                    'Media.media_id' => $id,
                    'Media.used_for' => 'Article')));
            $this->autoRender = false;
            if (!empty($file)) {
                return $file;
            } else {
                //$this->redirect('index');
            }
        }
    }

}
