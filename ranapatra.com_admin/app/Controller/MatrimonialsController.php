<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');
App::import('Controller', 'Common');

/**
 * Customers Controller
 *
 * @property Customer $Customer
 * @property PaginatorComponent $Paginator
 */
class MatrimonialsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Qimage', 'RequestHandler');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
        'order' => array(
            'Matrimonial.id' => 'desc',
            'MatrimonialAttachment.id' => 'desc',
        ),
    );

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow customers to register and logout.
        $this->Security->csrfCheck = false;
        $this->Security->validatePost = false;
        $this->Security->validatePost = false;
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard',
            ));
        }
        //end check permission
        $this->loadModel('UserRole');
        $this->Paginator->settings = $this->paginate;
        $this->Matrimonial->recursive = 0;
        $this->set('Matrimonials', $this->Paginator->paginate());
        $userID = $_SESSION['Auth']['User']['id'];
        $this->set("adminuserID", $userID);
        $this->set('title_for_layout', 'Matrimonial');

        $this->set('page_name', 'Matrimonial');
        $this->set('parent_page_name', 'Matrimonial');
        $this->set('page_title', 'List Matrimonial');
    }

    /**
     * view method
     *
     * @param  string              $id
     * @throws NotFoundException
     * @return void
     */
    public function view($id = null) {
        //check permission
        $this->loadModel('UserRole');
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard',
            ));
        }
        $this->loadModel('Customer');
        $this->loadModel('CustomerProfile');
        $this->loadModel('Option');
        //end check permission


        if (!$this->Matrimonial->exists($id)) {
            throw new NotFoundException(__('Invalid Matrimonial'));
        }

       $userID = $_SESSION['Auth']['User']['id'];

        $options = array('conditions' => array($this->Matrimonial->primaryKey => $id));
        $this->request->data = $this->Matrimonial->find('first', $options);



        $options = array('conditions' => array('Customer.status' => 1), 'fields' => array('Customer.id', 'Customer.username'), 'order' => array('Customer.username ASC'));
        $Customers = $this->Customer->find('list', $options);

        $options = array('conditions' => array('meta_key' => "payment_mail"), 'fields' => array('meta_value'));
        $payment_mail = $this->Option->find('first', $options);
        $this->set('payment_mail', $payment_mail['Option']['meta_value']);
        $this->set('customers', $Customers);
        $userID = $_SESSION['Auth']['User']['id'];
        $this->set("adminuserID", $userID);



        $this->set("adminuserID", $userID);

        $this->set('title_for_layout', 'Matrimonial');
        $this->set('page_name', 'View Matrimonial');
        $this->set('parent_page_name', 'List Matrimonial');
        $this->set('page_title', 'View Matrimonial');
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard',
            ));
        }
        //end check permission
        $this->loadModel('Customer');
        $this->loadModel('Option');
        if ($this->request->is('post')) {
            $userID = $_SESSION['Auth']['User']['id'];
            $this->request->data['Matrimonial']['process_by'] = 1;
            $this->request->data['Matrimonial']['system_id'] = $userID;

            $this->Matrimonial->create();
            if ($this->Matrimonial->save($this->request->data)) {
                $this->Session->setFlash(__('The Matrimonial has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }

        $options = array('conditions' => array('Customer.status' => 1), 'fields' => array('Customer.id', 'Customer.username'), 'order' => array('Customer.username ASC'));
        $Customers = $this->Customer->find('list', $options);

        $options = array('conditions' => array('meta_key' => "payment_mail"), 'fields' => array('meta_value'));
        $payment_mail = $this->Option->find('first', $options);

        $this->set('payment_mail', $payment_mail['Option']['meta_value']);
        $this->set('customers', $Customers);
        $userID = $_SESSION['Auth']['User']['id'];
        $this->set("adminuserID", $userID);

        $this->set('title_for_layout', 'Matrimonial');
        $this->set('page_name', 'Add Matrimonial');
        $this->set('parent_page_name', 'List Matrimonial');
        $this->set('page_title', 'Add Matrimonial');
    }

    /**
     * edit method
     *
     * @param  string              $id
     * @throws NotFoundException
     * @return void
     */
    public function edit($id = null) {
        //check permission
        $this->loadModel('UserRole');
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard',
            ));
        }
        $this->loadModel('Customer');
        $this->loadModel('Option');
        //end check permission


        if (!$this->Matrimonial->exists($id)) {
            throw new NotFoundException(__('Invalid Matrimonial'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $userID = $_SESSION['Auth']['User']['id'];


            if ($this->Matrimonial->save($this->request->data)) {
                $this->Session->setFlash(__('The Matrimonial has been updated.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $userID = $_SESSION['Auth']['User']['id'];

        $options = array('conditions' => array($this->Matrimonial->primaryKey => $id));
        $this->request->data = $this->Matrimonial->find('first', $options);



        $options = array('conditions' => array('Customer.status' => 1), 'fields' => array('Customer.id', 'Customer.username'), 'order' => array('Customer.username ASC'));
        $Customers = $this->Customer->find('list', $options);

        $options = array('conditions' => array('meta_key' => "payment_mail"), 'fields' => array('meta_value'));
        $payment_mail = $this->Option->find('first', $options);

        $this->set('payment_mail', $payment_mail['Option']['meta_value']);
        $this->set('customers', $Customers);
        $userID = $_SESSION['Auth']['User']['id'];
        $this->set("adminuserID", $userID);

        $this->set('title_for_layout', 'Matrimonial');
        $this->set('page_name', 'Edit Matrimonial');
        $this->set('parent_page_name', 'List Matrimonial');
        $this->set('page_title', 'Edit Matrimonial');
    }

    public function addKundali($id = null) {
        //check permission
        $this->loadModel('UserRole');
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard',
            ));
        }
        $this->loadModel('Customer');
        $this->loadModel('Option');
        $this->loadModel('MatrimonialAttachment');
        //end check permission
       
        if ($this->request->is(array('post', 'put'))) {
            $userID = $_SESSION['Auth']['User']['id'];
            $this->Matrimonial->create();


            $this->request->data['MatrimonialAttachment']['file_path'] = '';

            $lastid = $this->MatrimonialAttachment->save($this->request->data);

            $matKundali = $this->Option->find('first', array('conditions' => array('Option.meta_key' => 'matrimonialKundali')));
            $matKundali = rtrim($matKundali['Option']['meta_value'], '/') . '/';


            if ($lastid) {
                $ext = strtolower(pathinfo($this->request->data['MatrimonialAttachment']['file']['name'], PATHINFO_EXTENSION));

                $imagePath = $this->request->data['MatrimonialAttachment']['file']['name'] . '-' . uniqid() . '.' . $ext;
                $path = $matKundali .'/'. $imagePath;
                move_uploaded_file($this->request->data['MatrimonialAttachment']['file']['tmp_name'], $path);
                $this->request->data['MatrimonialAttachment']['file_path'] = $imagePath;

                $this->request->data['MatrimonialAttachment']['id'] = $lastid['MatrimonialAttachment']['id'];
                if ($this->MatrimonialAttachment->save($this->request->data)) {
                    $this->Session->setFlash(__('The kundali has been saved.'), 'default', array('class' => 'alert alert-success'));
                    $this->redirect(array('action' => "addKundali/" . $this->request->data['MatrimonialAttachment']['matrimonial_id']));
                } else {
                    $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
                }
            }
        }
        $userID = $_SESSION['Auth']['User']['id'];

        $options = array('conditions' => array($this->Matrimonial->primaryKey => $id));
        $this->request->data['MatrimonialData'] = $this->Matrimonial->find('first', $options);

        $options = array('conditions' => array('matrimonial_id' => $this->request->data['MatrimonialData']['Matrimonial']['id'], 'file_type' => 'kundali'));
        $this->request->data['MatrimonialAttachment'] = $this->MatrimonialAttachment->find('all', $options);



        $this->layout = 'mini';

        $userID = $_SESSION['Auth']['User']['id'];
        $this->set("adminuserID", $userID);
    }

    public function addImage($id = null) {
        //check permission
        $this->loadModel('UserRole');
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard',
            ));
        }
        $this->loadModel('Customer');
        $this->loadModel('Option');
        $this->loadModel('MatrimonialAttachment');
        //end check permission
       
        $matImage = $this->Option->find('first', array('conditions' => array('Option.meta_key' => 'matrimonialImages')));
        $matImage = rtrim($matImage['Option']['meta_value'], '/') . '/';

        if ($this->request->is(array('post', 'put'))) {
            $userID = $_SESSION['Auth']['User']['id'];
            $this->Matrimonial->create();


            $this->request->data['MatrimonialAttachment']['file_path'] = '';

            $lastid = $this->MatrimonialAttachment->save($this->request->data);



            if ($lastid) {
                $ext = strtolower(pathinfo($this->request->data['MatrimonialAttachment']['file']['name'], PATHINFO_EXTENSION));

                $imagePath = $this->request->data['MatrimonialAttachment']['file']['name'] . '-' . uniqid() . '.' . $ext;
                $path = $matImage  .'/'. $imagePath;
                move_uploaded_file($this->request->data['MatrimonialAttachment']['file']['tmp_name'], $path);
                $this->request->data['MatrimonialAttachment']['file_path'] = $imagePath;

                $this->request->data['MatrimonialAttachment']['id'] = $lastid['MatrimonialAttachment']['id'];
                if ($this->MatrimonialAttachment->save($this->request->data)) {
                    $this->Session->setFlash(__('The Image has been saved.'), 'default', array('class' => 'alert alert-success'));
                    $this->redirect(array('action' => "addImage/" . $this->request->data['MatrimonialAttachment']['matrimonial_id']));
                } else {
                    $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
                }
            }
        }
        $userID = $_SESSION['Auth']['User']['id'];

        $options = array('conditions' => array($this->Matrimonial->primaryKey => $id));
        $this->request->data['MatrimonialData'] = $this->Matrimonial->find('first', $options);

        $options = array('conditions' => array('matrimonial_id' => $this->request->data['MatrimonialData']['Matrimonial']['id'], 'file_type' => 'image'));
        $this->request->data['MatrimonialAttachment'] = $this->MatrimonialAttachment->find('all', $options);



        $this->layout = 'mini';

        $userID = $_SESSION['Auth']['User']['id'];
        $this->set("adminuserID", $userID);
    }

    public function deleteMatrimonialAttachment($id = null, $mid = null, $action = 'addImage') {
        //check permission
        $this->loadModel('UserRole');
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard',
            ));
        }
        $this->loadModel('Customer');
        $this->loadModel('Option');
        $this->loadModel('MatrimonialAttachment');
        //end check permission
        $matImage = $this->Option->find('first', array('conditions' => array('Option.meta_key' => 'matrimonialImages')));
        $matImage = rtrim($matImage['Option']['meta_value'], '/') . '/';

        $matKundali = $this->Option->find('first', array('conditions' => array('Option.meta_key' => 'matrimonialKundali')));
        $matKundali = rtrim($matKundali['Option']['meta_value'], '/') . '/';

        $getFilePath = $this->MatrimonialAttachment->find('first', array('conditions' => array('MatrimonialAttachment.id' => $id)));

        $this->request->allowMethod('post', 'delete');

        if (file_exists($matImage  .'/'. $getFilePath['MatrimonialAttachment']['file_path'])) {
            @unlink($matImage  .'/'. $getFilePath['MatrimonialAttachment']['file_path']);
        }elseif (file_exists($matKundali  .'/'. $getFilePath['MatrimonialAttachment']['file_path'])) {
            @unlink($matKundali  .'/'. $getFilePath['MatrimonialAttachment']['file_path']);
        }
        $this->MatrimonialAttachment->id = $id;
        if ($this->MatrimonialAttachment->delete()) {
            $this->Session->setFlash(__('The matrimonial attachment deleted.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => $action . "/" . $mid));
        } else {
            $this->Session->setFlash(__('The page could not be deleted, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => $action . "/" . $mid));
        }
    }

}
