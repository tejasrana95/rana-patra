<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');

class FileManagerController extends AppController {

    public $name = 'FileManager';
    public $uses = array();
    public $components = array('RequestHandler');
    public $helpers = array('Js', 'Html');

    public function access($attr, $path, $data, $volume) {
        return strpos(basename($path), '.') === 1       // if file/folder begins with '.' (dot)
                ? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
                : null;                                    // else elFinder decide it itself
    }

    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Security->csrfCheck = false;
        //$this->Security->validatePost = false;
        $this->Security->validatePost = false;
    }

    public function index($userID = null) {

        if ($userID == null) {
            //check permission
            $UserRolesController = new UserRolesController;
            $checkPermission = $UserRolesController->checkPermission();
            if (!$checkPermission) {
                $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
                $this->redirect(array(
                    'controller' => 'users', 'action' => 'dashboard'
                ));
            }
            //end check permission
        } else {
            $userID = $_SESSION['Auth']['User']['id'];
        }

        $this->loadModel('Option');
        $title_for_layout = 'Media Library';
        $this->set(compact('title_for_layout'));

        $publicDir = $this->Option->find('first', array(
            'conditions' => array('Option.meta_key' => "publicDir")));
        $site_url = $this->Option->find('first', array(
            'conditions' => array('Option.meta_key' => "site_url")));

        $this->set('userID', $userID);
        $this->set('title_for_layout', 'File Manager');
        $this->set('page_name', 'File Manager');
        $this->set('page_title', 'File Manager');
        $this->set('site_url', $site_url);
    }

    public function corefilemanager($userID = null) {
        $this->loadModel('Option');
        $this->layout = '';
        $site_url = $this->Option->find('first', array(
            'conditions' => array('Option.meta_key' => "site_url")));
        if ($this->RequestHandler->isAjax() || $this->RequestHandler->isPost()) {

            // run elFinder
            $connector = new elFinderConnector(new elFinder($this->opts($userID)));
            $connector->run();
        }
        $this->set('site_url', $site_url);
    }

    public function imageDispatcher() {

        if (isset($_GET['imageURL'])) {
            $imageURL = urldecode($_GET['imageURL']);
        } else {
            die();
        }
        if (isset($_GET['pix'])) {
            $pix = $_GET['pix'];
        } else {
            $pix = 32;
        }

        $extension = pathinfo($imageURL, PATHINFO_EXTENSION);
        if ($extension == 'png' || $extension == 'PNG') {
            header('Content-type: image/png');
        } elseif ($extension == 'gif' || $extension == 'GIF') {
            header('Content-type: image/gif');
        } else {
            header('Content-type: image/jpeg');
        }
        if ($imageURL) {


            $image_path = $imageURL;

            $image_size = getimagesize($image_path);
            $image_width = $image_size[0];
            $image_height = $image_size[1];

            $new_size = ($image_width + $image_height) / ($image_width * ($image_height / $pix));
            $new_width = $image_width * $new_size;
            $new_height = $image_height * $new_size;

            $new_image = imagecreatetruecolor($new_width, $new_height);
            if ($extension == 'png' || $extension == 'PNG') {
                $old_image = imagecreatefrompng($image_path);
            } elseif ($extension == 'gif' || $extension == 'GIF') {
                $old_image = imagecreatefromgif($image_path);
            } else {
                $old_image = imagecreatefromjpeg($image_path);
            }

            imagecopyresized($new_image, $old_image, 0, 0, 0, 0, $new_width, $new_height, $image_width, $image_height);
            if ($extension == 'png' || $extension == 'PNG') {
                imagepng($new_image);
            } elseif ($extension == 'gif' || $extension == 'GIF') {
                imagegif($new_image);
            } else {
                imagejpeg($new_image);
            }
        }
        die();
    }

    public function editorwithfilemanager($mini = null, $userID = null) {
        $this->loadModel('Option');
        $this->layout = "";
        $this->set('mini', $mini);
        $this->set('userID', $userID);
    }

}
