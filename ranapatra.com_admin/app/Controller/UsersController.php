<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'UserRoles');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $paginate = array(
        'limit' => RECORD_PER_PAGE,
        'order' => array(
            'User.created' => 'desc'
        )
    );

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('add', 'logout');
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->loadModel('UserRole');
        $this->Paginator->settings = $this->paginate;
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
        $this->set('title_for_layout', 'Users');
        $this->set('page_name', 'User');
        $this->set('page_title', 'User');
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->loadModel('UserRole');
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
        $this->set('title_for_layout', 'Users');
        $this->set('page_name', 'User');
        $this->set('page_title', 'User');
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->loadModel('UserRole');
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $userRoles = $this->UserRole->find('all', array('conditions' => array('UserRole.status' => INFI_ACTIVE_STATUS)));
        $DropuserRole = array();
        foreach ($userRoles as $userRole) {
            $DropuserRole[$userRole['UserRole']['id']] = $userRole['UserRole']['name'];
        }
        $this->set('DropuserRole', $DropuserRole);
        $this->set('title_for_layout', 'Add User');
        $this->set('page_name', 'Add User');
        $this->set('page_title', 'Add User');
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->loadModel('UserRole');
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been updated.'), 'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => "index"));
            } else {
                $this->Session->setFlash(__('Following error occurs, please try again.'), 'default', array('class' => 'alert alert-danger'));
            }
        }
        $userRoles = $this->UserRole->find('all', array('conditions' => array('UserRole.status' => INFI_ACTIVE_STATUS)));
        $DropuserRole = array();
        foreach ($userRoles as $userRole) {
            $DropuserRole[$userRole['UserRole']['id']] = $userRole['UserRole']['name'];
        }

        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->request->data = $this->User->find('first', $options);
        $this->request->data['User']['password'] = "";
        $this->set('DropuserRole', $DropuserRole);
        $this->set('title_for_layout', 'Users');
        $this->set('page_name', 'User');
        $this->set('page_title', 'User');
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        //check permission
        $UserRolesController = new UserRolesController;
        $checkPermission = $UserRolesController->checkPermission();
        if (!$checkPermission) {
            $this->Session->setFlash(__("You don't have sufficient permissions."), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array(
                'controller' => 'users', 'action' => 'dashboard'
            ));
        }
        //end check permission
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->User->delete()) {
            $this->Session->setFlash(__('The user has been deleted.'), 'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => "index"));
        } else {
            $this->Session->setFlash(__('The user could not be deleted, please try again.'), 'default', array('class' => 'alert alert-danger'));
            $this->redirect(array('action' => "index"));
        }
    }

    public function dashboard() {
        
    }

    /**
     * login method
     *
     * 
     */
    public function login() {
        $this->layout = 'admin_login';
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                return $this->redirect(array('controller' => "users", "action" => "dashboard"));
            } else {
                $this->Session->setFlash(__('Invalid username or password, try again'), 'default', array('class' => 'alert alert-danger'));
            }
        }
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

    public function profile() {
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->User->set($this->request->data);
            if ($this->User->validates()) {
                $this->User->id = $this->Auth->user('id');
                if ($this->User->saveField('password', $this->data['User']['confirm_password'])) {
                    $this->Session->setFlash(__('Password changed successfully.'), 'default', array('class' => "alert alert-success"));
                } else {
                    $this->Session->setFlash(__('password could not reset,following error occurred.'), 'default', array('class' => 'alert alert-danger'));
                }
            } else {
                $this->Session->setFlash(__('password could not reset,following error occurred.'), 'default', array('class' => 'alert alert-danger'));
            }
        } else {
            
        }
        $this->request->data['User']['password'] = "";
        $this->request->data['User']['confirm_password'] = "";
        $this->set('title_for_layout', 'Users');
        $this->set('page_name', 'User');
        $this->set('page_title', 'User');
    }

    public function getUserInfo($userID = null) {
        $userinfo = $this->User->find('first', array('conditions' => array('User.id' => $userID)));
        return $userinfo;
    }

}
