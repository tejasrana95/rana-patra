
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li><?php echo $this->Html->link(__($parent_page_name), array('action' => 'index')); ?></li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">

                <?php echo $this->Form->create('ArticleCategory', array('type' => 'file')); ?>
                <div class="box-body">

                    <div class="row">

                        <div class="input-field col s12 m6 l6">
                            <?php
                            echo $this->Form->input('main_parent', array('options' => array(),
                                'empty' => "Select articlecategory",
                                'div' => array('class' => "form-group"),
                                'class' => "form-control",
                                "required" => "required",
                                "id" => 'articlecategory',
                                "label" => false,
                                'onchange' => 'getmiddlecate()',
                            ));
                            ?>
                        </div>
                        <div class="input-field col s12 m6 l6">
                            <?php
                            echo $this->Form->input('parent', array('options' => array(),
                                'empty' => "Select parent articlecategory first",
                                'div' => array('class' => "form-group"),
                                'class' => "form-control",
                                "id" => 'SubArticleCategory',
                                'label' => false
                            ));
                            ?>
                        </div>
                        <div class="row">

                            <div class="input-field col s12 m6 l6">
                                <?php
                                echo $this->Form->input('label', array('div' => array('class' => "form-group"), 'label' => 'Name', 'class' => "form-control", "required" => "required"));
                                ?></div>
                            <div class="input-field col s12 m6 l6">
                                <?php
                                echo $this->Form->input('sort_order', array('type' => "number", "label" => "Sort Order", 'div' => array('class' => "form-group"), 'class' => "form-control", "value" => ""));
                                ?>
                            </div>
                        </div>
                        <div class="row">

                            <div class="input-field col s12 m10 l10">

                                <?php
                                echo $this->Form->input('img', array('type' => "text", "readonly" => 'readonly', "label" => "Banner", 'id' => 'bannerpath', 'div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                            <div class="input-field col s12 m2 l2">

                                <?php
                                echo $this->Form->button('Select Banner', array('type' => "button", 'id' => "selectflag", "onclick" => "openfilemanager('bannerpath')", 'class' => "waves-effect waves-light btn "));
                                ?>
                            </div>
                        </div>
                        <div class="row">

                            <div class="input-field col s12 m12 l12">
                                <div id="bannerpath_wrapper" style="display: none;">
                                    <img src="" id="bannerpath_img" style="max-width:300px;">
                                    <br/><a href="javascript:void()" onclick="removeimage('bannerpath')">Remove</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="input-field col s3">
                                <?php
                                echo $this->Form->input('fontaws_icon', array('type' => "text", "label" => "Font Awesome Icon", 'div' => array('class' => "form-group"), 'class' => "form-control", "value" => ""));
                                ?>
                            </div>

                            <div class="switch col s3">

                                Reserved : 
                                <label>
                                    No
                                    <?php
                                    echo $this->Form->input('reserved', array('div' => false, 'label' => false, 'type' => "checkbox", 'class' => ""));
                                    ?>
                                    <span class="lever"></span> Yes
                                </label>
                            </div>
                            <div class="switch col s3">

                                Show Siblings : 
                                <label>
                                    No
                                    <?php
                                    echo $this->Form->input('show_sibling', array('div' => false, 'label' => false, 'type' => "checkbox", 'class' => ""));
                                    ?>
                                    <span class="lever"></span> Yes
                                </label>
                            </div>
                            <div class="switch col s12 m3 l3">

                                Is Heading : 
                                <label>
                                    No
                                    <?php
                                    echo $this->Form->input('is_heading', array('div' => false, 'label' => false, 'type' => "checkbox", 'class' => ""));
                                    ?>
                                    <span class="lever"></span> Yes
                                </label>
                            </div>

                        </div>
                        <div class="row">

                            <div class="input-field col s12 m12 l12">
                                <?php
                                echo $this->Form->input('Save', array('label' => false, 'div' => array('class' => "form-group"), 'type' => "submit", 'class' => "btn btn-success btn-lg"));
                                ?>
                            </div>
                        </div><!-- /.box -->
                    </div><!-- /.box-body -->
                    <?php echo $this->Form->end(); ?>

                </div><!-- /.col (left) -->
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Actions</h3>
            </div>
            <div class="box-body dropdown">
                <ul >
                    <li><?php echo $this->Html->link(__('List ArticleCategory'), array('action' => 'index')); ?></li>
                </ul>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col (right) -->
</div><!-- /.row -->                    

<script>
    function getparentcate() {

        $.get('<?php echo Router::url('/', true) ?>articlecategory/parentcate/' + 'true', function (result) {
            $('#articlecategory').html(result);
            $('#articlecategory').material_select();
        });

    }

    getparentcate();

</script>
<script>
    // listen to events on the articlecategory dropdown
    function getmiddlecate() {
        // don't do anything if use selects "Select Cat"
        if ($("#articlecategory").val() !== "") {
            $.get('<?php echo Router::url('/', true) ?>articlecategory/subcate/' + $('#articlecategory').val() + '/articlecategory', function (result) {
                $('#SubArticleCategory').html(result);
                $('#SubArticleCategory').material_select();
            });
        }
    }
</script>