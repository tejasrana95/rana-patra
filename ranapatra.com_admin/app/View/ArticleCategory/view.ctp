<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pages
        </h1>
        <ol class="breadcrumb">
        		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><?php echo $this->Html->link(__('Pages'), array('controller'=>'pages','action'=>"index",'neoadmin'=>true));?></li>
            <li class="active">View pages</li>
        </ol>
    </section>
     <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-8">
          <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><?php echo __('View Page'); ?></h3>
            </div>
          
            <div class="box-body">
            	 <table class="table table-striped">
              <tr>
                  <td><?php echo __('Page Name'); ?></td>
                  <td>
                      <?php echo h($page['Page']['page_name']); ?>
                  </td>
              </tr>
              <tr>
              	<td><?php echo __('Page Title'); ?></td>
              	<td><?php echo h($page['Page']['page_title']); ?></td>
              </tr>
              <tr>
              	<td><?php echo __('Page Keywords'); ?></td>
              	<td><?php echo h($page['Page']['page_keywords']); ?></td>
              </tr>
              <tr>
              	<td><?php echo __('Content'); ?></td>
              	<td><?php echo ($page['Page']['content']); ?></td>
              </tr>
              <tr>
              	<td><?php echo __('Banner Image'); ?></td>
              	<td><?php
							    $img =  $this->requestAction(array (
							    	'controller'=>'pages',
							    	'action'=> 'get_file',
							    	$page['Page']['id']));
										$b64Src = "data:".$img['Media']['type'].";base64,".base64_encode($img['Media']['content']); 
									if(isset($img) && !empty($img['Media']['content'])){
										 echo '<img src="'.$b64Src.'" width="100" />';
									}
									?>
									</td>
              </tr>
			  <tr>
              	<td><?php echo __('Front Layout'); ?></td>
              	<td><?php echo h($page['Page']['layout']); ?></td>
              </tr>
              <tr>
                  <td><?php echo __('Status'); ?></td>
                  <td>
                      <?php 
                      if($page['Page']['status']==NEO_ACTIVE_STATUS){
											 		 echo $this->Form->postLink(
															$this->Html->tag('i','',
																		array('class'=>" fa fa-check")
															), 
															array(
																'action' => 'inactive',
																$page['Page']['id']
															),
															array(
																'escape'=>false,
																'title'=>"Click to inactive"
													));
											 }else{
											 		echo $this->Form->postLink(
															$this->Html->tag('i','',
																		array('class'=>" fa fa-times")
															), 
															array(
																'action' => 'active',
																$page['Page']['id']
															),
															array(
																'escape'=>false,
																'title'=>"Click to active"
													)); 
											 }
                     ?>
                  </td>
              </tr>
              <tr>
                  <td><?php echo __('Created'); ?></td>
                  <td>
                    <?php 
                    echo $this->Time->format($page['Page']['created'], '%B %e, %Y %H:%M %p');
                    ?>
                  </td>
              </tr>
              <tr>
                  <td><?php echo __('Modified'); ?></td>
                  <td>
                     <?php 
                      echo $this->Time->format($page['Page']['modified'], '%B %e, %Y %H:%M %p');
                     ?>
                  </td>
              </tr>
          </table>

            </div><!-- /.box-body -->
       
          </div><!-- /.box -->
        </div><!-- /.col (left) -->
        <div class="col-md-4">
          <div class="box">
              <div class="box-header">
                  <h3 class="box-title">Actions</h3>
              </div>
              <div class="box-body dropdown">
              	<ul>
									<li><?php echo $this->Html->link(__('Edit Page'), array('action' => 'edit', $page['Page']['id'])); ?> </li>
									<li><?php echo $this->Form->postLink(__('Delete Page'), array('action' => 'delete', $page['Page']['id']), null, __('Are you sure you want to delete # %s?', $page['Page']['page_name'])); ?> </li>
									<li><?php echo $this->Html->link(__('List Pages'), array('action' => 'index')); ?> </li>
									<li><?php echo $this->Html->link(__('New Page'), array('action' => 'add')); ?> </li>
								</ul>
              </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col (right) -->
      </div><!-- /.row -->                    
    </section><!-- /.content -->
</aside><!-- /.right-side -->
<style>
.table td{
word-break:normal !important;
}
</style>