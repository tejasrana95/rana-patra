<!-- START HEADER -->
<header id="header" class="page-topbar">
    <!-- start header nav-->
    <div class="navbar-fixed">
        <nav class="cyan">
            <div class="nav-wrapper">                    

                <ul class="left">                                            
                    <li class="no-hover"><a href="javascript:void();" data-activates="slide-out" class="menu-sidebar-collapse btn-floating btn-flat btn-medium waves-effect waves-light cyan"><i class="mdi-navigation-menu" ></i></a></li>
                    <li><h1 class="logo-wrapper"><a href="<?php echo Router::url('/', true); ?>" class="brand-logo darken-1"><img src="<?php echo Router::url('/', true); ?>images/materialize-logo.png" alt="materialize logo"></a> <span class="logo-text">Materialize</span></h1></li>
                </ul>
                <div class="header-search-wrapper hide-on-med-and-down">
                    <i class="mdi-action-search"></i>
                    <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"/>
                </div>
                <ul class="right hide-on-med-and-down">                        
                    <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light"><i class="mdi-navigation-apps"></i></a>
                    </li>                        
                    <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light"><i class="mdi-social-notifications"></i></a>
                    </li>                        
                    <li><a href="/" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!-- end header nav-->
</header>
<!-- END HEADER -->

<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START MAIN -->
<div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">

        <?php
        $nav = array(
            'Dashboard' => array(
                'tag_class' => 'mdi-action-dashboard',
                'a_class'=>' ',
                'link' => array('controller' => 'users', 'action' => "dashboard")
            ),
        );
        if ($roles['page']['add'] == 1 || $roles['page']['edit'] == 1 || $roles['page']['delete'] == 1) {
            $nav['Pages'] = array(
                'tag_class' => 'mdi-editor-insert-comment',
                'a_class'=>' ',
                'link' => array('controller' => 'pages', 'action' => "index")
            );
        }
        if ($is_admin == 1) {
             $nav['Category'] = array(
                'tag_class' => 'mdi-navigation-apps',
                'a_class'=>' ',
                'link' => array('controller' => 'category', 'action' => "index")
            );
        }
        if ($roles['article']['add'] == 1 || $roles['article']['edit'] == 1 || $roles['article']['delete'] == 1) {
            $nav['Articles Management'] = array(
                'tag_class' => 'mdi-action-note-add',
               'a_class'=>'collapsible-header ',
                'right_i_class' => '',
                 'link' => "javascript:void();",
                'sub_link' => array(
                    'Articles' => array(
                        'tag_class' => 'mdi-hardware-keyboard-arrow-right',
                        'link' => array('controller' => 'articles', 'action' => "index")
                    ),
                    'Categories' => array(
                        'tag_class' => 'mdi-hardware-keyboard-arrow-right',
                        'link' => array('controller' => 'articlecategory', 'action' => "index"),
                    ),
                )
            );
        }
        if ($roles['customer']['add'] == 1 || $roles['customer']['edit'] == 1 || $roles['customer']['delete'] == 1) {
            $nav['Customer Management'] = array(
                'tag_class' => 'mdi-social-people-outline',
               'a_class'=>'collapsible-header ',
                'right_i_class' => '',
                 'link' => "javascript:void();",
                'sub_link' => array(
                    'Customers' => array(
                        'tag_class' => 'mdi-hardware-keyboard-arrow-right',
                        'link' => array('controller' => 'customers', 'action' => "index")
                    ),
                    'Invoices' => array(
                        'tag_class' => 'mdi-hardware-keyboard-arrow-right',
                        'link' => array('controller' => 'customerinvoices', 'action' => "index"),
                    ),
                )
            );
        }
        $nav['Matrimonial'] = array(
            'tag_class' => 'mdi-social-group-add',
            'a_class'=>' ',
            'link' => array('controller' => 'matrimonials', 'action' => "index")
        );
        $nav['Testimonials'] = array(
            'tag_class' => 'mdi-image-tag-faces',
            'a_class'=>' ',
            'link' => array('controller' => 'testimonials', 'action' => "index")
        );
        $nav['Shortcodes'] = array(
            'tag_class' => 'mdi-action-label-outline',
            'a_class'=>' ',
            'link' => array('controller' => 'shortcodes', 'action' => "index")
        );
        $nav['Request to admin'] = array(
            'tag_class' => 'mdi-communication-quick-contacts-mail',
            'a_class'=>' ',
            'link' => array('controller' => 'admin_requests', 'action' => "index")
        );

        if ($is_admin == 1) {
            $nav['Menu'] = array(
                'tag_class' => 'mdi-navigation-menu',
                'a_class'=>' ',
                'link' => array('controller' => 'menus', 'action' => "index")
            );
             $nav['Category'] = array(
                'tag_class' => 'mdi-navigation-apps',
                'a_class'=>' ',
                'link' => array('controller' => 'category', 'action' => "index")
            );
            $nav['Report'] = array(
                'tag_class' => 'mdi-action-bug-report',
                'a_class'=>' ',
                'link' => array('controller' => 'report', 'action' => "index")
            );
        }
        if ($is_admin == 1) {
            $nav['User Management'] = array(
                'tag_class' => 'mdi-social-person-add',
               'a_class'=>'collapsible-header ',
                'right_i_class' => '',
                 'link' => "javascript:void();",
                'sub_link' => array(
                    'User' => array(
                        'tag_class' => 'mdi-hardware-keyboard-arrow-right',
                        'link' => array('controller' => 'users', 'action' => "index")
                    ),
                    'User Role' => array(
                        'tag_class' => 'mdi-hardware-keyboard-arrow-right',
                        'link' => array('controller' => 'user_roles', 'action' => "index"),
                    ),
                )
            );
        }
        if ($is_admin == 1) {
            $nav['Settings'] = array(
                'tag_class' => 'mdi-action-settings ',
                'a_class'=>'collapsible-header ',
                'right_i_class' => '',
                'link' => "javascript:void()",
                'sub_link' => array(
                    'Site options' => array(
                        'tag_class' => 'mdi-hardware-keyboard-arrow-right',
                        'link' => array('controller' => 'options', 'action' => "index")
                    ),
                    'Widgets' => array(
                        'tag_class' => 'mdi-hardware-keyboard-arrow-right',
                        'link' => array('controller' => 'widgets', 'action' => "index")
                    ),
                    'Slider' => array(
                        'tag_class' => 'mdi-hardware-keyboard-arrow-right ',
                        'link' => array('controller' => 'sliders', 'action' => "index")
                    ),
                    'Clear Cache' => array(
                        'tag_class' => 'mdi-editor-border-clear ',
                        'link' => $site_url . '?clearcache=true',
                        'target' => '_blank',
                        'onclick' => 'Are you sure to clear all caches?'
                    ),
                )
            );
        }

        if ($is_admin == 1) {
            $nav['File Manager'] = array(
                'tag_class' => 'mdi-file-folder','a_class'=>' ',
                'link' => array('controller' => 'filemanager', 'action' => "index")
            );
        }
        ?>



        <!-- START LEFT SIDEBAR NAV-->
        <aside id="left-sidebar-nav">
            <ul id="slide-out" class="side-nav leftside-navigation collapsible collapsible-accordion">
                <li class="user-details cyan darken-2">
                    <div class="row">
                        <div class="col col s4 m4 l4">
                            <img src="images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                        </div>
                        <div class="col col s8 m8 l8">
                            <ul id="profile-dropdown" class="dropdown-content">
                                <li>
                                    <?php echo $this->Html->link('<i class="mdi-action-face-unlock"></i> Profile',array('controller' => 'users', 'action' => 'edit', $_SESSION['Auth']['User']['id']),array('escape'=>false)); ?>
                                    
                                   
                                </li>
                                <li>
                                     <?php echo $this->Html->link('<i class="mdi-hardware-keyboard-tab"></i> Logout',array('controller' => 'users', 'action' => 'logout'),array('escape'=>false)); ?>
                                   
                                </li>
                            </ul>
                            <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="javascript:void();" data-activates="profile-dropdown"><?php echo ucwords($this->session->read('Auth.User.username')); ?><i class="mdi-navigation-arrow-drop-down right"></i></a>

                        </div>
                    </div>
                </li>



                <?php
// builing admin menu 
                foreach ($nav as $k => $v) {
                    $tag_right_link = "";
                    $active_class = "";
                    if (isset($v['sub_link']) && !empty($v['sub_link'])) {
                        $li_class = 'bold';
                        $tag_right_link = $this->Html->tag('i', '', array('class' => $v['right_i_class']));
                    } else {
                        $li_class = "";
                    }
                    if (is_array($v['link']) && $this->request->params['controller'] == $v['link']['controller']) {
                        $active_class = "active";
                    }
                    ?>
                    <li  class="<?php echo $li_class; ?> <?php echo $active_class; ?>">
                        <?php
                        echo $this->Html->link(
                                $this->Html->tag('i', '', array('class' => $v['tag_class'])) .
                                $this->Html->tag('span', $k) .
                                $tag_right_link, $v['link'], array('class'=>$v['a_class'],'escape' => false)
                        );
                        ?>
                        <?php
                        if (isset($v['sub_link']) && !empty($v['sub_link'])) {
                            ?>
                            <div class="collapsible-body">
                                <ul>

                                    <?php
                                    foreach ($v['sub_link'] as $k => $sub) {
                                        ?>
                                        <li class="">
                                            <?php
                                            if (isset($sub['target']) && $sub['target'] == "_blank") {
                                                $target = array('target' => '_blank');
                                            } else {
                                                $target = array('target' => '_self');
                                            }
                                            if (isset($sub['onclick']) && !empty($sub['onclick'])) {
                                                $onclick = $sub['onclick'];
                                            } else {
                                                $onclick = "";
                                            }
                                            $links = $this->Html->link(
                                                    $this->Html->tag('i', '', array('class' => $sub['tag_class'])) . $k, $sub['link'], array('escape' => false, 'target' => $target['target'], 'confirm' => $onclick)
                                            );

                                            echo $links;
                                            ?></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                            <?php
                        }
                        ?>
                    </li>
                    <?php
                }
                ?>


                <li class="li-hover"><div class="divider"></div></li>
            </ul>

        </aside>   