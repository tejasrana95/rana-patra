<?php
App::import('Controller', 'UserRoles');
$UserRolesController = new UserRolesController;
App::import('Controller', 'Users');
$UsersController = new UsersController;
App::import('Controller', 'Customers');
$CustomersController = new CustomersController;
?><div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li><?php echo $this->Html->link(__($parent_page_name), array('action' => 'index')); ?></li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<!--Basic Form-->
<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <h4 class="header2"><?php echo __($page_name); ?></h4>
                <div class="row">
                    <table class="table responsive-table striped">
                        <thead>
                            <tr>
                                <th align="center"><?php echo $this->Paginator->sort('id'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('Customer Name'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('item_type'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('item_name'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('payment_status'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('mc_gross'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('mc_currency'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('process_by'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('created'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('modified'); ?></th>
                                <th align="center" class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($CustomerInvoice)) {
                                foreach ($CustomerInvoice as $CustomerInvoice):
                                    ?>
                                    <tr>
                                        <td><?php echo h($CustomerInvoice['CustomerInvoice']['id']); ?>&nbsp;</td>
                                        <td><?php
                                        $customer_Detail=$CustomersController->getCustomerInfo($CustomerInvoice['CustomerInvoice']['userID']);
                                        echo h($customer_Detail['Customer']['username']); ?>&nbsp;</td>
                                        <td><?php echo h($item_type[$CustomerInvoice['CustomerInvoice']['item_type']]); ?>&nbsp;</td>
                                        <td><?php echo h($CustomerInvoice['CustomerInvoice']['item_name']); ?>&nbsp;</td>
                                        <td><?php echo h($CustomerInvoice['CustomerInvoice']['payment_status']); ?>&nbsp;</td>
                                        <td><?php echo h($CustomerInvoice['CustomerInvoice']['mc_gross']); ?>&nbsp;</td>
                                        <td><?php echo h($CustomerInvoice['CustomerInvoice']['mc_currency']); ?>&nbsp;</td>
                                        <td><?php  if ($CustomerInvoice['CustomerInvoice']['process_by'] == 1) {

                                            $getUserInfo = $UsersController->getUserInfo($CustomerInvoice['CustomerInvoice']['system_id']);
                                            echo h($getUserInfo['User']['username']);
                                        } else {
                                            echo h($process_by[$CustomerInvoice['CustomerInvoice']['process_by']]);
                                        } ?>&nbsp;</td>
                                        <td>
                                            <?php
                                            echo $this->Time->format($CustomerInvoice['CustomerInvoice']['created'], '%B %e, %Y %H:%M %p');
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo $this->Time->format($CustomerInvoice['CustomerInvoice']['modified'], '%B %e, %Y %H:%M %p');
                                            ?>
                                        </td>
                                        <td class="actions">
                                            <?php
                                            echo $this->Html->link(
                                                    $this->Html->tag('i', '', array('class' => "fa fa-search-plus")
                                                    ), array('action' => 'view', $CustomerInvoice['CustomerInvoice']['id']), array(
                                                'escape' => false,
                                                'title' => "Click to view",
                                            ));
                                            ?>
                                            <?php
                                            echo $this->Html->link(
                                                    $this->Html->tag('i', '', array('class' => "fa fa-pencil-square")), array(
                                                'action' => 'edit',
                                                $CustomerInvoice['CustomerInvoice']['id']), array(
                                                'escape' => false,
                                                'title' => "Click to Edit",
                                            ));
                                            ?>
                                            <?php
                                            $is_admin = $UserRolesController->getPermission($adminuserID);
                                            if ($is_admin['is_admin'] != 1) {
                                                echo $this->Form->postLink(
                                                        $this->Html->tag('i', '', array('class' => " fa fa-times-circle")
                                                        ), array(
                                                    'action' => 'delete',
                                                    $CustomerInvoice['CustomerInvoice']['id'],
                                                        ), array(
                                                    'escape' => false,
                                                    'title' => "Click to delete",
                                                        ), __('Are you sure you want to delete # %s?', $CustomerInvoice['CustomerInvoice']['username'])
                                                );
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="10" align="center">No records found.</td>
                                </tr>
                            <?php }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                 <th align="center"><?php echo $this->Paginator->sort('id'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('Customer Name'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('item_type'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('item_name'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('payment_status'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('mc_gross'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('mc_currency'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('process_by'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('created'); ?></th>
                                <th align="center"><?php echo $this->Paginator->sort('modified'); ?></th>
                                <th align="center" class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="col s12 m12 l12">
                        <div class="row">
                            <div class="paging">
                                <ul class="pagination pagination-sm">
                                    <?php
                                    echo $this->Paginator->prev(
                                            ' << ', array(
                                        'tag' => 'li',
                                        'class' => 'prev',
                                        'escape' => false,
                                            ), null, array(
                                        'tag' => 'li',
                                        'escape' => false,
                                        'class' => 'prev disabled',
                                        'disabledTag' => 'a',
                                    ));
                                    ?>
                                    <?php
                                    echo $this->Paginator->numbers(array(
                                        'separator' => '',
                                        'currentClass' => 'active',
                                        'currentTag' => 'a',
                                        'tag' => 'li',
                                    ));
                                    ?>
                                    <?php
                                    echo $this->Paginator->next('>>', array(
                                        'tag' => 'li',
                                        'class' => 'next',
                                        'escape' => false,
                                            ), null, array(
                                        'tag' => 'li',
                                        'escape' => false,
                                        'class' => 'next disabled',
                                        'disabledTag' => 'a',
                                    ));
                                    ?>
                                </ul>
                            </div>
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}'),
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating btn-large red', 'escape' => FALSE)); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    table img {
        max-width:100px
    }
</style>