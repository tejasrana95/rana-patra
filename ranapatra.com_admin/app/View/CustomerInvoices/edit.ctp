<?php
App::import('Controller', 'UserRoles');
$UserRolesController = new UserRolesController;
?>
<?php echo $this->Html->css(array('imageCrop/imgareaselect-animated')); ?>
<?php echo $this->Html->script(array('imageCrop/jquery.imgareaselect.pack', 'imageCrop/script')); ?>
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li><?php echo $this->Html->link(__($parent_page_name), array('action' => 'index')); ?></li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <?php echo $this->Form->create('CustomerInvoice', array('type' => 'file')); ?>
              <?php echo $this->Form->input('id'); ?>
            <div class="card-panel">

                <div class="row">

                    <div class="box-body">

                        <div class="row">

                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('userID', array('options' => $customers,
                                    'empty' => "Select Customer",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'id' => 'userID',
                                    'label' => false, 'onchange' => 'getCustomerDetail()'));
                                ?>
                            </div>
                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('item_type', array('options' => $item_type,
                                    'empty' => "Select Item Type",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('item_name', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('item_number', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('payment_status', array('options' => array('SUCCESS' => 'SUCCESS', 'FAILED' => 'FAILED'),
                                    'empty' => "Select payment status",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('mc_gross', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('mc_currency', array('options' => $currency,
                                    'empty' => "Select payment Currency",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('txn_id', array('div' => array('class' => "form-group"), 'type' => 'text', 'class' => "form-control"));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('receiver_email', array('div' => array('class' => "form-group"), 'class' => "form-control", 'value' => $payment_mail));
                                ?>
                            </div>
                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('payer_email', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('payment_gateway', array('options' => $payment_gateway,
                                    'empty' => "Select payment gateway",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('process_by', array('options' => $process_by,
                                    'empty' => "Select process by",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <?php
                                echo $this->Form->input('Save', array('label' => false, 'div' => array('class' => "form-group"), 'type' => "submit", 'class' => "btn btn-success btn-lg"));
                                ?>
                            </div>
                        </div>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div></div>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
        <i class="large mdi-editor-mode-edit"></i>
    </a>
    <ul>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-list')), array('action' => 'index'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
    </ul>
</div>

<script>

    function getCustomerDetail() {
        if ($('#userID').val() != "" ) {
            $.get('<?php echo Router::url('/', true); ?>customers/getcustomerinfo/' + $('#userID').val() + '/json', function (result) {
                var arr = $.parseJSON(result);
                
                $('#CustomerInvoicePayerEmail').val(arr.Customer.email);
                   $('#CustomerInvoicePayerEmail').material_text();
            });
        }
    }
// listen to events on the category dropdown
    function getstate() {
        // don't do anything if use selects "Select Cat"
        if ($("#country").val() !== "") {
            $.get('<?php echo Router::url('/', true); ?>common/state/' + $('#country').val(), function (result) {
                $('#state').html(result);

                $('#state').material_select();

            });
        }
    }
    function getcity() {
        // don't do anything if use selects "Select Cat"
        if ($("#state").val() !== "") {
            $.get('<?php echo Router::url('/', true); ?>common/city/' + $('#state').val(), function (result) {
                $('#city').html(result);
                $('#city').material_select();
            });
        }
    }

</script>
<script type="text/javascript">


</script>