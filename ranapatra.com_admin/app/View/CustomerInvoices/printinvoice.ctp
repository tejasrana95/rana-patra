<?php
App::import('Controller', 'UserRoles');
$UserRolesController = new UserRolesController;
App::import('Controller', 'Common');
$CommonController = new CommonController;
?>
<div class="container">
    <div id="invoice">
        <div class="card-panel">
            <div class="invoice-header">
                <div class="row section">
                    <div class="col s12 m6 l6">
                        <img src="images/generic-logo.png" alt="company logo">
                        <p>To,
                            <br>
                            <span class="strong"><?php echo $this->request->data['CustomerProfile']['name'] ?></span>
                            <br>
                            <span><?php echo $this->request->data['CustomerProfile']['address'] ?></span>
                            <br>
                            <span><?php
                                $country = $CommonController->country($this->request->data['CustomerProfile']['country']);
                                $state = $CommonController->state($this->request->data['CustomerProfile']['country'], $this->request->data['CustomerProfile']['state']);
                                $city = $CommonController->city($this->request->data['CustomerProfile']['state'], $this->request->data['CustomerProfile']['city']);

                                echo $city['Country']['local_name']
                                ?>, <?php echo $state['Country']['local_name'] ?> 
                                , <?php echo $country['Country']['local_name'] ?>
                            </span>
                            <br>
                            <span><?php echo $this->request->data['CustomerProfile']['contact'] ?></span>
                        </p>
                    </div>

                    <div class="col s12 m6 l6">
                        <div class="invoce-company-address right-align">
                            <span class="invoice-icon"><i class="mdi-social-location-city cyan-text"></i></span>

                            <p><span class="strong"><?php echo $company_info['payment_company']; ?></span>
                                <br>
                                <span><?php echo $company_info['payment_address']; ?></span>                                
                                <br>
                                <span><?php echo $company_info['payment_contact']; ?></span>
                            </p>
                        </div>

                        <div class="invoce-company-contact right-align">
                            <span class="invoice-icon"><i class="mdi-communication-quick-contacts-mail cyan-text"></i></span>
                            <p><span class="strong"><?php echo $company_info['payment_company_url']; ?></span>
                                <br>
                                <span><?php echo $company_info['payment_mail']; ?></span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="invoice-lable">
                <div class="row">
                    <div class="col s12 m3 l3 cyan">
                        <h4 class="white-text invoice-text">INVOICE</h4>
                    </div>
                    <div class="col s12 m9 l9 invoice-brief cyan white-text">
                        <div class="row">
                            <div class="col s12 m3 l3">
                                <p class="strong">Total Due</p>
                                <h4 class="header"><?php echo $this->request->data['CustomerInvoice']['mc_currency']; ?> 
                                    <?php echo $this->request->data['CustomerInvoice']['mc_gross']; ?>
                                </h4>
                            </div>
                            <div class="col s12 m4 l4">
                                <p class="strong">Invoice No</p>
                                <h4 class="header"><?php echo $this->request->data['CustomerInvoice']['txn_id']; ?></h4>
                            </div>
                            <div class="col s12 m2 l2">
                                <p class="strong">Due Date</p>
                                <h4 class="header"><?php echo $this->Time->format($this->request->data['CustomerInvoice']['created'], '%e.%m.%Y'); ?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="invoice-table">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <table class="striped">
                            <thead>
                                <tr>
                                    <th data-field="no">No</th>
                                    <th data-field="item">Item</th>
                                    <th data-field="uprice">Item Number</th>
                                    <th data-field="price">Item Type</th>
                                    <th data-field="price">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1.</td>
                                    <td><?php echo $this->request->data['CustomerInvoice']['item_name']; ?></td>
                                    <td><?php echo $this->request->data['CustomerInvoice']['item_number']; ?></td>
                                    <td><?php echo $item_type[$this->request->data['CustomerInvoice']['item_type']]; ?></td>
                                    <td><?php echo $this->request->data['CustomerInvoice']['mc_currency']; ?> 
                                        <?php echo $this->request->data['CustomerInvoice']['mc_gross']; ?></td>
                                </tr>

                                <tr>
                                    <td colspan="3" class="white"></td>
                                    <td class="cyan white-text">Grand Total</td>
                                    <td class="cyan strong white-text"><?php echo $this->request->data['CustomerInvoice']['mc_currency']; ?> 
                                        <?php echo $this->request->data['CustomerInvoice']['mc_gross']; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

            <div class="invoice-footer">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <p class="strong">Payment Method</p>
                        <p><?php echo strtoupper($this->request->data['CustomerInvoice']['payment_gateway']); ?></p>
                        <p class="strong">Terms &amp; Condition</p>
                        <ul>
                            <li>Please visit: <?php echo $site_url ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col s12 m6 l6 center-align z-depth-2 ">
                <div <?php
                if (strtoupper($this->request->data['CustomerInvoice']['payment_status']) == "FAILED") {
                    echo 'class="red darken-4"';
                } else {
                    echo 'class="green accent-4"';
                }
                ?>>
                    <div class="clearfix">&nbsp;</div>
                    <h6 class="header">Payment: <?php echo strtoupper($this->request->data['CustomerInvoice']['payment_status']); ?></h6>
                    <div class="clearfix">&nbsp;</div>
               </div>
            </div>
        </div>
    </div>
</div>