
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js">
    </script>
<?php
echo $this->Html->script(array("/elfinder/js/elfinder.min"));
?>
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="section">
        <!--Basic Form-->
        <div id="basic-form" class="section">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card-panel">
                        <h4 class="header2"><?php echo __($page_name); ?></h4>
                        <div class="row">
                            <div id="Elfinders"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    (function ($) {

        $('#Elfinders').elfinder({
            url: '<?php echo $site_url; ?>filemanager/<?php echo $userID; ?>', // connector URL (REQUIRED)

                    });
                })(jQuery);
</script>
