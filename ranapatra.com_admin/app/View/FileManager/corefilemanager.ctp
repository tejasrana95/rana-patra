<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.0/jquery.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.0.0/jquery-migrate-1.0.0.min.js">
    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js">
    </script>
    <?php
echo $this->
    Html->script(array("jquery-1.11.0.min"));echo $this->Html->css(array("/elfinder/css/elfinder.min", "/elfinder/themes/windows-10/css/theme"));
echo $this->Html->script(array("/elfinder/js/elfinder.min"));
?>
    <?php
        echo $this->
    Html->css(array(
            'bootstrap.min'
        ));
        ?>
    <?php echo $this->
    fetch('css'); ?>
    <div id="Elfinders">
    </div>
    <script type="text/javascript">
        var funcNum = getUrlParam('CKEditorFuncNum');
function getUrlParam(paramName) {
    var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
    var match = window.location.search.match(reParam) ;

    return (match && match.length > 1) ? match[1] : '' ;
}

  var FileBrowserDialogue = {
    init: function() {
      // Here goes your code for setting your custom things onLoad.
    },
    mySubmit: function (URL) {
      // pass selected file path to TinyMCE
     window.opener.CKEDITOR.tools.callFunction(funcNum, URL.url);
    window.close();
    }
  }

  $().ready(function() {
    var elf = $('#Elfinders').elfinder({
      // set your elFinder options here
      url: '<?php echo $site_url; ?>/filemanager/<?php echo $userID ?>/', // connector URL
      getFileCallback: function(file) { // editor callback
        // file.url - commandsOptions.getfile.onlyURL = false (default)
        // file     - commandsOptions.getfile.onlyURL = true
        FileBrowserDialogue.mySubmit(file); // pass selected file path to TinyMCE 
      }
    }).elfinder('instance');      
  });
    </script>
</link>