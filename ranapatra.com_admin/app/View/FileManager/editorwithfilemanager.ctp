/**
 * Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
/* exported initSample */
var advancedEditor = {
    toolbar: [{
        name: 'document',
        items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates']
    }, {
        name: 'clipboard',
        items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']
    }, {
        name: 'editing',
        items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']
    }, {
        name: 'forms',
        items: ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField']
    }, '/', {
        name: 'basicstyles',
        items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat']
    }, {
        name: 'paragraph',
        items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']
    }, {
        name: 'links',
        items: ['Link', 'Unlink', 'Anchor']
    }, {
        name: 'insert',
        items: ['Image', 'Flash', 'oembed', 'Table','btgrid', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe']
    }, '/', {
        name: 'styles',
        items: ['Styles', 'Format', 'Font', 'FontSize']
    }, {
        name: 'colors',
        items: ['TextColor', 'BGColor']
    }, {
        name: 'tools',
        items: ['Maximize', 'ShowBlocks']
    }, {
        name: 'about',
        items: ['About']
    }],
    filebrowserBrowseUrl: "<?php echo Router::url('/', true); ?>filemanager/corefilemanager/<?php echo $_SESSION['Auth']['User']['id']; ?>",
    extraPlugins: 'autogrow,oembed,btgrid',
    autoGrow_minHeight: 200,
    autoGrow_maxHeight: 1200,
    autoGrow_bottomSpace: 50,
    allowedContent :true,
    extraAllowedContent :'*(*);*{*}',
    contentsCss : '<?php echo $site_url; ?>css/editor.css',
};
var standardEditor = {
    toolbar: [{
        name: 'document',
        items: ['Source', '-']
    }, {
        name: 'basicstyles',
        items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat']
    }, {
        name: 'paragraph',
        items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']
    }, {
        name: 'links',
        items: ['Link', 'Unlink', 'Anchor']
    }, {
        name: 'insert',
        items: ['Image', 'Flash', 'oembed', 'Table', 'btgrid','HorizontalRule', 'SpecialChar', 'PageBreak', 'Iframe']
    }, {
        name: 'styles',
        items: ['Styles', 'Format', 'Font', 'FontSize']
    }, {
        name: 'colors',
        items: ['TextColor', 'BGColor']
    }, {
        name: 'tools',
        items: ['Maximize', 'ShowBlocks']
    }, ],
    filebrowserBrowseUrl: "<?php echo Router::url('/', true); ?>filemanager/corefilemanager/<?php echo $_SESSION['Auth']['User']['id']; ?>",
    extraPlugins: 'autogrow,oembed,btgrid',
    autoGrow_minHeight: 200,
    autoGrow_maxHeight: 1200,
    autoGrow_topSpace: 100,
    autoGrow_bottomSpace: 100,
    allowedContent :true,
    extraAllowedContent :'*(*);*{*}',
    contentsCss : '<?php echo $site_url; ?>css/editor.css',
    
};
var basicEditor = {
    toolbar: [{
        name: 'basicstyles',
        items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat']
    }, {
        name: 'paragraph',
        items: ['NumberedList', 'BulletedList']
    }, {
        name: 'links',
        items: ['Link', 'Unlink']
    }, {
        name: 'colors',
        items: ['TextColor', 'BGColor']
    }, ],
    extraPlugins: 'autogrow',
    autoGrow_minHeight: 200,
    autoGrow_maxHeight: 600,
    autoGrow_bottomSpace: 50
};
var minimalEditor = {
    toolbar: [{
        name: 'basicstyles',
        items: ['Bold', 'Italic', 'Underline']
    }, ],
    extraPlugins: 'autogrow',
    autoGrow_minHeight: 200,
    autoGrow_maxHeight: 600,
    autoGrow_bottomSpace: 50
};
//
if (document.getElementById("standardEditor") !== null) {
    CKEDITOR.replace('standardEditor', standardEditor);
}
if (document.getElementById("advancedEditor") !== null) {
    CKEDITOR.replace('advancedEditor', advancedEditor);
}
if (document.getElementById("basicEditor") !== null) {
    CKEDITOR.replace('basicEditor', basicEditor);
}
if (document.getElementById("minimalEditor") !== null) {
    CKEDITOR.replace('minimalEditor', minimalEditor);
}