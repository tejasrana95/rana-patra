<?php
echo $this->Html->script('nested_menu', array('inline' => false));
?>
<aside class="right-side">
    <section class="content-header">
        <h1>
            Menus
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Menus</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">


                        <h3 class="box-title">Edit Menu
                        </h3> 		                              
                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Custom HTML</h3>
                            </div>
                            <div class="box-body">
                                <?php echo $this->Form->create('Menu', array('type' => 'file')); ?>
                                <?php echo $this->Form->input('id'); ?>
                                <?php echo $this->Form->input('lang',array("type"=>"hidden")); ?>
                                <?php
                                echo $this->Form->input('content_name', array(
                                    'id' => 'custom_html_name',
                                    'placeholder' => "Title",
                                    "class" => "form-control",
                                    'label' => "Title"));
                                ?>
                                <?php
                                echo $this->Form->input('url', array('type' => "textarea", 'maxlength' => 200, 'div' => array('class' => "form-group"), 'class' => "form-control basicEditor",'id'=>'basicEditor', 'label'=>"Custom HTML",'id' => "custom_html"));
                                ?>
                                 <?php
                        echo $this->Form->input('Save', array('label' => false, 'div' => array('class' => "form-group"), 'type' => "submit", 'class' => "btn btn-success btn-lg"));
                        ?>
                                <?php echo $this->Form->end(); ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->


                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col-8 -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</aside>

<?php
echo $this->Html->script(Router::url('/', true).'js/tinymce/tinymce.min.js', array('inline' => false)); 
echo $this->Html->script(Router::url('/', true) . 'filemanager/editorwithfilemanager/true', array('inline' => false));
?>