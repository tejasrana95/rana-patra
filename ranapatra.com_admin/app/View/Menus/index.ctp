<?php echo $this->Html->script(array('nested_menu')); ?>
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>



        <!--Basic Form-->

        <div class="row">
            <div class="col s12 m8 l8">
                 <div class="card-panel">
                    <div class="box-header">


                        <h4 class="header2"><?php
                            if ($menu_for == "header-menu") {
                                $heading = "Header Menu";
                            }

                            if ($menu_for == "footer-menu") {
                                $heading = "Footer Menu";
                            }

                            if ($menu_for == "top-menu") {
                                $heading = "Top Menu";
                            }

                            echo __($heading);
                            ?>
                        </h4>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <table>
                            <tr>
                                <td>
                                    <?php
                                    echo $this->Form->create(false, array('url' => array('controller' => 'menus')));
                                    echo $this->Form->input('select_menu_type', array(
                                        'options' => $menu_pages,
                                        'selected' => $menu_for,
                                        'empty' => "Select menu for",
                                    ));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $options = array(
                                        'type' => "submit",
                                        'class' => "btn-floating btn-small waves-effect waves-light ",
                                    );
                                    echo $this->Form->button('Go', $options);

                                    echo $this->Form->end();
                                    ?>
                                </td>
                            </tr>
                        </table>

                        <!--rendering menu-->
                        <?php
                        echo '<div class="cf nestable-lists1">';
                        echo '<div class="dd" id="nestable">';
                        echo '<ol class="dd-list" id="menu-list">';

                        if (!empty($menu_headers)) {
                            // rendering headers
                            foreach ($menu_headers as $menu_header) {

                                if ($this->requestAction(array('controller' => 'menus', 'action' => 'check_status', $menu_header['Menu']['content_type'], $menu_header['Menu']['content_name']))) {
                                    //debug("yes");

                                    if ($menu_header['Menu']['content_type'] == "page") {
                                        $pg_name = $this->requestAction(array('controller' => 'menus', 'action' => 'get_page_name', $menu_header['Menu']['content_name']));
                                        echo '<li class=dd-item data-id="' . $menu_header['Menu']['content_name'] . '" data-type=' . $menu_header['Menu']['content_type'] . ' data-url=' . $menu_header['Menu']['url'] . ' data-login=' . $menu_header['Menu']['login_only'] . ' data-icon="' . $menu_header['Menu']['icon'] . '"><div class=dd-handle>' . $pg_name . '</div>';
                                    } elseif ($menu_header['Menu']['content_type'] == "category") {
                                        $categoryname = $this->requestAction('category/categorydata/' . $menu_header['Menu']['content_name']);
                                        echo '<li class="dd-item" data-id="' . $menu_header['Menu']['content_name'] . '" data-type="' . $menu_header['Menu']['content_type'] . '" data-url="' . $menu_header['Menu']['url'] . '"  data-url=' . $menu_header['Menu']['url'] . ' data-login=' . $menu_header['Menu']['login_only'] . ' data-icon="' . $menu_header['Menu']['icon'] . '"><div class=dd-handle>' . $categoryname['label'] . '</div>';
                                    } else {
                                        $pg_name = $menu_header['Menu']['content_name'];
                                        $d_id = $menu_header['Menu']['content_name'];
                                        echo '<li class=dd-item data-id="' . $d_id . '" data-type=' . $menu_header['Menu']['content_type'] . ' data-url=' . $menu_header['Menu']['url'] . ' data-icon="' . $menu_header['Menu']['icon'] . '  " data-url=' . $menu_header['Menu']['url'] . ' data-login=' . $menu_header['Menu']['login_only'] . '><div class=dd-handle>' . $pg_name . '</div>';
                                    }
                                    //debug($d_id);
                                    if ($menu_header['Menu']['content_type'] == "category") {
                                        $categoryname = $this->requestAction('category/categorydata/' . $menu_header['Menu']['content_name']);
                                        echo $this->Form->postLink($this->Html->tag('i', '', array('class' => " fa fa-times-circle")), array('action' => 'delete_menu_item',
                                            $menu_header['Menu']['id']), array('escape' => false, 'title' => "Click to delete"), __('Are you sure you want to delete # %s?', $categoryname['label']));
                                    } elseif ($menu_header['Menu']['content_type'] != "page") {
                                        echo $this->Form->postLink($this->Html->tag('i', '', array('class' => " fa fa-times-circle")), array('action' => 'delete_menu_item',
                                            $menu_header['Menu']['id']), array('escape' => false, 'title' => "Click to delete"), __('Are you sure you want to delete # %s?', $menu_header['Menu']['content_name']));
                                    } else {
                                        echo $this->Form->postLink($this->Html->tag('i', '', array('class' => " fa fa-times-circle")), array('action' => 'delete_menu_item',
                                            $menu_header['Menu']['id']), array('escape' => false, 'title' => "Click to delete"), __('Are you sure you want to delete # %s?', $menu_header['Menu']['content_name']));
                                    }
                                    //edit link for Custom HTML
                                    if ($menu_header['Menu']['content_type'] == "customHTML") {
                                        echo '&nbsp;&nbsp;' . $this->Html->link(
                                                $this->Html->tag('i', '', array('class' => "fa fa-pencil-square")), array(
                                            'action' => 'edit',
                                            $menu_header['Menu']['id']), array(
                                            'escape' => false,
                                            'title' => "Click to Edit",
                                        ));
                                    }
                                    //End Edit link
                                    //}
                                    if (array_key_exists($menu_header['Menu']['content_name'], $aux_array)) {
                                        // rendering children menu item
                                        echo '<ol class=dd-list>';
                                        foreach ($aux_array[$menu_header['Menu']['content_name']] as $v) {
                                            if ($this->requestAction(array('controller' => 'menus', 'action' => 'check_status', $v['Menu']['content_type'], $v['Menu']['content_name']))) {
                                                if ($v['Menu']['content_type'] == "page") {

                                                    $pg_name = $this->requestAction(array('controller' => 'menus', 'action' => 'get_page_name', $v['Menu']['content_name']));
                                                    echo '<li class=dd-item data-id="' . $v['Menu']['content_name'] . '" data-type=' . $v['Menu']['content_type'] . ' data-url=' . $v['Menu']['url'] . '  data-icon="' . $v['Menu']['icon'] . '"   data-login=' . $v['Menu']['login_only'] . '><div class=dd-handle>' . $pg_name . '</div>';
                                                } elseif ($v['Menu']['content_type'] == "category") {
                                                    $categoryname = $this->requestAction('category/categorydata/' . $v['Menu']['content_name']);
                                                    echo '<li class="dd-item" data-id="' . $v['Menu']['content_name'] . '" data-type="' . $v['Menu']['content_type'] . '" data-url="' . $v['Menu']['url'] . '" data-icon="' . $v['Menu']['icon'] . '"   data-login=' . $v['Menu']['login_only'] . '><div class=dd-handle>' . $categoryname['label'] . '</div>';
                                                } else {
                                                    $pg_name = $v['Menu']['content_name'];
                                                    $d_id = $v['Menu']['content_name'];
                                                    echo '<li class=dd-item data-id="' . $d_id . '" data-type=' . $v['Menu']['content_type'] . ' data-url=' . $v['Menu']['url'] . '  data-icon="' . $v['Menu']['icon'] . '"   data-login=' . $v['Menu']['login_only'] . '><div class=dd-handle>' . $pg_name . '</div>';
                                                }
                                                //delete link
                                                if ($v['Menu']['content_type'] == "category") {
                                                    $categoryname = $this->requestAction('category/categorydata/' . $v['Menu']['content_name']);
                                                    echo $this->Form->postLink($this->Html->tag('i', '', array('class' => " fa fa-times-circle")), array('action' => 'delete_menu_item',
                                                        $v['Menu']['id']), array('escape' => false, 'title' => "Click to delete"), __('Are you sure you want to delete # %s?', $categoryname['label']));
                                                } elseif ($v['Menu']['content_type'] != "page") {
                                                    echo $this->Form->postLink($this->Html->tag('i', '', array('class' => " fa fa-times-circle")), array('action' => 'delete_menu_item',
                                                        $v['Menu']['id']), array('escape' => false, 'title' => "Click to delete"), __('Are you sure you want to delete # %s?', $v['Menu']['content_name']));
                                                } else {
                                                    echo $this->Form->postLink($this->Html->tag('i', '', array('class' => " fa fa-times-circle")), array('action' => 'delete_menu_item',
                                                        $v['Menu']['id']), array('escape' => false, 'title' => "Click to delete"), __('Are you sure you want to delete # %s?', $v['Menu']['content_name']));
                                                }
                                                //delete link ends here`
                                                //edit link for custom HTML
                                                if ($v['Menu']['content_type'] == "customHTML") {
                                                    echo '&nbsp;&nbsp;' . $this->Html->link(
                                                            $this->Html->tag('i', '', array('class' => "fa fa-pencil-square")), array(
                                                        'action' => 'edit',
                                                        $v['Menu']['id']), array(
                                                        'escape' => false,
                                                        'title' => "Click to Edit",
                                                    ));
                                                }
                                                //end edit link

                                                if (array_key_exists($v['Menu']['content_name'], $aux_array)) {
                                                    echo '<ol class=dd-list>';
                                                    foreach ($aux_array[$v['Menu']['content_name']] as $c) {
                                                        if ($this->requestAction(array('controller' => 'menus', 'action' => 'check_status', $c['Menu']['content_type'], $c['Menu']['content_name']))) {
                                                            if ($c['Menu']['content_type'] == "page") {
                                                                $pg_name = $this->requestAction(array('controller' => 'menus', 'action' => 'get_page_name', $c['Menu']['content_name']));
                                                                echo '<li class=dd-item data-id="' . $c['Menu']['content_name'] . '" data-type=' . $c['Menu']['content_type'] . ' data-url=' . $c['Menu']['url'] . '  data-icon="' . $c['Menu']['icon'] . '"   data-login=' . $c['Menu']['login_only'] . '><div class=dd-handle>' . $pg_name . '</div>';
                                                            } elseif ($c['Menu']['content_type'] == "category") {
                                                                $categoryname = $this->requestAction('category/categorydata/' . $c['Menu']['content_name']);
                                                                echo '<li class="dd-item" data-id="' . $c['Menu']['content_name'] . '" data-type="' . $c['Menu']['content_type'] . '" data-url="' . $c['Menu']['url'] . '" data-icon="' . $c['Menu']['icon'] . '"   data-login=' . $c['Menu']['login_only'] . '><div class=dd-handle>' . $categoryname['label'] . '</div>';
                                                            } else {
                                                                $pg_name = $c['Menu']['content_name'];
                                                                $d_id = $c['Menu']['content_name'];
                                                                echo '<li class=dd-item data-id="' . $d_id . '" data-type=' . $c['Menu']['content_type'] . ' data-url=' . $c['Menu']['url'] . '  data-icon="' . $c['Menu']['icon'] . '"   data-login=' . $c['Menu']['login_only'] . '><div class=dd-handle>' . $pg_name . '</div>';
                                                            }
                                                            //delete link
                                                            if ($c['Menu']['content_type'] == "category") {
                                                                $categoryname = $this->requestAction('category/categorydata/' . $c['Menu']['content_name']);
                                                                echo $this->Form->postLink($this->Html->tag('i', '', array('class' => " fa fa-times-circle")), array('action' => 'delete_menu_item',
                                                                    $c['Menu']['id']), array('escape' => false, 'title' => "Click to delete"), __('Are you sure you want to delete # %s?', $categoryname['label']));
                                                            } elseif ($c['Menu']['content_type'] != "page") {
                                                                echo $this->Form->postLink($this->Html->tag('i', '', array('class' => " fa fa-times-circle")), array('action' => 'delete_menu_item',
                                                                    $c['Menu']['id']), array('escape' => false, 'title' => "Click to delete"), __('Are you sure you want to delete # %s?', $c['Menu']['content_name']));
                                                            } else {
                                                                echo $this->Form->postLink($this->Html->tag('i', '', array('class' => " fa fa-times-circle")), array('action' => 'delete_menu_item',
                                                                    $c['Menu']['id']), array('escape' => false, 'title' => "Click to delete"), __('Are you sure you want to delete # %s?', $c['Menu']['content_name']));
                                                            }
                                                            //delete link ends here`
                                                            //edit link for custom HTML
                                                            if ($c['Menu']['content_type'] == "customHTML") {
                                                                echo '&nbsp;&nbsp;' . $this->Html->link(
                                                                        $this->Html->tag('i', '', array('class' => "fa fa-pencil-square")), array(
                                                                    'action' => 'edit',
                                                                    $c['Menu']['id']), array(
                                                                    'escape' => false,
                                                                    'title' => "Click to Edit",
                                                                ));
                                                            }
                                                            //end edit link
                                                            if (array_key_exists($c['Menu']['content_name'], $aux_array)) {
                                                                echo '<ol class=dd-list>';
                                                                foreach ($aux_array[$c['Menu']['content_name']] as $d) {
                                                                    if ($this->requestAction(array('controller' => 'menus', 'action' => 'check_status', $d['Menu']['content_type'], $d['Menu']['content_name']))) {
                                                                        if ($d['Menu']['content_type'] == "page") {
                                                                            $pg_name = $this->requestAction(array('controller' => 'menus', 'action' => 'get_page_name', $d['Menu']['content_name']));
                                                                            echo '<li class=dd-item data-id="' . $d['Menu']['content_name'] . '" data-type=' . $d['Menu']['content_type'] . ' data-url=' . $d['Menu']['url'] . '  data-icon="' . $d['Menu']['icon'] . '"   data-login=' . $d['Menu']['login_only'] . '><div class=dd-handle>' . $pg_name . '</div>';
                                                                        } elseif ($d['Menu']['content_type'] == "category") {
                                                                            $categoryname = $this->requestAction('category/categorydata/' . $d['Menu']['content_name']);
                                                                            echo '<li class="dd-item" data-id="' . $d['Menu']['content_name'] . '" data-type="' . $d['Menu']['content_type'] . '" data-url="' . $d['Menu']['url'] . '" data-icon="' . $d['Menu']['icon'] . '"   data-login=' . $d['Menu']['login_only'] . '><div class=dd-handle>' . $categoryname['label'] . '</div>';
                                                                        } else {
                                                                            $pg_name = $d['Menu']['content_name'];
                                                                            $d_id = $d['Menu']['content_name'];
                                                                            echo '<li class=dd-item data-id="' . $d_id . '" data-type=' . $d['Menu']['content_type'] . ' data-url=' . $d['Menu']['url'] . '  data-icon="' . $d['Menu']['icon'] . '"   data-login=' . $d['Menu']['login_only'] . '><div class=dd-handle>' . $pg_name . '</div>';
                                                                        }
                                                                        //delete link
                                                                        if ($d['Menu']['content_type'] == "category") {
                                                                            $categoryname = $this->requestAction('category/categorydata/' . $d['Menu']['content_name']);
                                                                            echo $this->Form->postLink($this->Html->tag('i', '', array('class' => " fa fa-times-circle")), array('action' => 'delete_menu_item',
                                                                                $d['Menu']['id']), array('escape' => false, 'title' => "Click to delete"), __('Are you sure you want to delete # %s?', $categoryname['label']));
                                                                        } elseif ($d['Menu']['content_type'] != "page") {
                                                                            echo $this->Form->postLink($this->Html->tag('i', '', array('class' => " fa fa-times-circle")), array('action' => 'delete_menu_item',
                                                                                $d['Menu']['id']), array('escape' => false, 'title' => "Click to delete"), __('Are you sure you want to delete # %s?', $d['Menu']['content_name']));
                                                                        } else {
                                                                            echo $this->Form->postLink($this->Html->tag('i', '', array('class' => " fa fa-times-circle")), array('action' => 'delete_menu_item',
                                                                                $d['Menu']['id']), array('escape' => false, 'title' => "Click to delete"), __('Are you sure you want to delete # %s?', $d['Menu']['content_name']));
                                                                        }
                                                                        //delete link ends here`
                                                                        //edit link for custom HTML
                                                                        if ($d['Menu']['content_type'] == "customHTML") {
                                                                            echo '&nbsp;&nbsp;' . $this->Html->link(
                                                                                    $this->Html->tag('i', '', array('class' => "fa fa-pencil-square")), array(
                                                                                'action' => 'edit',
                                                                                $d['Menu']['id']), array(
                                                                                'escape' => false,
                                                                                'title' => "Click to Edit",
                                                                            ));
                                                                        }
                                                                        //end edit link
                                                                        if (array_key_exists($d['Menu']['content_name'], $aux_array)) {
                                                                            echo '<ol class=dd-list>';
                                                                            foreach ($aux_array[$d['Menu']['content_name']] as $e) {
                                                                                if ($this->requestAction(array('controller' => 'menus', 'action' => 'check_status', $e['Menu']['content_type'], $e['Menu']['content_name']))) {
                                                                                    if ($e['Menu']['content_type'] == "page") {
                                                                                        $pg_name = $this->requestAction(array('controller' => 'menus', 'action' => 'get_page_name', $e['Menu']['content_name']));
                                                                                        echo '<li class=dd-item data-id="' . $e['Menu']['content_name'] . '" data-type=' . $e['Menu']['content_type'] . ' data-url=' . $e['Menu']['url'] . '  data-icon="' . $e['Menu']['icon'] . '"   data-login=' . $e['Menu']['login_only'] . '><div class=dd-handle>' . $pg_name . '</div>';
                                                                                    } elseif ($e['Menu']['content_type'] == "category") {
                                                                                        $categoryname = $this->requestAction('category/categorydata/' . $e['Menu']['content_name']);
                                                                                        echo '<li class="dd-item" data-id="' . $d['Menu']['content_name'] . '" data-type="' . $e['Menu']['content_type'] . '" data-url="' . $e['Menu']['url'] . '" data-icon="' . $e['Menu']['icon'] . '"   data-login=' . $e['Menu']['login_only'] . '><div class=dd-handle>' . $categoryname['label'] . '</div>';
                                                                                    } else {
                                                                                        $pg_name = $e['Menu']['content_name'];
                                                                                        $d_id = $e['Menu']['content_name'];
                                                                                        echo '<li class=dd-item data-id="' . $d_id . '" data-type=' . $e['Menu']['content_type'] . ' data-url=' . $e['Menu']['url'] . '  data-icon="' . $e['Menu']['icon'] . '"   data-login=' . $e['Menu']['login_only'] . '><div class=dd-handle>' . $pg_name . '</div></li>';
                                                                                    }
                                                                                    //delete link
                                                                                    if ($e['Menu']['content_type'] == "category") {
                                                                                        $categoryname = $this->requestAction('category/categorydata/' . $e['Menu']['content_name']);
                                                                                        echo $this->Form->postLink($this->Html->tag('i', '', array('class' => " fa fa-times-circle")), array('action' => 'delete_menu_item',
                                                                                            $e['Menu']['id']), array('escape' => false, 'title' => "Click to delete"), __('Are you sure you want to delete # %s?', $categoryname['label']));
                                                                                    } elseif ($e['Menu']['content_type'] != "page") {
                                                                                        echo $this->Form->postLink($this->Html->tag('i', '', array('class' => " fa fa-times-circle")), array('action' => 'delete_menu_item',
                                                                                            $e['Menu']['id']), array('escape' => false, 'title' => "Click to delete"), __('Are you sure you want to delete # %s?', $e['Menu']['content_name']));
                                                                                    } else {
                                                                                        echo $this->Form->postLink($this->Html->tag('i', '', array('class' => " fa fa-times-circle")), array('action' => 'delete_menu_item',
                                                                                            $e['Menu']['id']), array('escape' => false, 'title' => "Click to delete"), __('Are you sure you want to delete # %s?', $e['Menu']['content_name']));
                                                                                    }
                                                                                    //delete link ends here`
                                                                                    //edit link for custom HTML
                                                                                    if ($e['Menu']['content_type'] == "customHTML") {
                                                                                        echo '&nbsp;&nbsp;' . $this->Html->link(
                                                                                                $this->Html->tag('i', '', array('class' => "fa fa-pencil-square")), array(
                                                                                            'action' => 'edit',
                                                                                            $e['Menu']['id']), array(
                                                                                            'escape' => false,
                                                                                            'title' => "Click to Edit",
                                                                                        ));
                                                                                    }
                                                                                    //end edit link
                                                                                }
                                                                            }
                                                                            echo '</ol>';
                                                                        }

                                                                        echo '</li>';
                                                                    }
                                                                }
                                                                echo '</ol>';
                                                            }

                                                            echo '</li>';
                                                        }
                                                    }
                                                    echo '</ol>';
                                                }

                                                echo '</li>';
                                            }
                                        }
                                        echo '</ol>';
                                    }
                                    echo '</li>';
                                }
                            } // foreach
                        } //if menu header
                        echo '</ol>';
                        echo '</div>';
                        echo '</div>';
                        ?>
                        <!-- menu ends here-->
                        <?php echo $this->Form->create(null, array('url' => array('controller' => 'menus', 'action' => 'save_menu'))); ?>
                        <?php echo $this->Form->hidden('menu_for', array('value' => $menu_for)); ?>
                        <?php echo $this->Form->textarea('nestable-output', array('id' => 'nestable-output')); ?>

                        <?php
                        $options = array(
                            'type' => "submit",
                            'class' => "btn-floating btn-small waves-effect waves-light ",
                        );
                        echo $this->Form->button('Go', $options);
                        echo $this->Form->end();
                        ?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col-8 -->
            <div class="col s12 m4 l4">
                 <div class="card-panel">
                    <div class="box-header">
                        <h4 class="header2">Pages</h4>

                    </div>
                    <div class="box-body">
                        <ul>
                            <?php
                            $a = false;
                            foreach ($pages as $page) {
                                if (!in_array($page, $page_menu_list, false)) {
                                    $pg_name = $this->requestAction(array('controller' => 'menus', 'action' => 'get_page_name', $page));
                                    echo '<li>' . '<input type="checkbox"' . ' class="page-menu-item"' . ' id ="' . $page . '" value="' . $pg_name . '" /><label for="' . $page . '">' . $pg_name . '</label></li>';
                                    $a = true;
                                }
                            }
//echo '<button class=btn-success id=page-add >Go</button>';
                            if ($a) {
                                $options = array(
                                    'type' => "submit",
                                    'class' => "btn-floating btn-small waves-effect waves-light ",
                                    'id' => 'page-add'
                                );
                                echo $this->Form->button('Go', $options);
                            } else {
                                echo "All pages have already been added in menus";
                            }
                            ?>
                        </ul>
                    </div><!-- /.box-body -->
                </div>
                <div class="card-panel">
                    <div class="box-header">
                        <h4 class="header2">Category</h4>
                    </div>
                    <div class="box-body">
                        <ul>
                            <?php
                            $a = false;

                            foreach ($categories as $category) {
                                if (!in_array($category, $category_list, false)) {
                                    echo '<li>' . '<input type="checkbox"' . ' class="category-menu-item"' . ' id ="' . $category['Category']['label'] . '" value="' . $category['Category']['id'] . '" /><label for="' . $category['Category']['label'] . '">' . $category['Category']['label'] . '</label></li>';
                                    echo '<li>';

                                    $a = true;
                                }
                            }
                            if ($a) {
                                $options = array(
                                    'type' => "submit",
                                    'class' => "btn-floating btn-small waves-effect waves-light ",
                                    'id' => 'category-add'
                                );
                                echo $this->Form->button('Go', $options);
                            } else {
                                echo "All Category have already been added in menus";
                            }
                            ?>
                        </ul>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <div class="card-panel">
                    <div class="box-header">
                        <h4 class="header2">Custom URLs</h4>
                    </div>
                    <div class="box-body">
                        <div class="input-field col s12 m6 l6">
                            <?php
                            echo $this->Form->input('custom_name', array(
                                'div' => array('class' => "form-group"),
                                'id' => 'custom-name',
                                'class' => 'form-control',
                                'label' => "Title",
                            ));
                            ?>
                        </div>


                        <div class="input-field col s12 m6 l6"><?php
                            echo $this->Form->input('custom_icon', array(
                                'div' => array('class' => "form-group"),
                                'id' => 'custom-icon',
                                'class' => 'form-control',
                                'label' => "Icon Code (i.e fa fa-circle-o)"));
                            ?><a href="http://fortawesome.github.io/Font-Awesome/icons/#new" target="_blank" class="smlFont" title="Click here to know more about font awesome Icons">Click Here</a></div>
                        <div class="input-field col s12 m12 l12"><?php
                            echo $this->Form->input('custom_url', array(
                                'div' => array('class' => "form-group"),
                                'id' => 'custom-url',
                                'class' => 'form-control',
                                'data-prefix' => '',
                                'label' => "URL"));
                            ?></div>
                            <div class="col s12 m12 l12">
                            <input type="checkbox" name="login_only" class="category-menu-item" id ="login_only" value="1" /><label for="login_only">Only logged in member?</label>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                        <button id="custom-add" class="btn waves-effect waves-light">Add</button>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                <div class="card-panel">
                    <div class="box-header">
                        <h4 class="header2">Custom HTML</h4>
                    </div>
                    <div class="box-body">
                        <div class="input-field col s12 m12 l12">
                            <?php
                            echo $this->Form->input('custom_html_name', array(
                                'div' => array('class' => "form-group"),
                                'id' => 'custom_html_name',
                                'label' => "Title",
                            ));
                            ?></div>
                        <div class="input-field col s12 m12 l12">
                            <?php
                            echo $this->Form->input('custom_html', array('type' => "textarea", 'maxlength' => 200, 'div' => array('class' => "form-group"), 'class' => "materialize-textarea basicEditor",'id'=>'basicEditor', 'id' => "custom_html"));
                            ?></div>
                        <button id="custom-html-add" class="btn waves-effect waves-light">Add</button>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div><!-- /.row -->