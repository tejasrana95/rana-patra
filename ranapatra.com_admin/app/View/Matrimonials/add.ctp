<?php
App::import('Controller', 'UserRoles');
$UserRolesController = new UserRolesController;
?>
<?php echo $this->Html->css(array('imageCrop/imgareaselect-animated')); ?>
<?php echo $this->Html->script(array('imageCrop/jquery.imgareaselect.pack', 'imageCrop/script')); ?>
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li><?php echo $this->Html->link(__($parent_page_name), array('action' => 'index')); ?></li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <?php echo $this->Form->create('Matrimonial', array('type' => 'file')); ?>
            <div class="card-panel">

                <div class="row">

                    <div class="box-body">

                        <div class="row">

                        <h4 class="header col s12 l12 m12">Basic Detail</h4>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('userID', array('options' => $customers,
                                    'empty' => "Select Customer",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'id' => 'userID',
                                    'label' => false, 'onchange' => 'getCustomerDetail()'));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('bride_groom', array('options' => $brideGroom,
                                    'empty' => "Select Bride/Groom",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>

                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('marital_status', array('options' => $marital_status,
                                    'empty' => "Select marital status",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                        </div>
                        <div class="divider"></div>
                        <div class="row">
                          <h4 class="header col s12 l12 m12">Profile Detail</h4>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('first_name', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('father_name', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('last_name', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('mother_name', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('contact', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('email', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                         <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('address', array('div' => array('class' => "form-group"), 'class' => "materialize-textarea"));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('pin_code', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('nationality', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                           
                            
                        </div>
                        <div class="row">
                        
                        <div class="input-field col s12 l4 m4">
                                <?php  echo $this->Form->input('placefinder', array('type' =>'text','div' => array('class' => "form-group"), 'class' => "form-control",'empty' => 'City','title'=>'Type your city name','label'=>'City','id'=>'placefinder')); ?>
                                <div class="display_none">
                                    <?php  echo $this->Form->input('country', array('type' =>'text' ,'id'=>'country','label'=>false)); ?>
                                    <?php  echo $this->Form->input('state', array('type' =>'text','id'=>'state','label'=>false)); ?>
                                    <?php  echo $this->Form->input('city', array('type' =>'text','id'=>'city','label'=>false)); ?>
                                    <?php  echo $this->Form->input('place_id', array('type' =>'text','id'=>'place_id','label'=>false)); ?>
                                </div>
                            </div>
                            
                            
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('dob', array('div' => array('class' => "form-group"), 'type' => "text", 'label' => "Date of Birth", 'class' => "form-control datepicker"));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('tob', array('div' => array('class' => "form-group"), 'type' => "text", 'label' => "Time of Birth (hh:mm:ss) (24 hrs)", 'class' => "form-control timepicker"));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('pob', array('div' => array('class' => "form-group"), 'class' => "form-control",'id'=>'pob'));
                                ?>
                            </div>
                        </div>
                        <div class="divider"></div>
                        <div class="row">
                            <h4 class="header col s12 l12 m12">About me</h4>
                            <div class="input-field col s12 m12">
                                <?php
                                    echo $this->Form->input('bio', array('type' => "textarea", 'rows' => 20, 'label' => false, 'div' => array('class' => "form-group"), 'class' => "form-control minimalEditor",'id'=>'minimalEditor'));
                                    ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m4">
                                <?php
                                echo $this->Form->input('mother_tongue', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                            <div class="input-field col s12 m4">
                                <?php
                                echo $this->Form->input('drink', array('options' => $drink_smoke,
                                    'empty' => "Do you drink?",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                            <div class="input-field col s12 m4">
                                <?php
                                echo $this->Form->input('smoke', array('options' => $drink_smoke,
                                    'empty' => "Do you smoke?",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m6">
                                <?php
                                echo $this->Form->input('complexion', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                            <div class="input-field col s12 m6">
                               <?php
                                echo $this->Form->input('diet', array('options' => $diet,
                                    'empty' => "Select your diet",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                        </div>
                         <div class="divider"></div>
                        <div class="row">
                            <h4 class="header col s12 l12 m12">Detailed Info</h4>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('height', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('weight', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('gautra', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('nakshtra', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('rashi', array('options' => $rashi,
                                    'empty' => "Select Rashi/ Zodiac Sign",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('blood_group', array('options' => $bloodGroup,
                                    'empty' => "Select blood group",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('manglik', array('options' => $yesNo,
                                    'empty' => "Select Manglik",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('education', array('options' => $education,
                                    'empty' => "Select education",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('qualification', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('employment', array('options' => $employement,
                                    'empty' => "Select employment",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('salary', array('options' => $salary,
                                    'empty' => "Select salary",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('native_place', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('father_occupation', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                echo $this->Form->input('mother_occupation', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                            <div class="input-field col s12 l4 m4">
                                <?php
                                $sisterBrother = array();
                                for ($i = 0; $i <= 10; $i++) {
                                    $sisterBrother[$i] = $i;
                                }
                                echo $this->Form->input('brothers', array('options' => $sisterBrother,
                                    'empty' => "Select brothers",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('sisters', array('options' => $sisterBrother,
                                    'empty' => "Select sisters",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('ready_to_go_abroad', array('options' => $yesNo,
                                    'empty' => "Ready to go abroad?",
                                    'div' => array('class' => "form-group"),
                                    'class' => "form-control",
                                    'label' => false,));
                                ?>
                            </div>
                            
                        </div>

                        <div class="row">
                            <div class="input-field col s12 l12 m12">
                                <?php
                                echo $this->Form->input('special_remarks', array('div' => array('class' => "form-group"), 'class' => "materialize-textarea"));
                                ?>
                            </div>

                        </div>
 <div class="row">

                            <div class="switch col s6">
                                Status : 
                                <label>
                                    Off
                                    <?php
                                    echo $this->Form->input('status', array('div' => false, 'label' => false, 'type' => "checkbox", 'class' => ""));
                                    ?>
                                    <span class="lever"></span> On
                                </label>

                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <?php



                                echo $this->Form->input('Save', array('label' => false, 'div' => array('class' => "form-group"), 'type' => "submit", 'class' => "btn btn-success btn-lg"));
                                ?>
                            </div>
                        </div>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div></div>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
        <i class="large mdi-editor-mode-edit"></i>
    </a>
    <ul>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-list')), array('action' => 'index'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
    </ul>
</div>

<script>

    function getCustomerDetail() {
        if ($('#userID').val() != "") {
            $.get('<?php echo Router::url('/', true); ?>customers/getcustomerinfo/' + $('#userID').val() + '/json', function (result) {
                var arr = $.parseJSON(result);

                $('#MatrimonialEmail').val(arr.Customer.email);
                $('#MatrimonialEmail').material_text();
            });
        }
    }
// listen to events on the category dropdown
 function getCountry() {

        $.get('<?php echo Router::url('/', true); ?>common/country/', function (result) {
            $('#country').html(result);
            $('#country').material_select();
        });

    }

    function getstate() {
        // don't do anything if use selects "Select Cat"
        if ($("#country").val() !== "") {
            $.get('<?php echo Router::url('/', true); ?>common/state/' + $('#country').val(), function (result) {
                $('#state').html(result);

                $('#state').material_select();

            });
        }
    }
    function getcity() {
        // don't do anything if use selects "Select Cat"
        if ($("#state").val() !== "") {
            $.get('<?php echo Router::url('/', true); ?>common/city/' + $('#state').val(), function (result) {
                $('#city').html(result);
                $('#city').material_select();
            });
        }
    }

</script>
