<?php
App::import('Controller', 'UserRoles');
$UserRolesController = new UserRolesController;
App::import('Controller', 'Users');
$UsersController = new UsersController;
?>

<?php
echo $this->Form->create('MatrimonialAttachment', array('type' => 'file'));
echo $this->Form->hidden('matrimonial_id', array('value' => $this->request->data['MatrimonialData']['Matrimonial']['id']));
?>
<div class="card-panel">

    <div class="row">

        <div class="box-body">

            <div class="row">

                <h4 class="header col s12 l12 m12">Upload Kundali</h4>
                <div class="input-field col s12 l12 m12">
                    <?php
                    echo $this->Form->input('file_name', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                    ?>
                </div>
                <div class="input-field col s12 l12 m12">
                    <form action="#">
                        <div class="file-field input-field">
                            <div class="btn">
                                <span>Kundali image</span>
                                <?php
                                echo $this->Form->input('file', array('type' => 'file', 'label' => false));
                                echo $this->Form->hidden('file_type', array('value' => 'kundali'));
                                ?>
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>
                    </form>

                </div>

                <div class="input-field col s12 l12 m12">
                    <?php
                    echo $this->Form->input('Save', array('label' => false, 'div' => array('class' => "form-group"), 'type' => "submit", 'class' => "btn btn-success btn-lg"));
                    ?>
                </div>
            </div>
            <div class="divider"></div>
        </div>
    </div>
</div>


<?php echo $this->Form->end(); ?>





<?php if (!empty($this->request->data['MatrimonialAttachment']) && isset($this->request->data['MatrimonialAttachment'])) { ?>
    <table class="striped" >
        <thead>
            <tr>
                <th data-field="id">Name</th>
                <th data-field="name">Image</th>
                <th data-field="price">Action</th>
            </tr>
        </thead>
        <?php
        foreach ($this->request->data['MatrimonialAttachment'] as $matrimonials):
            ?>
            <tbody>
                <tr>
                    <td><?php echo $matrimonials['MatrimonialAttachment']['file_name'] ?></td>
                    <td><?php
                        echo $this->Html->image($site_url . 'app/webroot/public/' . $matrimonials['MatrimonialAttachment']['file_path'], array('alt' => $matrimonials['MatrimonialAttachment']['file_name'], 'width' => '100px'));
                        ?></td>
                    <td>  <?php
                        $is_admin = $UserRolesController->getPermission($adminuserID);
                        if ($is_admin['is_admin'] == 1) {
                            echo $this->Form->postLink(
                                    $this->Html->tag('i', '', array('class' => " fa fa-times-circle")
                                    ), array(
                                'action' => 'deleteMatrimonialAttachment',
                                $matrimonials['MatrimonialAttachment']['id'], $matrimonials['MatrimonialAttachment']['matrimonial_id'], 'addKundali'
                                    ), array(
                                'escape' => false,
                                'title' => "Click to delete",
                                    ), __('Are you sure you want to delete # %s?', $matrimonials['MatrimonialAttachment']['file_name'])
                            );
                        }
                        ?>
                        &nbsp;
                        <?php
                        echo $this->Html->link(
                                $this->Html->tag('i', '', array('class' => "fa fa-search-plus")
                                ), $site_url . 'app/webroot/public/' . $matrimonials['MatrimonialAttachment']['file_path'], array('target' => '_blank', 'escape' => false));
                        ?>
                    </td>
                </tr>
            </tbody>
        <?php endforeach; ?>
    </table>
<?php } else { ?>
    <div class="center">No data found</div>
<?php } ?>
