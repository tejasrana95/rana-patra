<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Careers
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><?php echo $this->Html->link(__('List Career'), array('action' => 'index')); ?></li>
            <li class="active">Edit Career</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Add New Careers'); ?></h3>
                    </div>
                    <?php echo $this->Form->create('Career', array('type' => 'file')); ?>
                    <?php echo $this->Form->input('id'); ?>
                    <div class="box-body">
                        
                        <?php
                        $langaguagename = $this->requestAction('language/getlanguage/' . $this->data['Career']['lang']);
                        echo $this->Form->input('templang', array('div' => array('class' => "form-group"), 'label' => 'Language', 'class' => "form-control", 'empty' => '', "readonly" => "readonly", 'value' => $langaguagename['Language']['lang_name']));
                        echo $this->Form->input('lang', array('div' => array('class' => "form-group"), 'label' => 'Language', 'class' => "form-control", 'empty' => '', "type" => "hidden", 'value' => $this->data['Career']['lang']));
                        ?>                    
                        <?php
                        echo $this->Form->input('position', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                        ?>
                        <?php
                        echo $this->Form->input('total_experiance', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                        ?>
                        <?php
                        echo $this->Form->input('role', array('div' => array('class' => "form-group "), 'class' => "form-control basicEditor",'id'=>'basicEditor', "rows" => 10));
                        ?>
                        <?php
                        echo $this->Form->input('skills', array('div' => array('class' => "form-group "), 'class' => "form-control basicEditor",'id'=>'basicEditor', "rows" => 10));
                        ?>
                        <?php
                        echo $this->Form->input('desired_skills', array('div' => array('class' => "form-group "), 'class' => "form-control basicEditor",'id'=>'basicEditor', "rows" => 10));
                        ?>
                        <?php
                        echo $this->Form->input('mandatory_skills', array('div' => array('class' => "form-group "), 'class' => "form-control basicEditor",'id'=>'basicEditor', "rows" => 10));
                        ?>
                        <?php
                        echo $this->Form->input('qualification', array('div' => array('class' => "form-group "), 'class' => "form-control basicEditor",'id'=>'basicEditor', "rows" => 10));
                        ?>
                        <?php
                        echo $this->Form->input('job_description', array('div' => array('class' => "form-group "), 'class' => "form-control basicEditor",'id'=>'basicEditor', "rows" => 10));
                        ?>
                        <?php
                        echo $this->Form->input('key_task', array('div' => array('class' => "form-group "), 'class' => "form-control basicEditor",'id'=>'basicEditor', "rows" => 10));
                        ?>
                        <?php
                        echo $this->Form->input('key_responsibilities', array('div' => array('class' => "form-group "), 'class' => "form-control basicEditor",'id'=>'basicEditor', "rows" => 10));
                        ?>
                        <?php
                        echo $this->Form->input('job_location', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                        ?>
                        <?php
                        echo $this->Form->input('publish_date', array('type' => 'text', 'div' => array('class' => "form-group"), 'class' => "form-control", "id" => "CareerPublishDate"));
                        ?>
                         <?php if ($roles['career']['review'] != 1 || $is_admin==1) { ?>
                        <div class="form-group">
                            <label>Status</label>
                            <?php
                            echo $this->Form->input('status', array('div' => false, 'label' => false, 'type' => "checkbox", 'class' => "minimal"));
                            ?>
                        </div>
                         <?php } ?>


                        <?php
                        echo $this->Form->input('Save', array('label' => false, 'div' => array('class' => "form-group"), 'type' => "submit", 'class' => "btn btn-success btn-lg"));
                        ?>
                    </div><!-- /.box-body -->
                    <?php echo $this->Form->end(); ?>
                </div><!-- /.box -->
            </div><!-- /.col (left) -->
            <div class="col-md-4">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Actions</h3>
                    </div>
                    <div class="box-body dropdown">
                        <ul >
                            <li><?php echo $this->Html->link(__('List Careers'), array('action' => 'index')); ?></li>
                              <li><a href="<?php echo $site_url; ?>careers/open-positions/?preview=true" target="_blank">Live Preview</a></li>
                        </ul>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col (right) -->
        </div><!-- /.row -->                    
    </section><!-- /.content -->
</aside><!-- /.right-side -->
<?php
echo $this->Html->scriptBlock("
	$(document).ready(function () {
		$('#CareerPublishDate').datepicker({
				format: 'yyyy-mm-dd'
		});
		$('#CareerPublishDate').change(function () {
			$(this).datepicker('hide');
		});
});", array('inline' => false));
echo $this->Html->script(Router::url('/', true).'js/tinymce/tinymce.min.js', array('inline' => false)); 
echo $this->Html->script(Router::url('/', true) . 'filemanager/editorwithfilemanager', array('inline' => false));
?>