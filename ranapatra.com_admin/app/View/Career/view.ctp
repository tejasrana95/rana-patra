<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Career
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><?php echo $this->Html->link(__('Career'), array('controller' => 'career', 'action' => "index")); ?></li>
            <li class="active">View Career</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('View Career'); ?></h3>
                    </div>

                    <div class="box-body">
                        <table class="table table-striped">
                            <tr>
                                <td><?php echo __('Language'); ?></td>
                                <td><?php
                                    $langaguagename = $this->requestAction('language/getlanguage/' . $Career['Career']['lang']);
                                    echo $langaguagename['Language']['lang_name'];
                                    ?></td>
                            </tr>
                            <tr>
                                <td><?php echo __('Position'); ?></td>
                                <td><?php echo $Career['Career']['position']; ?></td>
                            </tr>
                            <tr>
                                <td><?php echo __('Total Experiance'); ?></td>
                                <td><?php echo strip_tags($Career['Career']['total_experiance']); ?></td>
                            </tr>
                            <?php if (isset($Career['Career']['role']) && !empty($Career['Career']['role'])) { ?>
                                <tr>
                                    <td><?php echo __('Role'); ?></td>
                                    <td><?php echo strip_tags($Career['Career']['role']); ?></td>
                                </tr>
                            <?php } ?>
                            <?php if (isset($Career['Career']['skills']) && !empty($Career['Career']['skills'])) { ?>
                                <tr>
                                    <td><?php echo __('Skills'); ?></td>
                                    <td><?php echo strip_tags($Career['Career']['skills']); ?></td>
                                </tr>
                            <?php } ?>
                            <?php if (isset($Career['Career']['desired_skills']) && !empty($Career['Career']['desired_skills'])) { ?>
                                <tr>
                                    <td><?php echo __('Desired Skills'); ?></td>
                                    <td><?php echo strip_tags($Career['Career']['desired_skills']); ?></td>
                                </tr>
                            <?php } ?>
                            <?php if (isset($Career['Career']['mandatory_skills']) && !empty($Career['Career']['mandatory_skills'])) { ?>
                                <tr>
                                    <td><?php echo __('Mandatory Skills'); ?></td>
                                    <td><?php echo strip_tags($Career['Career']['mandatory_skills']); ?></td>
                                </tr>
                            <?php } ?>
                            <?php if (isset($Career['Career']['qualification']) && !empty($Career['Career']['qualification'])) { ?>
                                <tr>
                                    <td><?php echo __('Qualification'); ?></td>
                                    <td><?php echo strip_tags($Career['Career']['qualification']); ?></td>
                                </tr>
                            <?php } ?>
                            <?php if (isset($Career['Career']['job_description']) && !empty($Career['Career']['job_description'])) { ?>
                                <tr>
                                    <td><?php echo __('Job Description'); ?></td>
                                    <td><?php echo strip_tags($Career['Career']['job_description']); ?></td>
                                </tr>
                            <?php } ?>
                            <?php if (isset($Career['Career']['key_task']) && !empty($Career['Career']['key_task'])) { ?>
                                <tr>
                                    <td><?php echo __('Key Task'); ?></td>
                                    <td><?php echo strip_tags($Career['Career']['key_task']); ?></td>
                                </tr>
                            <?php } ?>
                            <?php if (isset($Career['Career']['key_responsibilities']) && !empty($Career['Career']['key_responsibilities'])) { ?>
                                <tr>
                                    <td><?php echo __('Key Responsibilities'); ?></td>
                                    <td><?php echo strip_tags($Career['Career']['key_responsibilities']); ?></td>
                                </tr>
                            <?php } ?>
                            <?php if (isset($Career['Career']['job_location']) && !empty($Career['Career']['job_location'])) { ?>
                                <tr>
                                    <td><?php echo __('Job Location'); ?></td>
                                    <td><?php echo strip_tags($Career['Career']['job_location']); ?></td>
                                </tr>
                            <?php } ?>
                            <?php if ($roles['career']['review'] != 1 || $is_admin == 1) { ?>
                                <tr>
                                    <td><?php echo __('Status'); ?></td>
                                    <td>
                                        <?php
                                        if ($Career['Career']['status'] == NEO_ACTIVE_STATUS) {
                                            echo $this->Form->postLink(
                                                    $this->Html->tag('i', '', array('class' => " fa fa-check")
                                                    ), array(
                                                'action' => 'inactive',
                                                $Career['Career']['lang'], $Career['Career']['id']
                                                    ), array(
                                                'escape' => false,
                                                'title' => "Click to inactive"
                                            ));
                                        } else {
                                            echo $this->Form->postLink(
                                                    $this->Html->tag('i', '', array('class' => " fa fa-times")
                                                    ), array(
                                                'action' => 'active',
                                                $Career['Career']['lang'], $Career['Career']['id']
                                                    ), array(
                                                'escape' => false,
                                                'title' => "Click to active"
                                            ));
                                        }
                                        ?>
                                    </td>

                                </tr>
                            <?php } ?>
                            <tr>
                                <td><?php echo __('Publish Date'); ?></td>
                                <td>
                                    <?php
                                    echo $this->Time->format($Career['Career']['publish_date'], '%B %e, %Y');
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo __('Created'); ?></td>
                                <td>
                                    <?php
                                    echo $this->Time->format($Career['Career']['created'], '%B %e, %Y %H:%M %p');
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo __('Modified'); ?></td>
                                <td>
                                    <?php
                                    echo $this->Time->format($Career['Career']['modified'], '%B %e, %Y %H:%M %p');
                                    ?>
                                </td>
                            </tr>
                        </table>

                    </div><!-- /.box-body -->

                </div><!-- /.box -->
            </div><!-- /.col (left) -->
            <div class="col-md-4">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Actions</h3>
                    </div>
                    <div class="box-body dropdown">
                        <ul>
                            <?php if ($roles['career']['edit'] == 1) { ?>
                                <li><?php echo $this->Html->link(__('Edit Career'), array('action' => 'edit', $Career['Career']['lang'], $Career['Career']['id'])); ?> </li>
                            <?php } ?>
                            <?php if ($roles['career']['delete'] == 1) { ?>
                                <li><?php echo $this->Form->postLink(__('Delete Career'), array('action' => 'delete', $Career['Career']['lang'], $Career['Career']['id']), null, __('Are you sure you want to delete # %s?', $Career['Career']['position'])); ?> </li>
                            <?php } ?>
                            <li><?php echo $this->Html->link(__('List Career'), array('action' => 'index')); ?> </li>
                            <?php if ($roles['career']['add'] == 1) { ?>
                                <li><?php echo $this->Html->link(__('New Career'), array('action' => 'add')); ?> </li>
                            <?php } ?>
                                 <li><a href="<?php echo $site_url; ?>careers/open-positions?preview=true" target="_blank">Live Preview</a></li>
                        </ul>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col (right) -->
        </div><!-- /.row -->                    
    </section><!-- /.content -->
</aside><!-- /.right-side -->
<style>
    .table td{
        word-break:normal !important;
    }
</style>