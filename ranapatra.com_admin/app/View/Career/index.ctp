<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Career
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Career</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Career</h3>
                        <div class="btn-group pull-right">
                            <?php if ($roles['career']['add'] == 1) { ?>
                                <?php echo $this->Html->link(__('New Career'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
                            <?php } ?>
                        </div>                                    
                    </div><!-- /.box-header -->
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <?php foreach ($Langauges as $Language): ?>
                                <li <?php
                                if ($currentlangpage == $Language['Language']['iso_code']) {
                                    echo 'class="active"';
                                }
                                ?>><a href="<?php echo Router::url('/', true) ?>career/index/<?php echo $Language['Language']['iso_code'] ?>" aria-controls="<?php echo $Language['Language']['iso_code'] ?>" ><?php echo $Language['Language']['lang_name'] ?></a></li>
                                <?php endforeach; ?>
                        </ul>

                    </div>
                    <div class="box-body table-responsive">		
                        
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th><?php echo $this->Paginator->sort('id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('position'); ?></th>
                                    <th><?php echo $this->Paginator->sort('total_experiance'); ?></th>
                                    <?php if ($roles['career']['review'] != 1 || $is_admin == 1) { ?>
                                        <th align="center"><?php echo $this->Paginator->sort('status'); ?></th>
                                    <?php } ?>
                                    <th><?php echo $this->Paginator->sort('publish_date'); ?></th>
                                    <th><?php echo $this->Paginator->sort('created'); ?></th>
                                    <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                    <th class="actions"><?php echo __('Actions'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($Careers)) {
                                    foreach ($Careers as $career):
                                        ?>
                                        <tr>
                                            <td><?php echo h($career['Career']['id']); ?>&nbsp;</td>
                                            <td><?php
                                                echo $this->Text->truncate(
                                                        strip_tags($career['Career']['position']), NEO_CONTENT_TRUNCATE_LENGTH, array(
                                                    'ellipsis' => '...',
                                                    'exact' => false,
                                                    'html' => false
                                                        )
                                                );
                                                ?>&nbsp;</td>
                                            <td><?php
                                                echo $this->Text->truncate(
                                                        strip_tags($career['Career']['total_experiance']), NEO_CONTENT_TRUNCATE_LENGTH, array(
                                                    'ellipsis' => '...',
                                                    'exact' => false,
                                                    'html' => false
                                                        )
                                                );
                                                ?>&nbsp;</td>
                                            <?php if ($roles['career']['review'] != 1 || $is_admin == 1) { ?>
                                                <td>

                                                    <?php
                                                    if ($career['Career']['status'] == NEO_ACTIVE_STATUS) {
                                                        echo $this->Form->postLink(
                                                                $this->Html->tag('i', '', array('class' => " fa fa-check")
                                                                ), array(
                                                            'action' => 'inactive',
                                                            $currentlangpage, $career['Career']['id']
                                                                ), array(
                                                            'escape' => false,
                                                            'title' => "Click to inactive"
                                                        ));
                                                    } else {
                                                        echo $this->Form->postLink(
                                                                $this->Html->tag('i', '', array('class' => " fa fa-times")
                                                                ), array(
                                                            'action' => 'active',
                                                            $currentlangpage, $career['Career']['id']
                                                                ), array(
                                                            'escape' => false,
                                                            'title' => "Click to active"
                                                        ));
                                                    }
                                                    ?>

                                                </td>
                                            <?php } ?>
                                            <td>
                                                <?php
                                                echo $this->Time->format($career['Career']['publish_date'], '%B %e, %Y');
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo $this->Time->format($career['Career']['created'], '%B %e, %Y %H:%M %p');
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo $this->Time->format($career['Career']['modified'], '%B %e, %Y %H:%M %p');
                                                ?>
                                            </td>
                                            <td class="actions">
                                                <?php
                                                echo $this->Html->link(
                                                        $this->Html->tag('i', '', array('class' => "fa fa-search-plus")
                                                        ), array('action' => 'view', $currentlangpage, $career['Career']['id']), array(
                                                    'escape' => false,
                                                    'title' => "Click to view"
                                                ));
                                                ?>
                                                <?php if ($roles['career']['edit'] == 1) { ?>
                                                    <?php
                                                    echo $this->Html->link(
                                                            $this->Html->tag('i', '', array('class' => "fa fa-pencil-square")), array(
                                                        'action' => 'edit',
                                                        $currentlangpage, $career['Career']['id']), array(
                                                        'escape' => false,
                                                        'title' => "Click to Edit"
                                                    ));
                                                    ?>
                                                <?php } ?>
                                                <?php if ($roles['career']['delete'] == 1) { ?>
                                                    <?php
                                                    echo $this->Form->postLink(
                                                            $this->Html->tag('i', '', array('class' => " fa fa-times-circle")
                                                            ), array(
                                                        'action' => 'delete',
                                                        $currentlangpage, $career['Career']['id']
                                                            ), array(
                                                        'escape' => false,
                                                        'title' => "Click to delete"
                                                            ), __('Are you sure you want to delete # %s?', $career['Career']['position'])
                                                    );
                                                    ?>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="10" align="center">No records found.</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th><?php echo $this->Paginator->sort('id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('position'); ?></th>
                                    <th><?php echo $this->Paginator->sort('total_experiance'); ?></th>
                                    <?php if ($roles['career']['review'] != 1 || $is_admin == 1) { ?>
                                        <th align="center"><?php echo $this->Paginator->sort('status'); ?></th>
                                    <?php } ?>
                                    <th><?php echo $this->Paginator->sort('publish_date'); ?></th>
                                    <th><?php echo $this->Paginator->sort('created'); ?></th>
                                    <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                    <th class="actions"><?php echo __('Actions'); ?></th>					
                                </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body table-responsive -->
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="paging">
                                <ul class="pagination pagination-sm">
                                    <?php
                                    echo $this->Paginator->prev(
                                            ' << ', array(
                                        'tag' => 'li',
                                        'class' => 'prev',
                                        'escape' => false
                                            ), null, array(
                                        'tag' => 'li',
                                        'escape' => false,
                                        'class' => 'prev disabled',
                                        'disabledTag' => 'a'
                                    ));
                                    ?>
                                    <?php
                                    echo $this->Paginator->numbers(array(
                                        'separator' => '',
                                        'currentClass' => 'active',
                                        'currentTag' => 'a',
                                        'tag' => 'li'
                                    ));
                                    ?>
                                    <?php
                                    echo $this->Paginator->next('>>', array(
                                        'tag' => 'li',
                                        'class' => 'next',
                                        'escape' => false
                                            ), null, array(
                                        'tag' => 'li',
                                        'escape' => false,
                                        'class' => 'next disabled',
                                        'disabledTag' => 'a'
                                    ));
                                    ?>
                                </ul>
                            </div> 
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                        </div>
                    </div><!-- /.col-xs-12 -->
                </div><!-- /.box -->
            </div><!-- /.col-xs-12 -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</aside>