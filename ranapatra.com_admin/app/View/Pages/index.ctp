<?php
App::import('Controller', 'Category');
$categoryController = new CategoryController;
?>
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('id'); ?></th>
                                <th><?php echo $this->Paginator->sort('page_name'); ?></th>
                                <th><?php echo $this->Paginator->sort('page_title'); ?></th>
                                <th><?php echo $this->Paginator->sort('main_category', 'Parent Category'); ?></th>
                                <th><?php echo $this->Paginator->sort('category', 'Sub-Parent'); ?></th>
                                <th><?php echo $this->Paginator->sort('sub_category', 'Child Category'); ?></th>
                                <th><?php echo ('Banner Image'); ?></th>
                                <?php if ($roles['page']['review'] != 1 || $is_admin == 1) { ?>
                                    <th align="center"><?php echo $this->Paginator->sort('status'); ?></th>
                                <?php }
                                ?>
                                <th><?php echo $this->Paginator->sort('created'); ?></th>
                                <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($pages)) {
                                foreach ($pages as $page):
                                    ?>
                                    <tr>
                                        <td><?php echo h($page['Page']['id']); ?>&nbsp;</td>
                                        <td><?php echo h($page['Page']['page_name']); ?>&nbsp;</td>
                                        <td><?php echo h($page['Page']['page_title']); ?>&nbsp;</td>
                                        <td><?php
                                            if ($page['Page']['main_category'] == 0) {
                                                echo 'Top Category';
                                            } else {
                                                $categorydata = $categoryController->getcategorydata($page['Page']['main_category']);
                                                echo $categorydata['Category']['label'];
                                            }
                                            ?>&nbsp;</td>
                                        <td><?php
                                            if (!empty($page['Page']['category']) && isset($page['Page']['category'])) {
                                                $categorydata = $categoryController->getcategorydata($page['Page']['category']);
                                                echo $categorydata['Category']['label'];
                                            } else {
                                                echo '';
                                            }
                                            ?>&nbsp;</td>
                                        <td><?php
                                            if (!empty($page['Page']['sub_category']) && isset($page['Page']['sub_category'])) {
                                                $categorydata = $categoryController->getcategorydata($page['Page']['sub_category']);
                                                echo $categorydata['Category']['label'];
                                            } else {
                                                echo '';
                                            }
                                            ?>&nbsp;</td>
                                        <td>
                                            <?php
                                            $img = $this->requestAction(array(
                                                'controller' => 'pages',
                                                'action' => 'get_file',
                                                $page['Page']['id']));
                                            if (isset($img) && !empty($img['Media']['content'])) {
                                                echo '<img src="' . $img['Media']['content'] . '"  style="max-width:100px;"/>';
                                            }
                                            ?>&nbsp;
                                        </td>
                                        <?php if ($roles['page']['review'] != 1 || $is_admin == 1) {
                                            ?>
                                            <td>
                                                <?php
                                                if ($page['Page']['status'] == INFI_ACTIVE_STATUS) {
                                                    echo $this->Form->postLink(
                                                            $this->Html->tag('i', '', array('class' => " fa fa-check")
                                                            ), array(
                                                        'action' => 'inactive',
                                                        $page['Page']['id'],
                                                            ), array(
                                                        'escape' => false,
                                                        'title' => "Click to inactive",
                                                    ));
                                                } else {
                                                    echo $this->Form->postLink(
                                                            $this->Html->tag('i', '', array('class' => " fa fa-times")
                                                            ), array(
                                                        'action' => 'active',
                                                        $page['Page']['id'],
                                                            ), array(
                                                        'escape' => false,
                                                        'title' => "Click to active",
                                                    ));
                                                }
                                                ?>
                                            </td>
                                        <?php }
                                        ?>
                                        <td>
                                            <?php
                                            echo $this->Time->format($page['Page']['created'], '%B %e, %Y %H:%M %p');
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo $this->Time->format($page['Page']['modified'], '%B %e, %Y %H:%M %p');
                                            ?>
                                        </td>
                                        <td class="actions">
                                            <?php
                                            echo $this->Html->link(
                                                    $this->Html->tag('i', '', array('class' => "fa fa-search-plus")
                                                    ), array('action' => 'view', $page['Page']['id']), array(
                                                'escape' => false,
                                                'title' => "Click to view",
                                            ));
                                            ?>
                                            <?php if ($roles['page']['edit'] == 1) {
                                                ?>
                                                <?php
                                                echo $this->Html->link(
                                                        $this->Html->tag('i', '', array('class' => "fa fa-pencil-square")), array(
                                                    'action' => 'edit',
                                                    $page['Page']['id']), array(
                                                    'escape' => false,
                                                    'title' => "Click to Edit",
                                                ));
                                                ?>
                                            <?php }
                                            ?>
                                            <?php if ($roles['page']['delete'] == 1) {
                                                ?>
                                                <?php
                                                echo $this->Form->postLink(
                                                        $this->Html->tag('i', '', array('class' => " fa fa-times-circle")
                                                        ), array(
                                                    'action' => 'delete',
                                                    $page['Page']['id'],
                                                        ), array(
                                                    'escape' => false,
                                                    'title' => "Click to delete",
                                                        ), __('Are you sure you want to delete # %s?', $page['Page']['page_name'])
                                                );
                                                ?>
                                            <?php }
                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="10" align="center">No records found.</td>
                                </tr>
                            <?php }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th><?php echo $this->Paginator->sort('id'); ?></th>
                                <th><?php echo $this->Paginator->sort('page_name'); ?></th>
                                <th><?php echo $this->Paginator->sort('page_title'); ?></th>
                                <th><?php echo $this->Paginator->sort('main_category', 'Parent Category'); ?></th>
                                <th><?php echo $this->Paginator->sort('category', 'Sub-Parent'); ?></th>
                                <th><?php echo $this->Paginator->sort('sub_category', 'Child Category'); ?></th>
                                <th><?php echo ('Banner Image'); ?></th>
                                <?php if ($roles['page']['review'] != 1 || $is_admin == 1) { ?>
                                    <th align="center"><?php echo $this->Paginator->sort('status'); ?></th>
                                <?php }
                                ?>
                                <th><?php echo $this->Paginator->sort('created'); ?></th>
                                <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body table-responsive -->


            </div>
        </div>
    </div></div>
<div class="row">
    <div class="col s12 m12 l12">

        <div class="paging">
            <ul class="pagination pagination-sm">
                <?php
                echo $this->Paginator->prev(
                        ' << ', array(
                    'tag' => 'li',
                    'class' => 'prev',
                    'escape' => false,
                        ), null, array(
                    'tag' => 'li',
                    'escape' => false,
                    'class' => 'prev disabled',
                    'disabledTag' => 'a',
                ));
                ?>
                <?php
                echo $this->Paginator->numbers(array(
                    'separator' => '',
                    'currentClass' => 'active',
                    'currentTag' => 'a',
                    'tag' => 'li',
                ));
                ?>
                <?php
                echo $this->Paginator->next('>>', array(
                    'tag' => 'li',
                    'class' => 'next',
                    'escape' => false,
                        ), null, array(
                    'tag' => 'li',
                    'escape' => false,
                    'class' => 'next disabled',
                    'disabledTag' => 'a',
                ));
                ?>
            </ul>
        </div>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}'),
        ));
        ?>
    </div>
</div>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating btn-large red', 'escape' => FALSE)); ?>

</div>