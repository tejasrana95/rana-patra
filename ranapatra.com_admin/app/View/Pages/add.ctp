<script>
    function permalinks(id) {
        var str = $('#' + id).val();
        var re = /[^a-z0-9]+/gi; // global and case insensitive matching of non-char/non-numeric
        var re2 = /^-*|-*$/g;     // get rid of any leading/trailing dashes
        str = str.replace(re, '-');  // perform the 1st regexp
        str1 = str.replace(re2, '').toLowerCase(); // ..aaand the second + return lowercased result
        $('#' + id).val(str1);
    }
</script>
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li><?php echo $this->Html->link(__($parent_page_name), array('action' => 'index')); ?></li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Main content -->


<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="row">
                    <div class="input-field col s12">
                        <?php echo $this->Form->create('Page', array('type' => 'file')); ?>
                        <div class="box-body">
                            <div class="row">

                                <div class="input-field col s12 m6 l6">
                                    <?php
                                    $layout = "";

                                    echo $this->Form->input('layout', array('options' => $front_layout,
                                        'empty' => "Select layout",
                                        'div' => array('class' => "form-group"),
                                        'class' => "form-control",
                                        'label' => false));
                                    ?>
                                </div>


                                <div class="input-field col s12 m6 l6">
                                    <?php
                                    $page_name = "";

                                    echo $this->Form->input('page_name', array('div' => array('class' => "form-group"), 'class' => "form-control", 'empty' => '', "value" => $page_name, 'maxlength' => 70, 'label' => 'Page Name (max. 70 character)'));
                                    ?>
                                </div>
                            </div>
                            <div class="row">

                                <div class="input-field col s12 m6 l6">
<?php
echo $this->Form->input('page_heading', array('div' => array('class' => "form-group"), 'class' => "form-control", 'maxlength' => 70, 'label' => 'Page Heading (max. 70 character)'));
?>
                                </div>
                                <div class="input-field col s12 m6 l6">
<?php
echo $this->Form->input('page_slug', array('div' => array('class' => "form-group"), 'onkeyup' => "permalinks('permalink')", 'id' => 'permalink', 'class' => "form-control", 'empty' => '', "label" => "Page Slug (Leave it blank if dont want customize.)"));
?>
                                </div>
                            </div>
                            <div class="row">

                                <div class="input-field col s12 m6 l6">
<?php
$empty = "Select Category";

echo $this->Form->input('main_category', array('options' => "",
    'empty' => $empty,
    'div' => array('class' => "form-group"),
    'class' => "form-control",
    'label' => false,
    'id' => 'category',
    'onchange' => 'getsubparent()'));
?>
                                </div>

                                <div class="input-field col s12 m6 l6">
                                    <?php
                                    $empty = "Select Sub-Category";

                                    echo $this->Form->input('category', array('options' => "",
                                        'empty' => $empty,
                                        'div' => array('class' => "form-group"),
                                        'class' => "form-control",
                                        'label' => false,
                                        'id' => 'Subparent',
                                        'onchange' => 'getsubcate()'));
                                    ?>
                                </div>
                            </div>
                            <div class="row">

                                <div class="input-field col s12 m6 l6">
                                    <?php
                                    $subcate_selected = "";

                                    echo $this->Form->input('sub_category', array(
                                        'options' => "",
                                        'empty' => "Select Parent Category first",
                                        'div' => array('class' => "form-group"),
                                        'class' => "form-control",
                                        'label' => false,
                                        'id' => 'SubCategory',
                                    ));
                                    ?>
                                </div>
                                <div class="input-field col s12 m6 l6">
                                    <?php
                                    echo $this->Form->input('widgets', array(
                                        'options' => "",
                                        'empty' => "Select widgets",
                                        'div' => array('class' => "form-group"),
                                        'class' => "form-control",
                                        'label' => false,
                                        'id' => 'widget', 'multiple' => true));
                                    ?>
                                </div>
                            </div>
                            <div class="row">

                                <div class="input-field col s12 m12 l12">
                                    <?php
                                    echo $this->Form->input('content', array('type' => "textarea", 'rows' => 20, 'label' => false, 'div' => array('class' => "form-group"), 'class' => "form-control standardEditor",'id'=>'standardEditor'));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="input-field col s12 m10 l10">
<?php
echo $this->Form->input('banner_image_file', array('type' => "text", "readonly" => 'readonly', "label" => "Banner", 'id' => 'bannerpath', 'div' => array('class' => "form-group"), 'class' => "form-control"));
?>
                        </div>
                        <div class="input-field col s12 m2 l2">
<?php
echo $this->Form->button('Select Banner', array('type' => "button", 'id' => "selectflag", "onclick" => "openfilemanager('bannerpath')", 'class' => "waves-effect waves-light btn "));
?>
                        </div>
                        <div class="input-field col s12 m12 l12">
                            <div id="bannerpath_wrapper" style="display: none;">
                                <img src="" id="bannerpath_img" style="max-width:300px;">
                                <br/><a href="javascript:void()" onclick="removeimage('bannerpath')">Remove</a>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="input-field col s12">
<?php
echo $this->Form->input('banner_text', array('div' => array('class' => "form-group "), 'class' => "materialize-textarea", "rows" => 5));
?>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.box-body -->
        </div>
        <div class="row">

            <div class="col s12 m12 l12">
                <div class="card-panel">
                    <div class="row">
                        <h4 class="header2"><?php echo __('For SEO'); ?></h4>
                    </div>
                    <div class="box-body">
                        <div class="row">

                            <div class="input-field col s12 m6 l6">
<?php
echo $this->Form->input('page_title', array('div' => array('class' => "form-group"), 'class' => "form-control", 'empty' => ''));
?>
                            </div>


                            <div class="input-field col s12 m6 l6">
                                <?php
                                echo $this->Form->input('page_keywords', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                        </div>
                        <div class="row">

                            <div class="input-field col s12 m6 l6">
                                <?php
                                echo $this->Form->input('page_description', array('type' => 'textarea', 'div' => array('class' => "form-group"), 'class' => "materialize-textarea"));
                                ?>
                            </div>


                            <div class="input-field col s12 m6 l6">
                                <?php
                                echo $this->Form->input('sort_order', array('type' => 'number', 'label' => 'Sort Order (Optional)', 'div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                        </div>
                        <div class="row">

                            <div class="switch col s12">
                                <?php if ($roles['page']['review'] != 1 || $is_admin == 1) {
                                    ?>

                                    Status : 
                                    <label>
                                        Off
                                    <?php
                                    echo $this->Form->input('status', array('div' => false, 'label' => false, 'type' => "checkbox", 'class' => ""));
                                    ?>
                                        <span class="lever"></span> On
                                    </label>




                                    <?php }
                                    ?>
                            </div>
                        </div>
                        <div class="row">

                            <div class="input-field col s12">
                                <?php
                                echo $this->Form->input('Save', array('label' => false, 'div' => array('class' => "form-group"), 'type' => "submit", 'class' => "btn btn-success btn-lg"));
                                ?>
                            </div>
                        </div>
                    </div><!-- /.box -->
                                <?php echo $this->Form->end(); ?>
                </div><!-- /.box-body -->
            </div><!-- /.col (left) -->
        </div>
    </div>  </div>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
        <i class="large mdi-editor-mode-edit"></i>
    </a>
    <ul>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-list')), array('action' => 'index'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
    </ul>
</div>
<script>

    function getparentcate() {

        $.get('<?php echo Router::url('/', true); ?>category/parentcate/', function (result) {
            $('#category').html(result);
            $('#SubCategory').html('<option value="">Select Parent Category first</option>');
            $('#Subparent').html('<option value="">Select Parent Category first</option>');
            $('#category').material_select();
            $('#SubCategory').material_select();
            $('#Subparent').material_select();
        });



        //

        $.get('<?php echo Router::url('/', true); ?>widgets/widgetlist/', function (result) {
            $('#widget').html(result);
            $('#widget').material_select();
        });


    }
    getparentcate();
// listen to events on the category dropdown
    function getsubparent() {
        // don't do anything if use selects "Select Cat"
        if ($("#category").val() !== "") {
            $.get('<?php echo Router::url('/', true); ?>category/subcate/' + $('#category').val(), function (result) {
                $('#Subparent').html(result);

                $('#Subparent').material_select();

            });
        }
    }
    function getsubcate() {
        // don't do anything if use selects "Select Cat"
        if ($("#Subparent").val() !== "") {
            $.get('<?php echo Router::url('/', true); ?>category/subcate/' + $('#Subparent').val(), function (result) {
                $('#SubCategory').html(result);
                $('#SubCategory').material_select();
            });
        }
    }
</script>