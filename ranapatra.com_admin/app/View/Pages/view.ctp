
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li><?php echo $this->Html->link(__($parent_page_name), array('action' => 'index')); ?></li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">


                <div class="box-body">

                    <div class="row">

                        <div class="input-field col s12 m12 l12">
                            <table class="table table-striped">
                                <tr>
                                    <td><?php echo __('Page Name'); ?></td>
                                    <td>
                                        <?php echo h($page['Page']['page_name']); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo __('Page Title'); ?></td>
                                    <td><?php echo h($page['Page']['page_title']); ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo __('Page Keywords'); ?></td>
                                    <td><?php echo h($page['Page']['page_keywords']); ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo __('Content'); ?></td>
                                    <td style="max-width:300px;"><?php echo (strip_tags($page['Page']['content'])); ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo __('Banner Image'); ?></td>
                                    <td><?php
                                        $img = $this->requestAction(array(
                                            'controller' => 'pages',
                                            'action' => 'get_file',
                                            $page['Page']['id']));
                                        $b64Src = $img['Media']['content'];
                                        if (isset($img) && !empty($img['Media']['content'])) {
                                            echo '<img src="' . $b64Src . '" width="100" />';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo __('Front Layout'); ?></td>
                                    <td><?php echo h($page['Page']['layout']); ?></td>
                                </tr>
                                <?php if ($roles['page']['review'] != 1 || $is_admin == 1) { ?>
                                    <tr>
                                        <td><?php echo __('Status'); ?></td>
                                        <td>
                                            <?php
                                            if ($page['Page']['status'] == INFI_ACTIVE_STATUS) {
                                                echo $this->Form->postLink(
                                                        $this->Html->tag('i', '', array('class' => " fa fa-check")
                                                        ), array(
                                                    'action' => 'inactive',
                                                    $page['Page']['id']
                                                        ), array(
                                                    'escape' => false,
                                                    'title' => "Click to inactive"
                                                ));
                                            } else {
                                                echo $this->Form->postLink(
                                                        $this->Html->tag('i', '', array('class' => " fa fa-times")
                                                        ), array(
                                                    'action' => 'active',
                                                    $page['Page']['id']
                                                        ), array(
                                                    'escape' => false,
                                                    'title' => "Click to active"
                                                ));
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td><?php echo __('Created'); ?></td>
                                    <td>
                                        <?php
                                        echo $this->Time->format($page['Page']['created'], '%B %e, %Y %H:%M %p');
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo __('Modified'); ?></td>
                                    <td>
                                        <?php
                                        echo $this->Time->format($page['Page']['modified'], '%B %e, %Y %H:%M %p');
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div><!-- /.box-body -->


                </div><!-- /.col (left) -->
            </div>
        </div>
    </div>
</div>

<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
        <i class="large mdi-editor-mode-edit"></i>
    </a>
    <ul>
        <li><a href="javascript:void();" class="btn-floating red" onclick="otherInfo('Shortcode', '[[PAGE][<?php echo $page['Page']['page_slug']; ?>]]', true)"><i class="large mdi-action-info"></i></a></li>
        <li>
            <?php
            echo $this->Form->postLink(
                    $this->Html->tag('i', '', array('class' => " large mdi-action-delete")
                    ), array(
                'action' => 'delete',
                $page['Page']['id']
                    ), array(
                'class' => 'btn-floating red', 'escape' => FALSE,
                'title' => "Click to delete"
                    ), __('Are you sure you want to delete # %s?', $page['Page']['page_name'])
            );
            ?>

        </li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-list')), array('action' => 'index'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
    </ul>
</div>