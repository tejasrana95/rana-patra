<?php
App::import('Controller', 'UserRoles');
$UserRolesController = new UserRolesController;
App::import('Controller', 'Users');
$UsersController = new UsersController;
?>
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div id="profile-page" class="section">
            <!-- profile-page-header -->
            <div id="profile-page-header" class="card">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" src="images/user-profile-bg.jpg" alt="user background">
                </div>
                <?php if (isset($this->request->data['CustomerProfile']['profilePic']) && !empty($this->request->data['CustomerProfile']['profilePic'])) { ?>
                    <figure class="card-profile-image">
                        <img src="<?php echo $site_url . 'public/' . $this->request->data['CustomerProfile']['profilePic']; ?>" alt="profile image" class="circle z-depth-2 responsive-img activator">
                    </figure>
                <?php }
                ?>

                <div class="card-content">
                    <div class="row">
                        <div class="col s3 offset-s2">
                            <h4 class="card-title grey-text text-darken-4"><?php echo $this->request->data['Customer']['username']; ?></h4>
                            <p class="medium-small grey-text"><a href="mailto:<?php
                                echo $this->request->data['Customer']['email'];
                                ?>"><?php
                                                                     echo $this->request->data['Customer']['email'];
                                                                     ?></a></p>
                        </div>
                        <div class="col s2 center-align">
                            <h2 class="card-title grey-text text-darken-4">City</h2>

                            <p class="medium-small grey-text"><?php echo ($city['Country']['local_name']); ?></p>
                        </div>
                        <div class="col s2 center-align">
                            <h3 class="card-title grey-text text-darken-4">State</h3>

                            <p class="medium-small grey-text"><?php echo ($state['Country']['local_name']); ?></p>

                        </div>
                        <div class="col s2 center-align">
                            <h3 class="card-title grey-text text-darken-4">Country</h3>

                            <p class="medium-small grey-text"><?php echo ($country['Country']['local_name']); ?></p>

                        </div>
                        <div class="col s1 right-align">
                            <a class="btn-floating activator waves-effect waves-light darken-2 right">
                                <?php echo ($this->request->data['Customer']['status']) ? '<i class="mdi-navigation-check"></i>' : '<i class="mdi-navigation-close"></i>'; ?>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-reveal" style="display: none; transform: translateY(0px);">
                    <p>
                        <span class="card-title grey-text text-darken-4"><?php echo ($this->request->data['CustomerProfile']['name']); ?> <i class="mdi-navigation-close right"></i></span>
                        <span><?php echo ($this->request->data['CustomerProfile']['gender']) ? "Male" : "Female"; ?></span>
                    </p>

                    <p><i class="mdi-communication-location-on cyan-text text-darken-2"></i><?php echo ($this->request->data['CustomerProfile']['address']); ?></p>

                    <p><i class="mdi-action-perm-phone-msg cyan-text text-darken-2"></i> <?php echo ($this->request->data['CustomerProfile']['contact']); ?></p>
                    <p><i class='mdi-communication-email cyan-text text-darken-2'></i> <?php echo ($this->request->data['CustomerProfile']['newsLatter']) ? "Yes" : "
No"; ?></p>
                </div>
            </div>
            <!--/ profile-page-header -->

            <!-- profile-page-content -->
        </div>
    </div>
</div>

<div id="basic-form" class="section">
    <div class="row">

        <div class="col s12 m12 l12">
            <div class="">

                <h4 class="header2"><?php echo __("Payment Details"); ?></h4>
            </div>
        </div>
        <div class="">
            <div class="">
                <div class="row">
                    <?php foreach ($CustomerInvoice as $invoice): ?>

                        <div class="col s12 m3 l3 ">
                            <div id="profile-card" class="card hoverable">
                                <div class="card-image waves-effect waves-block waves-light">
                                    <img class="activator" src="images/user-bg.jpg" alt="user background">
                                    <span class="card-title absolute-center activator  <?php echo ($invoice['CustomerInvoice']['payment_status'] == "SUCCESS") ? "green-text" : "red-text"; ?> text-accent-3 center-align">
                                        <?php
                                        echo $invoice['CustomerInvoice']['payment_status'];
                                        ?><br/>
                                        <?php
                                        echo $invoice['CustomerInvoice']['mc_currency'];
                                        ?> <?php echo $invoice['CustomerInvoice']['mc_gross']; ?>
                                    </span>

                                </div>
                                <div class="card-content">
                                    <?php if (isset($this->request->data['CustomerProfile']['profilePic']) && !empty($this->request->data['CustomerProfile']['profilePic'])) { ?>
                                        <img src="<?php echo $site_url . 'public/' . $this->request->data['CustomerProfile']['profilePic']; ?>" alt="" class="circle responsive-img activator card-profile-image">
                                    <?php }
                                    ?>
                                    <a class="btn-floating activator btn-move-up waves-effect waves-light darken-2 right">
                                        <i class="<?php echo ($invoice['CustomerInvoice']['payment_status'] == "SUCCESS") ? "mdi-action-thumb-up" : "mdi-action-thumb-down"; ?>"></i>
                                    </a>

                                    <span class="card-title activator grey-text text-darken-4" title="Item Name"><?php
                                        echo $invoice['CustomerInvoice']['item_name'];
                                        ?></span>
                                    <p><i class="mdi-editor-insert-invitation cyan-text text-darken-2" title="Item Type"></i> <?php
                                        echo $invoice['CustomerInvoice']['created'];
                                        ?></p>

                                    <p><i class="mdi-action-perm-identity cyan-text text-darken-2" title="Item Type"></i> <?php
                                        echo $item_type[$invoice['CustomerInvoice']['item_type']];
                                        ?></p>
                                    <p><i class="mdi-action-assignment cyan-text text-darken-2"></i> <?php
                                        echo $invoice['CustomerInvoice']['item_number'];
                                        ?></p>
                                    <p><i class="mdi-communication-email cyan-text text-darken-2"></i> <?php
                                        echo $invoice['CustomerInvoice']['payer_email'];
                                        ?></p>
                                     
                                    <p><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-print')), array('controller'=>'CustomerInvoices','action' => 'view', $invoice['CustomerInvoice']['id']), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></p>
                                </div>
                                <div class="card-reveal">
                                    <span class="card-title grey-text text-darken-4"><?php
                                        echo $invoice['CustomerInvoice']['item_name'];
                                        ?> <i class="mdi-navigation-close right"></i></span>
                                    <p><i class="mdi-action-add-shopping-cart cyan-text text-darken-2"></i> <?php
                                        echo $invoice['CustomerInvoice']['txn_id'];
                                        ?></p>
                                    <p><i class="mdi-action-account-balance-wallet cyan-text text-darken-2"></i> <?php
                                        echo $invoice['CustomerInvoice']['payment_gateway'];
                                        ?></p>
                                    <p><i class="mdi-communication-quick-contacts-mail cyan-text text-darken-2"></i> <?php
                                        echo $invoice['CustomerInvoice']['receiver_email'];
                                        ?></p>
                                    <p><i class="mdi-social-person-outline cyan-text text-darken-2"></i> <?php
                                        echo $process_by[$invoice['CustomerInvoice']['process_by']];
                                        ?> (
                                        <?php
                                        if ($invoice['CustomerInvoice']['process_by'] == 1) {

                                            $getUserInfo = $UsersController->getUserInfo($invoice['CustomerInvoice']['system_id']);
                                            echo $getUserInfo['User']['username'];
                                        } else {
                                            echo $process_by[$invoice['CustomerInvoice']['process_by']];
                                        }
                                        ?>)</p>

                                    <p><i class="mdi-action-event cyan-text text-darken-2"></i> <?php
                                        echo $invoice['CustomerInvoice']['created'];
                                        ?></p>
                                    <p><i class="mdi-editor-insert-invitation cyan-text text-darken-2"></i> <?php
                                        echo $invoice['CustomerInvoice']['modified'];
                                        ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div><!-- /.box -->
            </div>
        </div>
    </div>
</div>

<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
        <i class="large mdi-editor-mode-edit"></i>
    </a>
    <ul>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-list')), array('action' => 'index'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-border-color')), array('action' => 'edit', $this->request->data['Customer']['id']), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
    </ul>
</div>
