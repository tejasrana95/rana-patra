<?php
App::import('Controller', 'UserRoles');
$UserRolesController = new UserRolesController;
?>
<?php echo $this->Html->css(array('imageCrop/imgareaselect-animated')); ?>
<?php echo $this->Html->script(array('imageCrop/jquery.imgareaselect.pack', 'imageCrop/script')); ?>
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <?php echo $this->Form->create('Customer', array('type' => 'file')); ?>
            <div class="card-panel">
                <h4 class="header2"><?php echo __("Basic Details"); ?></h4>
                <div class="row">

                    <div class="box-body">

                        <div class="row">

                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('Customer.username', array('div' => array('class' => "form-group"), 'class' => "form-control",));
                                ?>
                            </div>
                             <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('Customer.email', array('div' => array('class' => "form-group"), 'class' => "form-control",));
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('Customer.password', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                            <div class="input-field col s12 l6 m6">
                                <?php
                                echo $this->Form->input('Customer.confirm_password', array('type' => "password", 'div' => array('class' => "form-group"), 'class' => "form-control"));
                                ?>
                            </div>
                        </div>
                        <div class="row">

                            <div class="switch col s6">
                                Status : 
                                <label>
                                    Off
                                    <?php
                                    echo $this->Form->input('Customer.status', array('div' => false, 'label' => false, 'type' => "checkbox", 'class' => ""));
                                    ?>
                                    <span class="lever"></span> On
                                </label>

                            </div>
                        </div>

                    </div><!-- /.box-body -->

                </div><!-- /.box -->
            </div>
            <div class="card-panel">
                <h4 class="header2"><?php echo __("Profile Details"); ?></h4>
                <div class="row">

                    <div class="box-body">

                        <div class="row">

                            <div class="input-field col s12 m6 l6">
                                <?php
                                echo $this->Form->input('CustomerProfile.name', array('div' => array('class' => "form-group"), 'class' => "form-control", 'empty' => ''));
                                ?>
                            </div>
                            <div class="input-field col s12 m6 l6">
                                <?php
                                echo $this->Form->input('CustomerProfile.gender', array(
                                    "type" => "radio",
                                    'separator' => '&nbsp;&nbsp;&nbsp;',
                                    'options' => array(0 => "Female", 1 => "Male")
                                ));
                                ?>
                            </div>
                        </div>
                        <div class="row">

                            <div class="input-field col s12">
                                <?php
                                echo $this->Form->input('CustomerProfile.address', array('div' => array('class' => "form-group"), 'type' => "textarea", 'class' => "materialize-textarea"));
                                ?>

                            </div>
                        </div>
                        <div class="row">
                        
                        <div class="input-field col s12 m4 l4">
                            <?php  echo $this->Form->input('CustomerProfile.placefinder', array('type' =>'text','div' => array('class' => "form-group"), 'class' => "form-control",'empty' => 'City','title'=>'Type your city name','label'=>'City','id'=>'placefinder')); ?>
                            <div class="display_none">
                                <?php  echo $this->Form->input('CustomerProfile.country', array('type' =>'text' ,'id'=>'country','label'=>false)); ?>
                                <?php  echo $this->Form->input('CustomerProfile.state', array('type' =>'text','id'=>'state','label'=>false)); ?>
                                <?php  echo $this->Form->input('CustomerProfile.city', array('type' =>'text','id'=>'city','label'=>false)); ?>
                                <?php  echo $this->Form->input('CustomerProfile.place_id', array('type' =>'text','id'=>'place_id','label'=>false)); ?>
                            </div>
                        </div>
                    </div>
                        <div class="row">

                            <div class="switch col s12 l4 m4">
                                Newslatter : 
                                <label>
                                    No
                                    <?php
                                    echo $this->Form->input('CustomerProfile.newsLatter', array('div' => false, 'label' => false, 'type' => "checkbox", 'class' => ""));
                                    ?>
                                    <span class="lever"></span> Yes
                                </label>

                            </div>
                            <div class="input-field col s12 m8 l8">
                                <?php
                                echo $this->Form->input('CustomerProfile.contact', array('div' => array('class' => "form-group"), 'class' => "form-control", 'empty' => ''));
                                ?>
                            </div>
                        </div>
                        <div class="row">

                            <div class="input-field file-field  col s12 m12 l12">
                                <?php
                                echo $this->Form->input('other.profile_pic', array('div' => array('class' => "btn btn-lg"), 'between' => '<span >Profile Pic</span>', 'type' => "file", 'label' => false));
                                ?>
 <?php
                                echo $this->Form->input('other.tempUrl', array('div' => array('class' => "file-path-wrapper"), 'class' => "file-path validate", 'label'=>false));
                                ?>
                               

                            </div>
                        </div>
                        <div class="row">
                            <div class="" style="display: none">
                                 <?php
                                echo $this->Form->input('cropImage.x', array('id' => "x", 'type' => 'text'));
                                 echo $this->Form->input('cropImage.y', array('id' => "y", 'type' => 'text'));
                                  echo $this->Form->input('cropImage.w', array('id' => "w", 'type' => 'text'));
                                   echo $this->Form->input('cropImage.h', array('id' => "h", 'type' => 'text'));
                                ?>
                            </div>
                            <img id="uploadPreview" style="display:none;"/>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <?php
                                echo $this->Form->input('Save', array('label' => false, 'div' => array('class' => "form-group"), 'type' => "submit", 'class' => "btn btn-success btn-lg"));
                                ?>
                            </div>
                        </div>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div></div>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
        <i class="large mdi-editor-mode-edit"></i>
    </a>
    <ul>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-list')), array('action' => 'index'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
    </ul>
</div>

<script>

    function getCountry() {

        $.get('<?php echo Router::url('/', true); ?>common/country/', function (result) {
            $('#country').html(result);
            $('#country').material_select();
        });

    }
// listen to events on the category dropdown
    function getstate() {
        // don't do anything if use selects "Select Cat"
        if ($("#country").val() !== "") {
            $.get('<?php echo Router::url('/', true); ?>common/state/' + $('#country').val(), function (result) {
                $('#state').html(result);

                $('#state').material_select();

            });
        }
    }
    function getcity() {
        // don't do anything if use selects "Select Cat"
        if ($("#state").val() !== "") {
            $.get('<?php echo Router::url('/', true); ?>common/city/' + $('#state').val(), function (result) {
                $('#city').html(result);
                $('#city').material_select();
            });
        }
    }

</script>
<script type="text/javascript">


</script>