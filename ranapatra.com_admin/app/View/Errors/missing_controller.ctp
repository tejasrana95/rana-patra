<div class="row">
	<div class="col s12">
		<div class="browser-window">
			<div class="top-bar">
				<div class="circles">
					<div class="circle" id="close-circle">
					</div>
					<div class="circle" id="minimize-circle">
					</div>
					<div class="circle" id="maximize-circle">
					</div>
				</div>
			</div>
			<div class="content">
				<div class="row">
					<div class="col s12" id="site-layout-example-top">
						<p class="flat-text-logo center white-text caption-uppercase">
							<?php echo $message; ?>
						</p>
					</div>
					<div class="col s12 m12 l12" id="site-layout-example-right">
						<div class="row center">
							<p class="text-shadow col s12">
								<?php if ($message != "The request has been black-holed") {
									?>
									<?php echo __d('cake', 'Error'); ?>
									:
									<?php printf(__d('cake', 'The requested address %s was not found on this server.'),"'{$url}'");?>
									<?php } ?>
								</p>
								<div class="debug">
									<?php
									if (Configure::read('debug') > 0):
										echo $this->element('exception_stack_trace');
									endif;
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<p class="center s12">
	<a class="btn waves-effect waves-light" href="javascript:void(1);" onclick="reloadthispage()">Refresh this page</a>
</p>
<script>
	function reloadthispage(){
		window.location.href=window.location;
	}
</script>