<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @package       app.View.Errors
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         CakePHP(tm) v 0.10.0.1076
 */

?>
                    <div class="">
                        <div class="col s12 m6 l3">

                                <div class="card-content  green white-text">
                                    <p class="card-stats-title"><?php echo $message; ?></p>
                                   <p class="error">
<?php if ($message != "The request has been black-holed") {
    ?>
	<strong><?php echo __d('cake', 'Error'); ?>: </strong>
	<?php printf(
        __d('cake', 'The requested address %s was not found on this server.'),
        "<strong>'{$url}'</strong>"
    );?>
	<?php } else {?>
	<strong>Error:</strong> form is not submitted due to following reasons.
	<ul>
		<li>You go back to previous form.</li>
		<li>You have reload the form.</li>
		<li>You stay on this form for long time.</li>
	</ul>
	<p><a href="javascript:void(1);" onclick="reloadthispage()">Refresh this page</a></p>
	<script>
	function reloadthispage(){
	window.location.href=window.location;
	}
	</script>
	<?php }
?>
</p>
<?php if ($message != "The request has been black-holed") {
    ?>
<?php
if (Configure::read('debug') > 0):
        echo $this->element('exception_stack_trace');
    endif;
    ?>
<?php }
?>
                                </div>


                        </div>
                    </div>
