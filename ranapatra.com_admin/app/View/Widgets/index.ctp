<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Main content -->

        <div id="basic-form" class="section">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card-panel">
                        <h4 class="header2"><?php echo __($page_name); ?></h4>

                        <div class="row">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->Paginator->sort('id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('name'); ?></th>
                                        <th><?php echo $this->Paginator->sort('value'); ?></th>
                                        <th align="center"><?php echo $this->Paginator->sort('status'); ?></th>
                                        <th><?php echo $this->Paginator->sort('created'); ?></th>
                                        <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                        <th class="actions"><?php echo __('Actions'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($widgets)) {
                                        foreach ($widgets as $widget):
                                            ?>
                                            <tr>
                                                <td><?php echo h($widget['Widget']['id']); ?>&nbsp;</td>
                                                <td><?php echo h($widget['Widget']['name']); ?>&nbsp;</td>
                                                <td><?php
                                                    echo $this->Text->truncate(
                                                            strip_tags($widget['Widget']['value']), INFI_CONTENT_TRUNCATE_LENGTH, array(
                                                        'ellipsis' => '...',
                                                        'exact' => false,
                                                        'html' => false
                                                            )
                                                    );
                                                    ?></td>
                                                <td>
                                                    <?php
                                                    if ($widget['Widget']['status'] == INFI_ACTIVE_STATUS) {
                                                        echo $this->Form->postLink(
                                                                $this->Html->tag('i', '', array('class' => " fa fa-check")
                                                                ), array(
                                                            'action' => 'inactive',
                                                            $widget['Widget']['id']
                                                                ), array(
                                                            'escape' => false,
                                                            'title' => "Click to inactive"
                                                        ));
                                                    } else {
                                                        echo $this->Form->postLink(
                                                                $this->Html->tag('i', '', array('class' => " fa fa-times")
                                                                ), array(
                                                            'action' => 'active',
                                                            $widget['Widget']['id']
                                                                ), array(
                                                            'escape' => false,
                                                            'title' => "Click to active"
                                                        ));
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo $this->Time->format($widget['Widget']['created'], '%B %e, %Y %H:%M %p');
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo $this->Time->format($widget['Widget']['modified'], '%B %e, %Y %H:%M %p');
                                                    ?>
                                                </td>
                                                <td class="actions">
                                                    <?php
                                                    echo $this->Html->link(
                                                            $this->Html->tag('i', '', array('class' => "fa fa-search-plus")
                                                            ), array('action' => 'view', $widget['Widget']['id']), array(
                                                        'escape' => false,
                                                        'title' => "Click to view"
                                                    ));
                                                    ?>
                                                    <?php
                                                    echo $this->Html->link(
                                                            $this->Html->tag('i', '', array('class' => "fa fa-pencil-square")), array(
                                                        'action' => 'edit',
                                                        $widget['Widget']['id']), array(
                                                        'escape' => false,
                                                        'title' => "Click to Edit"
                                                    ));
                                                    ?>
                                                    <?php
                                                    echo $this->Form->postLink(
                                                            $this->Html->tag('i', '', array('class' => " fa fa-times-circle")
                                                            ), array(
                                                        'action' => 'delete',
                                                        $widget['Widget']['id']
                                                            ), array(
                                                        'escape' => false,
                                                        'title' => "Click to delete"
                                                            ), __('Are you sure you want to delete # %s?', $widget['Widget']['name'])
                                                    );
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php } else { ?>
                                        <tr>
                                            <td colspan="10" align="center">No records found.</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th><?php echo $this->Paginator->sort('id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('name'); ?></th>
                                        <th><?php echo $this->Paginator->sort('value'); ?></th>
                                        <th align="center"><?php echo $this->Paginator->sort('status'); ?></th>
                                        <th><?php echo $this->Paginator->sort('created'); ?></th>
                                        <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                        <th class="actions"><?php echo __('Actions'); ?></th>
                                    </tr>
                                </tfoot>
                            </table>

                            <div class="col s12 m12 l12">
                                <div class="row">
                                    <div class="paging">
                                        <ul class="pagination pagination-sm">
                                            <?php
                                            echo $this->Paginator->prev(
                                                    ' << ', array(
                                                'tag' => 'li',
                                                'class' => 'prev',
                                                'escape' => false
                                                    ), null, array(
                                                'tag' => 'li',
                                                'escape' => false,
                                                'class' => 'prev disabled',
                                                'disabledTag' => 'a'
                                            ));
                                            ?>
                                            <?php
                                            echo $this->Paginator->numbers(array(
                                                'separator' => '',
                                                'currentClass' => 'active',
                                                'currentTag' => 'a',
                                                'tag' => 'li'
                                            ));
                                            ?>
                                            <?php
                                            echo $this->Paginator->next('>>', array(
                                                'tag' => 'li',
                                                'class' => 'next',
                                                'escape' => false
                                                    ), null, array(
                                                'tag' => 'li',
                                                'escape' => false,
                                                'class' => 'next disabled',
                                                'disabledTag' => 'a'
                                            ));
                                            ?>
                                        </ul>
                                    </div> 
                                    <?php
                                    echo $this->Paginator->counter(array(
                                        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                    ));
                                    ?>
                                </div>
                            </div>
                            <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
                                <?php echo $this->Html->link($this->Html->tag( 'i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating btn-large red','escape' => FALSE)); ?>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<style>
    table img {
        max-width:100px
    }
</style>