<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

        <!--Basic Form-->
        <div id="basic-form" class="section">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card-panel">
                        <h4 class="header2"><?php echo __('Add/Update ' . $page_name); ?></h4>
                        <div class="row">
                            <?php echo $this->Form->create('Option', array('type' => 'file', 'class' => 'col s12')); ?>
                            <?php
                            foreach ($this->request->data as $k => $data) {
                                if ($data['Option']['field_type'] == 'editor') {
                                    echo $this->Form->input('Option.' . $k . '.meta_value', array(
                                        'div' => array('class' => "form-group"),
                                        'class' => "form-control basicEditor",'id'=>'basicEditor',
                                        'type' => "textarea",
                                        'label' => Inflector::humanize(h($data['Option']['meta_key'])),
                                        'value' => $data['Option']['meta_value']));
                                    ?>
                                    <?php
                                } elseif ($data['Option']['field_type'] == 'readonly') {
                                    echo $this->Form->input('Option.' . $k . '.meta_value', array(
                                        'div' => array('class' => "form-group"),
                                        'class' => "form-control",
                                        'type' => "text",
                                        'readonly' => "readonly",
                                        'label' => Inflector::humanize(h($data['Option']['meta_key'])),
                                        'value' => $data['Option']['meta_value']));
                                } else {
                                    echo $this->Form->input('Option.' . $k . '.meta_value', array(
                                        'div' => array('class' => "form-group"),
                                        'class' => "form-control",
                                        'type' => "text",
                                        'label' => Inflector::humanize(h($data['Option']['meta_key'])),
                                        'value' => $data['Option']['meta_value']));
                                }
                                echo $this->Form->hidden('Option.' . $k . '.id', array(
                                    'type' => 'hidden',
                                    'value' => $data['Option']['id'],
                                ));
                            }
                            ?>

                            <div class="row">
                                <div class="input-field col s12">
                                    <?php
                                    echo $this->Form->input('Save', array('label' => false, 'div' => array('class' => "form-group"), 'type' => "submit", 'class' => "btn cyan btn-lg  right"));
                                    ?>
                                </div>
                            </div>
                            <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>