<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li><?php echo $this->Html->link(__($parent_page_name), array('action' => 'index')); ?></li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">


                <div class="box-body">

                    <div class="row">

                        <div class="input-field col s12 m12 l12">
                            <table class="table table-striped">
                                <tr>
                                    <td><?php echo __('Request Type'); ?></td>
                                    <td><?php
                                        if ($adminrequests['AdminRequest']['request_type'] == '1') {
                                            echo 'Delete Request';
                                        } elseif ($adminrequests['AdminRequest']['request_type'] == '2') {
                                            echo 'Edit Request';
                                        } elseif ($adminrequests['AdminRequest']['request_type'] == '3') {
                                            echo 'Approval Request';
                                        }
                                        ?></td>
                                    </tr>

                                    <tr>
                                        <td><?php echo __('Message'); ?></td>
                                        <td style="max-width:300px;"><?php echo $adminrequests['AdminRequest']['message']; ?></td>
                                    </tr>

                                    <tr>
                                        <td><?php echo __('Status'); ?></td>
                                        <td>
                                            <?php
                                            if ($adminrequests['AdminRequest']['proceed'] == '0') {
                                                echo 'Pending';
                                            } elseif ($adminrequests['AdminRequest']['proceed'] == '1') {
                                                echo 'Reject';
                                            } elseif ($adminrequests['AdminRequest']['proceed'] == '2') {
                                                echo 'Approve';
                                            }
                                            ?>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td><?php echo __('Created'); ?></td>
                                        <td>
                                            <?php
                                            echo $this->Time->format($adminrequests['AdminRequest']['created'], '%B %e, %Y %H:%M %p');
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __('Modified'); ?></td>
                                        <td>
                                            <?php
                                            echo $this->Time->format($adminrequests['AdminRequest']['modified'], '%B %e, %Y %H:%M %p');
                                            ?>
                                        </td>
                                    </tr>
                                </table>

                            </div><!-- /.box-body -->

                        </div><!-- /.box -->
                    </div><!-- /.col (left) -->
                </div>
            </div>
        </div>
    </div>





    <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
        <a class="btn-floating btn-large red">
            <i class="large mdi-editor-mode-edit"></i>
        </a>
        <ul>
            <li>
                <?php
                echo $this->Form->postLink(
                    $this->Html->tag('i', '', array('class' => " large mdi-action-delete")
                        ), array(
                        'action' => 'delete',
                        $adminrequests['AdminRequest']['id']
                        ), array(
                        'class' => 'btn-floating red', 'escape' => FALSE,
                        'title' => "Click to delete"
                        ), __('Are you sure you want to delete # %s?',$adminrequests['AdminRequest']['id'])
                        );
                        ?>

                    </li>
                    <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-list')), array('action' => 'index'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
                    <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
                    <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-edit')), array('action' => 'edit',$adminrequests['AdminRequest']['id']), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
                    <?php if ($is_admin != 1) { ?>
                    <li><?php echo $this->Html->link(__('New Admin Request'), array('action' => 'add') ,array('class' => 'btn-floating red', 'escape' => FALSE)); ?> </li>
                    <?php } ?>
                </ul>
            </div>
