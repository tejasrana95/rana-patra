<?php
App::import('Controller', 'Users');
$UsersController = new UsersController();
?>
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!--Basic Form-->
<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('id'); ?></th>
                                <th><?php echo $this->Paginator->sort('request_type'); ?></th>
                                <th><?php echo $this->Paginator->sort('message'); ?></th>
                                <th><?php echo $this->Paginator->sort('proceed', 'Status'); ?></th>
                                <th><?php echo $this->Paginator->sort('userID', 'Request by'); ?></th>
                                <th><?php echo $this->Paginator->sort('created'); ?></th>
                                <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($admin_requests)) {

                                foreach ($admin_requests as $admin_request):
                                    ?>
                                <tr>
                                    <td>
                                        <?php echo h($admin_request['AdminRequest']['id']); ?>&nbsp;
                                    </td>
                                    <td><?php
                                        if ($admin_request['AdminRequest']['request_type'] == '1') {
                                            echo 'Delete Request';
                                        } elseif ($admin_request['AdminRequest']['request_type'] == '2') {
                                            echo 'Edit Request';
                                        } elseif ($admin_request['AdminRequest']['request_type'] == '3') {
                                            echo 'Approval Request';
                                        }
                                        ?>&nbsp;</td>
                                        <td style="max-width: 300px;"><?php echo $admin_request['AdminRequest']['message']; ?></td>
                                        <td>
                                            <?php
                                            if ($admin_request['AdminRequest']['proceed'] == '0') {
                                                echo 'Pending';
                                            } elseif ($admin_request['AdminRequest']['proceed'] == '1') {
                                                echo 'Reject';
                                            } elseif ($admin_request['AdminRequest']['proceed'] == '2') {
                                                echo 'Approve';
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $userinfo = $UsersController->getUserInfo($admin_request['AdminRequest']['userID']);
                                            echo $userinfo['User']['username'];
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo $this->Time->format($admin_request['AdminRequest']['created'], '%B %e, %Y %H:%M %p');
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo $this->Time->format($admin_request['AdminRequest']['modified'], '%B %e, %Y %H:%M %p');
                                            ?>
                                        </td>
                                        <td class="actions">
                                            <?php
                                            echo $this->Html->link(
                                                $this->Html->tag('i', '', array('class' => "fa fa-search-plus")
                                                    ), array('action' => 'view', $admin_request['AdminRequest']['id']), array(
                                                    'escape' => false,
                                                    'title' => "Click to view"
                                                    ));
                                            echo '&nbsp;';
                                            if ($is_admin == 1) { 
                                                echo $this->Html->link(
                                                    $this->Html->tag('i', '', array('class' => "fa fa-pencil-square")), array(
                                                        'action' => 'edit',
                                                        $admin_request['AdminRequest']['id']), array(
                                                        'escape' => false,
                                                        'title' => "Click to Edit"
                                                        ));
                                                echo '&nbsp;';
                                            }  if ($is_admin == 1) { 
                                                echo $this->Form->postLink(
                                                    $this->Html->tag('i', '', array('class' => " fa fa-times-circle")
                                                        ), array(
                                                        'action' => 'delete',
                                                        $admin_request['AdminRequest']['id']
                                                        ), array(
                                                        'escape' => false,
                                                        'title' => "Click to delete"
                                                        ), __('Are you sure you want to delete this request?')
                                                        );
                                                echo '&nbsp;';
                                            } 
                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach; } else { ?>
                                <tr>
                                    <td colspan="10" align="center">No records found.</td>
                                </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th><?php echo $this->Paginator->sort('id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('request_type'); ?></th>
                                    <th><?php echo $this->Paginator->sort('message'); ?></th>
                                    <th><?php echo $this->Paginator->sort('proceed', 'Status'); ?></th>
                                    <th><?php echo $this->Paginator->sort('userID', 'Request by'); ?></th>
                                    <th><?php echo $this->Paginator->sort('created'); ?></th>
                                    <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                    <th class="actions"><?php echo __('Actions'); ?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body table-responsive -->

                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div class="paging">
                                <ul class="pagination pagination-sm">
                                    <?php
                                    echo $this->Paginator->prev(
                                        ' << ', array(
                                            'tag' => 'li',
                                            'class' => 'prev',
                                            'escape' => false
                                            ), null, array(
                                            'tag' => 'li',
                                            'escape' => false,
                                            'class' => 'prev disabled',
                                            'disabledTag' => 'a'
                                            ));

                                    echo $this->Paginator->numbers(array(
                                        'separator' => '',
                                        'currentClass' => 'active',
                                        'currentTag' => 'a',
                                        'tag' => 'li'
                                        ));

                                    echo $this->Paginator->next('>>', array(
                                        'tag' => 'li',
                                        'class' => 'next',
                                        'escape' => false
                                        ), null, array(
                                        'tag' => 'li',
                                        'escape' => false,
                                        'class' => 'next disabled',
                                        'disabledTag' => 'a'
                                        ));
                                        ?>
                                    </ul>

                                    <?php
                                    echo $this->Paginator->counter(array(
                                        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                        ));
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
                                <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating btn-large red', 'escape' => FALSE)); ?>
                            </div>

