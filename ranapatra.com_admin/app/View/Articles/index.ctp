<?php
App::import('Controller', 'ArticleCategory');
$articlecategoryController = new ArticleCategoryController;
?>
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $article_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li class="active"><?php echo $article_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('id'); ?></th>
                                <th><?php echo $this->Paginator->sort('article_name'); ?></th>
                                <th><?php echo $this->Paginator->sort('main_category', 'Parent Article Category'); ?></th>
                                <th><?php echo $this->Paginator->sort('category', 'Sub-Parent'); ?></th>
                                <th><?php echo $this->Paginator->sort('sub_category', 'Child Article Category'); ?></th>
                                <th><?php echo $this->Paginator->sort('isAdmin', 'Is Admin?'); ?></th>
                                <th><?php echo $this->Paginator->sort('userID','User Name'); ?></th>
                                <?php if ($roles['article']['review'] != 1 || $is_admin == 1) { ?>
                                <th align="center"><?php echo $this->Paginator->sort('status'); ?></th>
                                <?php }
                                ?>
                                <?php if ($roles['article']['review'] != 1 || $is_admin == 1) { ?>
                                <th align="center"><?php echo $this->Paginator->sort('featured'); ?></th>
                                <?php }
                                ?>
                                <th><?php echo $this->Paginator->sort('created'); ?></th>
                                <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($articles)) {
                                foreach ($articles as $article):
                                    ?>
                                <tr>
                                    <td><?php echo h($article['Article']['id']); ?>&nbsp;</td>
                                    <td><?php echo h($article['Article']['article_name']); ?>&nbsp;</td>

                                    <td><?php
                                        if ($article['Article']['main_category'] == 0) {
                                            echo 'Top Article Category';
                                        } else {
                                            $articlecategorydata = $articlecategoryController->getarticlecategorydata($article['Article']['main_category']);
                                            echo $articlecategorydata['ArticleCategory']['label'];
                                        }
                                        ?>&nbsp;</td>
                                        <td><?php
                                            if (!empty($article['Article']['category']) && isset($article['Article']['category'])) {
                                                $articlecategorydata = $articlecategoryController->getarticlecategorydata($article['Article']['category']);
                                                echo $articlecategorydata['ArticleCategory']['label'];
                                            } else {
                                                echo '';
                                            }
                                            ?>&nbsp;</td>
                                            <td><?php
                                                if (!empty($article['Article']['sub_category']) && isset($article['Article']['sub_category'])) {
                                                    $articlecategorydata = $articlecategoryController->getarticlecategorydata($article['Article']['sub_category']);
                                                    echo $articlecategorydata['ArticleCategory']['label'];
                                                } else {
                                                    echo '';
                                                }
                                                ?>&nbsp;</td>
                                                <td>
                                                    <?php
                                                    if ($article['Article']['isAdmin'] == 0) {
                                                        echo 'No';
                                                    } else {
                                                        echo 'Yes';
                                                    }
                                                    ?> &nbsp;
                                                </td>
                                                <td><?php
                                                    if ($article['Article']['isAdmin'] == 0) {
                                                        echo 'No';
                                                    } else {
                                                        $userInfo = $this->requestAction('users/getUserInfo/' . $article['Article']['userID']);
                                                        print ($userInfo['User']['username']);
                                                    }
                                                    ?>&nbsp;</td>
                                                    <?php if ($roles['article']['review'] != 1 || $is_admin == 1) {
                                                        ?>
                                                        <td>
                                                            <?php
                                                            if ($article['Article']['status'] == INFI_ACTIVE_STATUS) {
                                                                echo $this->Form->postLink(
                                                                    $this->Html->tag('i', '', array('class' => " fa fa-check")
                                                                        ), array(
                                                                        'action' => 'inactive',
                                                                        $article['Article']['id'],
                                                                        ), array(
                                                                        'escape' => false,
                                                                        'title' => "Click to inactive",
                                                                        ));
                                                            } else {
                                                                echo $this->Form->postLink(
                                                                    $this->Html->tag('i', '', array('class' => " fa fa-times")
                                                                        ), array(
                                                                        'action' => 'active',
                                                                        $article['Article']['id'],
                                                                        ), array(
                                                                        'escape' => false,
                                                                        'title' => "Click to active",
                                                                        ));
                                                            }
                                                            ?>
                                                        </td>
                                                        <?php }
                                                        ?>

                                                            <td>
                                                                <?php
                                                                if ($article['Article']['featured'] == INFI_ACTIVE_STATUS) {
                                                                    echo '<i class=" fa fa-check"></i>';
                                                                } else {
                                                                    echo  '<i class=" fa fa-times"></i>';
                                                                }
                                                                ?>
                                                            </td>
                                                            
                                                            <td>
                                                                <?php
                                                                echo $this->Time->format($article['Article']['created'], '%B %e, %Y %H:%M %p');
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                echo $this->Time->format($article['Article']['modified'], '%B %e, %Y %H:%M %p');
                                                                ?>
                                                            </td>
                                                            <td class="actions">
                                                                <?php
                                                                echo $this->Html->link(
                                                                    $this->Html->tag('i', '', array('class' => "fa fa-search-plus")
                                                                        ), array('action' => 'view', $article['Article']['id']), array(
                                                                        'escape' => false,
                                                                        'title' => "Click to view",
                                                                        ));
                                                                        ?>
                                                                        <?php if ($roles['article']['edit'] == 1) {
                                                                            ?>
                                                                            <?php
                                                                            echo $this->Html->link(
                                                                                $this->Html->tag('i', '', array('class' => "fa fa-pencil-square")), array(
                                                                                    'action' => 'edit',
                                                                                    $article['Article']['id']), array(
                                                                                    'escape' => false,
                                                                                    'title' => "Click to Edit",
                                                                                    ));
                                                                                    ?>
                                                                                    <?php }
                                                                                    ?>
                                                                                    <?php if ($roles['article']['delete'] == 1) {
                                                                                        ?>
                                                                                        <?php
                                                                                        echo $this->Form->postLink(
                                                                                            $this->Html->tag('i', '', array('class' => " fa fa-times-circle")
                                                                                                ), array(
                                                                                                'action' => 'delete',
                                                                                                $article['Article']['id'],
                                                                                                ), array(
                                                                                                'escape' => false,
                                                                                                'title' => "Click to delete",
                                                                                                ), __('Are you sure you want to delete # %s?', $article['Article']['article_name'])
                                                                                                );
                                                                                                ?>
                                                                                                <?php }
                                                                                                ?>
                                                                                            </td>
                                                                                        </tr>
                                                                                    <?php endforeach; ?>
                                                                                    <?php } else { ?>
                                                                                    <tr>
                                                                                        <td colspan="10" align="center">No records found.</td>
                                                                                    </tr>
                                                                                    <?php }
                                                                                    ?>
                                                                                </tbody>
                                                                                <tfoot>
                                                                                    <tr>
                                                                                        <th><?php echo $this->Paginator->sort('id'); ?></th>
                                                                                        <th><?php echo $this->Paginator->sort('article_name'); ?></th>

                                                                                        <th><?php echo $this->Paginator->sort('main_category', 'Parent ArticleCategory'); ?></th>
                                                                                        <th><?php echo $this->Paginator->sort('articlecategory', 'Sub-Parent'); ?></th>
                                                                                        <th><?php echo $this->Paginator->sort('sub_articlecategory', 'Child ArticleCategory'); ?></th>
                                                                                        <th><?php echo $this->Paginator->sort('isAdmin', 'Is Admin?'); ?></th>
                                                                                        <th><?php echo $this->Paginator->sort('userID','User Name'); ?></th>
                                                                                        <?php if ($roles['article']['review'] != 1 || $is_admin == 1) { ?>
                                                                                        <th align="center"><?php echo $this->Paginator->sort('status'); ?></th>
                                                                                        <?php }
                                                                                        ?>
                                                                                        <th><?php echo $this->Paginator->sort('created'); ?></th>
                                                                                        <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                                                                        <th class="actions"><?php echo __('Actions'); ?></th>
                                                                                    </tr>
                                                                                </tfoot>
                                                                            </table>
                                                                        </div><!-- /.box-body table-responsive -->


                                                                    </div>
                                                                </div>
                                                            </div></div>
                                                            <div class="row">
                                                                <div class="col s12 m12 l12">

                                                                    <div class="paging">
                                                                        <ul class="pagination pagination-sm">
                                                                            <?php
                                                                            echo $this->Paginator->prev(
                                                                                ' << ', array(
                                                                                    'tag' => 'li',
                                                                                    'class' => 'prev',
                                                                                    'escape' => false,
                                                                                    ), null, array(
                                                                                    'tag' => 'li',
                                                                                    'escape' => false,
                                                                                    'class' => 'prev disabled',
                                                                                    'disabledTag' => 'a',
                                                                                    ));
                                                                                    ?>
                                                                                    <?php
                                                                                    echo $this->Paginator->numbers(array(
                                                                                        'separator' => '',
                                                                                        'currentClass' => 'active',
                                                                                        'currentTag' => 'a',
                                                                                        'tag' => 'li',
                                                                                        ));
                                                                                        ?>
                                                                                        <?php
                                                                                        echo $this->Paginator->next('>>', array(
                                                                                            'tag' => 'li',
                                                                                            'class' => 'next',
                                                                                            'escape' => false,
                                                                                            ), null, array(
                                                                                            'tag' => 'li',
                                                                                            'escape' => false,
                                                                                            'class' => 'next disabled',
                                                                                            'disabledTag' => 'a',
                                                                                            ));
                                                                                            ?>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <?php
                                                                                    echo $this->Paginator->counter(array(
                                                                                        'format' => __('Article {:article} of {:articles}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}'),
                                                                                        ));
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
                                                                                    <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating btn-large red', 'escape' => FALSE)); ?>

                                                                                </div>