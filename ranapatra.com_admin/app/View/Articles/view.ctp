
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $article_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li><?php echo $this->Html->link(__($parent_article_name), array('action' => 'index')); ?></li>
                    <li class="active"><?php echo $article_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">


                <div class="box-body">

                    <div class="row">

                        <div class="input-field col s12 m12 l12">
                            <table class="table table-striped">
                                <tr>
                                    <td><?php echo __('Article Name'); ?></td>
                                    <td>
                                        <?php echo h($article['Article']['article_name']); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo __('Article Title'); ?></td>
                                    <td><?php echo h($article['Article']['article_title']); ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo __('Article Keywords'); ?></td>
                                    <td><?php echo h($article['Article']['article_keywords']); ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo __('Content'); ?></td>
                                    <td style="max-width:300px;"><?php echo (strip_tags($article['Article']['content'])); ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo __('Is Admin?'); ?></td>
                                    <td><?php
                                        if ($article['Article']['isAdmin'] == 0) {
                                                echo 'No';
                                            } else {
                                                echo 'Yes';
                                            }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo __('User Name'); ?></td>
                                    <td><?php
                                        if ($article['Article']['isAdmin'] == 0) {
                                                echo 'No';
                                            } else {
                                                $userInfo = $this->requestAction('users/getUserInfo/' . $article['Article']['userID']);
                                                print ($userInfo['User']['username']);
                                            }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo __('Article Layout'); ?></td>
                                    <td><?php echo h($article['Article']['layout']); ?></td>
                                </tr>
                                <?php if ($roles['article']['review'] != 1 || $is_admin == 1) { ?>
                                    <tr>
                                        <td><?php echo __('Status'); ?></td>
                                        <td>
                                            <?php
                                            if ($article['Article']['status'] == INFI_ACTIVE_STATUS) {
                                                echo $this->Form->postLink(
                                                        $this->Html->tag('i', '', array('class' => " fa fa-check")
                                                        ), array(
                                                    'action' => 'inactive',
                                                    $article['Article']['id']
                                                        ), array(
                                                    'escape' => false,
                                                    'title' => "Click to inactive"
                                                ));
                                            } else {
                                                echo $this->Form->postLink(
                                                        $this->Html->tag('i', '', array('class' => " fa fa-times")
                                                        ), array(
                                                    'action' => 'active',
                                                    $article['Article']['id']
                                                        ), array(
                                                    'escape' => false,
                                                    'title' => "Click to active"
                                                ));
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td><?php echo __('Created'); ?></td>
                                    <td>
                                        <?php
                                        echo $this->Time->format($article['Article']['created'], '%B %e, %Y %H:%M %p');
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo __('Modified'); ?></td>
                                    <td>
                                        <?php
                                        echo $this->Time->format($article['Article']['modified'], '%B %e, %Y %H:%M %p');
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div><!-- /.box-body -->


                </div><!-- /.col (left) -->
            </div>
        </div>
    </div>
</div>

<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
        <i class="large mdi-editor-mode-edit"></i>
    </a>
    <ul>
        <li><a href="javascript:void();" class="btn-floating red" onclick="otherInfo('Shortcode', '[[ARTICLE][<?php echo $article['Article']['article_slug']; ?>]]', true)"><i class="large mdi-action-info"></i></a></li>
        <li>
            <?php
            echo $this->Form->postLink(
                    $this->Html->tag('i', '', array('class' => " large mdi-action-delete")
                    ), array(
                'action' => 'delete',
                $article['Article']['id']
                    ), array(
                'class' => 'btn-floating red', 'escape' => FALSE,
                'title' => "Click to delete"
                    ), __('Are you sure you want to delete # %s?', $article['Article']['article_name'])
            );
            ?>

        </li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-list')), array('action' => 'index'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
    </ul>
</div>