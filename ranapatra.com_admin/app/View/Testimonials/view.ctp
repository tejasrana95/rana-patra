<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li><?php echo $this->Html->link(__($parent_page_name), array('action' => 'index')); ?></li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">


                <div class="box-body">

                    <div class="row">

                        <div class="input-field col s12 m12 l12">
                            <table class="table table-striped">
                                <tr>
                                    <td><?php echo __('Name'); ?></td>
                                    <td><?php
                                        echo $Testimonials['Testimonial']['name'];
                                        ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __('Designation'); ?></td>
                                        <td style="max-width:300px;"><?php echo $Testimonials['Testimonial']['designation']; ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __('Message'); ?></td>
                                        <td style="max-width:300px;"><?php echo $Testimonials['Testimonial']['message']; ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __('Image'); ?></td>
                                        <td><?php
                                            if (isset($Testimonials['Testimonial']['image']) && !empty($Testimonials['Testimonial']['image'])) {
                                                echo '<img src="' . $Testimonials['Testimonial']['image'] . '" width="100" />';
                                            }
                                            ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><?php echo __('Created'); ?></td>
                                        <td>
                                            <?php
                                            echo $this->Time->format($Testimonials['Testimonial']['created'], '%B %e, %Y %H:%M %p');
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo __('Modified'); ?></td>
                                        <td>
                                            <?php
                                            echo $this->Time->format($Testimonials['Testimonial']['modified'], '%B %e, %Y %H:%M %p');
                                            ?>
                                        </td>
                                    </tr>
                                </table>

                            </div><!-- /.box-body -->

                        </div><!-- /.box -->
                    </div><!-- /.col (left) -->
                </div>
            </div>
        </div>
    </div>





    <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
        <a class="btn-floating btn-large red">
            <i class="large mdi-editor-mode-edit"></i>
        </a>
        <ul>
            <li>
                <?php
                echo $this->Form->postLink(
                    $this->Html->tag('i', '', array('class' => " large mdi-action-delete")
                        ), array(
                        'action' => 'delete',
                        $Testimonials['Testimonial']['id']
                        ), array(
                        'class' => 'btn-floating red', 'escape' => FALSE,
                        'title' => "Click to delete"
                        ), __('Are you sure you want to delete # %s?',$Testimonials['Testimonial']['id'])
                        );
                        ?>

                    </li>
                    <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-list')), array('action' => 'index'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
                    <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
                    <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-edit')), array('action' => 'edit',$Testimonials['Testimonial']['id']), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
                    <?php if ($is_admin != 1) { ?>
                    <li><?php echo $this->Html->link(__('New Admin Request'), array('action' => 'add') ,array('class' => 'btn-floating red', 'escape' => FALSE)); ?> </li>
                    <?php } ?>
                </ul>
            </div>
