<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li><?php echo $this->Html->link(__($parent_page_name), array('action' => 'index')); ?></li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <?php echo $this->Form->create('Testimonial', array('type' => 'file')); ?>
                <?php echo $this->Form->input('id'); ?>
                <div class="box-body">
                 <div class="row">
                     <div class="input-field col s12 m6 l6">
                        <?php
                        echo $this->Form->input('name', array('div' => array('class' => "form-group"), 'class' => "form-control", 'maxlength' => 70, 'label' => 'Name'));
                        ?>
                    </div>
                    <div class="input-field col s12 m6 l6">
                        <?php
                        echo $this->Form->input('designation', array('div' => array('class' => "form-group"), 'class' => "form-control", 'maxlength' => 70, 'label' => 'Designation'));
                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12 m12 l12">                            
                        <?php
                        echo $this->Form->input('message', array('type' => 'textarea', 'div' => array('class' => "form-group"), 'class' => "materialize-textarea"));
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 m10 l10">
                        <?php

                        echo $this->Form->input('image', array('type' => "text", "readonly" => 'readonly', "label" => "Banner", 'id' => 'bannerpath', 'div' => array('class' => "form-group"), 'class' => "form-control"));
                        ?>
                    </div>
                    <div class="input-field col s12 m2 l2">
                        <?php
                        echo $this->Form->button('Select Banner', array('type' => "button", 'id' => "selectflag", "onclick" => "openfilemanager('bannerpath')", 'class' => "waves-effect waves-light btn "));
                        ?>
                    </div>
                    <div class="input-field col s12 m12 l12">
                        <div id="bannerpath_wrapper" style="display: none;">
                            <img src="" id="bannerpath_img" style="max-width:300px;">
                            <br/><a href="javascript:void()" onclick="removeimage('bannerpath')">Remove</a>
                        </div>
                    </div>
                    <?php
                    if (isset($this->request->data['Testimonial']['image']) && !empty($this->request->data['Testimonial']['image'])) {
                        ?>
                        <script>
                            $('#bannerpath_img').attr("src", "<?php echo $this->request->data['Testimonial']['image']; ?>");
                            $('#bannerpath_wrapper').fadeIn();
                        </script>
                        <?php
                    }
                    ?>
                </div>

                <?php
                echo $this->Form->input('Save', array('label' => false, 'div' => array('class' => "form-group"), 'type' => "submit", 'class' => "btn btn-success btn-lg"));
                ?>
            </div><!-- /.box-body -->
            <?php echo $this->Form->end(); ?>
        </div><!-- /.box -->
    </div><!-- /.col (left) -->
</div>
</div>

<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
        <i class="large mdi-editor-mode-edit"></i>
    </a>
    <ul>
     <li>
         <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-list')), array('action' => 'index'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?>
     </li>
     <li>
         <?php
         echo $this->Form->postLink(
            $this->Html->tag('i', '', array('class' => " large mdi-action-delete")
                ), array(
                'action' => 'delete',
                $this->request->data['Testimonial']['id']
                ), array(
                'class' => 'btn-floating red', 'escape' => FALSE,
                'title' => "Click to delete"
                ), __('Are you sure you want to delete # %s?',$this->request->data['Testimonial']['id'])
                );
                ?>
            </li>
            <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
            <?php if ($is_admin != 1) { ?>
            <li><?php echo $this->Html->link(__('New Testimonial'), array('action' => 'add') ,array('class' => 'btn-floating red', 'escape' => FALSE)); ?> </li>
            <?php } ?>
        </ul>
    </div>
