<?php
App::import('Controller', 'Category');
$categoryController = new CategoryController;
$page_title = "Shortcode";
$page_name = "Shortcode";
?>
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="col-md-4">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Latest Thinking</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <input class="form-control" readonly="readonly" value="[[LATESTHINKING][LATESTHINKING]]" type="text">
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div class="col-md-4">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">RightBar</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <input class="form-control" readonly="readonly" value="[[RIGHTBAR][RIGHTBAR]]" type="text">
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div class="col-md-4">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sub Category</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <input class="form-control" readonly="readonly" value="[[SUBCATEGORY][SUBCATEGORY]]" type="text">
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <div class="col-md-4">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Career Openings</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <input class="form-control" readonly="readonly" value="[[CAREERS][OPENINGS]]" type="text">
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>


            </div>
        </div>
    </div></div>