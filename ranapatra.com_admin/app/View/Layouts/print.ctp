<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="msapplication-tap-highlight" content="no">
        <title><?php echo $title_for_layout; ?> &raquo; <?php echo $site_name ?> &raquo; <?php echo $org_name; ?></title>
        <?php echo $this->Html->css(array('materialize', 'style', 'custom-style', 'style-fullscreen', '/js/plugins/perfect-scrollbar/perfect-scrollbar', '/js/plugins/jvectormap/jquery-jvectormap',
            '/js/plugins/perfect-scrollbar/perfect-scrollbar', '/js/plugins/chartist-js/chartist.min', 'font-awesome/css/font-awesome.min','nested_menu'));
         echo $this->Html->css(array("/elfinder/css/elfinder.min","/elfinder/css/theme"));
   
        ?>
        <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/themes/smoothness/jquery-ui.css">
        <base href="<?php echo Router::url('/', true); ?>" >
        <?php  echo $this->Html->script(array("jquery-1.11.2.min")); ?>
    </head>

    <body>


        <!-- START CONTENT -->
        <section id="content">
            
             <?php echo $this->fetch('content'); ?>
           
        </section>
      

<!-- ================================================
Scripts
================================================ -->
<!-- Toast Notification -->
<?php echo $this->Html->script(array( 'materialize', 'plugins/perfect-scrollbar/perfect-scrollbar.min', 'plugins/sparkline/jquery.sparkline.min', 'plugins/sparkline/sparkline-script'));
 
?>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
<!-- google map api -->
<?php
 echo $this->Html->script(array("/elfinder/js/elfinder.min"));
echo $this->Html->script(array('plugins/jvectormap/jquery-jvectormap-1.2.2.min', 'plugins/jvectormap/jquery-jvectormap-world-mill-en',
    'plugins','jquery.nestable'));
   
?>
<script>
    <?php  $toastMsg=$this->Session->flash();

    if(!empty($toastMsg)){  ?>
      $(window).load(function() {
        setTimeout(function() {
            Materialize.toast('<span><?php echo $toastMsg; ?></span>', 3000);
        }, 500);
      
    });
    <?php } ?>
function otherInfo(title,info,textbox=false){
    
    if(textbox==true){
        var stringForOtherInfo='<textarea id="textarea1" class="materialize-textarea" readonly>'+info+'</textarea>';
    }
    else{
        var stringForOtherInfo="<p>"+info+"</p>";
    }
    stringForOtherInfo="<h4>"+title+"</h4>"+stringForOtherInfo;
    $('#contentForOtherInfo').html(stringForOtherInfo);
      $('#otherInfo').openModal();
}
          
            function openfilemanager(filepaths) {
                $.filepaths = filepaths;
               
                  $('#elfinder-modal').openModal();
                var elf = $('#elfinders').elfinder({
                    url: '<?php echo $site_url; ?>filemanager/index/<?php echo $userID ?>/',
                    height: $(window).height() - 260,
                    uiOptions: {
                        toolbar: [
                            // toolbar configuration
                            ['back', 'forward'],
                            ['reload'],
                            ['home', 'up'],
                            ['mkdir', 'mkfile', 'upload'],
                            ['open'],
                            ['info'],
                            ['quicklook'],
                            ['copy', 'cut', 'paste'],
                            ['rm'],
                            ['duplicate', 'rename', 'edit'],
                            ['extract', 'archive'],
                            ['search'],
                            ['view'],
                        ]
                    },
                    contextmenu: {
                        files: [
                            'getfile', '|', 'open', '|', 'copy', 'cut', 'paste', 'duplicate', '|',
                            'rm', '|', 'edit', 'rename', '|', 'info'
                        ]
                    },
                    closeOnEditorCallback: true,
                    getFileCallback: function (url) {
                        $("#" + $.filepaths).val(url['url'])
                        $("#" + $.filepaths + '_img').attr("src", url['url']);
                       $("#" + $.filepaths + '_wrapper').fadeIn();
                       //$("#filemanager").dialog('close');

  $('#elfinder-modal').closeModal();
        
                    }
                }).elfinder('instance');
 
            }
            function removeimage(filepath) {
                $("#" + filepath + '_wrapper').hide();
                $("#" + filepath).val("")
                $("#" + filepath + '_img').attr("src", "");

            }
        </script> 

<?php
    echo $this->Html->script(Router::url('/', true) . 'js/tinymce/tinymce.min.js');
    echo $this->Html->script(Router::url('/', true) . 'filemanager/editorwithfilemanager');
    ?>
 
</body>

</html>