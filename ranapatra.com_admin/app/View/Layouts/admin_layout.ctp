<!DOCTYPE html>
<html lang="en">
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" name="viewport"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <meta content="no" name="msapplication-tap-highlight"/>
        <title>
            <?php echo $title_for_layout; ?>
            »
            <?php echo $site_name ?>
            »
            <?php echo $org_name; ?>
        </title>
        <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <?php
        echo $this->
        Html->css(array('materialize', 'style', 'custom-style', 'style-fullscreen', '/js/plugins/perfect-scrollbar/perfect-scrollbar', '/js/plugins/jvectormap/jquery-jvectormap',
            '/js/plugins/perfect-scrollbar/perfect-scrollbar', '/js/plugins/chartist-js/chartist.min', 'font-awesome/css/font-awesome.min', 'nested_menu'));
        echo $this->Html->css(array("/elfinder/css/elfinder.min", "/elfinder/themes/windows-10/css/theme"));
        ?>
        <base href="<?php echo Router::url('/', true); ?>">
            <?php echo $this->
            Html->script(array("jquery-1.11.2.min")); ?>
        </base>
    </head>
    <body>
        <!-- Start Page Loading -->
        <div id="loader-wrapper">
            <div id="loader">
            </div>
            <div class="loader-section section-left">
            </div>
            <div class="loader-section section-right">
            </div>
        </div>
        <!-- End Page Loading -->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <?php echo $this->
        element('admin_nav_bar'); ?>
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content">
            <!--start container-->
            <?php echo $this->
            fetch('content'); ?>
        </section>
        <!-- END CONTENT -->
        <?php echo $this->
        element('admin_side_bar') ?>
    </body>
    <!-- END WRAPPER -->
    <!-- END MAIN -->
    <!-- Modal Structure -->
    <div class="modal filemanager_modal " id="elfinder-modal">
        <div class="modal-content">
            <div id="filemanager" title="File Manager">
                <div class="nonborderbox" id="elfinders">
                </div>
            </div>
        </div>
    </div>
    <div class="modal otherInfo bottom-sheet" id="otherInfo">
        <div class="modal-content" id="contentForOtherInfo">
        </div>
    </div>
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START FOOTER -->
    <footer class="page-footer">
        <div class="footer-copyright">
            <div class="container">
                Copyright ©
                <?php echo date('Y') ?>
                <a class="grey-text text-lighten-4" href="http://www.infitechnology.com" target="_blank">
                    Infi Technology
                </a>
                All rights reserved.
                <span class="right">
                    Design and Developed by
                    <a class="grey-text text-lighten-4" href="http://www.infitechnology.com/">
                        Infi Technology
                    </a>
                </span>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->
    <!-- ================================================
Scripts
================================================ -->
    <!-- Toast Notification -->
    <?php echo $this->
    Html->script(array('materialize', 'plugins/perfect-scrollbar/perfect-scrollbar.min', 'plugins/sparkline/jquery.sparkline.min', 'plugins/sparkline/sparkline-script'));
?>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js">
    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js">
    </script>
    <!-- google map api -->
    <?php
echo $this->
    Html->script(array("/elfinder/js/elfinder.min"));
echo $this->Html->script(array('plugins/jvectormap/jquery-jvectormap-1.2.2.min', 'plugins/jvectormap/jquery-jvectormap-world-mill-en',
    'plugins', 'jquery.nestable', 'formatter','jquery.auto-complete.min', 'custom'));
?>
    <script>
        $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 50, // Creates a dropdown of 15 years to control year
        format: 'yyyy-mm-dd',
        max: new Date(<?php echo date('Y') - 18 ?>, 11, 31)
    });


<?php $toastMsg = $this->Session->flash();

if (!empty($toastMsg)) {
    ?>
        $(window).load(function () {
            setTimeout(function () {
                Materialize.toast('<span><?php echo $toastMsg; ?></span>', 3000);
            }, 500);

        });
<?php } ?>
    function otherInfo(title, info, textbox = false){

        if (textbox == true) {
            var stringForOtherInfo = '<textarea id="textarea1" class="materialize-textarea" readonly>' + info + '</textarea>';
        }
        else {
            var stringForOtherInfo = "<p>" + info + "</p>";
        }
        stringForOtherInfo = "<h4>" + title + "</h4>" + stringForOtherInfo;
        $('#contentForOtherInfo').html(stringForOtherInfo);
        $('#otherInfo').openModal();
    }

    function openfilemanager(filepaths) {
        $.filepaths = filepaths;

        $('#elfinder-modal').openModal();
        var elf = $('#elfinders').elfinder({
            url: '<?php echo $site_url; ?>filemanager/index/<?php echo $userID ?>/',
            height: $(window).height() - 260,
            uiOptions: {
                toolbar: [
                    // toolbar configuration
                    ['back', 'forward'],
                    ['reload'],
                    ['home', 'up'],
                    ['mkdir', 'mkfile', 'upload'],
                    ['open'],
                    ['info'],
                    ['quicklook'],
                    ['copy', 'cut', 'paste'],
                    ['rm'],
                    ['duplicate', 'rename', 'edit'],
                    ['extract', 'archive'],
                    ['search'],
                    ['view'],
                ]
            },
            contextmenu: {
                files: [
                    'getfile', '|', 'open', '|', 'copy', 'cut', 'paste', 'duplicate', '|',
                    'rm', '|', 'edit', 'rename', '|', 'info'
                ]
            },
            closeOnEditorCallback: true,
            getFileCallback: function (url) {
                $("#" + $.filepaths).val(url['url'])
                $("#" + $.filepaths + '_img').attr("src", url['url']);
                $("#" + $.filepaths + '_wrapper').fadeIn();
                //$("#filemanager").dialog('close');

                $('#elfinder-modal').closeModal();

            }
        }).elfinder('instance');

    }
    function removeimage(filepath) {
        $("#" + filepath + '_wrapper').hide();
        $("#" + filepath).val("")
        $("#" + filepath + '_img').attr("src", "");

    }
    </script>
    <?php
echo $this->
    Html->script(Router::url('/', true) . 'ckeditor/ckeditor.js');
//echo $this->Html->script(Router::url('/', true) . 'ckeditor/sample.js');
echo $this->Html->script(Router::url('/', true) . 'filemanager/editorwithfilemanager');
?>
</html>