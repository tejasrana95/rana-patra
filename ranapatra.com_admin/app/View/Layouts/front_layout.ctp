<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
<title>NEOHAPSIS</title>
<?php
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
?>
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<!-- Custom styles for this template -->
<link href="css/customStyle.css" rel="stylesheet">
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div id="wrap">
<div class="container-fluid container">
  <div class="row">
    <div class="col-md-6 topBar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">BLOGS</a></li>
        <li><a href="#about">SUPPORT</a></li>
        <li><a href="#contact">CAREER</a></li>
      </ul>
    </div>
    <div class="col-md-6 text-right socialLink"> <span><i class="fa fa-facebook"></i>&nbsp; </span> <span><i class="fa fa-linkedin"></i>&nbsp; </span> <span><i class="fa fa-twitter"></i>&nbsp; </span> <span><i class="fa fa-google-plus"></i>&nbsp; </span> </div>
  </div>
</div>
<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="#"><span class="companyLogo">&nbsp;</span></a> </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li class="active"><a href="#">HOME</a></li>
        <li><a href="advisory-services.html">SERVICES</a></li>
        <li><a href="#">CUSTOMERS</a></li>
        <li><a href="#">RESOURCES</a></li>
        <li><a href="about-us.html">ABOUT US</a></li>
        <li><a href="#">CONTACT US</a></li>
	 
        <!--<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>-->
      </ul>
    </div>
    <!--/.nav-collapse --> 
  </div>
</div>
<?php echo $this->fetch('content'); ?>
<div id="footer">
<div class="container footer" >
  <div class="col-md-6 ">
    <ul class="btmNav">
      <li><a href="#">TERMS OF USE</a></li>
      <li><a href="#">PRIVACY</a></li>
      <li><a href="#">DISCLOSURE POLICY</a></li>
    </ul>
  </div>
  <div class="col-md-6 text-right"> � 2014 NEOHAPSIS. ALL RIGHTS RESERVED. </div>
</div>
</div>
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> 
<script src="js/bootstrap.min.js"></script>
<?php 
echo $this->fetch('script');
?>
</body>
<?php echo $this->element('sql_dump'); ?>
</html>