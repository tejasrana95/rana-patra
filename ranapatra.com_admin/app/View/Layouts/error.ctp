<!DOCTYPE html>
<html lang="en">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" name="viewport"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <meta content="no" name="msapplication-tap-highlight"/>
    <meta content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. " name="description"/>
    <meta content="materialize, admin template, dashboard template, flat admin template, responsive admin template," name="keywords"/>
    <title>
        Error
    </title>
    <!-- Favicons-->
    <link href="images/favicon/favicon-32x32.png" rel="icon" sizes="32x32"/>
    <!-- Favicons-->
    <link href="images/favicon/apple-touch-icon-152x152.png" rel="apple-touch-icon-precomposed"/>
    <!-- For iPhone -->
    <meta content="#00bcd4" name="msapplication-TileColor"/>
    <meta content="images/favicon/mstile-144x144.png" name="msapplication-TileImage"/>
    <?php echo $this->
    Html->css(array('materialize','style','custom-style','style-fullscreen','page-center','prism','/js/plugins/perfect-scrollbar/perfect-scrollbar')); ?>
</head>
<body class="cyan">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader">
        </div>
        <div class="loader-section section-left">
        </div>
        <div class="loader-section section-right">
        </div>
    </div>
    <?php echo $this->fetch('content'); ?>
        <!-- ================================================
    Scripts
    ================================================ -->
    <?php echo $this->Html->script(array('jquery-1.11.2.min','materialize','prism','plugins/perfect-scrollbar/perfect-scrollbar.min','plugins'));
    ?>
</body>
</html>