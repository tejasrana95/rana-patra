<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!--Basic Form-->
<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('id'); ?></th>
                                <th><?php echo $this->Paginator->sort('parent'); ?></th>
                                <th><?php echo $this->Paginator->sort('label', "Sub Menu"); ?></th>
                                <th><?php echo $this->Paginator->sort('label', "Reserved"); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($categories)) {
                                foreach ($categories as $category):
                                    ?>
                                    <tr>
                                        <td><?php echo h($category['Category']['id']); ?>&nbsp;</td>
                                        <td><?php
                                            if ($category['Category']['parent'] == "0") {
                                                echo h($category['Category']['label']);
                                            } else {
                                                $categorydata = $this->requestAction('category/categorydata/' . $category['Category']['parent']);
                                                if (isset($categorydata) && !empty($categorydata)) {
                                                    echo h($categorydata['label']);
                                                }
                                            }
                                            ?>&nbsp;</td>
                                        <td><?php
                                            if ($category['Category']['parent'] != "0") {
                                                echo h($category['Category']['label']);
                                            }
                                            ?>&nbsp;</td>
                                        <td><?php
                                            if ($category['Category']['reserved'] != "0") {
                                                echo "Yes";
                                            } else {
                                                echo "Public";
                                            }
                                            ?>&nbsp;</td>
                                        <td class="actions">
                                            <?php
                                            if ($roles['category']['edit'] == 1) {
                                                echo $this->Html->link(
                                                        $this->Html->tag('i', '', array('class' => "fa fa-pencil-square")), array(
                                                    'action' => 'edit',
                                                    $category['Category']['id']), array(
                                                    'escape' => false,
                                                    'title' => "Click to Edit"
                                                ));
                                            }
                                            ?>
                                            <?php
                                            if ($roles['category']['delete'] == 1) {
                                                $categorydata = $this->requestAction('category/getchild/' . $category['Category']['id']);
                                                if (empty($categorydata)) {

                                                    echo $this->Form->postLink(
                                                            $this->Html->tag('i', '', array('class' => " fa fa-times-circle")
                                                            ), array(
                                                        'action' => 'delete',
                                                        $category['Category']['id']
                                                            ), array(
                                                        'escape' => false,
                                                        'title' => "Click to delete"
                                                            ), __('Are you sure you want to delete # %s?', $category['Category']['label'])
                                                    );
                                                }
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="10" align="center">No records found.</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th><?php echo $this->Paginator->sort('id'); ?></th>
                                <th><?php echo $this->Paginator->sort('parent'); ?></th>
                                <th><?php echo $this->Paginator->sort('label', "Sub Menu"); ?></th>
                                <th><?php echo $this->Paginator->sort('label', "Reserved"); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="row"><div class="col s12 m12 l12">
        <div class="paging">
            <ul class="pagination pagination-sm">
                <?php
                echo $this->Paginator->prev(
                        ' << ', array(
                    'tag' => 'li',
                    'class' => 'prev',
                    'escape' => false
                        ), null, array(
                    'tag' => 'li',
                    'escape' => false,
                    'class' => 'prev disabled',
                    'disabledTag' => 'a'
                ));
                ?>
                <?php
                echo $this->Paginator->numbers(array(
                    'separator' => '',
                    'currentClass' => 'active',
                    'currentTag' => 'a',
                    'tag' => 'li'
                ));
                ?>
                <?php
                echo $this->Paginator->next('>>', array(
                    'tag' => 'li',
                    'class' => 'next',
                    'escape' => false
                        ), null, array(
                    'tag' => 'li',
                    'escape' => false,
                    'class' => 'next disabled',
                    'disabledTag' => 'a'
                ));
                ?>
            </ul>
        </div> 
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>
    </div>
</div>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating btn-large red', 'escape' => FALSE)); ?>

</div>

