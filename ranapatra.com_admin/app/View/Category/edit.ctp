
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li><?php echo $this->Html->link(__($parent_page_name), array('action' => 'index')); ?></li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col s12 m12 l12">
        <div class="card-panel">

            <?php echo $this->Form->create('Category', array('type' => 'file')); ?>
            <div class="box-body">
                
                <div class="row">

                    <div class="input-field col s12 m6 l6">
                        <?php
  
                        echo $this->Form->input('parent', array('options' =>$parentCate,
                            'empty' => "Select category",
                            'div' => array('class' => "form-group"),
                            'class' => "form-control",
                            "required" => "required",
                            "id" => 'category',
                            "label" => false,
                            'onchange' => 'getmiddlecate()',
                             "selected" => $mainparent['id'],
                        ));
                        ?>
                    </div>
                    <div class="input-field col s12 m6 l6">
                        <?php

                        echo $this->Form->input('sub_parent', array('options' => $allsubparent,
                            'empty' => "Select parent category first",
                            'div' => array('class' => "form-group"),
                            'class' => "form-control",
                            "id" => 'SubCategory',
                            'label' => false,
                             "selected" => $subparent['id'],
                        ));
                        ?>
                    </div>
                    <div class="row">

                        <div class="input-field col s12 m6 l6">
                            <?php
                            echo $this->Form->input('label', array('div' => array('class' => "form-group"), 'label' => 'Name', 'class' => "form-control", "required" => "required", "value" => $category['label']));
                            ?></div>
                        <div class="input-field col s12 m6 l6">
                            <?php
                            echo $this->Form->input('sort_order', array('type' => "number", "label" => "Sort Order", 'div' => array('class' => "form-group"), 'class' => "form-control", "value" => $category['sort_order']));
                            ?>
                        </div>
                    </div>
                    <div class="row">

                        <div class="input-field col s12 m10 l10">

                            <?php
                            echo $this->Form->input('img', array('type' => "text", "readonly" => 'readonly', "label" => "Banner", 'id' => 'bannerpath', 'div' => array('class' => "form-group"), 'class' => "form-control", "value" => $category['img']));
                            ?>
                        </div>
                        <div class="input-field col s12 m2 l2">

                            <?php
                            echo $this->Form->button('Select Banner', array('type' => "button", 'id' => "selectflag", "onclick" => "openfilemanager('bannerpath')", 'class' => "waves-effect waves-light btn "));
                            ?>
                        </div>
                    </div>
                    <div class="row">

                        <div class="input-field col s12 m12 l12">
                            <div id="bannerpath_wrapper" style="display: none;">
                                <img src="" id="bannerpath_img" style="max-width:300px;">
                                <br/><a href="javascript:void()" onclick="removeimage('bannerpath')">Remove</a>
                            </div>
                            <?php
                        if (isset($category['img']) && !empty($category['img'])) {
                            ?>
                            <script>
                                $('#bannerpath_img').attr("src", '<?php echo $category['img']; ?>');
                                $('#bannerpath_wrapper').fadeIn();
                            </script>
                            <?php
                        }
                        ?>
                        </div>
                    </div>
                    <div class="row">

                        <div class="input-field col s3">
                            <?php
                            echo $this->Form->input('fontaws_icon', array('type' => "text", "label" => "Font Awesome Icon", 'div' => array('class' => "form-group"), 'class' => "form-control", "value" => $category['fontaws_icon']));
                            ?>
                        </div>

                        <div class="switch col s3">

                            Reserved : 
                            <label>
                                No
                                <?php
                                 if ($category['reserved'] == 1) {
                                $checked = 'checked';
                            } else {
                                $checked = '';
                            }
                                echo $this->Form->input('reserved', array('div' => false, 'label' => false, 'type' => "checkbox", 'class' => "", 'checked' => $checked));
                                ?>
                                <span class="lever"></span> Yes
                            </label>
                        </div>
                        <div class="switch col s3">

                            Show Siblings : 
                            <label>
                                No
                                <?php
                                 if ($category['show_sibling'] == 1) {
                                $checked = 'checked';
                            } else {
                                $checked = '';
                            }
                                echo $this->Form->input('show_sibling', array('div' => false, 'label' => false, 'type' => "checkbox", 'class' => "", 'checked' => $checked));
                                ?>
                                <span class="lever"></span> Yes
                            </label>
                        </div>
                        <div class="switch col s12 m3 l3">

                            Is Heading : 
                            <label>
                                No
                                <?php
                                 if ($category['is_heading'] == 1) {
                                $checked = 'checked';
                            } else {
                                $checked = '';
                            }
                                echo $this->Form->input('is_heading', array('div' => false, 'label' => false, 'type' => "checkbox", 'class' => "", 'checked' => $checked));
                                ?>
                                <span class="lever"></span> Yes
                            </label>
                        </div>

                    </div>
                    <div class="row">

                        <div class="input-field col s12 m12 l12">
                            <?php
                            echo $this->Form->input('Save', array('label' => false, 'div' => array('class' => "form-group"), 'type' => "submit", 'class' => "btn btn-success btn-lg"));
                            ?>
                        </div>
                    </div><!-- /.box -->
                </div><!-- /.box-body -->
                <?php echo $this->Form->end(); ?>

            </div><!-- /.col (left) -->
        </div>
    </div>
</div>


<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
        <i class="large mdi-editor-mode-edit"></i>
    </a>
    <ul>
        <li>
            <?php
            echo $this->Form->postLink(
                    $this->Html->tag('i', '', array('class' => " large mdi-action-delete")
                    ), array(
                'action' => 'delete',
               $category['id']
                    ), array(
                'class' => 'btn-floating red', 'escape' => FALSE,
                'title' => "Click to delete"
                    ), __('Are you sure you want to delete # %s?', $category['label'])
            );
            ?>

        </li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-list')), array('action' => 'index'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
    </ul>
</div>
<script>
    function getparentcate() {
      
            $.get('<?php echo Router::url('/', true) ?>category/parentcate/' + 'true', function (result) {
                $('#category').html(result);
                $('#category').material_select();
            });
      
    }
  
          
  
</script>
<script>
    // listen to events on the category dropdown
    function getmiddlecate() {
        // don't do anything if use selects "Select Cat"
        if ($("#category").val() !== "") {
            $.get('<?php echo Router::url('/', true) ?>category/subcate/' + $('#category').val() + '/category', function (result) {
                $('#SubCategory').html(result);
                 $('#SubCategory').material_select();
            });
        }
    }
</script>