<?php
App::import('Controller', 'UserRoles');
$UserRolesController = new UserRolesController;
?> 
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
        <!--Basic Form-->
        <div id="basic-form" class="section">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card-panel">
                        <h4 class="header2"><?php echo __($page_name); ?></h4>
                        <div class="row">
                            <table class="table table-striped">
                                <tr>
                                    <td><?php echo __('User Name'); ?></td>
                                    <td>
                                        <?php echo h($user['User']['username']); ?>
                                    </td>

                                </tr>
                                <tr>
                                    <td><?php echo __('Last login'); ?></td>
                                    <td>
                                        <?php
                                        echo $this->Time->timeAgoInWords($user['User']['last_login'], array('format' => 'F jS, Y'));
                                        ?>
                                    </td>

                                </tr>
                                <?php
                                $is_admin = $UserRolesController->getPermission($user['User']['id']);
                                if ($is_admin['is_admin'] != 1) {
                                    ?>
                                    <tr>
                                        <td><?php echo __('Status'); ?></td>
                                        <td>
                                            <?php
                                            if ($user['User']['status'] == INFI_ACTIVE_STATUS) {
                                                echo $this->Form->postLink(
                                                        $this->Html->tag('i', '', array('class' => " fa fa-check")
                                                        ), array(
                                                    'action' => 'inactive',
                                                    $user['User']['id']
                                                        ), array(
                                                    'escape' => false,
                                                    'title' => "Click to inactive"
                                                ));
                                            } else {
                                                echo $this->Form->postLink(
                                                        $this->Html->tag('i', '', array('class' => " fa fa-times")
                                                        ), array(
                                                    'action' => 'active',
                                                    $user['User']['id']
                                                        ), array(
                                                    'escape' => false,
                                                    'title' => "Click to active"
                                                ));
                                            }
                                            ?>
                                        </td>

                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td><?php echo __('Created'); ?></td>
                                    <td>
                                        <?php echo $this->Time->format($user['User']['created'], '%B %e, %Y %H:%M %p'); ?>
                                    </td>

                                </tr>
                                <tr>
                                    <td><?php echo __('Modified'); ?></td>
                                    <td>
                                        <?php echo $this->Time->format($user['User']['modified'], '%B %e, %Y %H:%M %p'); ?>
                                    </td>

                                </tr>
                            </table>


                        </div><!-- /.box-body -->

                    </div>
                </div>
            </div>
        </div>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
        <i class="large mdi-editor-mode-edit"></i>
    </a>
    <ul>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-border-color')), array('action' => 'edit', $user['User']['id']), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li><?php
            echo $this->Form->postLink(
                    $this->Html->tag('i', '', array('class' => " large mdi-action-delete")
                    ), array(
                'action' => 'delete',
                $user['User']['id']
                    ), array(
                'class' => 'btn-floating red', 'escape' => FALSE,
                'title' => "Click to delete"
                    ), __('Are you sure you want to delete # %s?', $user['User']['username'])
            );
            ?></li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-list')), array('action' => 'index'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
    </ul>
</div>