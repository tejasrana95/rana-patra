<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
        <?php
        echo $this->Form->create('User', array(
            "class" => "login-form",
            "role" => "form",
            'inputDefaults' => array(
                'label' => false,
                'div' => false
        )));
        ?>
        <div class="row">
            <div class="input-field col s12 center">
                <?php echo $this->Html->image('/images/infilogo1.png', array('alt' => '','class'=>" responsive-img valign ")); ?>
                <p class="center login-form-text">asd</p>
            </div>
        </div>
        
        <div class="row margin">
            <div class="input-field col s12">
                <i class="mdi-social-person-outline prefix"></i>
                <?php echo $this->Form->input('username', array("placeholder" => "User name", "id" => "username", 'autocomplete' => 'off')); ?>
                <label for="username" class="center-align">Username</label>
            </div>
        </div>
        <div class="row margin">
            <div class="input-field col s12">
                <i class="mdi-action-lock-outline prefix"></i>
                <?php echo $this->Form->input('password', array("id" => "password", "placeholder" => "Password", 'autocomplete' => 'off')); ?>

                <label for="password">Password</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <?php
                echo $this->Form->button('Login', array("class" => "btn waves-effect waves-light col s12", "type" => "submit"));
                ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
