<?php
App::import('Controller', 'UserRoles');
$UserRolesController = new UserRolesController;
?>
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

  <div id="basic-form" class="section">
            <div class="row">
    <div class="col s12 m12 l12">
        <div class="card-panel">
            <h4 class="header2"><?php echo __($page_name); ?></h4>
            <div class="row">
                <?php
                echo $this->Form->create('User', array('type' => 'file'));
                ?>
                <?php
                echo $this->Form->input('id');
                ?>
                <div class="box-body">
                    
                    <div class="row">

                        <div class="input-field col s12">
                            <?php
                            echo $this->Form->input('username', array('div' => array('class' => "form-group"), 'class' => "form-control", 'empty' => ''));
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <?php
                            echo $this->Form->input('password', array('div' => array('class' => "form-group"), 'class' => "form-control"));
                            ?>
                        </div>
                        <div class="input-field col s6">
                            <?php
                            echo $this->Form->input('confirm_password', array('type' => "password", 'div' => array('class' => "form-group"), 'class' => "form-control"));
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <?php
                            echo $this->Form->input('role_id', array('options' => $DropuserRole,
                                'empty' => "Please select user role",
                                'div' => array('class' => "form-group"),
                                'class' => "form-control",
                                'label' => false,));
                            ?>
                        </div>
                        <div class="switch col s6">
                            <?php
                            $is_admin = $UserRolesController->getPermission($this->request->data['User']['id']);
                            if ($is_admin['is_admin'] != 1) {
                                ?>
                                Status : 
                                <label>
                                    Off
                                    <?php
                                    echo $this->Form->input('status', array('div' => false, 'label' => false, 'type' => "checkbox", 'class' => ""));
                                    ?>
                                    <span class="lever"></span> On
                                </label>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">

                                                <div class="input-field col s12 m10 l10">
                                                    <?php

                                                    echo $this->Form->input('pic', array('type' => "text", "readonly" => 'readonly', "label" => "Profile Pic", 'id' => 'bannerpath', 'div' => array('class' => "form-group"), "value" => $this->data['User']['pic'], 'class' => "form-control"));
                                                    ?>
                                                </div>
                                                <div class="input-field col s12 m2 l2">
                                                    <?php
                                                    echo $this->Form->button('Select Banner', array('type' => "button", 'id' => "selectflag", "onclick" => "openfilemanager('bannerpath')", 'class' => "waves-effect waves-light btn "));
                                                    ?>
                                                </div>
                                                <div class="input-field col s12 m12 l12">
                                                    <div id="bannerpath_wrapper" style="display: none;">
                                                        <img src="" id="bannerpath_img" style="max-width:300px;">
                                                        <br/><a href="javascript:void()" onclick="removeimage('bannerpath')">Remove</a>
                                                    </div>
                                                </div>

                                                <?php
                                                if (isset($this->data['User']['pic']) && !empty($this->data['User']['pic'])) {
                                                    ?>
                                                    <script>
                                                    $('#bannerpath_img').attr("src", '<?php echo $this->data['User']['pic']; ?>');
                                                        $('#bannerpath_wrapper').fadeIn();
                                                    </script>
                                                    <?php
                                                }
                                                ?>
                                            </div>

                                        </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <?php
                            echo $this->Form->input('Save', array('label' => false, 'div' => array('class' => "form-group"), 'type' => "submit", 'class' => "btn btn-success btn-lg"));
                            ?>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <?php echo $this->Form->end(); ?>
            </div><!-- /.box -->
        </div>
    </div>
</div></div>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
        <i class="large mdi-editor-mode-edit"></i>
    </a>
    <ul>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-pageview')), array('action' => 'view', $this->request->data['User']['id']), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li>
            <?php
            echo $this->Form->postLink(
                    $this->Html->tag('i', '', array('class' => " large mdi-action-delete")
                    ), array(
                'action' => 'delete',
                $this->request->data['User']['id']
                    ), array(
                'class' => 'btn-floating red', 'escape' => FALSE,
                'title' => "Click to delete"
                    ), __('Are you sure you want to delete # %s?', $this->request->data['User']['username'])
            );
            ?>

        </li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-list')), array('action' => 'index'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
    </ul>
</div>
