<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="section">
        <!--Basic Form-->
        <div id="basic-form" class="section">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card-panel">
                        <h4 class="header2"><?php echo __($page_name); ?></h4>

                        <div class="row">
                            <table class="table table-striped">
                                <tr>
                                    <td><?php echo __('Name'); ?></td>
                                    <td>
                                        <?php echo h($userrole['UserRole']['name']); ?>
                                    </td>

                                </tr>
                                <tr>
                                    <td><?php echo __('Roles'); ?></td>
                                    <td>
                                        <fieldset>
                                            <legend>Page: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['page']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['page']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['page']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['page']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Category: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['category']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['category']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['category']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['category']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/><fieldset>
                                            <legend>Articles: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['article']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['article']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['article']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['article']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Articles: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['customer']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['customer']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['customer']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['customer']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Invoice: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['invoice']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['invoice']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['invoice']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['invoice']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Invoice: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['invoice']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['invoice']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['invoice']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['invoice']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/>
                                         <fieldset>
                                            <legend>Matrimonial: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['matrimonal']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['matrimonal']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['matrimonal']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['matrimonal']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Category for articles: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['articlecategory']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['articlecategory']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['articlecategory']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['articlecategory']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>News: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['news']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['news']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['news']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['news']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>

                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Career: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['career']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['career']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['career']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['career']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Event: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['event']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['event']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['event']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['event']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>

                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>History: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['history']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['history']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['history']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['history']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Awards: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['awards']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['awards']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['awards']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['awards']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>

                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Customer Logo: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['customer_logo']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['customer_logo']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['customer_logo']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['customer_logo']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Partner Speak: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['partner_speak']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['partner_speak']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['partner_speak']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['partner_speak']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Customer Speak: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['customer_speak']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['customer_speak']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['customer_speak']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['customer_speak']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>

                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Employee Speak: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['latest_in_hr']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['latest_in_hr']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['latest_in_hr']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['latest_in_hr']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Media Kit: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['media_kit']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['media_kit']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['media_kit']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['media_kit']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Insight Management: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['insight']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['insight']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['insight']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['insight']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>                                       
                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Insight Type: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['insight_type']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['insight_type']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['insight_type']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['insight_type']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Gallery Management: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['gallery']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['gallery']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['gallery']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['gallery']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Gallery Category: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['gallery_category']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['gallery_category']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['gallery_category']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['gallery_category']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>

                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Team Management: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['team']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['team']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['team']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['team']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Team Category: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['team_category']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['team_category']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['team_category']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['team_category']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>

                                        </fieldset>

                                        <br/>
                                        <fieldset>
                                            <legend>Infographics: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['infographics']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['infographics']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['infographics']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['infographics']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>

                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <legend>Blog: </legend>
                                            <label>Add : </label>
                                            <?php if ($userrole['UserRole']['roles']['blog']['add']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Edit : </label>
                                            <?php if ($userrole['UserRole']['roles']['blog']['edit']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Delete : </label>
                                            <?php if ($userrole['UserRole']['roles']['blog']['delete']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                            &nbsp;<label>Super Admin Review : </label>
                                            <?php if ($userrole['UserRole']['roles']['blog']['review']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>

                                        </fieldset>
                                        <br/>
                                        <fieldset>
                                            <label>Super Admin : </label>
                                            <?php if ($userrole['UserRole']['is_admin']) { ?>
                                                <i class=" fa fa-check"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-times"></i>
                                            <?php } ?>
                                        </fieldset>
                                    </td>

                                </tr>
                                <tr>
                                    <td><?php echo __('Status'); ?></td>
                                    <td>
                                        <?php
                                        if ($userrole['UserRole']['status'] == INFI_ACTIVE_STATUS) {
                                            echo $this->Form->postLink(
                                                    $this->Html->tag('i', '', array('class' => " fa fa-check")
                                                    ), array(
                                                'action' => 'inactive',
                                                $userrole['UserRole']['id']
                                                    ), array(
                                                'escape' => false,
                                                'title' => "Click to inactive"
                                            ));
                                        } else {
                                            echo $this->Form->postLink(
                                                    $this->Html->tag('i', '', array('class' => " fa fa-times")
                                                    ), array(
                                                'action' => 'active',
                                                $userrole['UserRole']['id']
                                                    ), array(
                                                'escape' => false,
                                                'title' => "Click to active"
                                            ));
                                        }
                                        ?>
                                    </td>

                                </tr>
                                <tr>
                                    <td><?php echo __('Created'); ?></td>
                                    <td>
                                        <?php echo $this->Time->format($userrole['UserRole']['created'], '%B %e, %Y %H:%M %p'); ?>
                                    </td>

                                </tr>
                                <tr>
                                    <td><?php echo __('Modified'); ?></td>
                                    <td>
                                        <?php echo $this->Time->format($userrole['UserRole']['modified'], '%B %e, %Y %H:%M %p'); ?>
                                    </td>

                                </tr>
                            </table>

                        </div><!-- /.box-body -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
        <i class="large mdi-editor-mode-edit"></i>
    </a>
    <ul>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-border-color')), array('action' => 'edit', $userrole['UserRole']['id']), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li><?php
            echo $this->Form->postLink(
                    $this->Html->tag('i', '', array('class' => " large mdi-action-delete")
                    ), array(
                'action' => 'delete',
                $userrole['UserRole']['id']
                    ), array(
                'class' => 'btn-floating red', 'escape' => FALSE,
                'title' => "Click to delete"
                    ), __('Are you sure you want to delete # %s?', $userrole['UserRole']['name'])
            );
            ?></li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-list')), array('action' => 'index'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
    </ul>
</div>