<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <!-- Search for small screen -->
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title"><?php echo $page_title; ?></h5>
                <ol class="breadcrumb">
                    <li><a href="<?php echo Router::url('/', true); ?>">Dashboard</a>
                    </li>
                    <li class="active"><?php echo $page_name; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 m12 l12">
        <div class="card-panel">
            <h4 class="header2"><?php echo __($page_name); ?></h4>
            <div class="row">
                <?php
                echo $this->Form->create('UserRole', array('type' => 'file'));
                ?>
                <?php
                echo $this->Form->input('id');
                ?>
                <div class="box-body">
                    
                    <div class="row">

                        <div class="input-field col s12">
                            <?php
                            echo $this->Form->input('name', array('div' => array('class' => "form-group"), 'class' => "form-control", 'empty' => ''));
                            ?>
                        </div>
                    </div>
                    <div class="row">


                        <div class="switch col s12">
                            Status : 
                            <label>
                                Off
                                <?php
                                echo $this->Form->input('status', array('div' => false, 'label' => false, 'type' => "checkbox", 'class' => ""));
                                ?>
                                <span class="lever"></span> On
                            </label>


                        </div>
                    </div>
                    <div class="row">
                        <fieldset>
                            <legend>Page: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.page.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.page.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.page.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.page.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>Category: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.category.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.category.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.category.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.category.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                         <fieldset>
                        <legend>Article: </legend>
                        
                        <?php
                        echo $this->Form->input('UserRole.roles.article.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                        ?>
                        &nbsp;
                        <?php
                        echo $this->Form->input('UserRole.roles.article.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                        ?>
                        &nbsp;
                        <?php
                        echo $this->Form->input('UserRole.roles.article.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                        ?>
                        &nbsp;
                        <?php
                        echo $this->Form->input('UserRole.roles.article.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                        ?>
                    </fieldset>
                    <br/>
                    <fieldset>
                        <legend>Category for Articles: </legend>
                        
                        <?php
                        echo $this->Form->input('UserRole.roles.articlecategory.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                        ?>
                        &nbsp;
                        <?php
                        echo $this->Form->input('UserRole.roles.articlecategory.edit', array('div' => false,  'label' => ": Edit",'type' => "checkbox", 'class' => "roles"));
                        ?>
                        &nbsp;
                        <?php
                        echo $this->Form->input('UserRole.roles.articlecategory.delete', array('div' => false,  'label' => ": Delete",'type' => "checkbox", 'class' => "roles"));
                        ?>
                        &nbsp;
                        <?php
                        echo $this->Form->input('UserRole.roles.articlecategory.review', array('div' => false,  'label' => ": Super Admin Review",'type' => "checkbox", 'class' => "filled-in"));
                        ?>
                    </fieldset>
                      <br/>
                    <fieldset>
                        <legend>Customer: </legend>
                        
                        <?php
                        echo $this->Form->input('UserRole.roles.customer.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                        ?>
                        &nbsp;
                        <?php
                        echo $this->Form->input('UserRole.roles.customer.edit', array('div' => false,  'label' => ": Edit",'type' => "checkbox", 'class' => "roles"));
                        ?>
                        &nbsp;
                        <?php
                        echo $this->Form->input('UserRole.roles.customer.delete', array('div' => false,  'label' => ": Delete",'type' => "checkbox", 'class' => "roles"));
                        ?>
                        &nbsp;
                        <?php
                        echo $this->Form->input('UserRole.roles.customer.review', array('div' => false,  'label' => ": Super Admin Review",'type' => "checkbox", 'class' => "filled-in"));
                        ?>
                    </fieldset>
                    <br/>
                    <fieldset>
                        <legend>Invoice: </legend>
                        
                        <?php
                        echo $this->Form->input('UserRole.roles.invoice.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                        ?>
                        &nbsp;
                        <?php
                        echo $this->Form->input('UserRole.roles.invoice.edit', array('div' => false,  'label' => ": Edit",'type' => "checkbox", 'class' => "roles"));
                        ?>
                        &nbsp;
                        <?php
                        echo $this->Form->input('UserRole.roles.invoice.delete', array('div' => false,  'label' => ": Delete",'type' => "checkbox", 'class' => "roles"));
                        ?>
                        &nbsp;
                        <?php
                        echo $this->Form->input('UserRole.roles.invoice.review', array('div' => false,  'label' => ": Super Admin Review",'type' => "checkbox", 'class' => "filled-in"));
                        ?>
                    </fieldset>
                    <br/>
                    <fieldset>
                        <legend>Matrimonial: </legend>
                        
                        <?php
                        echo $this->Form->input('UserRole.roles.matrimonal.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                        ?>
                        &nbsp;
                        <?php
                        echo $this->Form->input('UserRole.roles.matrimonal.edit', array('div' => false,  'label' => ": Edit",'type' => "checkbox", 'class' => "roles"));
                        ?>
                        &nbsp;
                        <?php
                        echo $this->Form->input('UserRole.roles.matrimonal.delete', array('div' => false,  'label' => ": Delete",'type' => "checkbox", 'class' => "roles"));
                        ?>
                        &nbsp;
                        <?php
                        echo $this->Form->input('UserRole.roles.matrimonal.review', array('div' => false,  'label' => ": Super Admin Review",'type' => "checkbox", 'class' => "filled-in"));
                        ?>
                    </fieldset>
                    <br/>
                        <fieldset>
                            <legend>News: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.news.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.news.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.news.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.news.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>Career: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.career.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.career.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.career.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.career.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>Event: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.event.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.event.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.event.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.event.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>History: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.history.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.history.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.history.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.history.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>Awards: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.awards.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.awards.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.awards.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.awards.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>Customer Logo: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.customer_logo.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.customer_logo.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.customer_logo.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.customer_logo.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>Partner Speak: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.partner_speak.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.partner_speak.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.partner_speak.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.partner_speak.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>Customer Speak: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.customer_speak.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.customer_speak.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.customer_speak.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.customer_speak.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>

                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>Employee Speak: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.latest_in_hr.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.latest_in_hr.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.latest_in_hr.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.latest_in_hr.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>Media Kit: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.media_kit.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.media_kit.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.media_kit.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.media_kit.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>Insight Management: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.insight.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.insight.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.insight.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.insight.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>Insight Type: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.insight_type.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.insight_type.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.insight_type.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.insight_type.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>Gallery Management: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.gallery.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.gallery.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.gallery.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.gallery.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => ""));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>Gallery Category: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.gallery_category.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.gallery_category.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.gallery_category.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.gallery_category.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>Team Management: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.team.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.team.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.team.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.team.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>Team Category: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.team_category.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.team_category.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.team_category.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.team_category.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>

                        <fieldset>
                            <legend>Infographics: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.infographics.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.infographics.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.infographics.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.infographics.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <legend>Blog: </legend>

                            <?php
                            echo $this->Form->input('UserRole.roles.blog.add', array('div' => false, 'label' => ": Add", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.blog.edit', array('div' => false, 'label' => ": Edit", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.blog.delete', array('div' => false, 'label' => ": Delete", 'type' => "checkbox", 'class' => "roles"));
                            ?>
                            &nbsp;
                            <?php
                            echo $this->Form->input('UserRole.roles.blog.review', array('div' => false, 'label' => ": Super Admin Review", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                        <br/>
                        <fieldset>
                            <?php
                            echo $this->Form->input('is_admin', array('div' => false, 'label' => ": Super Admin", 'type' => "checkbox", 'class' => "filled-in"));
                            ?>
                        </fieldset>
                    </div>
                    <?php
                    echo $this->Form->input('Save', array('label' => FALSE, 'div' => array('class' => "form-group"), 'type' => "submit", 'class' => "btn btn-success btn-lg"));
                    ?>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div><!-- /.box -->
    <?php echo $this->Form->end(); ?>
</div><!-- /.box -->
</div>
</div>
</div>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
        <i class="large mdi-editor-mode-edit"></i>
    </a>
    <ul>
         <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-pageview')), array('action' => 'view', $this->request->data['UserRole']['id']), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li>
            <?php
            echo $this->Form->postLink(
                    $this->Html->tag('i', '', array('class' => " large mdi-action-delete")
                    ), array(
                'action' => 'delete',
                $this->request->data['UserRole']['id']
                    ), array(
                'class' => 'btn-floating red', 'escape' => FALSE,
                'title' => "Click to delete"
                    ), __('Are you sure you want to delete # %s?', $this->request->data['UserRole']['name'])
            );
            ?>

        </li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-action-list')), array('action' => 'index'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
        <li><?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'large mdi-editor-mode-comment')), array('action' => 'add'), array('class' => 'btn-floating red', 'escape' => FALSE)); ?></li>
    </ul>
</div>
