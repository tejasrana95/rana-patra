<?php

App::uses('AppModel', 'Model');

/**
 * Article Model
 *
 */
class Article extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'article_name';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'article_name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Article name can not be left empty.',
                'required' => true,
            ),
        ),
        'layout' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Article layout can not be left empty',
                'required' => true,
            ),
        ),
        'article_title' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Article title can not be left empty',
                'required' => true,
            ),
        ),
        'article_heading' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Article heading can not be left empty',
            ),
        ),
        'main_category' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Article category can not be left empty',
            ),
        ),
        'content' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Article content can not be left empty',
                'allowEmpty' => true,
                'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    /**
     * Before Save Callback
     * @param array $options
     * @return boolean
     */
    public function beforeSave($options = array()) {

        // using to create article slug from article name
        if (isset($this->data[$this->alias]['article_name']) && !empty($this->data[$this->alias]['article_name'])) {
            if (empty($this->data[$this->alias]['article_slug'])) {
                $result = $this->find('first', [
                    'conditions' => [
                        $this->alias . '.' . 'article_slug' => Inflector::slug(strtolower($this->data[$this->alias]['article_name']), '-')
                    ]
                ]);
                if (empty($result)) {
                    $this->data[$this->alias]['article_slug'] = Inflector::slug(strtolower($this->data[$this->alias]['article_name']), '-');
                } else {
                    $timeStamp = time();
                    $this->data[$this->alias]['article_slug'] = Inflector::slug(strtolower($this->data[$this->alias]['article_name'] . ' ' . $timeStamp), '-');
                }
            }
        }
        return parent::beforeSave($options);
    }

    /**
     * before validation callback
     * @param array $options
     * @return boolean
     */
    public function beforeValidate($options = array()) {
        parent::beforeSave($options);
        if ($this->data['Article']['banner_image_file'] == '') {
            unset($this->data['Article']['banner_image_file']);
        }
        return true; //this is required, otherwise validation will always fail
    }

}
