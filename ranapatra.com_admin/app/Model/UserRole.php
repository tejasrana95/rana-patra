<?php

App::uses('AppModel', 'Model');

/**
 * User Model
 *
 */
class UserRole extends AppModel {

    public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'A role name is required'
            )
        ),
    );

    public function beforeSave($options = array()) {

        return true;
    }

}
