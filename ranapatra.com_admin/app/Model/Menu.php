<?php

App::uses('AppModel', 'Model');
App::uses('Page', 'Model');

/**
 * Menu Model
 *
 */
class Menu extends AppModel {

    public $name = 'Menu';
    public $menu_type = array(
        'header-menu' => 'Header Menu',
        'footer-menu' => 'Footer Menu',
        'top-menu' => 'Top Menu',
    );
    public $model_list = array(
        'services' => 'Services',
        'newsletters' => 'Newsletters',
        'news' => 'News',
        'datasheets' => 'Datasheets',
        'events' => 'Events',
        'partners' => 'Partners',
        'whitepapers' => 'Whitepapers',
        'webinars' => 'Webinars',
        'customers' => 'Customers'
    );

}

?>