<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 *
 */
class User extends AppModel {
	 public $validate = array(
		'username' => array(
            'alphaNumeric' => array(
                'rule' => 'alphaNumeric',
                'required' => true,
                'message' => 'Letters and numbers only'
            ),
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'A password is required'
            ),
		  			'alphaNumeric' => array(
        			'rule'     => array('custom', '/(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/'),
        			//'required' => true,
        			'message' => 'Password should be at least 7 characters, must have at least 1 upper case, lower case ,numeric, special character.',
       			// 'on' => 'create'
    	 		)
		),
		'confirm_password' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Re-enter your password.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
			),
		  'alphaNumeric' => array(
        'rule'     => array('custom', '/(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/'),
        //'required' => true,
        'message' => 'Password should be at least 7 characters, must have at least 1 upper case, lower case ,numeric, special character.',
        'on' => 'create'
     ),
			'checkMatch' => array(
			 'rule'=> array('compare_password'),
			 'message' => "Both password not matched."
			)
			
		)
   );
   public function beforeSave($options = array()) {
    if (isset($this->data[$this->alias]['password'])) {
        $passwordHasher = new SimplePasswordHasher();
        $this->data[$this->alias]['password'] = $passwordHasher->hash(
            $this->data[$this->alias]['password']
        );
    }
    return true;
	 }
	 public function compare_password(){
	 if($this->data[$this->alias]['password']===$this->data[$this->alias]['confirm_password']){
	  return true;	
	 }
	}
}
