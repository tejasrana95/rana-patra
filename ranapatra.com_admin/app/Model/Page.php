<?php

App::uses('AppModel', 'Model');

/**
 * Page Model
 *
 */
class Page extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'page_name';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'page_name' => array(
            'notBlank' => array(
                'rule' => array('checkUnique', array("page_name")),
                'message' => 'Page name already exists.',
                'required' => true,
            ),
        ),
        'layout' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Page layout can not be left empty',
                'required' => true,
            ),
        ),
        'page_title' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Page title can not be left empty',
                'required' => true,
            ),
        ),
        'page_heading' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Page heading can not be left empty',
            ),
        ),
        
        'main_category' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Page category can not be left empty',
            ),
        ),
        'content' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Page content can not be left empty',
                'allowEmpty' => true,
                'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'banner_image_file' => array(
            'extension' => array(
                'rule' => array('extension', array('gif', 'png', 'jpg', 'jpeg')),
                'message' => 'Select Valid Image. Please use jpeg, jpg, png or gif type image.',
                'required' => FALSE,
                'allowEmpty' => TRUE,
            ),
        )
    );

    /**
     * Before Save Callback
     * @param array $options
     * @return boolean
     */
    public function beforeSave($options = array()) {

        // using to create page slug from page name
        if (isset($this->data[$this->alias]['page_name']) && !empty($this->data[$this->alias]['page_name'])) {
            if (empty($this->data[$this->alias]['page_slug'])) {
                $this->data[$this->alias]['page_slug'] = Inflector::slug(strtolower($this->data[$this->alias]['page_name']), '-');
            }
        }
        return parent::beforeSave($options);
    }

    /**
     * before validation callback
     * @param array $options
     * @return boolean
     */
    public function beforeValidate($options = array()) {
        parent::beforeSave($options);
        if ($this->data['Page']['banner_image_file'] == '') {
            unset($this->data['Page']['banner_image_file']);
        }
        return true; //this is required, otherwise validation will always fail
    }

}
