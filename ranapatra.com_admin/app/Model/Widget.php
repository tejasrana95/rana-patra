<?php

App::uses('AppModel', 'Model');

/**
 * Widget Model
 *
 */
class Widget extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notBlank' => array(
                'rule' => array('checkUnique', array("name",)),
                'message' => 'Widget already exists.',
                'required' => true,
            ),
        ),
       
        'position' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Please select position.',
            ),
        ),
    );

    /**
     * Before Save Callback
     * @param array $options
     * @return boolean
     */
    //before save callback function
    public function beforeSave($options = array()) {
        // using to create whitepaper slug from title
        if (isset($this->data[$this->alias]['name']) && !empty($this->data[$this->alias]['name'])) {
            if (empty($this->data[$this->alias]['identifier'])) {
                $this->data[$this->alias]['identifier'] = Inflector::slug(strtolower($this->data[$this->alias]['name']), '-');
            }
        }
        return parent::beforeSave($options);
    }

}
