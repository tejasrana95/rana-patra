<?php

App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 */
class CustomerProfile extends AppModel {

    public $validate = array(
        'name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Name can not be left empty',
                'required' => true,
            ),
        ),
        'gender' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Gender can not be left empty',
                'required' => true,
            ),
        ),
        'address' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Address can not be left empty',
                'required' => true,
            ),
        ),
        'contact' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Contact can not be left empty',
                'required' => true,
            ),
        ),
        'state' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'State can not be left empty',
                'required' => true,
            ),
        ),
        'country' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Country can not be left empty',
                'required' => true,
            ),
        ),
    );

    public function beforeSave($options = array()) {
       
        return true;
    }

}
