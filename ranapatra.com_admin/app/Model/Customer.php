<?php

App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 */
class Customer extends AppModel {

    public $validate = array(
        'username' => array(
            'alphaNumeric' => array(
                'rule' => 'alphaNumeric',
                'required' => true,
                'message' => 'Letters and numbers only'
            ), 'between' => array(
                'rule' => array('between', 4, 20),
                'message' => "The username must be between %d and %d characters.",
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => "This username is already taken.",
            ),
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'A password is required'
            ),
//            'alphaNumeric' => array(
//               // 'rule' => array('custom', '/(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/'),
//                //'required' => true,
//              //  'message' => 'Password should be at least 7 characters, must have at least 1 upper case, lower case ,numeric, special character.',
//            // 'on' => 'create'
//            )
        ),
        'confirm_password' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Re-enter your password.',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            ),
            'checkMatch' => array(
                'rule' => array('compare_password'),
                'message' => "Both password not matched."
            )
        ),
        'email' => array(
            'rule' => array('email', true),
            'message' => 'Please enter a valid email address.',
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => "This username is already taken.",
            ),
            
        )
    );

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new SimplePasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                    $this->data[$this->alias]['password']
            );
        }
        return true;
    }

    public function compare_password() {
        if ($this->data[$this->alias]['password'] === $this->data[$this->alias]['confirm_password']) {
            return true;
        }
    }

}
