<?php

App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 */
class MatrimonialAttachment extends AppModel {

    public $validate = array(
        'file_name' => array(
            'rule' => 'notBlank',
            'message' => 'File name required'
        ),
        'file' => array(
            'rule' => array(
                'extension',
                array('gif', 'jpeg', 'png', 'jpg')
            ),
            'required' => true,
            'message' => 'Please supply a valid image.'
        )
    );

}
