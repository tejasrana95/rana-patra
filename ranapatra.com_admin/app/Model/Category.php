<?php

App::uses('AppModel', 'Model');

/**
 * Page Model
 *
 */
class Category extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'label';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'label' => array(
            'notBlank' => array(
                'rule' => array('checkUnique', array("label", "parent")),
                'message' => 'Category name already exists.',
                'required' => true,
            ),
        ),
        'img' => array(
            'rule1' => array(
                'rule' => array('extension', array('jpeg', 'jpg', 'png', 'gif')),
                'message' => 'Select Valid Image. Please use jpeg, jpg, png or gif type image.',
                'last' => true,
                'allowEmpty' => true,
            ),
        ),
    );

    /**
     * Before Save Callback
     * @param array $options
     * @return boolean
     */
    public function beforeSave($options = array()) {

        // using to create page slug from page name
        if (isset($this->data[$this->alias]['label']) && !empty($this->data[$this->alias]['label'])) {
            if (empty($this->data[$this->alias]['identifier'])) {
                $this->data[$this->alias]['identifier'] = Inflector::slug(strtolower($this->data[$this->alias]['label']), '-');
            }
        }
        return parent::beforeSave($options);
    }

}
