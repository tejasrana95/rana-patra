<?php

App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 */
class CustomerInvoice extends AppModel {

    public $validate = array(
        'customerId' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Select Customer Please'
            ),
        ),
        'item_type' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Item type required.'
            ),
        ),
        'item_name' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Item name required.'
            ),
        ),
        'item_number' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Item number required.'
            ),
        ),
        'payment_status' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Payment status required.'
            ),
        ),
        'mc_gross' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Gross required.'
            ),
        ),
        'mc_currency' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Currency status required.'
            ),
        ),
        'payment_status' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Payment status required.'
            ),
        ),
        'txn_id' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'TXN Id required.'
            ),
        ),
        'payment_gateway' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Payment gateway required.'
            ),
        ),
        'process_by' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Process by required.'
            ),
        ),
        'receiver_email' => array(
            'rule' => array('email', true),
            'message' => 'Please enter a valid email address.',
        ),
        'payer_email' => array(
            'rule' => array('email', true),
            'message' => 'Please enter a valid email address.',
        )
    );

}
