$(document).ready(function ()
{
    var updateOutput = function (e)
    {
        var list = e.length ? e : $(e.target),
                output = list.data('output');

        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };
    // activate Nestable for list 1
    $('#nestable').nestable({
        group: 1,
        maxDepth: 5
    })
            .on('change', updateOutput);

    $('#nestable-menu').on('click', function (e)
    {
        var target = $(e.target),
                action = target.data('action');
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });

    // to add menu items for models 
    var a = []; //auxiliary array to store items added into the menu 
    $('#model-add').click(function (event) {
        $(".model-menu-item").each(function (index) {

            if ($(this).is(':checked') && jQuery.inArray($(this).val(), a) == -1)
            {
                a.push($(this).val());
                $('#menu-list').append(
                        "<li class='dd-item' data-type='model' data-id=" + $(this).val() +
                        " id=" + "menu-item-" + $(this).val() +
                        "><div class='dd-handle'>" +
                        $(this).val() +
                        "</li>");

            }
            if (!$(this).is(':checked') && jQuery.inArray($(this).val(), a) != -1) {
                a.splice($.inArray($(this).val(), a), 1);
                $('#menu-item-' + $(this).val()).remove();

            }

        });
        updateOutput($('#nestable').data('output', $('#nestable-output')));
    });

// to add menu items for category 
    var a = []; //auxiliary array to store items added into the menu 
    $('#category-add').click(function (event) {
        $(".category-menu-item").each(function (index) {

            if ($(this).is(':checked') && jQuery.inArray($(this).val(), a) == -1)
            {
                a.push($(this).val());
                $('#menu-list').append(
                        "<li class='dd-item' data-type='category' data-id=" + $(this).val() +
                        " id=" + "category-item-" + $(this).val() +
                        "><div class='dd-handle'>" +
                        $(this).attr("id") +
                        "</li>");

            }
            if (!$(this).is(':checked') && jQuery.inArray($(this).val(), a) != -1) {
                a.splice($.inArray($(this).val(), a), 1);
                $('#menu-item-' + $(this).val()).remove();

            }

        });
        updateOutput($('#nestable').data('output', $('#nestable-output')));
    });

    $('#insight-add').click(function (event) {
        $(".insight-menu-item").each(function (index) {

            if ($(this).is(':checked') && jQuery.inArray($(this).val(), a) == -1)
            {
                a.push($(this).val());
                $('#menu-list').append(
                        "<li class='dd-item' data-type='insight' data-id=" + $(this).val() +
                        " id=" + "insight-item-" + $(this).val() +
                        "><div class='dd-handle'>" +
                        $(this).attr("id") +
                        "</li>");

            }
            if (!$(this).is(':checked') && jQuery.inArray($(this).val(), a) != -1) {
                a.splice($.inArray($(this).val(), a), 1);
                $('#menu-item-' + $(this).val()).remove();

            }

        });
        updateOutput($('#nestable').data('output', $('#nestable-output')));
    });
    updateOutput($('#nestable').data('output', $('#nestable-output')));

    // to add menu items for pages 
    var b = []; //auxiliary array to store items added into the menu 
    $('#page-add').click(function (event) {
     
        $(".page-menu-item").each(function (index) {

            if ($(this).is(':checked') && jQuery.inArray($(this).val(), b) == -1)
            {
                b.push($(this).val());
                //console.log("from first :: "+ $(this).val()+ " " + b);
                $('#menu-list').append(
                        "<li class='dd-item' data-type='page' data-id=" +
                        $(this).attr('id') +
                        " id=" + "menu-item-" +
                        "'" + $(this).val() + "'><div class='dd-handle'>" +
                        $(this).val() +
                        "</div></li>");
            }
            if (!$(this).is(':checked') && jQuery.inArray($(this).val(), b) != -1) {
                b.splice($.inArray($(this).val(), a), 1);
                //console.log("removing :: " + $(this).val()+" array" + b);
                $('#menu-item-' + $(this).val()).remove();

            }

        });
        updateOutput($('#nestable').data('output', $('#nestable-output')));
    });

    $('#custom-add').click(function (event) {
        if ($("#custom-name").val() != "" && $("#custom-url").val() != "") {
            $("#menu-list").append(
                    "<li class='dd-item' data-type='custom' data-id='" + $("#custom-name").val() + "' id='menu-item-" + $("#custom-name").val() + "' data-url='" + $("#custom-url").val() + "' data-icon='" + $("#custom-icon").val() + "'  data-login='" + $("#login_only").val() + "' ><div class='dd-handle'>" + $("#custom-name").val() +
                    "</div></li>");
            updateOutput($('#nestable').data('output', $('#nestable-output')));
            $("#custom-name").val('');
            $("#custom-url").val('');
            $("#custom-icon").val('');
             $("#login_only").val('');
        } else {
            alert('Please enter title and url');
            return false;
        }
    });
    $('#custom-html-add').click(function (event) {
        for (i = 0; i < tinymce.editors.length; i++) {
            var content = tinymce.editors[i].getContent(); // get the content

            // Create Base64 Object
            var Base64 = {_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) {
                    var t = "";
                    var n, r, i, s, o, u, a;
                    var f = 0;
                    e = Base64._utf8_encode(e);
                    while (f < e.length) {
                        n = e.charCodeAt(f++);
                        r = e.charCodeAt(f++);
                        i = e.charCodeAt(f++);
                        s = n >> 2;
                        o = (n & 3) << 4 | r >> 4;
                        u = (r & 15) << 2 | i >> 6;
                        a = i & 63;
                        if (isNaN(r)) {
                            u = a = 64
                        } else if (isNaN(i)) {
                            a = 64
                        }
                        t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
                    }
                    return t
                }, decode: function (e) {
                    var t = "";
                    var n, r, i;
                    var s, o, u, a;
                    var f = 0;
                    e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
                    while (f < e.length) {
                        s = this._keyStr.indexOf(e.charAt(f++));
                        o = this._keyStr.indexOf(e.charAt(f++));
                        u = this._keyStr.indexOf(e.charAt(f++));
                        a = this._keyStr.indexOf(e.charAt(f++));
                        n = s << 2 | o >> 4;
                        r = (o & 15) << 4 | u >> 2;
                        i = (u & 3) << 6 | a;
                        t = t + String.fromCharCode(n);
                        if (u != 64) {
                            t = t + String.fromCharCode(r)
                        }
                        if (a != 64) {
                            t = t + String.fromCharCode(i)
                        }
                    }
                    t = Base64._utf8_decode(t);
                    return t
                }, _utf8_encode: function (e) {
                    e = e.replace(/\r\n/g, "\n");
                    var t = "";
                    for (var n = 0; n < e.length; n++) {
                        var r = e.charCodeAt(n);
                        if (r < 128) {
                            t += String.fromCharCode(r)
                        } else if (r > 127 && r < 2048) {
                            t += String.fromCharCode(r >> 6 | 192);
                            t += String.fromCharCode(r & 63 | 128)
                        } else {
                            t += String.fromCharCode(r >> 12 | 224);
                            t += String.fromCharCode(r >> 6 & 63 | 128);
                            t += String.fromCharCode(r & 63 | 128)
                        }
                    }
                    return t
                }, _utf8_decode: function (e) {
                    var t = "";
                    var n = 0;
                    var r = c1 = c2 = 0;
                    while (n < e.length) {
                        r = e.charCodeAt(n);
                        if (r < 128) {
                            t += String.fromCharCode(r);
                            n++
                        } else if (r > 191 && r < 224) {
                            c2 = e.charCodeAt(n + 1);
                            t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                            n += 2
                        } else {
                            c2 = e.charCodeAt(n + 1);
                            c3 = e.charCodeAt(n + 2);
                            t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                            n += 3
                        }
                    }
                    return t
                }}
            //end

            $('#custom_html').val(Base64.encode(content)); // put it in the textarea
        }
        if ($("#custom_html_name").val() != "" && $("#custom_html").val() != "") {
            $("#menu-list").append(
                    "<li class='dd-item' data-type='customHTML' data-id='" + $("#custom_html_name").val() + "' id='menu-item-" + $("#custom_html_name").val() + "' data-url='" + $("#custom_html").val() + "'><div class='dd-handle'>" + $("#custom_html_name").val() +
                    "</div></li>");
            updateOutput($('#nestable').data('output', $('#nestable-output')));
            $("#custom_html_name").val('');
            $("#custom_html").val('');
        } else {
            alert('Please enter title and url');
            return false;
        }
    });

    $('#custom-url').val($('#custom-url').data('prefix'));
    $('#custom_html').val($('#custom_html').data('prefix'));
    original_value = $('#custom-url').val();
    $('#custom-url').keyup(function () {
        var f = $(this),
                prefix = f.data('prefix'),
                rx = new RegExp('^' + escape(prefix));

        new_value = prefix + unescape(escape(f.val()).replace(rx, ''));

        if (original_value != new_value) {
            original_value = new_value;
            f.val(new_value);
        }
    });


});