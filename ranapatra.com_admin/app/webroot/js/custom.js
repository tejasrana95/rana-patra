$('#MatrimonialTob').formatter({
          'pattern': '{{99}}:{{99}}:{{99}}',
});

$('#MatrimonialHeight').formatter({
          'pattern': "{{99}}'"+'{{99}}"',
});
$('#MatrimonialWeight').formatter({
          'pattern': "{{999}} k.g.",
});

$(function () {
    var xhr;
    $('#placefinder').autoComplete({
        minChars: 3,
        source: function (term, suggest) {
            term = term.toLowerCase();
            xhr = $.getJSON('/common/placeFinder/', { placeFinder: term }, function (data) {
                if (data.status === 'OK') {
                    var suggestions = [];
                    $.each(data, function( index, value ) { if(value.hasOwnProperty("location")){suggestions.push(value);}});
                    suggest(suggestions);
                }
            });
        },
        renderItem: function (item, search) {
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
           return '<div class="autocomplete-suggestion" data-place_id="' + item.place_id + '" data-val="' + item.location + '" data-city="'+item.terms[0].value+'" data-state="'+item.terms[1].value+'" data-country="'+item.terms[2].value+'">' + item.location.replace(re, "<b>$1</b>") + '</div>';
        },
        onSelect: function (e, term, item) {
            $("#country").val(item.data('country'));
            $("#city").val(item.data('city'));
            $("#state").val(item.data('state'));
            $("#place_id").val(item.data('place_id'));
        }
    });
});

$(function () {
    var xhr;
    $('#pob').autoComplete({
        minChars: 3,
        source: function (term, suggest) {
            term = term.toLowerCase();
            xhr = $.getJSON('/common/placeFinder/', { placeFinder: term }, function (data) {
                if (data.status === 'OK') {
                    var suggestions = [];
                    $.each(data, function( index, value ) { if(value.hasOwnProperty("location")){suggestions.push(value);}});
                    suggest(suggestions);
                }
            });
        },
        renderItem: function (item, search) {
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
           return '<div class="autocomplete-suggestion" data-place_id="' + item.place_id + '" data-val="' + item.location + '" data-city="'+item.terms[0].value+'" data-state="'+item.terms[1].value+'" data-country="'+item.terms[2].value+'">' + item.location.replace(re, "<b>$1</b>") + '</div>';
        },
        onSelect: function (e, term, item) {
        }
    });
});