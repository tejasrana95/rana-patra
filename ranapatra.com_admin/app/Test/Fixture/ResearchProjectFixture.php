<?php
/**
 * ResearchProjectFixture
 *
 */
class ResearchProjectFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'research_project_category_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'reaearcher_names' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 500, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'research_link' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'release_date' => array('type' => 'date', 'null' => false, 'default' => null),
		'status' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'research_project_category_id' => 1,
			'reaearcher_names' => 'Lorem ipsum dolor sit amet',
			'research_link' => 'Lorem ipsum dolor sit amet',
			'release_date' => '2014-06-23',
			'status' => 1
		),
	);

}
