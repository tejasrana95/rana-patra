<?php
App::uses('DirectorsInvestor', 'Model');

/**
 * DirectorsInvestor Test Case
 *
 */
class DirectorsInvestorTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.directors_investor'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DirectorsInvestor = ClassRegistry::init('DirectorsInvestor');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DirectorsInvestor);

		parent::tearDown();
	}

}
