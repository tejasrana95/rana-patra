<?php
App::uses('SecurityToolCategory', 'Model');

/**
 * SecurityToolCategory Test Case
 *
 */
class SecurityToolCategoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.security_tool_category',
		'app.security_tool'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SecurityToolCategory = ClassRegistry::init('SecurityToolCategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SecurityToolCategory);

		parent::tearDown();
	}

}
