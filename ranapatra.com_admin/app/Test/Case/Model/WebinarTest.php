<?php
App::uses('Webinar', 'Model');

/**
 * Webinar Test Case
 *
 */
class WebinarTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.webinar'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Webinar = ClassRegistry::init('Webinar');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Webinar);

		parent::tearDown();
	}

}
