<?php
App::uses('PartnerType', 'Model');

/**
 * PartnerType Test Case
 *
 */
class PartnerTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.partner_type',
		'app.partner'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PartnerType = ClassRegistry::init('PartnerType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PartnerType);

		parent::tearDown();
	}

}
