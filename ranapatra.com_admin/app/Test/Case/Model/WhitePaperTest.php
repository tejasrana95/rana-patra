<?php
App::uses('WhitePaper', 'Model');

/**
 * WhitePaper Test Case
 *
 */
class WhitePaperTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.white_paper',
		'app.white_paper_log'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WhitePaper = ClassRegistry::init('WhitePaper');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WhitePaper);

		parent::tearDown();
	}

}
