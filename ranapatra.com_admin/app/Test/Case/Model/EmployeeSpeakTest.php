<?php
App::uses('EmployeeSpeak', 'Model');

/**
 * EmployeeSpeak Test Case
 *
 */
class EmployeeSpeakTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.employee_speak'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->EmployeeSpeak = ClassRegistry::init('EmployeeSpeak');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->EmployeeSpeak);

		parent::tearDown();
	}

}
