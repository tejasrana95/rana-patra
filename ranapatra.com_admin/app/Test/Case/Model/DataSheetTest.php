<?php
App::uses('DataSheet', 'Model');

/**
 * DataSheet Test Case
 *
 */
class DataSheetTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.data_sheet'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DataSheet = ClassRegistry::init('DataSheet');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DataSheet);

		parent::tearDown();
	}

}
