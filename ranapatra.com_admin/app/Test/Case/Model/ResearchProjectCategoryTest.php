<?php
App::uses('ResearchProjectCategory', 'Model');

/**
 * ResearchProjectCategory Test Case
 *
 */
class ResearchProjectCategoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.research_project_category',
		'app.research_project'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ResearchProjectCategory = ClassRegistry::init('ResearchProjectCategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ResearchProjectCategory);

		parent::tearDown();
	}

}
