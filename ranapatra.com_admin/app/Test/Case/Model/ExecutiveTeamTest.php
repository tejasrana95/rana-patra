<?php
App::uses('ExecutiveTeam', 'Model');

/**
 * ExecutiveTeam Test Case
 *
 */
class ExecutiveTeamTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.executive_team'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ExecutiveTeam = ClassRegistry::init('ExecutiveTeam');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ExecutiveTeam);

		parent::tearDown();
	}

}
