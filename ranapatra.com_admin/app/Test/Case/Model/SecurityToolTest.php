<?php
App::uses('SecurityTool', 'Model');

/**
 * SecurityTool Test Case
 *
 */
class SecurityToolTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.security_tool',
		'app.security_tool_category'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SecurityTool = ClassRegistry::init('SecurityTool');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SecurityTool);

		parent::tearDown();
	}

}
