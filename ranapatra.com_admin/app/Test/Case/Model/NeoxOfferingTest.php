<?php
App::uses('NeoxOffering', 'Model');

/**
 * NeoxOffering Test Case
 *
 */
class NeoxOfferingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.neox_offering'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->NeoxOffering = ClassRegistry::init('NeoxOffering');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->NeoxOffering);

		parent::tearDown();
	}

}
