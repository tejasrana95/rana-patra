<?php
App::uses('ResearchProject', 'Model');

/**
 * ResearchProject Test Case
 *
 */
class ResearchProjectTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.research_project',
		'app.research_project_category'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ResearchProject = ClassRegistry::init('ResearchProject');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ResearchProject);

		parent::tearDown();
	}

}
