<?php
App::uses('WhitePaperLog', 'Model');

/**
 * WhitePaperLog Test Case
 *
 */
class WhitePaperLogTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.white_paper_log',
		'app.white_paper'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WhitePaperLog = ClassRegistry::init('WhitePaperLog');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WhitePaperLog);

		parent::tearDown();
	}

}
