<?php
App::uses('EmployeeSpeaksController', 'Controller');

/**
 * EmployeeSpeaksController Test Case
 *
 */
class EmployeeSpeaksControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.employee_speak'
	);

/**
 * testNeoadminIndex method
 *
 * @return void
 */
	public function testNeoadminIndex() {
		$this->markTestIncomplete('testNeoadminIndex not implemented.');
	}

/**
 * testNeoadminView method
 *
 * @return void
 */
	public function testNeoadminView() {
		$this->markTestIncomplete('testNeoadminView not implemented.');
	}

/**
 * testNeoadminAdd method
 *
 * @return void
 */
	public function testNeoadminAdd() {
		$this->markTestIncomplete('testNeoadminAdd not implemented.');
	}

/**
 * testNeoadminEdit method
 *
 * @return void
 */
	public function testNeoadminEdit() {
		$this->markTestIncomplete('testNeoadminEdit not implemented.');
	}

/**
 * testNeoadminDelete method
 *
 * @return void
 */
	public function testNeoadminDelete() {
		$this->markTestIncomplete('testNeoadminDelete not implemented.');
	}

}
