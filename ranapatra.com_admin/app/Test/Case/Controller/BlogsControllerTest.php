<?php
App::uses('BlogsController', 'Controller');

/**
 * BlogsController Test Case
 *
 */
class BlogsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.blog'
	);

/**
 * testNeoadminIndex method
 *
 * @return void
 */
	public function testNeoadminIndex() {
		$this->markTestIncomplete('testNeoadminIndex not implemented.');
	}

/**
 * testNeoadminView method
 *
 * @return void
 */
	public function testNeoadminView() {
		$this->markTestIncomplete('testNeoadminView not implemented.');
	}

/**
 * testNeoadminAdd method
 *
 * @return void
 */
	public function testNeoadminAdd() {
		$this->markTestIncomplete('testNeoadminAdd not implemented.');
	}

/**
 * testNeoadminEdit method
 *
 * @return void
 */
	public function testNeoadminEdit() {
		$this->markTestIncomplete('testNeoadminEdit not implemented.');
	}

/**
 * testNeoadminDelete method
 *
 * @return void
 */
	public function testNeoadminDelete() {
		$this->markTestIncomplete('testNeoadminDelete not implemented.');
	}

}
