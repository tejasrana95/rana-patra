<?php
App::uses('NewsController', 'Controller');

/**
 * NewsController Test Case
 *
 */
class NewsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.news'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

/**
 * testNeoadminIndex method
 *
 * @return void
 */
	public function testNeoadminIndex() {
		$this->markTestIncomplete('testNeoadminIndex not implemented.');
	}

/**
 * testNeoadminView method
 *
 * @return void
 */
	public function testNeoadminView() {
		$this->markTestIncomplete('testNeoadminView not implemented.');
	}

/**
 * testNeoadminAdd method
 *
 * @return void
 */
	public function testNeoadminAdd() {
		$this->markTestIncomplete('testNeoadminAdd not implemented.');
	}

/**
 * testNeoadminEdit method
 *
 * @return void
 */
	public function testNeoadminEdit() {
		$this->markTestIncomplete('testNeoadminEdit not implemented.');
	}

/**
 * testNeoadminDelete method
 *
 * @return void
 */
	public function testNeoadminDelete() {
		$this->markTestIncomplete('testNeoadminDelete not implemented.');
	}

}
